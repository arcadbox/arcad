//go:build plugin_example
// +build plugin_example

package plugin

import "gitlab.com/arcadbox/arcad/plugin/example"

func init() {
	registry.Add(example.NewPlugin())
}
