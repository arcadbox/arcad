// +build plugin_route_yjs

package plugin

import "gitlab.com/arcadbox/arcad/plugin/route/yjs"

func init() {
	registry.Add(yjs.NewPlugin())
}
