package email

type Config struct {
	SMTP SMTPConfig `mapstructure:"smtp" yaml:"smtp"`
}

type SMTPConfig struct {
	Host               string `mapstructure:"host"  yaml:"host"`
	Port               int    `mapstructure:"port"  yaml:"port"`
	User               string `mapstructure:"user"  yaml:"user"`
	Password           string `mapstructure:"password"  yaml:"password"`
	InsecureSkipVerify bool   `mapstructure:"insecureSkipVerify"  yaml:"insecureSkipVerify"`
	UseStartTLS        bool   `mapstructure:"useStartTLS"  yaml:"useStartTLS"`
}

func NewDefaultConfig() *Config {
	return &Config{
		SMTP: SMTPConfig{
			Host:               "127.0.0.1",
			Port:               25,
			User:               "arcad",
			Password:           "arcad",
			InsecureSkipVerify: false,
			UseStartTLS:        true,
		},
	}
}
