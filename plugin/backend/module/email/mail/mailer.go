package mail

const (
	ContentTypeHTML = "text/html"
	ContentTypeText = "text/plain"
)

type Option struct {
	Host               string
	Port               int
	User               string
	Password           string
	InsecureSkipVerify bool
	UseStartTLS        bool
}

type OptionFunc func(*Option)

type Mailer struct {
	opt *Option
}

func WithTLS(useStartTLS, insecureSkipVerify bool) OptionFunc {
	return func(opt *Option) {
		opt.UseStartTLS = useStartTLS
		opt.InsecureSkipVerify = insecureSkipVerify
	}
}

func WithServer(host string, port int) OptionFunc {
	return func(opt *Option) {
		opt.Host = host
		opt.Port = port
	}
}

func WithCredentials(user, password string) OptionFunc {
	return func(opt *Option) {
		opt.User = user
		opt.Password = password
	}
}

func NewMailer(funcs ...OptionFunc) *Mailer {
	opt := &Option{}

	for _, fn := range funcs {
		fn(opt)
	}

	return &Mailer{opt}
}
