package email

import (
	"io/ioutil"
	"os"
	"testing"

	"cdr.dev/slog"
	"gitlab.com/arcadbox/arcad/internal/app"
	"gitlab.com/arcadbox/arcad/internal/app/module"
	"gitlab.com/arcadbox/arcad/internal/repository"
	"gitlab.com/arcadbox/arcad/plugin/backend/module/email/mail"
	"gitlab.com/wpetit/goweb/logger"
)

func TestBackendEmailModule(t *testing.T) {
	t.Parallel()

	if os.Getenv("TEST_BACKEND_EMAIL_MODULE") != "yes" {
		t.Skip("Test skipped. Set environment variable TEST_BACKEND_EMAIL_MODULE=yes to run.")

		return
	}

	logger.SetLevel(slog.LevelDebug)

	appID := repository.AppID("test")

	mailer := mail.NewMailer(
		mail.WithServer("127.0.0.1", 2525),
		mail.WithCredentials("fakesmtp", "fakesmtp"),
	)

	backend := app.NewBackend(appID,
		module.ConsoleModuleFactory(),
		EmailModuleFactory(mailer),
	)

	data, err := ioutil.ReadFile("testdata/email.js")
	if err != nil {
		t.Fatal(err)
	}

	if err := backend.Load(string(data)); err != nil {
		t.Fatal(err)
	}

	backend.Start()
	defer backend.Stop()
}
