package email

import (
	"context"

	"github.com/dop251/goja"
	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/app"
	"gitlab.com/arcadbox/arcad/internal/repository"
	"gitlab.com/arcadbox/arcad/plugin/backend/module/email/mail"
	"gitlab.com/wpetit/goweb/logger"
)

type Module struct {
	ctx     context.Context
	mailer  *mail.Mailer
	backend *app.Backend
}

func (m *Module) Name() string {
	return "email"
}

func (m *Module) Export(export *goja.Object) {
	if err := export.Set("send", m.send); err != nil {
		panic(errors.Wrap(err, "could not set 'send' function"))
	}
}

func (m *Module) send(call goja.FunctionCall) goja.Value {
	if len(call.Arguments) < 1 {
		panic(m.backend.ToValue("unexpected number of arguments"))
	}

	sendRawOptions, ok := call.Arguments[0].Export().(map[string]interface{})
	if !ok {
		m.logAndPanic("unexpected send options format", nil)

		return nil
	}

	sendOptions := &sendOptions{}

	if err := mapstructure.Decode(sendRawOptions, sendOptions); err != nil {
		m.logAndPanic("unexpected send options format", errors.WithStack(err))

		return nil
	}

	sendFuncs := make([]mail.SendFunc, 0)

	if sendOptions.SenderAddress != "" || sendOptions.SenderName != "" {
		sendFuncs = append(
			sendFuncs,
			mail.WithSender(sendOptions.SenderAddress, sendOptions.SenderName),
		)
	}

	if sendOptions.Recipients != nil && len(sendOptions.Recipients) > 0 {
		sendFuncs = append(
			sendFuncs,
			mail.WithRecipients(sendOptions.Recipients...),
		)
	}

	if sendOptions.Subject != "" {
		sendFuncs = append(
			sendFuncs,
			mail.WithSubject(sendOptions.Subject),
		)
	}

	if sendOptions.HTMLBody != "" {
		sendFuncs = append(
			sendFuncs,
			mail.WithBody(mail.ContentTypeHTML, sendOptions.HTMLBody, nil),
		)
	}

	if sendOptions.TextBody != "" {
		if sendOptions.HTMLBody == "" {
			sendFuncs = append(
				sendFuncs,
				mail.WithBody(mail.ContentTypeText, sendOptions.TextBody, nil),
			)
		} else {
			sendFuncs = append(
				sendFuncs,
				mail.WithAlternativeBody(mail.ContentTypeText, sendOptions.TextBody, nil),
			)
		}
	}

	if err := m.mailer.Send(sendFuncs...); err != nil {
		m.logAndPanic("could not send email", errors.WithStack(err))

		return nil
	}

	return nil
}

func (m *Module) logAndPanic(msg string, err error) {
	err = errors.Wrap(err, msg)
	logger.Error(context.Background(), msg, logger.E(err))
	panic(m.backend.ToValue(err.Error()))
}

func EmailModuleFactory(mailer *mail.Mailer) app.BackendModuleFactory {
	return func(appID repository.AppID, backend *app.Backend) app.BackendModule {
		return &Module{
			ctx: logger.With(
				context.Background(),
				logger.F("appID", appID),
			),
			mailer:  mailer,
			backend: backend,
		}
	}
}

type sendOptions struct {
	SenderAddress string   `mapstructure:"senderAddress"`
	SenderName    string   `mapstructure:"senderName"`
	Recipients    []string `mapstructure:"recipients"`
	Subject       string   `mapstructure:"subject"`
	HTMLBody      string   `mapstructure:"htmlBody"`
	TextBody      string   `mapstructure:"textBody"`
}
