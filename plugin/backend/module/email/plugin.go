package email

import (
	"context"

	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/app"
	"gitlab.com/arcadbox/arcad/internal/hook"
	"gitlab.com/arcadbox/arcad/plugin/backend/module/email/mail"
	"gitlab.com/wpetit/goweb/plugin"
)

type Plugin struct {
	config *Config
	mailer *mail.Mailer
}

func (p *Plugin) PluginName() string {
	return "backend_module_email"
}

func (p *Plugin) PluginVersion() string {
	return "0.0.0"
}

func (p *Plugin) HookDumpConfig(ctx context.Context) (map[string]interface{}, error) {
	defaultConfig := NewDefaultConfig()
	dumpedConfig := make(map[string]interface{})

	if err := mapstructure.Decode(defaultConfig, &dumpedConfig); err != nil {
		return nil, errors.Wrap(err, "could not encode configuration")
	}

	return dumpedConfig, nil
}

func (p *Plugin) HookConfig(ctx context.Context, data map[string]interface{}) error {
	config := NewDefaultConfig()

	if err := mapstructure.Decode(data, config); err != nil {
		return errors.Wrap(err, "could not decode configuration")
	}

	// Configure mailer with provided configuration
	mailer := mail.NewMailer(
		mail.WithServer(config.SMTP.Host, config.SMTP.Port),
		mail.WithCredentials(config.SMTP.User, config.SMTP.Password),
		mail.WithTLS(config.SMTP.UseStartTLS, config.SMTP.InsecureSkipVerify),
	)

	p.mailer = mailer

	return nil
}

func (p *Plugin) HookBackendModules(ctx context.Context, factories []app.BackendModuleFactory) ([]app.BackendModuleFactory, error) {
	factories = append(factories, EmailModuleFactory(p.mailer))

	return factories, nil
}

func NewPlugin() *Plugin {
	return &Plugin{
		config: &Config{},
	}
}

// Assert interfaces implementation.
var (
	_ plugin.Plugin       = &Plugin{}
	_ hook.Config         = &Plugin{}
	_ hook.DumpConfig     = &Plugin{}
	_ hook.BackendModules = &Plugin{}
)
