
email.send({
  senderAddress: "test@arcad.app",
  recipients: ["user1@foobar.com"],
  textBody: "Hello world !",
  htmlBody: "<html><body><h1>Hello world !</h1></body></html>",
  subject: "Test Email " + Date.now(),
})