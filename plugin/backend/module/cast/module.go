package cast

import (
	"context"
	"sync"
	"time"

	"github.com/dop251/goja"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/app"
	"gitlab.com/arcadbox/arcad/internal/app/module"
	"gitlab.com/arcadbox/arcad/internal/repository"
	"gitlab.com/wpetit/goweb/logger"
)

const (
	defaultTimeout = 30 * time.Second
)

type Module struct {
	ctx     context.Context
	backend *app.Backend
	mutex   struct {
		devices        sync.RWMutex
		refreshDevices sync.Mutex
		loadURL        sync.Mutex
		quitApp        sync.Mutex
		getStatus      sync.Mutex
	}
	devices []*Device
}

func (m *Module) Name() string {
	return "cast"
}

func (m *Module) Export(export *goja.Object) {
	if err := export.Set("refreshDevices", m.refreshDevices); err != nil {
		panic(errors.Wrap(err, "could not set 'refreshDevices' function"))
	}

	if err := export.Set("getDevices", m.getDevices); err != nil {
		panic(errors.Wrap(err, "could not set 'getDevices' function"))
	}

	if err := export.Set("loadUrl", m.loadUrl); err != nil {
		panic(errors.Wrap(err, "could not set 'loadUrl' function"))
	}

	if err := export.Set("quitApp", m.quitApp); err != nil {
		panic(errors.Wrap(err, "could not set 'quitApp' function"))
	}

	if err := export.Set("getStatus", m.getStatus); err != nil {
		panic(errors.Wrap(err, "could not set 'getStatus' function"))
	}
}

func (m *Module) refreshDevices(call goja.FunctionCall) goja.Value {
	rawTimeout := call.Argument(0).String()

	timeout, err := m.parseTimeout(rawTimeout)
	if err != nil {
		panic(errors.WithStack(err))
	}

	promise := m.backend.NewPromise()

	go func() {
		m.mutex.refreshDevices.Lock()
		defer m.mutex.refreshDevices.Unlock()

		ctx, cancel := context.WithTimeout(context.Background(), timeout)
		defer cancel()

		devices, err := findDevices(ctx)
		if err != nil && !errors.Is(err, context.DeadlineExceeded) {
			err = errors.WithStack(err)
			logger.Error(ctx, "error refreshing casting devices list", logger.E(errors.WithStack(err)))

			promise.Reject(err)

			return
		}

		if err == nil {
			m.mutex.devices.Lock()
			m.devices = devices
			m.mutex.devices.Unlock()
		}

		devicesCopy := m.getDevicesCopy(devices)
		promise.Resolve(devicesCopy)
	}()

	return m.backend.ToValue(promise)
}

func (m *Module) getDevices(call goja.FunctionCall) goja.Value {
	m.mutex.devices.RLock()
	defer m.mutex.devices.RUnlock()

	devices := m.getDevicesCopy(m.devices)

	return m.backend.ToValue(devices)
}

func (m *Module) loadUrl(call goja.FunctionCall) goja.Value {
	if len(call.Arguments) < 2 {
		panic(errors.WithStack(module.ErrUnexpectedArgumentsNumber))
	}

	deviceUUID := call.Argument(0).String()
	url := call.Argument(1).String()

	rawTimeout := call.Argument(2).String()

	timeout, err := m.parseTimeout(rawTimeout)
	if err != nil {
		panic(errors.WithStack(err))
	}

	promise := m.backend.NewPromise()

	go func() {
		m.mutex.loadURL.Lock()
		defer m.mutex.loadURL.Unlock()

		ctx, cancel := context.WithTimeout(context.Background(), timeout)
		defer cancel()

		err := loadURL(ctx, deviceUUID, url)
		if err != nil {
			err = errors.WithStack(err)
			logger.Error(ctx, "error while casting url", logger.E(err))

			promise.Reject(err)

			return
		}

		promise.Resolve(nil)
	}()

	return m.backend.ToValue(promise)
}

func (m *Module) quitApp(call goja.FunctionCall) goja.Value {
	if len(call.Arguments) < 1 {
		panic(errors.WithStack(module.ErrUnexpectedArgumentsNumber))
	}

	deviceUUID := call.Argument(0).String()

	rawTimeout := call.Argument(1).String()

	timeout, err := m.parseTimeout(rawTimeout)
	if err != nil {
		panic(errors.WithStack(err))
	}

	promise := m.backend.NewPromise()

	go func() {
		m.mutex.quitApp.Lock()
		defer m.mutex.quitApp.Unlock()

		ctx, cancel := context.WithTimeout(context.Background(), timeout)
		defer cancel()

		err := quitApp(ctx, deviceUUID)
		if err != nil {
			err = errors.WithStack(err)
			logger.Error(ctx, "error while quitting casting device app", logger.E(errors.WithStack(err)))

			promise.Reject(err)

			return
		}

		promise.Resolve(nil)
	}()

	return m.backend.ToValue(promise)
}

func (m *Module) getStatus(call goja.FunctionCall) goja.Value {
	if len(call.Arguments) < 1 {
		panic(errors.WithStack(module.ErrUnexpectedArgumentsNumber))
	}

	deviceUUID := call.Argument(0).String()
	rawTimeout := call.Argument(1).String()

	timeout, err := m.parseTimeout(rawTimeout)
	if err != nil {
		panic(errors.WithStack(err))
	}

	promise := m.backend.NewPromise()

	go func() {
		m.mutex.getStatus.Lock()
		defer m.mutex.getStatus.Unlock()

		ctx, cancel := context.WithTimeout(context.Background(), timeout)
		defer cancel()

		status, err := getStatus(ctx, deviceUUID)
		if err != nil {
			err = errors.WithStack(err)
			logger.Error(ctx, "error while getting casting device status", logger.E(err))

			promise.Reject(err)

			return
		}

		promise.Resolve(status)
	}()

	return m.backend.ToValue(promise)
}

func (m *Module) getDevicesCopy(devices []*Device) []Device {
	devicesCopy := make([]Device, 0, len(m.devices))

	for _, d := range devices {
		devicesCopy = append(devicesCopy, Device{
			UUID: d.UUID,
			Name: d.Name,
			Host: d.Host,
			Port: d.Port,
		})
	}

	return devicesCopy
}

func (m *Module) parseTimeout(rawTimeout string) (time.Duration, error) {
	var (
		timeout time.Duration
		err     error
	)

	if rawTimeout == "undefined" {
		timeout = defaultTimeout
	} else {
		timeout, err = time.ParseDuration(rawTimeout)
		if err != nil {
			return defaultTimeout, errors.Wrapf(err, "invalid duration format '%s'", rawTimeout)
		}
	}

	return timeout, nil
}

func CastModuleFactory() app.BackendModuleFactory {
	return func(appID repository.AppID, backend *app.Backend) app.BackendModule {
		return &Module{
			ctx: logger.With(
				context.Background(),
				logger.F("appID", appID),
			),
			backend: backend,
			devices: make([]*Device, 0),
		}
	}
}
