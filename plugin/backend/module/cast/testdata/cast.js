
cast.refreshDevices('5s')
  .then(function(devices) {  
    console.log(devices)
  
    if (devices === null) {
      throw new Error("devices should not be null");
    }
    
    if (devices.length === 0) {
      throw new Error("devices.length should not be 0");
    }

    return devices
  })
  .then(function(devices) {
    return cast.getStatus(devices[0].UUID)
  })
  .then(function(status) {
    console.log(status)
  })
