package cast

import (
	"io/ioutil"
	"os"
	"testing"
	"time"

	"cdr.dev/slog"
	"github.com/davecgh/go-spew/spew"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/app"
	"gitlab.com/arcadbox/arcad/internal/app/module"
	"gitlab.com/arcadbox/arcad/internal/repository"
	"gitlab.com/wpetit/goweb/logger"
)

func TestBackendCastModule(t *testing.T) {
	t.Parallel()

	if os.Getenv("TEST_BACKEND_CAST_MODULE") != "yes" {
		t.Skip("Test skipped. Set environment variable TEST_BACKEND_CAST_MODULE=yes to run.")

		return
	}

	logger.SetLevel(slog.LevelDebug)

	appID := repository.AppID("test")

	backend := app.NewBackend(appID,
		module.ConsoleModuleFactory(),
		CastModuleFactory(),
	)

	data, err := ioutil.ReadFile("testdata/cast.js")
	if err != nil {
		t.Fatal(err)
	}

	if err := backend.Load(string(data)); err != nil {
		t.Fatal(err)
	}

	backend.Start()
	defer backend.Stop()

	time.Sleep(20 * time.Second)
}

func TestBackendCastModuleRefreshDevices(t *testing.T) {
	t.Parallel()

	if os.Getenv("TEST_BACKEND_CAST_MODULE") != "yes" {
		t.Skip("Test skipped. Set environment variable TEST_BACKEND_CAST_MODULE=yes to run.")

		return
	}

	logger.SetLevel(slog.LevelDebug)

	appID := repository.AppID("test")

	backend := app.NewBackend(appID,
		module.ConsoleModuleFactory(),
		CastModuleFactory(),
	)

	data, err := ioutil.ReadFile("testdata/refresh_devices.js")
	if err != nil {
		t.Fatal(err)
	}

	if err := backend.Load(string(data)); err != nil {
		t.Fatal(err)
	}

	backend.Start()
	defer backend.Stop()

	result, err := backend.ExecFuncByName("refreshDevices")
	if err != nil {
		t.Error(errors.WithStack(err))
	}

	promise, ok := backend.IsPromise(result)
	if !ok {
		t.Fatal("expected promise")
	}

	value := backend.WaitForPromise(promise)

	spew.Dump(value.Export())
}
