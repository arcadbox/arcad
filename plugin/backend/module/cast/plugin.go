package cast

import (
	"context"

	"gitlab.com/arcadbox/arcad/internal/app"
	"gitlab.com/arcadbox/arcad/internal/hook"
	"gitlab.com/wpetit/goweb/plugin"
)

type Plugin struct{}

func (p *Plugin) PluginName() string {
	return "backend_module_cast"
}

func (p *Plugin) PluginVersion() string {
	return "0.0.0"
}

func (p *Plugin) HookBackendModules(ctx context.Context, factories []app.BackendModuleFactory) ([]app.BackendModuleFactory, error) {
	factories = append(factories, CastModuleFactory())

	return factories, nil
}

func NewPlugin() *Plugin {
	return &Plugin{}
}

// Assert interfaces implementation.
var (
	_ plugin.Plugin       = &Plugin{}
	_ hook.BackendModules = &Plugin{}
)
