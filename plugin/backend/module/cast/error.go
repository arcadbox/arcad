package cast

import "errors"

var ErrDeviceNotFound = errors.New("device not found")
