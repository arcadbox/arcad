package cast

import (
	"context"
	"os"
	"testing"
	"time"

	"cdr.dev/slog"
	"github.com/davecgh/go-spew/spew"
	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/logger"
)

func TestCastLoadURL(t *testing.T) {
	t.Parallel()

	if os.Getenv("TEST_BACKEND_CAST_MODULE") != "yes" {
		t.Skip("Test skipped. Set environment variable TEST_BACKEND_CAST_MODULE=yes to run.")

		return
	}

	logger.SetLevel(slog.LevelDebug)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	devices, err := findDevices(ctx)
	if err != nil {
		t.Error(errors.WithStack(err))
	}

	if e, g := 1, len(devices); e != g {
		t.Fatalf("len(devices): expected '%v', got '%v'", e, g)
	}

	dev := devices[0]

	ctx, cancel2 := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel2()

	if err := loadURL(ctx, dev.UUID, "https://go.dev"); err != nil {
		t.Error(errors.WithStack(err))
	}

	ctx, cancel3 := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel3()

	status, err := getStatus(ctx, dev.UUID)
	if err != nil {
		t.Error(errors.WithStack(err))
	}

	spew.Dump(status)

	ctx, cancel4 := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel4()

	if err := quitApp(ctx, dev.UUID); err != nil {
		t.Error(errors.WithStack(err))
	}
}
