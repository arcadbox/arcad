package config

type Config struct {
	HTTP     HTTPConfig     `yaml:"http" mapstructure:"http"`
	Database DatabaseConfig `yaml:"database" mapstructure:"database"`
	IPFS     IPFSConfig     `yaml:"ipfs" mapstructure:"ipfs"`
	Indexer  IndexerConfig  `yaml:"indexer" mapstructure:"indexer"`
	Feature  FeatureConfig  `yaml:"feature" mapstructure:"feature"`
}

type HTTPConfig struct {
	Address string `yaml:"address" mapstructure:"address" env:"ARCAD_PLUGIN_MARKET_HTTP_ADDRESS"`
}

type DatabaseConfig struct {
	Migrations string `yaml:"migrations"  mapstructure:"migrations" env:"ARCAD_PLUGIN_MARKET_DATABASE_MIGRATIONS"`
	DSN        string `yaml:"dsn" mapstructure:"dsn" env:"ARCAD_PLUGIN_MARKET_DATABASE_DSN"`
}

type IPFSConfig struct {
	GatewayURL string `yaml:"gatewayUrl" mapstructure:"gatewayUrl" env:"ARCAD_PLUGIN_MARKET_IPFS_GATEWAY_URL"`
}

type IndexerConfig struct {
	Path            string `yaml:"path" mapstructure:"path" env:"ARCAD_PLUGIN_MARKET_INDEXER_PATH"`
	DefaultAnalyzer string `yaml:"defaultAnalyzer" mapstructure:"defaultAnalyzer" env:"ARCAD_PLUGIN_MARKET_INDEXER_DEFAULT_ANALYZER"`
}

type FeatureConfig struct {
	RegistrationEnabled bool `yaml:"registrationEnabled" mapstructure:"registrationEnabled" env:"ARCAD_PLUGIN_MARKET_FEATURE_REGISTRATION_ENABLED"`
}

func NewDefault() *Config {
	return &Config{
		HTTP: HTTPConfig{
			Address: ":3002",
		},
		Database: DatabaseConfig{
			Migrations: "file://plugin/market/migrations/sqlite",
			DSN:        "sqlite://data/market.sqlite3?cache=shared",
		},
		IPFS: IPFSConfig{
			GatewayURL: "https://ipfs.io",
		},
		Indexer: IndexerConfig{
			Path:            "market.index",
			DefaultAnalyzer: "fr",
		},
		Feature: FeatureConfig{
			RegistrationEnabled: false,
		},
	}
}
