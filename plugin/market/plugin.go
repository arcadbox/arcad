package registry

import (
	"context"

	"github.com/caarlos0/env/v6"
	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
	"gitlab.com/arcadbox/arcad/internal/hook"
	"gitlab.com/arcadbox/arcad/plugin/market/command"
	"gitlab.com/arcadbox/arcad/plugin/market/config"
	"gitlab.com/wpetit/goweb/plugin"

	_ "github.com/doug-martin/goqu/v9/dialect/sqlite3"
	_ "github.com/golang-migrate/migrate/v4/database/sqlite"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "modernc.org/sqlite"

	// Bleve analyzers
	_ "github.com/blevesearch/bleve/v2/analysis/lang/en"
	_ "github.com/blevesearch/bleve/v2/analysis/lang/fr"
)

type Plugin struct {
	config *config.Config
}

func (p *Plugin) PluginName() string {
	return "market"
}

func (p *Plugin) PluginVersion() string {
	return "0.0.0"
}

func (p *Plugin) HookDumpConfig(ctx context.Context) (map[string]interface{}, error) {
	dumpedConfig := make(map[string]interface{})

	if err := mapstructure.Decode(p.config, &dumpedConfig); err != nil {
		return nil, errors.Wrap(err, "could not encode configuration")
	}

	return dumpedConfig, nil
}

func (p *Plugin) HookConfig(ctx context.Context, data map[string]interface{}) error {
	if err := mapstructure.Decode(data, p.config); err != nil {
		return errors.Wrap(err, "could not decode configuration")
	}

	if err := env.Parse(p.config); err != nil {
		return errors.Wrap(err, "could not parse environment")
	}

	return nil
}

func (p *Plugin) HookCommands(ctx context.Context, commands []*cli.Command) ([]*cli.Command, error) {
	commands = append(commands, command.Commands(p.config)...)

	return commands, nil
}

func NewPlugin() *Plugin {
	return &Plugin{
		config: config.NewDefault(),
	}
}

// Assert interfaces implementation.
var (
	_ plugin.Plugin   = &Plugin{}
	_ hook.Config     = &Plugin{}
	_ hook.DumpConfig = &Plugin{}
	_ hook.Commands   = &Plugin{}
)
