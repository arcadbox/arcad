package repository

import (
	"context"
	"time"

	"github.com/doug-martin/goqu/v9"
	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/logger"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	ID        int64
	Username  string    `db:"username" json:",omitempty"`
	Email     string    `db:"email" json:",omitempty"`
	CreatedAt time.Time `db:"created_at"`
	UpdatedAt time.Time `db:"updated_at"`
}

type UserRepository struct {
	driver string
	db     *goqu.Database
}

func (r *UserRepository) Create(ctx context.Context, username, email, password string) (int64, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return 0, errors.WithStack(err)
	}

	var lastInsertID int64

	err = r.db.WithTx(func(tx *goqu.TxDatabase) error {
		existingAccounts, err := tx.Select(goqu.COUNT("id")).
			From("users").
			Where(goqu.ExOr{
				"username": goqu.Op{"eq": username},
				"email":    goqu.Op{"eq": email},
			}).
			Executor().
			QueryContext(ctx)
		if err != nil {
			return errors.WithStack(err)
		}

		defer func() {
			if err := existingAccounts.Close(); err != nil {
				logger.Error(ctx, "could not close rows", logger.E(errors.WithStack(err)))
			}
		}()

		for existingAccounts.Next() {
			var count int64
			if err := existingAccounts.Scan(&count); err != nil {
				return errors.WithStack(err)
			}

			if count > 0 {
				return ErrAccountAlreadyExists
			}
		}

		if existingAccounts.Err() != nil {
			return errors.WithStack(err)
		}

		result, err := tx.Insert("users").
			Rows(
				goqu.Record{
					"username": username,
					"password": hashedPassword,
					"email":    email,
				},
			).
			Executor().
			ExecContext(ctx)
		if err != nil {
			return errors.WithStack(err)
		}

		lastInsertID, err = result.LastInsertId()
		if err != nil {
			return errors.WithStack(err)
		}

		return nil
	})
	if err != nil {
		return 0, errors.WithStack(err)
	}

	return lastInsertID, nil
}

func (r *UserRepository) Authenticate(ctx context.Context, username string, password string) (bool, error) {
	rows, err := r.db.From("users").
		Select("password").
		Where(goqu.C("username").Eq(username)).
		Executor().
		QueryContext(ctx)
	if err != nil {
		return false, errors.WithStack(err)
	}

	defer func() {
		if err := rows.Close(); err != nil {
			logger.Error(
				ctx,
				"could not close rows",
				logger.E(errors.WithStack(err)),
			)
		}
	}()

	for rows.Next() {
		var hashedPassword string
		if err := rows.Scan(&hashedPassword); err != nil {
			return false, errors.WithStack(err)
		}

		if err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password)); err != nil {
			if errors.Is(err, bcrypt.ErrMismatchedHashAndPassword) {
				return false, nil
			}

			return false, errors.WithStack(err)
		}

		return true, nil
	}

	if err := rows.Err(); err != nil {
		return false, errors.WithStack(err)
	}

	return false, nil
}

func (r *UserRepository) FindByID(ctx context.Context, id int64) (*User, error) {
	user, err := r.findBy(ctx, goqu.C("id").Eq(id))
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return user, nil
}

func (r *UserRepository) FindByUsername(ctx context.Context, username string) (*User, error) {
	user, err := r.findBy(ctx, goqu.C("username").Eq(username))
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return user, nil
}

func (r *UserRepository) findBy(ctx context.Context, expressions ...goqu.Expression) (*User, error) {
	rows, err := r.db.From("users").
		Select("id", "username", "email", "created_at", "updated_at").
		Where(expressions...).
		Limit(1).
		Executor().
		QueryContext(ctx)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	defer func() {
		if err := rows.Close(); err != nil {
			logger.Error(
				ctx,
				"could not close rows",
				logger.E(errors.WithStack(err)),
			)
		}
	}()

	for rows.Next() {
		user := &User{}

		if err := rows.Scan(&user.ID, &user.Username, &user.Email, &user.CreatedAt, &user.UpdatedAt); err != nil {
			return nil, errors.WithStack(err)
		}

		return user, nil
	}

	if err := rows.Err(); err != nil {
		return nil, errors.WithStack(err)
	}

	return nil, errors.WithStack(ErrNotFound)
}

func NewUserRepository(driver string, db *goqu.Database) *UserRepository {
	return &UserRepository{driver: driver, db: db}
}

func NewUserRepositoryFromDSN(dsn string) (*UserRepository, func() error, error) {
	driver, db, closeFn, err := openGoquDB(dsn)
	if err != nil {
		return nil, noOpClose, errors.WithStack(err)
	}

	return NewUserRepository(driver, db), closeFn, nil
}
