package repository

import (
	"context"
	"strings"
	"time"

	"github.com/doug-martin/goqu/v9"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/bundle"
	"gitlab.com/arcadbox/arcad/internal/repository"
	"gitlab.com/wpetit/goweb/logger"
	"golang.org/x/mod/semver"
)

type App struct {
	ID              int64      `db:"id"`
	OwnerID         int64      `db:"owner_id"`
	Owner           *User      `db:"owner" json:",omitempty"`
	Name            string     `db:"name"`
	CreatedAt       time.Time  `db:"created_at"`
	UpdatedAt       time.Time  `db:"updated_at"`
	LatestReleaseID *int64     `db:"latest_release_id" json:",omitempty"`
	LatestRelease   *Release   `db:"latest_release" json:",omitempty"`
	Releases        []*Release `db:"releases" json:",omitempty"`
}

type Release struct {
	ID            int64     `db:"id"`
	AppID         int64     `db:"app_id"`
	ArchiveFormat string    `db:"archive_format"`
	Version       string    `db:"version"`
	Description   string    `db:"description"`
	CreatedAt     time.Time `db:"created_at"`
	CID           string    `db:"cid"`
}

type AppRepository struct {
	driver string
	db     *goqu.Database
}

func (r *AppRepository) CreateApp(ctx context.Context, ownerID int64, cid string, archiveFormat bundle.ArchiveExt, appManifest *repository.AppManifest) (int64, error) {
	var appID int64

	err := r.db.WithTx(func(tx *goqu.TxDatabase) error {
		count, err := tx.Select("id").
			From("apps").
			Where(goqu.Ex{
				"name":     goqu.Op{"eq": appManifest.ID},
				"owner_id": goqu.Op{"eq": ownerID},
			}).
			Count()
		if err != nil {
			return errors.WithStack(err)
		}

		if count > 0 {
			return ErrAppAlreadyExists
		}

		result, err := tx.Insert("apps").
			Rows(
				goqu.Record{
					"name":     appManifest.ID,
					"owner_id": ownerID,
				},
			).
			Executor().
			ExecContext(ctx)
		if err != nil {
			return errors.WithStack(err)
		}

		appID, err = result.LastInsertId()
		if err != nil {
			return errors.WithStack(err)
		}

		_, err = r.createRelease(ctx, tx, ownerID, appID, cid, archiveFormat, appManifest)
		if err != nil {
			return errors.WithStack(err)
		}

		return nil
	})
	if err != nil {
		return 0, errors.WithStack(err)
	}

	return appID, nil
}

func (r *AppRepository) CreateRelease(ctx context.Context, ownerID, appID int64, cid string, archiveFormat bundle.ArchiveExt, appManifest *repository.AppManifest) (int64, error) {
	logger.Debug(
		ctx, "creating new app release",
		logger.F("appID", appID), logger.F("ownerID", ownerID),
		logger.F("CID", cid),
	)

	var (
		releaseID int64
		err       error
	)

	err = r.db.WithTx(func(tx *goqu.TxDatabase) error {
		releaseID, err = r.createRelease(ctx, tx, ownerID, appID, cid, archiveFormat, appManifest)
		if err != nil {
			return errors.WithStack(err)
		}

		return nil
	})
	if err != nil {
		return 0, errors.WithStack(err)
	}

	return releaseID, nil
}

func (r *AppRepository) createRelease(ctx context.Context, tx *goqu.TxDatabase, ownerID, appID int64, cid string, archiveFormat bundle.ArchiveExt, appManifest *repository.AppManifest) (int64, error) {
	app := struct {
		ID            int64   `db:"id"`
		OwnerID       int64   `db:"owner_id"`
		LatestVersion *string `db:"version"`
	}{}

	found, err := tx.Select("apps.id", "apps.owner_id", "releases.version").
		From("apps").
		Where(goqu.Ex{
			"apps.id": goqu.Op{"eq": appID},
		}).
		LeftJoin(
			goqu.T("releases"),
			goqu.On(goqu.Ex{"apps.latest_release_id": goqu.I("releases.id")}),
		).
		Limit(1).
		Executor().
		ScanStructContext(ctx, &app)
	if err != nil {
		return 0, errors.WithStack(err)
	}

	if !found {
		return 0, errors.WithStack(ErrNotFound)
	}

	if app.OwnerID != ownerID {
		return 0, errors.WithStack(ErrForbidden)
	}

	if app.LatestVersion != nil {
		versionComp := semver.Compare(appManifest.Version, *app.LatestVersion)
		if versionComp <= 0 {
			return 0, errors.WithStack(ErrStaledVersion)
		}
	}

	result, err := tx.Insert("releases").
		Rows(
			goqu.Record{
				"version":        strings.TrimSpace(appManifest.Version),
				"archive_format": archiveFormat,
				"description":    strings.TrimSpace(appManifest.Description),
				"cid":            cid,
				"app_id":         appID,
			},
		).
		Executor().
		ExecContext(ctx)
	if err != nil {
		return 0, errors.WithStack(err)
	}

	releaseID, err := result.LastInsertId()
	if err != nil {
		return 0, errors.WithStack(err)
	}

	_, err = tx.Update("apps").
		Where(goqu.Ex{
			"id": goqu.Op{"eq": appID},
		}).
		Set(goqu.Record{"latest_release_id": releaseID}).
		Executor().
		ExecContext(ctx)
	if err != nil {
		return 0, errors.WithStack(err)
	}

	return releaseID, nil
}

func (r *AppRepository) FetchApps(ctx context.Context, appIDs []int64) ([]*App, error) {
	query := r.getFullAppSelectDataset().
		Where(
			goqu.Ex{
				"apps.id": goqu.Op{"in": appIDs},
			},
		)

	sql, params, err := query.ToSQL()
	if err != nil {
		return nil, errors.WithStack(err)
	}

	logger.Debug(ctx, "executing query", logger.F("sql", sql), logger.F("params", params))

	apps := make([]*App, 0)

	if err := query.Executor().ScanStructsContext(ctx, &apps); err != nil {
		return nil, errors.WithStack(err)
	}

	return apps, nil
}

func (r *AppRepository) FindByID(ctx context.Context, id int64) (*App, error) {
	app, err := r.findBy(ctx, goqu.Ex{
		"apps.id": goqu.Op{"eq": id},
	})
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return app, nil
}

func (r *AppRepository) findBy(ctx context.Context, expressions ...goqu.Expression) (*App, error) {
	exec := r.getFullAppSelectDataset().
		Where(expressions...).
		Limit(1).
		Executor()

	sql, params, err := exec.ToSQL()
	if err != nil {
		return nil, errors.WithStack(err)
	}

	logger.Debug(ctx, "executing query", logger.F("sql", sql), logger.F("params", params))

	app := &App{}

	found, err := exec.ScanStructContext(ctx, app)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	if !found {
		return nil, errors.WithStack(ErrNotFound)
	}

	return app, nil
}

func (r *AppRepository) ForEachApp(ctx context.Context, iterator func(context.Context, *App) error) error {
	scan, err := r.getFullAppSelectDataset().
		Executor().
		ScannerContext(ctx)
	if err != nil {
		return errors.WithStack(err)
	}

	defer func() {
		if err := scan.Close(); err != nil {
			logger.Error(
				ctx,
				"could not close scanner",
				logger.E(errors.WithStack(err)),
			)
		}
	}()

	for scan.Next() {
		app := &App{}

		err = scan.ScanStruct(app)
		if err != nil {
			return errors.WithStack(err)
		}

		if err := iterator(ctx, app); err != nil {
			return errors.WithStack(err)
		}
	}

	return nil
}

func (r *AppRepository) getFullAppSelectDataset() *goqu.SelectDataset {
	return r.db.From("apps").
		LeftJoin(
			goqu.T("releases").As("latest_release"),
			goqu.On(goqu.Ex{"apps.latest_release_id": goqu.I("latest_release.id")}),
		).
		LeftJoin(
			goqu.T("users").As("owner"),
			goqu.On(goqu.Ex{"apps.owner_id": goqu.I("owner.id")}),
		).
		Select(
			"apps.id",
			"apps.owner_id",
			"apps.name",
			"apps.created_at",
			"apps.updated_at",
			"apps.latest_release_id",
			goqu.I("owner.id").As(goqu.C("owner.id")),
			goqu.I("owner.username").As(goqu.C("owner.username")),
			goqu.I("owner.created_at").As(goqu.C("owner.created_at")),
			goqu.I("owner.updated_at").As(goqu.C("owner.updated_at")),
			goqu.I("latest_release.id").As(goqu.C("latest_release.id")),
			goqu.I("latest_release.app_id").As(goqu.C("latest_release.app_id")),
			goqu.I("latest_release.version").As(goqu.C("latest_release.version")),
			goqu.I("latest_release.description").As(goqu.C("latest_release.description")),
			goqu.I("latest_release.archive_format").As(goqu.C("latest_release.archive_format")),
			goqu.I("latest_release.cid").As(goqu.C("latest_release.cid")),
			goqu.I("latest_release.created_at").As(goqu.C("latest_release.created_at")),
		)
}

func NewAppRepository(driver string, db *goqu.Database) *AppRepository {
	return &AppRepository{
		driver: driver,
		db:     db,
	}
}

func NewAppRepositoryFromDSN(dsn string) (*AppRepository, func() error, error) {
	driver, db, closeFn, err := openGoquDB(dsn)
	if err != nil {
		return nil, noOpClose, errors.WithStack(err)
	}

	return NewAppRepository(driver, db), closeFn, nil
}
