package repository

import (
	"context"
	"database/sql"
	"testing"
	"time"

	"github.com/doug-martin/goqu/v9"
	_ "github.com/doug-martin/goqu/v9/dialect/sqlite3"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/sqlite"
	_ "github.com/golang-migrate/migrate/v4/database/sqlite"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/pkg/errors"
	_ "modernc.org/sqlite"
)

func TestUserRepositoryCreateAndAuthenticate(t *testing.T) {
	db, err := setupDatabase()
	if err != nil {
		t.Fatal(errors.WithStack(err))
	}

	defer func() {
		if err := db.Close(); err != nil {
			t.Error(errors.WithStack(err))
		}
	}()

	dialect := goqu.Dialect("sqlite3")
	wrappedDB := dialect.DB(db)

	usersRepo := NewUserRepository("sqlite", wrappedDB)

	ctx := context.Background()

	userID, err := usersRepo.Create(ctx, "test", "test@test.com", "test")
	if err != nil {
		t.Fatal(errors.WithStack(err))
	}

	results, err := wrappedDB.From("users").
		Select("id", "username", "password", "created_at", "updated_at").
		Where(goqu.C("id").Eq(userID)).
		Limit(1).
		Executor().
		QueryContext(ctx)
	if err != nil {
		t.Error(errors.WithStack(err))
	}

	defer results.Close()

	totalFoundUsers := 0

	for results.Next() {
		user := struct {
			ID        int64
			Username  string
			Password  string
			CreatedAt time.Time
			UpdatedAt time.Time
		}{}

		if err := results.Scan(&user.ID, &user.Username, &user.Password, &user.CreatedAt, &user.UpdatedAt); err != nil {
			t.Fatal(errors.WithStack(err))
		}

		if e, g := "test", user.Username; e != g {
			t.Errorf("user.Username: expected '%v', got '%v'", e, g)
		}

		totalFoundUsers++
	}

	if e, g := 1, totalFoundUsers; e != g {
		t.Errorf("totalFoundUsers: expected '%v', got '%v'", e, g)
	}

	authenticated, err := usersRepo.Authenticate(ctx, "test", "test")
	if err != nil {
		t.Error(errors.WithStack(err))
	}

	if e, g := true, authenticated; e != g {
		t.Errorf("authenticated: expected '%v', got '%v'", e, g)
	}

	if err := results.Err(); err != nil {
		t.Fatal(errors.WithStack(err))
	}
}

func setupDatabase() (*sql.DB, error) {
	db, err := sql.Open("sqlite", ":memory:")
	if err != nil {
		return nil, errors.WithStack(err)
	}

	driver, err := sqlite.WithInstance(db, &sqlite.Config{})
	if err != nil {
		return nil, errors.WithStack(err)
	}

	migr, err := migrate.NewWithDatabaseInstance(
		"file://../migrations/sqlite",
		"",
		driver,
	)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	if err := migr.Up(); err != nil {
		return nil, errors.WithStack(err)
	}

	return db, nil
}
