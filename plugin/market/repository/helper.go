package repository

import (
	"context"
	"database/sql"
	"net/url"
	"strings"

	"github.com/doug-martin/goqu/v9"
	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/logger"
)

func noOpClose() error {
	return nil
}

func Open(dsn string) (string, *sql.DB, error) {
	url, err := url.Parse(dsn)
	if err != nil {
		return "", nil, errors.WithStack(err)
	}

	driver := url.Scheme
	headlessDSN := strings.TrimPrefix(dsn, url.Scheme+"://")

	logger.Debug(context.Background(), "opening registry database", logger.F("dsn", dsn))

	db, err := sql.Open(driver, headlessDSN)
	if err != nil {
		return "", nil, errors.WithStack(err)
	}

	return driver, db, nil
}

func openGoquDB(dsn string) (string, *goqu.Database, func() error, error) {
	driver, db, err := Open(dsn)
	if err != nil {
		return "", nil, noOpClose, errors.WithStack(err)
	}

	closeFn := func() error {
		if err := db.Close(); err != nil {
			return errors.WithStack(err)
		}

		return nil
	}

	dialect := goqu.Dialect(driver)

	return driver, dialect.DB(db), closeFn, nil
}
