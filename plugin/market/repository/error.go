package repository

import "errors"

var (
	ErrNotFound             = errors.New("not found")
	ErrAccountAlreadyExists = errors.New("account already exists")
	ErrAppAlreadyExists     = errors.New("app already exists")
	ErrReleaseAlreadyExists = errors.New("release already exists")
	ErrInvalidVersion       = errors.New("invalid version")
	ErrStaledVersion        = errors.New("staled version")
	ErrForbidden            = errors.New("forbidden")
)
