package client

import (
	"github.com/urfave/cli/v2"
	"gitlab.com/arcadbox/arcad/plugin/market/config"
)

func RegisterCommand(conf *config.Config) *cli.Command {
	return &cli.Command{
		Name:  "register",
		Usage: "Register to the specified arcad market",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "username",
				Aliases:  []string{"u"},
				Value:    "",
				Usage:    "Your username",
				Required: true,
				EnvVars:  []string{"ARCAD_REGISTRY_USERNAME"},
			},
			&cli.StringFlag{
				Name:     "email",
				Aliases:  []string{"e"},
				Value:    "",
				Usage:    "Your email",
				Required: true,
				EnvVars:  []string{"ARCAD_REGISTRY_EMAIL"},
			},
			&cli.StringFlag{
				Name:     "password",
				Aliases:  []string{"p"},
				Value:    "",
				Usage:    "Your password",
				Required: true,
				EnvVars:  []string{"ARCAD_REGISTRY_PASSWORD"},
			},
		},
		Action: func(ctx *cli.Context) error {
			return nil
		},
	}
}
