package client

import (
	"github.com/urfave/cli/v2"
	"gitlab.com/arcadbox/arcad/plugin/market/config"
)

func Commands(conf *config.Config) *cli.Command {
	return &cli.Command{
		Name:  "client",
		Usage: "Manage registry client related operations",
		Subcommands: []*cli.Command{
			RegisterCommand(conf),
		},
	}
}
