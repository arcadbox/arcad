package command

import (
	"github.com/urfave/cli/v2"
	"gitlab.com/arcadbox/arcad/plugin/market/command/client"
	"gitlab.com/arcadbox/arcad/plugin/market/command/server"
	"gitlab.com/arcadbox/arcad/plugin/market/config"
)

func Commands(conf *config.Config) []*cli.Command {
	return []*cli.Command{
		{
			Name:  "market",
			Usage: "Manage market related operations",
			Subcommands: []*cli.Command{
				server.Commands(conf),
				client.Commands(conf),
			},
		},
	}
}
