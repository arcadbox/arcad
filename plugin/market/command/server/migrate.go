package server

import (
	_ "github.com/golang-migrate/migrate/v4/database/sqlite"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
	"gitlab.com/arcadbox/arcad/plugin/market/config"
)

func MigrateCommand(conf *config.Config) *cli.Command {
	return &cli.Command{
		Name:  "migrate",
		Usage: "Migrate database schema to latest version",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:  "target",
				Usage: "Migration target, default to latest",
				Value: "latest",
			},
			&cli.IntFlag{
				Name:  "force",
				Usage: "Force migration to version",
				Value: -1,
			},
		},
		Action: func(ctx *cli.Context) error {
			target := ctx.String("target")
			forcedVersion := ctx.Int("force")

			if err := migrateDatabase(ctx.Context, conf, target, forcedVersion); err != nil {
				return errors.WithStack(err)
			}

			return nil
		},
	}
}
