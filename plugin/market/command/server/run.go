package server

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
	"gitlab.com/arcadbox/arcad/plugin/market/config"
	"gitlab.com/arcadbox/arcad/plugin/market/route"
	"gitlab.com/wpetit/goweb/logger"
)

func RunCommand(conf *config.Config) *cli.Command {
	return &cli.Command{
		Name:  "run",
		Usage: "Run registry server",
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:    "auto-reindex",
				Aliases: []string{"r"},
				Value:   false,
			},
			&cli.BoolFlag{
				Name:    "auto-migrate",
				Aliases: []string{"m"},
				Value:   false,
			},
		},
		Action: func(ctx *cli.Context) error {
			autoMigrate := ctx.Bool("auto-migrate")
			autoReindex := ctx.Bool("auto-reindex")

			if autoMigrate {
				if err := migrateDatabase(ctx.Context, conf, MigrateVersionLatest, -1); err != nil {
					return errors.WithStack(err)
				}
			}

			if autoReindex {
				if err := reindexDatabase(ctx.Context, conf); err != nil {
					return errors.WithStack(err)
				}
			}

			r := chi.NewRouter()

			// Define base middlewares
			r.Use(middleware.Logger)
			r.Use(middleware.Recoverer)
			r.Use(middleware.DefaultCompress)

			// Define routes
			if err := route.Mount(r, conf); err != nil {
				return errors.Wrap(err, "could not mount routes")
			}

			logger.Info(ctx.Context, "listening", logger.F("address", conf.HTTP.Address))

			if err := http.ListenAndServe(conf.HTTP.Address, r); err != nil {
				return errors.Wrap(err, "could not listen")
			}

			return nil
		},
	}
}
