package server

import (
	_ "github.com/golang-migrate/migrate/v4/database/sqlite"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
	"gitlab.com/arcadbox/arcad/plugin/market/config"
)

func IndexCommand(conf *config.Config) *cli.Command {
	return &cli.Command{
		Name:  "index",
		Usage: "Regenerate the index for the current registry database",
		Action: func(ctx *cli.Context) error {
			if err := reindexDatabase(ctx.Context, conf); err != nil {
				return errors.WithStack(err)
			}

			return nil
		},
	}
}
