package server

import (
	"context"

	"github.com/golang-migrate/migrate/v4"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/plugin/market/config"
	"gitlab.com/arcadbox/arcad/plugin/market/indexer"
	"gitlab.com/arcadbox/arcad/plugin/market/repository"
	"gitlab.com/wpetit/goweb/logger"
)

const (
	MigrateVersionUp     = "up"
	MigrateVersionLatest = "latest"
	MigrateVersionDown   = "down"
)

func migrateDatabase(ctx context.Context, conf *config.Config, target string, forceVersion int) error {
	ctx = logger.With(
		ctx,
		logger.F("DSN", conf.Database.DSN),
	)
	logger.Info(ctx, "migrating database")

	migr, err := migrate.New(
		conf.Database.Migrations,
		conf.Database.DSN,
	)
	if err != nil {
		return errors.WithStack(err)
	}

	defer func() {
		if srcErr, dbErr := migr.Close(); srcErr != nil || dbErr != nil {
			if srcErr != nil {
				panic(errors.WithStack(srcErr))
			}

			if dbErr != nil {
				panic(errors.WithStack(dbErr))
			}
		}
	}()

	version, dirty, err := migr.Version()
	if err != nil && !errors.Is(err, migrate.ErrNilVersion) {
		return errors.WithStack(err)
	}

	logger.Info(
		ctx, "current database shema",
		logger.F("version", version),
		logger.F("dirty", dirty),
	)

	if forceVersion != -1 {
		logger.Info(ctx, "forcing database schema version", logger.F("version", forceVersion))

		if err := migr.Force(forceVersion); err != nil {
			return errors.WithStack(err)
		}

		return nil
	}

	switch target {
	case "":
		fallthrough
	case MigrateVersionLatest:
		err = migr.Up()
	case MigrateVersionDown:
		err = migr.Steps(-1)
	case MigrateVersionUp:
		err = migr.Steps(1)
	default:
		return errors.Errorf(
			"unknown migration target: '%s', available: '%s' (default), '%s' or '%s'",
			target, MigrateVersionLatest, MigrateVersionUp, MigrateVersionDown,
		)
	}

	if err != nil && !errors.Is(err, migrate.ErrNoChange) {
		return errors.Wrap(err, "could not apply migration")
	}

	version, dirty, err = migr.Version()
	if err != nil && !errors.Is(err, migrate.ErrNilVersion) {
		return errors.WithStack(err)
	}

	logger.Info(
		ctx, "database shema after migration",
		logger.F("version", version),
		logger.F("dirty", dirty),
	)

	logger.Info(ctx, "database migration finished sucessfully")

	return nil
}

func reindexDatabase(ctx context.Context, conf *config.Config) error {
	ctx = logger.With(
		ctx,
		logger.F("path", conf.Indexer.Path),
		logger.F("defaultAnalyzer", conf.Indexer.DefaultAnalyzer),
	)

	logger.Info(ctx, "reindexing database")

	indexer, err := indexer.NewIndexerFromPath(conf.Indexer.Path, conf.Indexer.DefaultAnalyzer)
	if err != nil {
		return errors.WithStack(err)
	}

	defer func() {
		if err := indexer.Close(); err != nil {
			panic(errors.Wrap(err, "could not close indexer"))
		}
	}()

	repo, closeFunc, err := repository.NewAppRepositoryFromDSN(conf.Database.DSN)
	if err != nil {
		return errors.WithStack(err)
	}

	defer func() {
		if err := closeFunc(); err != nil {
			panic(errors.Wrap(err, "could not close repository"))
		}
	}()

	err = repo.ForEachApp(ctx, func(ctx context.Context, app *repository.App) error {
		logger.Info(ctx, "indexing app", logger.F("appID", app.ID))

		if err := indexer.IndexApp(app); err != nil {
			return errors.WithStack(err)
		}

		return nil
	})
	if err != nil {
		return errors.WithStack(err)
	}

	logger.Info(ctx, "reindexing finished successfully")

	return nil
}
