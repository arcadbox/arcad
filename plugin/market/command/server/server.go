package server

import (
	"github.com/urfave/cli/v2"
	"gitlab.com/arcadbox/arcad/plugin/market/config"
)

func Commands(conf *config.Config) *cli.Command {
	return &cli.Command{
		Name:  "server",
		Usage: "Manage registry server related operations",
		Subcommands: []*cli.Command{
			RunCommand(conf),
			MigrateCommand(conf),
			IndexCommand(conf),
		},
	}
}
