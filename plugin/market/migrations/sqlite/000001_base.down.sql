DROP TABLE "releases";

DROP TRIGGER "apps_updated_at_trigger";
DROP TABLE "apps";

DROP TRIGGER "users_updated_at_trigger";
DROP TABLE "users";
