-- Table "users"

CREATE TABLE "users" (
    id INTEGER PRIMARY KEY,
    username TEXT NOT NULL UNIQUE,
    email TEXT NOT NULL UNIQUE,
    password TEXT NOT NULL,
    created_at DATETIME NOT NULL DEFAULT (DATETIME('now', 'localtime')),
    updated_at DATETIME NOT NULL DEFAULT (DATETIME('now', 'localtime'))
);

CREATE TRIGGER users_updated_at_trigger AFTER UPDATE 
ON users
FOR EACH ROW
BEGIN
    UPDATE users SET updated_at = DATETIME('now', 'localtime') WHERE id == NEW.id;
END;

-- Table "apps"

CREATE TABLE "apps" (
    id INTEGER PRIMARY KEY,
    owner_id INTEGER NOT NULL,
    name STRING NOT NULL,
    latest_release_id INTEGER,
    created_at DATETIME NOT NULL DEFAULT (DATETIME('now', 'localtime')),
    updated_at DATETIME NOT NULL DEFAULT (DATETIME('now', 'localtime')),
    FOREIGN KEY(owner_id) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY(latest_release_id) REFERENCES releases(id)
);

CREATE TRIGGER apps_updated_at_trigger AFTER UPDATE 
ON apps
FOR EACH ROW
BEGIN
    UPDATE apps SET updated_at = DATETIME('now', 'localtime') WHERE id == NEW.id;
END;

CREATE TABLE "releases" (
    id INTEGER PRIMARY KEY,
    app_id INTEGER NOT NULL,
    cid TEXT NOT NULL,
    archive_format TEXT NOT NULL,
    version TEXT NOT NULL,
    description TEXT,
    created_at DATETIME NOT NULL DEFAULT (DATETIME('now', 'localtime')),
    FOREIGN KEY(app_id) REFERENCES apps(id) ON DELETE CASCADE
);
