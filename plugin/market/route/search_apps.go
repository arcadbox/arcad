package route

import (
	"net/http"

	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/plugin/market/indexer"
	"gitlab.com/arcadbox/arcad/plugin/market/repository"
	"gitlab.com/wpetit/goweb/api"
	"gitlab.com/wpetit/goweb/logger"
)

func SearchApps(dsn string, indexer *indexer.Indexer) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		repo, closeFunc, err := repository.NewAppRepositoryFromDSN(dsn)
		if err != nil {
			handleUnexpectedError(ctx, w, errors.WithStack(err))

			return
		}

		defer func() {
			if err := closeFunc(); err != nil {
				logger.Error(
					ctx, "could not close database connection",
					logger.E(errors.WithStack(err)),
				)
			}
		}()

		search := r.URL.Query().Get("search")

		if search == "" {
			search = "*"
		}

		appIDs, err := indexer.SearchApps(ctx, search)
		if err != nil {
			handleUnexpectedError(ctx, w, errors.WithStack(err))

			return
		}

		apps, err := repo.FetchApps(ctx, appIDs)
		if err != nil {
			handleUnexpectedError(ctx, w, errors.WithStack(err))

			return
		}

		api.DataResponse(w, http.StatusOK, struct {
			Apps []*repository.App
		}{
			Apps: apps,
		})
	}
}
