package route

import (
	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/plugin/market/config"
	"gitlab.com/arcadbox/arcad/plugin/market/indexer"
	"gitlab.com/arcadbox/arcad/plugin/market/route/middleware"
)

func Mount(r chi.Router, conf *config.Config) error {
	indexer, err := indexer.NewIndexerFromPath(conf.Indexer.Path, conf.Indexer.DefaultAnalyzer)
	if err != nil {
		return errors.WithStack(err)
	}

	r.Route("/api/v1", func(r chi.Router) {
		// Public API
		r.Group(func(r chi.Router) {
			r.Get("/apps", SearchApps(conf.Database.DSN, indexer))
			r.Get("/apps/{ID}", GetApp(conf.Database.DSN))

			r.Post("/account", RegisterAccount(conf.Database.DSN, conf.Feature.RegistrationEnabled))
			r.Get("/users/{ID}", GetUser(conf.Database.DSN))
		})

		// Authenticated API
		r.Group(func(r chi.Router) {
			r.Use(middleware.Authenticate("registry", createUserAuthFunc(conf.Database.DSN)))

			r.Get("/account", GetAccount(conf.Database.DSN))

			r.Post("/apps", CreateApp(conf.Database.DSN, conf.IPFS.GatewayURL, indexer))
			r.Post("/apps/{appID}/releases", CreateRelease(conf.Database.DSN, conf.IPFS.GatewayURL, indexer))
		})
	})

	return nil
}
