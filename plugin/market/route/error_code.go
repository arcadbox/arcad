package route

import "gitlab.com/wpetit/goweb/api"

const (
	ErrCodeBadRequest           api.ErrorCode = "bad-request"
	ErrCodeNotFound             api.ErrorCode = "not-found"
	ErrCodeInternalServerError  api.ErrorCode = "internal-server-error"
	ErrCodeAccountAlreadyExists api.ErrorCode = "account-already-exists"
	ErrCodeAppAlreadyExists     api.ErrorCode = "app-already-exists"
	ErrCodeReleaseAlreadyExists api.ErrorCode = "release-already-exists"
	ErrCodeStalledVersion       api.ErrorCode = "stalled-version"
	ErrCodeForbidden            api.ErrorCode = "forbidden"
	ErrCodeRegistrationDisabled api.ErrorCode = "registration-disabled"
)
