package route

import (
	"net/http"

	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/plugin/market/repository"
	"gitlab.com/wpetit/goweb/api"
	"gitlab.com/wpetit/goweb/logger"
)

func GetApp(dsn string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		id, ok := assertInt64URLParam(w, r, "ID")
		if !ok {
			return
		}

		repo, closeFunc, err := repository.NewAppRepositoryFromDSN(dsn)
		if err != nil {
			handleUnexpectedError(ctx, w, errors.WithStack(err))

			return
		}

		defer func() {
			if err := closeFunc(); err != nil {
				logger.Error(
					ctx, "could not close database connection",
					logger.E(errors.WithStack(err)),
				)
			}
		}()

		app, err := repo.FindByID(ctx, id)
		if err != nil {
			if errors.Is(err, repository.ErrNotFound) {
				api.ErrorResponse(w, http.StatusNotFound, ErrCodeNotFound, nil)

				return
			}

			handleUnexpectedError(ctx, w, errors.WithStack(err))

			return
		}

		api.DataResponse(w, http.StatusOK, struct {
			App *repository.App
		}{
			App: app,
		})
	}
}
