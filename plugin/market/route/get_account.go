package route

import (
	"net/http"

	"gitlab.com/arcadbox/arcad/plugin/market/repository"
	"gitlab.com/wpetit/goweb/api"
)

func GetAccount(dsn string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		user, ok := assertRequestUser(w, r, dsn)
		if !ok {
			return
		}

		api.DataResponse(w, http.StatusOK, struct {
			User *repository.User
		}{
			User: user,
		})
	}
}
