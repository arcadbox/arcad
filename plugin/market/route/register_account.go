package route

import (
	"net/http"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/plugin/market/repository"
	"gitlab.com/wpetit/goweb/api"
	"gitlab.com/wpetit/goweb/logger"
)

type registerAccountRequest struct {
	Email    string `json:"email" validate:"email"`
	Username string `json:"username" validate:"printascii,gte=3"`
	Password string `json:"password" validate:"gte=8"`
}

func RegisterAccount(dsn string, registrationEnabled bool) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		if !registrationEnabled {
			api.ErrorResponse(w, http.StatusForbidden, ErrCodeRegistrationDisabled, nil)

			return
		}

		registerAccountReq := &registerAccountRequest{}

		if ok := api.Bind(w, r, registerAccountReq); !ok {
			return
		}

		repo, closeFunc, err := repository.NewUserRepositoryFromDSN(dsn)
		if err != nil {
			handleUnexpectedError(ctx, w, errors.WithStack(err))

			return
		}

		defer func() {
			if err := closeFunc(); err != nil {
				logger.Error(
					ctx, "could not close database connection",
					logger.E(errors.WithStack(err)),
				)
			}
		}()

		lowercaseUsername := strings.ToLower(registerAccountReq.Username)

		userID, err := repo.Create(ctx, lowercaseUsername, registerAccountReq.Email, registerAccountReq.Password)
		if err != nil {
			if errors.Is(err, repository.ErrAccountAlreadyExists) {
				api.ErrorResponse(w, http.StatusConflict, ErrCodeAccountAlreadyExists, nil)

				return
			}

			handleUnexpectedError(ctx, w, errors.WithStack(err))

			return
		}

		user, err := repo.FindByID(ctx, userID)
		if err != nil {
			if errors.Is(err, repository.ErrNotFound) {
				api.ErrorResponse(w, http.StatusNotFound, ErrCodeNotFound, nil)

				return
			}

			handleUnexpectedError(ctx, w, errors.WithStack(err))

			return
		}

		api.DataResponse(w, http.StatusOK, struct {
			User *repository.User
		}{
			User: user,
		})
	}
}
