package route

import (
	"context"
	"net/http"
	"os"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/app"
	"gitlab.com/arcadbox/arcad/internal/bundle"
	"gitlab.com/arcadbox/arcad/internal/market/client"
	arcadRepository "gitlab.com/arcadbox/arcad/internal/repository"
	"gitlab.com/arcadbox/arcad/plugin/market/repository"
	"gitlab.com/arcadbox/arcad/plugin/market/route/middleware"
	"gitlab.com/wpetit/goweb/api"
	"gitlab.com/wpetit/goweb/logger"
)

func createUserAuthFunc(dsn string) middleware.AuthFunc {
	return func(ctx context.Context, username, password string) (bool, error) {
		repo, closeFn, err := repository.NewUserRepositoryFromDSN(dsn)

		defer func() {
			if err := closeFn(); err != nil {
				logger.Error(
					context.Background(),
					"could not close database connection",
					logger.E(errors.WithStack(err)),
				)
			}
		}()

		ok, err := repo.Authenticate(ctx, username, password)
		if err != nil {
			return false, errors.WithStack(err)
		}

		return ok, nil
	}
}

func handleUnexpectedError(ctx context.Context, w http.ResponseWriter, err error) {
	logger.Error(
		ctx,
		"unexpected error",
		logger.E(errors.WithStack(err)),
	)

	api.ErrorResponse(w, http.StatusInternalServerError, ErrCodeInternalServerError, nil)
}

func assertInt64URLParam(w http.ResponseWriter, r *http.Request, param string) (int64, bool) {
	rawID := chi.URLParam(r, param)

	id, err := strconv.ParseInt(rawID, 10, 64)
	if err != nil {
		api.ErrorResponse(w, http.StatusBadRequest, ErrCodeBadRequest, nil)

		return 0, false
	}

	return id, true
}

func assertRequestUser(w http.ResponseWriter, r *http.Request, dsn string) (*repository.User, bool) {
	ctx := r.Context()

	username, err := middleware.CtxUsername(ctx)
	if err != nil {
		handleUnexpectedError(ctx, w, errors.WithStack(err))

		return nil, false
	}

	repo, closeFunc, err := repository.NewUserRepositoryFromDSN(dsn)
	if err != nil {
		handleUnexpectedError(ctx, w, errors.WithStack(err))

		return nil, false
	}

	defer func() {
		if err := closeFunc(); err != nil {
			logger.Error(
				ctx, "could not close database connection",
				logger.E(errors.WithStack(err)),
			)
		}
	}()

	user, err := repo.FindByUsername(ctx, username)
	if err != nil {
		if errors.Is(err, repository.ErrNotFound) {
			logger.Error(ctx, "could not find user", logger.F("username", username), logger.E(errors.WithStack(err)))
			api.ErrorResponse(w, http.StatusNotFound, ErrCodeNotFound, nil)

			return nil, false
		}

		handleUnexpectedError(ctx, w, errors.WithStack(err))

		return nil, false
	}

	return user, true
}

func assertRequestApp(w http.ResponseWriter, r *http.Request, dsn string) (*repository.App, bool) {
	ctx := r.Context()

	appID, ok := assertInt64URLParam(w, r, "appID")
	if !ok {
		return nil, false
	}

	repo, closeFunc, err := repository.NewAppRepositoryFromDSN(dsn)
	if err != nil {
		handleUnexpectedError(ctx, w, errors.WithStack(err))

		return nil, false
	}

	defer func() {
		if err := closeFunc(); err != nil {
			logger.Error(
				ctx, "could not close database connection",
				logger.E(errors.WithStack(err)),
			)
		}
	}()

	app, err := repo.FindByID(ctx, appID)
	if err != nil {
		if errors.Is(err, repository.ErrNotFound) {
			logger.Error(ctx, "could not find app", logger.F("id", appID), logger.E(errors.WithStack(err)))
			api.ErrorResponse(w, http.StatusNotFound, ErrCodeNotFound, nil)

			return nil, false
		}

		handleUnexpectedError(ctx, w, errors.WithStack(err))

		return nil, false
	}

	return app, true
}

func fetchAppManifest(ctx context.Context, gatewayURL string, cid string, archiveFormat bundle.ArchiveExt) (*arcadRepository.AppManifest, error) {
	client := client.New(
		client.WithIPFSGatewayURL(gatewayURL),
	)

	bundle, filepath, err := client.DownloadApp(ctx, cid, archiveFormat)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	defer func() {
		if err := os.Remove(filepath); err != nil {
			logger.Error(
				ctx, "could not remove downloaded app archive",
				logger.E(errors.WithStack(err)),
				logger.F("filepath", filepath),
			)
		}
	}()

	appManifest, err := app.LoadAppManifest(bundle)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return appManifest, nil
}
