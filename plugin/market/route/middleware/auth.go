package middleware

import (
	"context"
	"fmt"
	"net/http"

	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/logger"
)

type contextKey string

const (
	usernameContextKey contextKey = "username"
)

type AuthFunc func(ctx context.Context, username string, password string) (bool, error)

func Authenticate(realm string, authFunc AuthFunc) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			ctx := r.Context()

			username, password, ok := r.BasicAuth()
			if !ok {
				basicAuthFailed(w, realm)

				return
			}

			ok, err := authFunc(ctx, username, password)
			if err != nil {
				logger.Error(
					ctx, "could not authenticate user",
					logger.E(errors.WithStack(err)),
					logger.F("username", username),
				)

				basicAuthFailed(w, realm)

				return
			}

			if !ok {
				logger.Warn(
					ctx, "authentication failed",
					logger.F("username", username),
				)

				basicAuthFailed(w, realm)

				return
			}

			ctx = context.WithValue(ctx, usernameContextKey, username)

			r = r.WithContext(ctx)

			next.ServeHTTP(w, r)
		}

		return http.HandlerFunc(fn)
	}
}

func basicAuthFailed(w http.ResponseWriter, realm string) {
	w.Header().Add("WWW-Authenticate", fmt.Sprintf(`Basic realm="%s"`, realm))
	w.WriteHeader(http.StatusUnauthorized)
}

func CtxUsername(ctx context.Context) (string, error) {
	username, ok := ctx.Value(usernameContextKey).(string)
	if !ok {
		return "", errors.New("could not find username in context")
	}

	return username, nil
}
