package route

import (
	"net/http"

	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/plugin/market/indexer"
	"gitlab.com/arcadbox/arcad/plugin/market/repository"
	"gitlab.com/wpetit/goweb/api"
	"gitlab.com/wpetit/goweb/logger"
)

func CreateRelease(dsn string, gatewayURL string, indexer *indexer.Indexer) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		createReq := &createRequest{}

		if ok := api.Bind(w, r, createReq); !ok {
			return
		}

		user, ok := assertRequestUser(w, r, dsn)
		if !ok {
			return
		}

		app, ok := assertRequestApp(w, r, dsn)
		if !ok {
			return
		}

		appManifest, err := fetchAppManifest(ctx, gatewayURL, createReq.CID, createReq.ArchiveFormat)
		if err != nil {
			handleUnexpectedError(ctx, w, errors.WithStack(err))

			return
		}

		repo, closeFunc, err := repository.NewAppRepositoryFromDSN(dsn)
		if err != nil {
			handleUnexpectedError(ctx, w, errors.WithStack(err))

			return
		}

		defer func() {
			if err := closeFunc(); err != nil {
				logger.Error(
					ctx, "could not close database connection",
					logger.E(errors.WithStack(err)),
				)
			}
		}()

		appID, err := repo.CreateRelease(ctx, user.ID, app.ID, createReq.CID, createReq.ArchiveFormat, appManifest)
		if err != nil {
			if errors.Is(err, repository.ErrForbidden) {
				api.ErrorResponse(w, http.StatusForbidden, ErrCodeForbidden, nil)

				return
			}

			if errors.Is(err, repository.ErrReleaseAlreadyExists) {
				api.ErrorResponse(w, http.StatusBadRequest, ErrCodeReleaseAlreadyExists, nil)

				return
			}

			if errors.Is(err, repository.ErrStaledVersion) {
				api.ErrorResponse(w, http.StatusBadRequest, ErrCodeStalledVersion, nil)

				return
			}

			handleUnexpectedError(ctx, w, errors.WithStack(err))

			return
		}

		app, err = repo.FindByID(ctx, appID)
		if err != nil {
			if errors.Is(err, repository.ErrNotFound) {
				api.ErrorResponse(w, http.StatusNotFound, ErrCodeNotFound, nil)

				return
			}

			handleUnexpectedError(ctx, w, errors.WithStack(err))

			return
		}

		defer func() {
			if err := indexer.IndexApp(app); err != nil {
				logger.Error(
					ctx,
					"unexpected error",
					logger.E(errors.WithStack(err)),
				)

				return
			}
		}()

		api.DataResponse(w, http.StatusOK, struct {
			App *repository.App
		}{
			App: app,
		})
	}
}
