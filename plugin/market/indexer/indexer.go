package indexer

import (
	"context"
	"os"
	"strconv"
	"time"

	"github.com/blevesearch/bleve/v2"
	"github.com/blevesearch/bleve/v2/mapping"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/plugin/market/repository"
)

type Indexer struct {
	index bleve.Index
}

type indexedApp struct {
	ID          int64
	Name        string
	Description string
	Owner       string
	Version     string
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

func (a *indexedApp) Type() string {
	return "app"
}

func NewIndexer(index bleve.Index) *Indexer {
	return &Indexer{
		index: index,
	}
}

func (i *Indexer) SearchApps(ctx context.Context, search string) ([]int64, error) {
	query := bleve.NewQueryStringQuery(search)
	req := bleve.NewSearchRequest(query)

	req.Fields = []string{"ID"}

	results, err := i.index.SearchInContext(ctx, req)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	appIDs := make([]int64, 0, results.Size())

	for _, r := range results.Hits {
		appID, ok := r.Fields["ID"].(float64)
		if !ok {
			continue
		}

		appIDs = append(appIDs, int64(appID))
	}

	return appIDs, nil
}

func (i *Indexer) Close() error {
	if err := i.index.Close(); err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func (i *Indexer) IndexApp(app *repository.App) error {
	id := strconv.FormatInt(app.ID, 10)
	indexedApp := &indexedApp{
		ID:          app.ID,
		Name:        app.Name,
		CreatedAt:   app.CreatedAt,
		UpdatedAt:   app.UpdatedAt,
		Version:     "",
		Owner:       "",
		Description: "",
	}

	if app.LatestRelease != nil {
		indexedApp.Version = app.LatestRelease.Version
		indexedApp.Description = app.LatestRelease.Description
	}

	if app.Owner != nil {
		indexedApp.Owner = app.Owner.Username
	}

	if err := i.index.Index(id, indexedApp); err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func NewIndexerFromPath(path string, defaultAnalyzer string) (*Indexer, error) {
	var index bleve.Index

	if _, err := os.Stat(path); os.IsNotExist(err) {
		mapping := getIndexMapping(defaultAnalyzer)

		index, err = bleve.New(path, mapping)
		if err != nil {
			return nil, errors.WithStack(err)
		}
	} else {
		index, err = bleve.Open(path)
		if err != nil {
			return nil, errors.WithStack(err)
		}
	}

	return NewIndexer(index), nil
}

func getIndexMapping(defaultAnalyzer string) mapping.IndexMapping {
	indexMapping := bleve.NewIndexMapping()

	appMapping := bleve.NewDocumentMapping()
	appMapping.DefaultAnalyzer = defaultAnalyzer

	idFieldMapping := bleve.NewNumericFieldMapping()
	idFieldMapping.Store = true
	appMapping.AddFieldMappingsAt("ID", idFieldMapping)

	nameFieldMapping := bleve.NewTextFieldMapping()
	nameFieldMapping.Store = false
	appMapping.AddFieldMappingsAt("Name", nameFieldMapping)

	descriptionFieldMapping := bleve.NewTextFieldMapping()
	descriptionFieldMapping.Store = false
	appMapping.AddFieldMappingsAt("Description", descriptionFieldMapping)

	versionFieldMapping := bleve.NewTextFieldMapping()
	versionFieldMapping.Store = false
	appMapping.AddFieldMappingsAt("Version", versionFieldMapping)

	ownerFieldMapping := bleve.NewTextFieldMapping()
	ownerFieldMapping.Store = false
	appMapping.AddFieldMappingsAt("Owner", ownerFieldMapping)

	createdAtFieldMapping := bleve.NewDateTimeFieldMapping()
	createdAtFieldMapping.Store = false
	appMapping.AddFieldMappingsAt("CreatedAt", createdAtFieldMapping)

	updatedAtFieldMapping := bleve.NewDateTimeFieldMapping()
	updatedAtFieldMapping.Store = false
	appMapping.AddFieldMappingsAt("UpdatedAt", updatedAtFieldMapping)

	indexMapping.AddDocumentMapping("app", appMapping)

	return indexMapping
}
