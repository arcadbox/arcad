//go:build plugin_backend_module_cast
// +build plugin_backend_module_cast

package plugin

import "gitlab.com/arcadbox/arcad/plugin/backend/module/cast"

func init() {
	registry.Add(cast.NewPlugin())
}
