//go:build plugin_market
// +build plugin_market

package plugin

import market "gitlab.com/arcadbox/arcad/plugin/market"

func init() {
	registry.Add(market.NewPlugin())
}
