package example

import (
	"context"

	"gitlab.com/arcadbox/arcad/internal/hook"
	"gitlab.com/wpetit/goweb/logger"
	"gitlab.com/wpetit/goweb/plugin"
)

type Plugin struct{}

func (p *Plugin) PluginName() string {
	return "example"
}

func (p *Plugin) PluginVersion() string {
	return "0.0.0"
}

func (p *Plugin) HookConfig(ctx context.Context, config map[string]interface{}) error {
	logger.Debug(ctx, "example plugin configured", logger.F("config", config))

	return nil
}

func NewPlugin() *Plugin {
	return &Plugin{}
}

// Assert interfaces implementation.
var (
	_ plugin.Plugin = &Plugin{}
	_ hook.Config   = &Plugin{}
)
