package yjs

import (
	"encoding/json"
	"net/http"
	"sync"
	"time"

	"github.com/gorilla/websocket"
	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/logger"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

var (
	subscribers = NewSubscribers()
	connections = NewConnSet()
)

const (
	MessageTypeSubscribe   = "subscribe"
	MessageTypeUnsubscribe = "unsubscribe"
	MessageTypePublish     = "publish"
	MessageTypePing        = "ping"
	MessageTypePong        = "pong"
)

func handleSignaling(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		logger.Error(ctx, "could not upgrade connection", logger.E(errors.WithStack(err)))

		return
	}

	connections.Add(conn)
	defer func() {
		connections.Remove(conn)
	}()

	var (
		pongReceived      = true
		pongReceivedMutex sync.RWMutex
	)

	ticker := time.NewTicker(30 * time.Second)
	go func() {
		defer func() {
			ticker.Stop()
			conn.Close()
		}()

		for {
			_, ok := <-ticker.C
			if !ok {
				return
			}

			pongReceivedMutex.RLock()
			received := pongReceived
			pongReceivedMutex.RUnlock()

			if !received {
				pongReceivedMutex.RUnlock()

				return
			}

			if err := sendPing(conn); err != nil {
				logger.Error(ctx, "could not send ping", logger.E(errors.WithStack(err)))

				return
			}
		}
	}()

	subscriptions := NewSubscriptions()

	// Cleanup subscriptions on exit
	defer func() {
		connections.Remove(conn)

		topics := subscriptions.All()
		for _, t := range topics {
			subscribers.Remove(t, conn)
		}
	}()

	for {
		messageType, data, err := conn.ReadMessage()
		if err != nil {
			closeErr, ok := err.(*websocket.CloseError)

			if !ok || closeErr.Code != websocket.CloseGoingAway {
				logger.Error(ctx, "could not read websocket message", logger.E(errors.WithStack(err)))
			}

			return
		}

		if messageType != websocket.TextMessage {
			logger.Warn(ctx, "unexpected websocket message type ignored", logger.F("message", string(data)))

			continue
		}

		logger.Debug(ctx, "incoming signaling message", logger.F("message", string(data)))

		message := make(map[string]interface{})
		if err := json.Unmarshal(data, &message); err != nil {
			logger.Error(ctx, "could not parse websocket message", logger.E(errors.WithStack(err)))

			return
		}

		switch message["type"] {
		case MessageTypeSubscribe:
			if err := handleSubscribe(conn, subscriptions, message); err != nil {
				logger.Error(ctx, "could not handle subscribe message", logger.E(errors.WithStack(err)))

				return
			}

		case MessageTypeUnsubscribe:
			if err := handleUnsubscribe(conn, subscriptions, message); err != nil {
				logger.Error(ctx, "could not handle unsubscribe message", logger.E(errors.WithStack(err)))

				return
			}

		case MessageTypePublish:
			if err := handlePublish(conn, message); err != nil {
				logger.Error(ctx, "could not handle publish message", logger.E(errors.WithStack(err)))

				return
			}

		case MessageTypePing:
			if err := sendPong(conn); err != nil {
				logger.Error(ctx, "could not send pong message", logger.E(errors.WithStack(err)))

				return
			}

		case MessageTypePong:
			pongReceivedMutex.Lock()
			pongReceived = true
			pongReceivedMutex.Unlock()

		default:
			logger.Error(ctx, "unexpected message", logger.F("message", message))

			continue
		}
	}
}

func handleSubscribe(conn *websocket.Conn, subscriptions *Subscriptions, message map[string]interface{}) error {
	topics, ok := message["topics"].([]interface{})
	if !ok {
		return errors.New("unexpected 'topics' field type")
	}

	for _, rt := range topics {
		t, ok := rt.(string)
		if !ok {
			continue
		}

		subscribers.Add(t, conn)
		subscriptions.Add(t)
	}

	return nil
}

func handleUnsubscribe(conn *websocket.Conn, subscriptions *Subscriptions, message map[string]interface{}) error {
	topics, ok := message["topics"].([]interface{})
	if !ok {
		return errors.New("unexpected 'topics' field type")
	}

	for _, rt := range topics {
		t, ok := rt.(string)
		if !ok {
			continue
		}

		subscribers.Remove(t, conn)
		subscriptions.Remove(t)
	}

	return nil
}

func handlePublish(conn *websocket.Conn, message map[string]interface{}) error {
	topic, ok := message["topic"].(string)
	if !ok {
		return errors.New("unexpected 'topic' field type")
	}

	conns := subscribers.All(topic)

	for _, c := range conns {
		if err := connections.SendTo(message, c); err != nil {
			return errors.WithStack(err)
		}
	}

	return nil
}

func sendPong(conn *websocket.Conn) error {
	message := make(map[string]interface{})
	message["type"] = MessageTypePong

	if err := connections.SendTo(message, conn); err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func sendPing(conn *websocket.Conn) error {
	message := make(map[string]interface{})
	message["type"] = MessageTypePing

	if err := connections.SendTo(message, conn); err != nil {
		return errors.WithStack(err)
	}

	return nil
}
