package yjs

import (
	"context"

	"github.com/go-chi/chi"
	"gitlab.com/arcadbox/arcad/internal/hook"
	"gitlab.com/wpetit/goweb/plugin"
)

type Plugin struct{}

func (p *Plugin) PluginName() string {
	return "route_yjs"
}

func (p *Plugin) PluginVersion() string {
	return "0.0.0"
}

func (p *Plugin) HookMountRoutes(ctx context.Context, r chi.Router) error {
	r.HandleFunc("/yjs/signaling", handleSignaling)

	return nil
}

func NewPlugin() *Plugin {
	return &Plugin{}
}

// Assert interfaces implementation.
var (
	_ plugin.Plugin    = &Plugin{}
	_ hook.MountRoutes = &Plugin{}
)
