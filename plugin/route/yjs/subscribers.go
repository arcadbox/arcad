package yjs

import (
	"sync"

	"github.com/gorilla/websocket"
	"github.com/pkg/errors"
)

type Subscribers struct {
	topics map[string]map[*websocket.Conn]struct{}
	mutex  sync.RWMutex
}

func (s *Subscribers) All(topic string) []*websocket.Conn {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	conns := make([]*websocket.Conn, 0)

	set, exists := s.topics[topic]
	if !exists {
		return conns
	}

	for c := range set {
		conns = append(conns, c)
	}

	return conns
}

func (s *Subscribers) Add(topic string, conn *websocket.Conn) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	set, exists := s.topics[topic]
	if !exists {
		set = make(map[*websocket.Conn]struct{})
	}

	set[conn] = struct{}{}
	s.topics[topic] = set
}

func (s *Subscribers) Remove(topic string, conn *websocket.Conn) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	set, exists := s.topics[topic]
	if !exists {
		return
	}

	delete(set, conn)
	s.topics[topic] = set
}

func NewSubscribers() *Subscribers {
	return &Subscribers{
		topics: make(map[string]map[*websocket.Conn]struct{}),
		mutex:  sync.RWMutex{},
	}
}

type ConnSet struct {
	conns map[*websocket.Conn]sync.Mutex
	mutex sync.RWMutex
}

func (s *ConnSet) Add(conn *websocket.Conn) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	s.conns[conn] = sync.Mutex{}
}

func (s *ConnSet) Remove(conn *websocket.Conn) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	delete(s.conns, conn)
}

func (s *ConnSet) SendTo(message interface{}, conns ...*websocket.Conn) error {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	for _, c := range conns {
		l, exists := s.conns[c]
		if !exists {
			continue
		}

		l.Lock()
		if err := c.WriteJSON(message); err != nil {
			return errors.WithStack(err)
		}
		l.Unlock()
	}

	return nil
}

func NewConnSet() *ConnSet {
	return &ConnSet{
		conns: make(map[*websocket.Conn]sync.Mutex),
		mutex: sync.RWMutex{},
	}
}

type Subscriptions struct {
	topics map[string]struct{}
	mutex  sync.RWMutex
}

func (s *Subscriptions) Add(topic string) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	s.topics[topic] = struct{}{}
}

func (s *Subscriptions) Remove(topic string) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	delete(s.topics, topic)
}

func (s *Subscriptions) All() []string {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	topics := make([]string, 0, len(s.topics))

	for t := range s.topics {
		topics = append(topics, t)
	}

	return topics
}

func NewSubscriptions() *Subscriptions {
	return &Subscriptions{
		topics: make(map[string]struct{}),
		mutex:  sync.RWMutex{},
	}
}
