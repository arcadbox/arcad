// +build plugin_backend_module_email

package plugin

import "gitlab.com/arcadbox/arcad/plugin/backend/module/email"

func init() {
	registry.Add(email.NewPlugin())
}
