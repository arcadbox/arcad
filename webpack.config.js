const path = require('path');

// Plugins
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const env = process.env;

module.exports = {
  mode: `${env.NODE_ENV ? env.NODE_ENV : 'production'}`,
  entry: {
    "server": path.join(__dirname, './cmd/arcad/public/src/js/server/index.js'),
    "admin": path.join(__dirname, './cmd/arcad/public/src/js/server/admin-index.js'),
    "client": path.join(__dirname, './cmd/arcad/public/src/js/client/index.js'),
    "sock": path.join(__dirname, './cmd/arcad/public/src/js/client/sock.js')
  },
  output: {
    filename: '[name].js',
    path: path.join(__dirname, './cmd/arcad/public/dist')
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /(node_modules|bower_components)/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env'],
          plugins: ['@babel/plugin-proposal-class-properties']
        }
      }
    },{
      test: /\.s(c|a)ss$/,
      exclude: /node_modules/,
      use: [
        MiniCssExtractPlugin.loader,
        {
          loader: "css-loader",
          options: {}
        },
        {
          loader: "resolve-url-loader",
          options: {}
        },
        {
          loader: 'postcss-loader'
        },
        {
          loader: "sass-loader",
          options: {
            sourceMap: true,
            sourceMapContents: false
          }
        }
      ]
    },{
      test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
      use: [{
          loader: 'file-loader',
          options: {
              name: '[name].[ext]',
              outputPath: '/fonts/'
          }
      }]
    },{
      test: /\.(png|jpg)(\?v=\d+\.\d+\.\d+)?$/,
      use: [{
          loader: 'file-loader',
          options: {
              name: '[name].[ext]',
              outputPath: '/img/'
          }
      }]
    },
    {
      test: /\.svg$/,
      use: [
        'svg-sprite-loader',
        'svgo-loader'
      ]
    }]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "/css/[name].css",
      chunkFilename: "/css/[id].css"
    })
  ]
}