package main

import (
	"context"
	"os"
	"sort"

	"gitlab.com/arcadbox/arcad/cmd/dev-cli/cmd"
	"gitlab.com/wpetit/goweb/logger"

	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
)

func main() {
	ctx := context.Background()

	app := &cli.App{
		Name:     "dev-cli",
		Usage:    "The arcad developer cli helper",
		Commands: cmd.Commands(),
		Before: func(ctx *cli.Context) error {
			workdir := ctx.String("workdir")
			// Switch to new working directory if defined
			if workdir != "" {
				if err := os.Chdir(workdir); err != nil {
					return errors.Wrap(err, "could not change working directory")
				}
			}

			logFormat := ctx.String("log-format")
			logLevel := ctx.Int("log-level")

			logger.SetFormat(logger.Format(logFormat))
			logger.SetLevel(logger.Level(logLevel))

			logger.Debug(ctx.Context, "setting log format", logger.F("format", logFormat))
			logger.Debug(ctx.Context, "setting log level", logger.F("level", logLevel))

			return nil
		},
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:  "workdir",
				Value: "",
				Usage: "The application working directory",
			},
			&cli.IntFlag{
				Name:  "log-level",
				Value: 1,
				Usage: "The application logging level",
			},

			&cli.StringFlag{
				Name:  "log-format",
				Value: "human",
				Usage: "The application logging format",
			},
		},
	}

	sort.Sort(cli.FlagsByName(app.Flags))
	sort.Sort(cli.CommandsByName(app.Commands))

	err := app.RunContext(ctx, os.Args)
	if err != nil {
		logger.Fatal(
			ctx,
			"could not run app",
			logger.E(err),
		)
	}
}
