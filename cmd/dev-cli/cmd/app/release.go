package app

import (
	"os"
	"path"
	"strings"
	"time"

	"github.com/davecgh/go-spew/spew"
	shell "github.com/ipfs/go-ipfs-api"
	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
	"gitlab.com/arcadbox/arcad/internal/bundle"
	market "gitlab.com/arcadbox/arcad/internal/market/client"
)

func ReleaseCommand() *cli.Command {
	return &cli.Command{
		Name:  "release",
		Usage: "Release a new app package",
		Flags: []cli.Flag{
			&cli.IntFlag{
				Name:    "app-id",
				Usage:   "use existing app identifier `APP_ID`",
				Aliases: []string{"i"},
				EnvVars: []string{"ARCAD_MARKET_APP_ID"},
				Value:   -1,
			},
			&cli.StringFlag{
				Name:     "package",
				Usage:    "use package `FILE`",
				Aliases:  []string{"p"},
				EnvVars:  []string{"ARCAD_MARKET_PACKAGE"},
				Required: true,
			},
			&cli.StringFlag{
				Name:    "market-url",
				Usage:   "use market `URL`",
				Aliases: []string{"m"},
				Value:   "https://market.arcad.app",
			},
			&cli.StringFlag{
				Name:    "user",
				Usage:   "use market user `USERNAME`",
				EnvVars: []string{"ARCAD_MARKET_USER"},
			},
			&cli.StringFlag{
				Name:    "password",
				Usage:   "use market password `PASSWORD`",
				EnvVars: []string{"ARCAD_MARKET_PASSWORD"},
			},
			&cli.StringFlag{
				Name:    "ipfs-host",
				Usage:   "use IPFS `HOST`",
				EnvVars: []string{"ARCAD_MARKET_IPFS_HOST"},
				Value:   "localhost:5001",
			},
			&cli.BoolFlag{
				Name:    "ipfs-pin",
				Usage:   "pin package on IPFS host",
				EnvVars: []string{"ARCAD_MARKET_IPFS_PIN"},
				Value:   true,
			},
		},
		Action: func(ctx *cli.Context) error {
			ipfsHost := ctx.String("ipfs-host")
			packagePath := ctx.String("package")
			pinFile := ctx.Bool("ipfs-pin")

			packageFile, err := os.Open(packagePath)
			if err != nil {
				return errors.Wrapf(err, "could not open file '%s'", packagePath)
			}

			defer func() {
				if err := packageFile.Close(); err != nil && !errors.Is(err, os.ErrClosed) {
					panic(errors.Wrapf(err, "could not close file '%s'", packagePath))
				}
			}()

			sh := shell.NewShell(ipfsHost)

			cid, err := sh.Add(packageFile,
				shell.Pin(pinFile),
			)
			if err != nil {
				return errors.Wrapf(err, "could not add file '%s' to ipfs host", packagePath)
			}

			marketURL := ctx.String("market-url")
			appID := ctx.Int("app-id")
			user := ctx.String("user")
			password := ctx.String("password")

			client := market.New(
				market.WithMarketURL(marketURL),
				market.WithRetryMax(5),
				market.WithTimeout(5*time.Minute),
			)

			packageExt := strings.TrimPrefix(path.Ext(packagePath), ".")
			archiveExt := bundle.ArchiveExt(packageExt)

			if appID == -1 {
				result, err := client.CreateApp(ctx.Context, user, password, cid, archiveExt)
				if err != nil {
					return errors.Wrap(err, "could not create app")
				}

				spew.Dump(result)
			} else {
				result, err := client.ReleaseApp(ctx.Context, user, password, appID, cid, archiveExt)
				if err != nil {
					return errors.Wrap(err, "could not create app")
				}

				spew.Dump(result)
			}

			return nil
		},
	}
}
