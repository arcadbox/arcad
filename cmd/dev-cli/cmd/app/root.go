package app

import "github.com/urfave/cli/v2"

func Command() *cli.Command {
	return &cli.Command{
		Name:  "app",
		Usage: "Manage app related operations",
		Subcommands: []*cli.Command{
			PackageCommand(),
			ReleaseCommand(),
		},
	}
}
