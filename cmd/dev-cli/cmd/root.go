package cmd

import (
	"github.com/urfave/cli/v2"
	"gitlab.com/arcadbox/arcad/cmd/dev-cli/cmd/app"
)

func Commands() []*cli.Command {
	return []*cli.Command{
		app.Command(),
	}
}
