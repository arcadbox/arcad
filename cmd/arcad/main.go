package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"runtime"
	"sort"
	"syscall"

	"gitlab.com/arcadbox/arcad/internal/command"
	"gitlab.com/arcadbox/arcad/internal/hook"
	"gitlab.com/arcadbox/arcad/plugin"

	"gitlab.com/wpetit/goweb/logger"

	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
	"gitlab.com/arcadbox/arcad/internal/config"
)

// nolint: gochecknoglobals
var (
	GitRef         = "unknown"
	ProjectVersion = "unknown"
	BuildDate      = "unknown"
)

func main() {
	ctx := context.Background()

	app := &cli.App{
		Name:        "arcad",
		Usage:       "",
		Description: "An multi-purpose apps server",
		Commands:    command.Commands(),
		Before: func(ctx *cli.Context) error {
			workdir := ctx.String("workdir")
			// Switch to new working directory if defined
			if workdir != "" {
				if err := os.Chdir(workdir); err != nil {
					return errors.Wrap(err, "could not change working directory")
				}
			}

			if err := ctx.Set("projectVersion", ProjectVersion); err != nil {
				return errors.WithStack(err)
			}

			if err := ctx.Set("gitRef", GitRef); err != nil {
				return errors.WithStack(err)
			}

			if err := ctx.Set("buildDate", BuildDate); err != nil {
				return errors.WithStack(err)
			}

			configFile := ctx.String("config")

			conf, err := loadConfig(configFile)
			if err != nil {
				return errors.Wrap(err, "could not load configuration file")
			}

			// Configure plugins
			if err := hook.InvokeConfigHook(ctx.Context, plugin.Registry(), conf.Plugins); err != nil {
				return errors.Wrap(err, "could not configure plugins")
			}

			logger.SetFormat(conf.Log.Format)
			logger.SetLevel(conf.Log.Level)

			logger.Debug(ctx.Context, "setting log format", logger.F("format", conf.Log.Format))
			logger.Debug(ctx.Context, "setting log level", logger.F("level", conf.Log.Level.String()))

			ctx.Context = config.WithConfig(ctx.Context, conf)

			return nil
		},
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:  "config",
				Value: "",
				Usage: "The Arcad configuration file path",
			},
			&cli.StringFlag{
				Name:  "workdir",
				Value: "",
				Usage: "The application working directory",
			},
			&cli.StringFlag{
				Name:   "projectVersion",
				Value:  "",
				Hidden: true,
			},
			&cli.StringFlag{
				Name:   "gitRef",
				Value:  "",
				Hidden: true,
			},
			&cli.StringFlag{
				Name:   "buildDate",
				Value:  "",
				Hidden: true,
			},
		},
	}

	sort.Sort(cli.FlagsByName(app.Flags))
	sort.Sort(cli.CommandsByName(app.Commands))

	go handleDebugSignal()

	err := app.RunContext(ctx, os.Args)
	if err != nil {
		logger.Fatal(
			ctx,
			"could not run app",
			logger.E(err),
		)
	}
}

func loadConfig(configFile string) (*config.Config, error) {
	// Load configuration file if defined, use default configuration otherwise
	var (
		conf *config.Config
		err  error
	)

	if configFile != "" {
		conf, err = config.NewFromFile(configFile)
		if err != nil {
			return nil, errors.WithStack(err)
		}
	} else {
		conf = config.NewDefault()
	}

	if err := config.WithEnvironment(conf); err != nil {
		return nil, errors.WithStack(err)
	}

	return conf, nil
}

func handleDebugSignal() {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGQUIT)

	buf := make([]byte, 1<<20) // nolint: gomnd

	for {
		<-sigs

		stacklen := runtime.Stack(buf, true)
		log.Printf("=== received SIGQUIT ===\n*** goroutine dump...\n%s\n*** end\n", buf[:stacklen])
	}
}
