{{define "title"}}{{.App.Manifest.Title}} - Tableau des scores - Arcad{{end}}
{{define "body"}}
<section class="home is-fullheight section">
  <div class="container">
    {{template "header" .}}
    <div class="level">
      <div class="level-left">
        <h1 class="title level-item">{{.App.Manifest.Title}}</h1>
        <h2 class="subtitle level-item">Tableau des scores</h2>
      </div>
      <div class="level-right">
        <a class="level-item button is-primary is-large" href="/apps/{{ .App.ID }}">
          <span>Ouvrir</span>
          <svg class="icon">
            <use xlink:href="#chevron-right"></use>
          </svg>
        </a>
      </div>
    </div>
    <div class="columns">
      <div class="column">
        {{template "podium" .}}
      </div>
      <div class="column">
        {{template "highscores" .}}
      </div>
    </div>
    {{template "footer" .}}
  </div>
</section>
{{end}}
{{template "base" .}}

{{define "podium"}}
<div class="podium-container">
  <div class="podium">
    <div class="pod second">
      {{ if gt (len .Highscores) 1 }}
      {{ $secondPlayerHighscore := index .Highscores 1 }}
      {{if $secondPlayerHighscore}}
      <div class="pod-player">
        {{ $secondPlayer := (index .Users $secondPlayerHighscore.UserID) }}
        <span class="player-nickname">{{with $secondPlayer}}{{ .Nickname }}{{else}}???{{end}}</span>
        <span class="player-score">{{ $secondPlayerHighscore.Score }}</span>
      </div>
      {{end}}
      {{end}}
      <div class="pod-stair">
        <span class="pod-position">2</span>
      </div>
    </div>

    <div class="pod first">
      {{ if gt (len .Highscores) 0 }}
      {{ $firstPlayerHighscore := index .Highscores 0 }}
      {{if $firstPlayerHighscore}}
      <div class="pod-player">
        {{ $firstPlayer := (index .Users $firstPlayerHighscore.UserID) }}
        <svg class="icon player-crown">
          <use xlink:href="#crown"></use>
        </svg>
        <span class="player-nickname">{{with $firstPlayer}}{{ .Nickname }}{{else}}???{{end}}</span>
        <span class="player-score">{{ $firstPlayerHighscore.Score }}</span>
      </div>
       {{end}}
      {{end}}
      <div class="pod-stair">
        <span class="pod-position">1</span>
      </div>
    </div>
    
    <div class="pod third">
      {{ if gt (len .Highscores) 2 }}
      {{ $thirdPlayerHighscore := index .Highscores 2 }}
      {{if $thirdPlayerHighscore}}
      <div class="pod-player">
        {{ $thirdPlayer := (index .Users $thirdPlayerHighscore.UserID) }}
        <span class="player-nickname">{{with $thirdPlayer}}{{ .Nickname }}{{else}}???{{end}}</span>
        <span class="player-score">{{ $thirdPlayerHighscore.Score }}</span>
      </div>
      {{end}}
      {{end}}
      <div class="pod-stair">
        <div class="pod-position">3</div>
      </div>
    </div>

  </div>
</div>
{{end}}


{{define "highscores"}}
{{ $users := .Users }}
<div class="table-container">
  <table class="table is-fullwidth is-hoverable is-striped">
    <thead>
      <th>Position</th>
      <th>Pseudonyme</th>
      <th>Score</th>
      <th>Date</th>
    </thead>
    <tbody>
      {{range $i, $h := .Highscores}}
        {{ $user := (index $users $h.UserID) }}
        <tr>
          {{ $position := addInt $i 1 }}
          <td>{{ $position }}</td>
          <td>{{with $user}}{{ .Nickname }}{{else}}???{{end}}</td>
          <td>{{ $h.Score }}</td>
          <td>{{ localeTimeFormat $h.CreationTime "02/01/2006 15:04" "fr_FR" }}</td>
        </tr>
      {{else}}
        <tr>
          <td colspan="4" class="has-text-centered is-italic">Aucun score pour l'instant.</td>
        </tr>
      {{end}}
    </tbody>
  </table>
</div>
{{end}}