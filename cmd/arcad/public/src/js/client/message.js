import { TypeInit, InitMessage } from "./message/init";

export class Message {
  constructor(type, payload) {
    this._type = type;
    this._payload = payload;
  }

  getType() {
    return this._type;
  }

  getPayload() {
    return this._payload;
  }

  toJSON() {
    return {
      t: this._type,
      p: this._payload
    };
  }

}

export function messageFrom(raw) {
  switch (raw.t) {
    case TypeInit:
      return new InitMessage(raw.p.gid);
    default:
      return new Message(raw.t, raw.p);
  }
}
