import { Message } from "../message";

export const TypeEvent = "event"

export class EventMessage extends Message {
  constructor(data) {
    super(TypeEvent, { d: data });
  }
}