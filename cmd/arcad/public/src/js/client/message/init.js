import { Message } from "../message";

export const TypeInit = "init"

export class InitMessage extends Message {
  constructor(appId) {
    super(TypeInit, { id: appId });
  }
}