import { Controller } from "stimulus"

export default class extends Controller {
  static targets = ["frame"];

  constructor(...args) {
    super(...args);
    this.onFrameHashChange = this.onFrameHashChange.bind(this);
    this.onHashChange = this.onHashChange.bind(this);
    this.titleObserver = null;
  }

  connect() {
    window.addEventListener("hashchange", this.onHashChange);
    this.frameTarget.src = this.data.get("frameSrc") + window.location.hash;
  }

  disconnect() {
    window.removeEventListener("hashchange", this.onHashChange);
    this.cancelFrameTitleObserver();
  }

  onFrameLoad() {
    this.frameTarget.contentWindow.addEventListener("hashchange", this.onFrameHashChange);
    this.initFrameTitleObserver();
  }

  onFrameBeforeUnload() {
    this.frameTarget.contentWindow.removeEventListener("hashchange", this.onFrameHashChange);
    this.cancelFrameTitleObserver();
  }

  onHashChange() {
    this.frameTarget.contentWindow.location.hash = window.location.hash;
  }

  onFrameHashChange() {
    window.location.hash = this.frameTarget.contentWindow.location.hash;
  }

  initFrameTitleObserver() {
    if (this.titleObserver) this.cancelFrameTitleObserver();

    this.titleObserver = new MutationObserver(function(mutations) {
      document.title = mutations[0].target.textContent;
    });

    this.titleObserver.observe(
      this.frameTarget.contentWindow.document.querySelector('title'),
      { subtree: true, characterData: true, childList: true }
    );
  }

  cancelFrameTitleObserver() {
    if (!this.titleObserver) return;
    this.titleObserver.disconnect();
    this.titleObserver = null;
  }
  
}