import { Controller } from "stimulus"

const BackVisibleClass = 'has-back-visible';

export default class extends Controller {

  connect() {
    this.setLinkTargetsBlank();
  }

  flip() {
    if (this.backVisible()) {
      this.hideBack();
    } else {
      this.showBack();
    }
  }

  backVisible() {
    return this.element.classList.contains(BackVisibleClass);
  }

  hideBack() {
    this.element.classList.remove(BackVisibleClass);
  }

  showBack() {
    this.element.classList.add(BackVisibleClass);
  }

  setLinkTargetsBlank() {
    Array.from(this.element.querySelectorAll('.content a')).forEach(el => {
      el.target = '_blank';
    });
  }

}