const config = window.__CONFIG__ || {};

export const SENTRY_DSN = 'SENTRY_DSN';
export const SENTRY_ENVIRONMENT = 'SENTRY_ENVIRONMENT';
export const SENTRY_TRACE_SAMPLE_RATE = 'SENTRY_TRACE_SAMPLE_RATE';
export const DEBUG = 'DEBUG';

export function get(key) {
  return config[key];
}

export function has(key) {
  return config.hasOwnProperty(key);
}