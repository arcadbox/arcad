import * as Sentry from '@sentry/browser';
import { Integrations } from "@sentry/tracing";

import "../../scss/main.scss";
import "./stimulus";
import "./icons";
import { get, has, SENTRY_DSN, SENTRY_ENVIRONMENT, SENTRY_TRACE_SAMPLE_RATE, DEBUG } from './config';

if (has(SENTRY_DSN)) {
  Sentry.init({ 
    dsn: get(SENTRY_DSN),
    environment: get(SENTRY_ENVIRONMENT),
    tracesSampleRate: get(SENTRY_TRACE_SAMPLE_RATE),
    debug: get(DEBUG),
    integrations: [new Integrations.BrowserTracing()],
  });
}