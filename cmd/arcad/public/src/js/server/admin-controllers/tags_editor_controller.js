import { Controller } from "stimulus"

const TAGS_SEPARATOR = ',';

export default class extends Controller {

  static targets = ["tagsSource", "tagsContainer", "newTag"];

  tags = new Set();

  connect() {
    this.loadTags();
    this.render();
  }

  loadTags() {
    this.tags = this.tagsSourceTarget.value
      .split(TAGS_SEPARATOR)
      .reduce((tags, t) => {
        t = t.trim();
        if (!t) return tags;
        tags.add(t);
        return tags;
      }, new Set())
    ;    
  }

  render() {
    // Render tags elements
    this.tagsContainerTarget.innerHTML = "";
    Array.from(this.tags).forEach(t => {
      const el = document.createElement('span');
      el.classList.add("tag", "is-medium", "is-info");
      el.innerHTML = `${t} <div data-action="click->tags-editor#onRemoveTag" data-tag="${t}" class="delete is-small"></div>`;
      this.tagsContainerTarget.appendChild(el);
    });

    // Update tags source input
    this.tagsSourceTarget.value = Array.from(this.tags).join(TAGS_SEPARATOR);
  }

  onAddTag() {
    const newTag = this.newTagTarget.value.trim();
    if (!newTag) return;
    this.tags.add(newTag);
    this.newTagTarget.value = "";
    this.render();
  }

  onRemoveTag(evt) {
    const removedTag = evt.target.dataset.tag;
    this.tags.delete(removedTag);
    this.render();
  }
}