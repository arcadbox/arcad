import { Controller } from "stimulus"

export default class extends Controller {
  connect() {
    this.sendLocalTime();
  }

  sendLocalTime() {
    const now = new Date();
    fetch("/admin/api/v1/clock-sync", {
      headers: {
        "Content-Type": "application/json",
      },
      credentials: 'include',
      method: "POST",
      body: JSON.stringify({
        t: now,
      })
    })
  }
}