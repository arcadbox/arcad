import { Controller } from "stimulus"

const RELOAD_TIMEOUT = 10000;

export default class extends Controller {

  static targets = ["installButton", "cancelButton", "message"];

  onInstallClick() {
    this.installButtonTarget.classList.add('is-loading');
    this.installButtonTarget.disabled = true;
    this.cancelButtonTarget.classList.add('is-hidden');

    this.messageTarget.textContent = "Installation en cours. Veuillez patienter...";

    fetch(window.location.href, {
      method: 'POST'
    })
    .then(res => res.json())
    .then(() => {
      this.installButtonTarget.classList.add('is-hidden');
      this.messageTarget.innerHTML = `
        Installation terminée ! <b>Le serveur va maintenant redémarrer...</b>
        <br /><br />
        Vous serez automatiquement redirigé vers la liste des applications dans ${RELOAD_TIMEOUT/1000} secondes.
      `;

      setTimeout(() => {
        window.location.replace("/admin/apps");
      }, RELOAD_TIMEOUT)
    })
  }

}