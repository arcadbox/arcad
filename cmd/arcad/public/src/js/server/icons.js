import "../../img/logo.svg";
// FontAwesome icons
import '@fortawesome/fontawesome-free/svgs/regular/address-card.svg';
import '@fortawesome/fontawesome-free/svgs/solid/question.svg';
import '@fortawesome/fontawesome-free/svgs/solid/trophy.svg';
import '@fortawesome/fontawesome-free/svgs/solid/chevron-right.svg';
import '@fortawesome/fontawesome-free/svgs/solid/chevron-left.svg';
import '@fortawesome/fontawesome-free/svgs/solid/crown.svg';
import '@fortawesome/fontawesome-free/svgs/solid/user.svg';
import '@fortawesome/fontawesome-free/svgs/solid/sign-out-alt.svg';
import '@fortawesome/fontawesome-free/svgs/solid/edit.svg';
import '@fortawesome/fontawesome-free/svgs/solid/home.svg';