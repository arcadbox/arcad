module gitlab.com/arcadbox/arcad

go 1.13

require (
	cdr.dev/slog v1.4.1
	forge.cadoles.com/wpetit/go-captiveportal v0.0.0-20210921091920-34fa420040d0
	forge.cadoles.com/wpetit/go-tunnel v0.0.0-20201028074005-96c1575a0b79
	github.com/DataDog/zstd v1.4.4 // indirect
	github.com/Masterminds/goutils v1.1.1 // indirect
	github.com/Masterminds/semver v1.5.0 // indirect
	github.com/Masterminds/sprig v2.22.0+incompatible
	github.com/alecthomas/chroma v0.9.2 // indirect
	github.com/asdine/storm v2.1.2+incompatible
	github.com/asdine/storm/v3 v3.0.0
	github.com/avct/uasurfer v0.0.0-20191028135549-26b5daa857f1
	github.com/barnybug/go-cast v0.0.0-20201201064555-a87ccbc26692
	github.com/bits-and-blooms/bitset v1.2.1 // indirect
	github.com/blevesearch/bleve/v2 v2.2.2
	github.com/btcsuite/btcd v0.22.0-beta // indirect
	github.com/caarlos0/env/v6 v6.4.0
	github.com/davecgh/go-spew v1.1.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/dop251/goja v0.0.0-20211211112501-fb27c91c26ed
	github.com/dop251/goja_nodejs v0.0.0-20211022123610-8dd9abb0616d
	github.com/doug-martin/goqu/v9 v9.18.0
	github.com/fatih/color v1.13.0 // indirect
	github.com/fogleman/gg v1.3.0
	github.com/genjidb/genji v0.10.0
	github.com/getsentry/sentry-go v0.10.0
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-http-utils/etag v0.0.0-20161124023236-513ea8f21eb1
	github.com/go-http-utils/fresh v0.0.0-20161124030543-7231e26a4b27 // indirect
	github.com/go-http-utils/headers v0.0.0-20181008091004-fed159eddc2a // indirect
	github.com/golang-migrate/migrate/v4 v4.15.0
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/goodsign/monday v0.0.0-20191212105357-5d0e7ec6778c
	github.com/google/uuid v1.2.0
	github.com/gorilla/csrf v1.6.1
	github.com/gorilla/sessions v1.2.0
	github.com/gorilla/websocket v1.4.2
	github.com/hashicorp/go-retryablehttp v0.7.0
	github.com/hashicorp/mdns v1.0.4 // indirect
	github.com/huandu/xstrings v1.3.1 // indirect
	github.com/igm/sockjs-go/v3 v3.0.0
	github.com/imdario/mergo v0.3.12 // indirect
	github.com/ipfs/go-cid v0.1.0 // indirect
	github.com/ipfs/go-ipfs-api v0.3.0
	github.com/karlseguin/ccache/v2 v2.0.8 // indirect
	github.com/libp2p/go-libp2p-core v0.13.0 // indirect
	github.com/mattn/go-colorable v0.1.11 // indirect
	github.com/mitchellh/copystructure v1.0.0 // indirect
	github.com/mitchellh/mapstructure v1.4.1
	github.com/multiformats/go-base32 v0.0.4 // indirect
	github.com/multiformats/go-multiaddr v0.5.0 // indirect
	github.com/multiformats/go-multihash v0.1.0 // indirect
	github.com/olekukonko/tablewriter v0.0.5
	github.com/orcaman/concurrent-map v0.0.0-20210106121528-16402b402231
	github.com/pborman/uuid v1.2.1
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.8.0
	github.com/robfig/cron/v3 v3.0.0
	github.com/speps/go-hashids v2.0.0+incompatible
	github.com/spf13/afero v1.3.1
	github.com/streamrail/concurrent-map v0.0.0-20160823150647-8bf1e9bacbf6
	github.com/urfave/cli/v2 v2.3.0
	github.com/whyrusleeping/tar-utils v0.0.0-20201201191210-20a61371de5b // indirect
	gitlab.com/golang-commonmark/markdown v0.0.0-20191127184510-91b5b3c99c19
	gitlab.com/wpetit/goweb v0.0.0-20210615124357-cc89f755cf45
	go.etcd.io/bbolt v1.3.6
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3
	golang.org/x/image v0.0.0-20191009234506-e7c1f5e7dbb8 // indirect
	golang.org/x/mod v0.5.1
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.1.7 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/auyer/steganography.v2 v2.0.0-20190427051610-2664780d40a3
	gopkg.in/mail.v2 v2.3.1
	gopkg.in/yaml.v2 v2.4.0
	lukechampine.com/blake3 v1.1.7 // indirect
	modernc.org/libc v1.11.28 // indirect
	modernc.org/sqlite v1.13.1
)

// replace forge.cadoles.com/wpetit/go-tunnel => ../../go-tunnel
