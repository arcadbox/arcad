<div align="center">
    <img src="https://gitlab.com/arcadbox/arcad/-/raw/develop/misc/presskit/resources/logo.svg?inline=false" width="25%" />
    <div>
        <h3>Services numériques de proximité</h3>
    </div>
</div>

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=arcadbox_arcad&metric=alert_status)](https://sonarcloud.io/dashboard?id=arcadbox_arcad)

<hr />

Arcad est une suite de [logiciels libres](https://fr.wikipedia.org/wiki/Logiciel_libre) permettant de proposer des services numériques (_applications web_) de proximité i.e. hébergés sur des équipements réseaux du type borne WiFi ([Linksys WRT1900AC](https://www.linksys.com/fr/p/P-WRT1900AC/) par exemple) ou micro-serveur domestique (serveur NAS, [Raspberry Pi](https://www.raspberrypi.org/)).

Un des objectifs du projet est d'offrir un système capable de fonctionner en autonomie: les terminaux numériques (ordinateur, smartphones, tablettes...) des utilisateurs ne doivent pas nécessiter de connexion Internet pour pouvoir accéder aux services.

De manière optionnelle, les services intégrés sur une instance Arcad peuvent être exposés sur Internet via un serveur relais disposant d'une adresse publique.

**Arcad est actuellement au stade d'alpha. Des bugs peuvent donc apparaître et l’expérience utilisateur peut encore manquer de fluidité.**

## Télécharger les binaires

### Variantes

Plusieurs variantes de construction sont disponibles:

- `full` - Version comprenant l'ensemble des plugins disponibles;
- `openwrt` - Version conçue pour le déploiement sur une borne OpenWRT. Certains plugins superflus sont retirés du profil de construction.

### Dernières version stable

`TODO`

### Version de développement

[Voir l'ensemble des versions disponibles](https://gitlab.com/arcadbox/arcad/-/jobs/artifacts/develop/browse/release/?job=Release)
## Documentation

- [Gestion des thèmes](./doc/theming.md)
- [Apps API](./doc/apps_api/README.md)
- [Tutoriels](./doc/tutorials/README.md)

## Démarrer avec les sources

### Dépendances

- [Go >= 1.13](https://golang.org/)

### Procédure

```bash
git clone git@gitlab.com:arcadbox/arcad.git
cd arcad
make install-git-hooks                  # Installer les hooks Git
npm install                             # Installer les dépendances Webpack
make watch                              # Surveiller les modifications sur le sources et compiler/démarrer le serveur
```

Par défaut, l'application devrait être disponible à l'adresse http://localhost:3000/.

Le fichier de configuration sera généré automatiquement dans `data/arcad.yml`. Vous pouvez le modifier pour correspondre à votre environnement de développement.

Des variables d'environnement peuvent être injectées dans le fichier de configuration (grâce au package [`github.com/a8m/envsubst`](https://github.com/a8m/envsubst)). Une partie des variables par défaut sont surchargées dans le fichier `.env` qui peut également être personnalisé.

## Licences

Code source publié sous licence [AGPL-3.0](./LICENSE).

Police "Comfortaa" créée par Johan Aakerlund, Cyreal et publiée sour licence [Open Font Licence](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL) (voir la page [Google Fonts](https://fonts.google.com/specimen/Comfortaa)).
