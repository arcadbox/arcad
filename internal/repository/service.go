package repository

import (
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/setting"
	"gitlab.com/wpetit/goweb/service"
)

const (
	ServiceName service.Name = "repository"
)

func ServiceProvider(highscore HighscoreRepository, user UserRepository, app AppRepository, setting setting.Repository) service.Provider {
	repository := NewRepository(highscore, user, app, setting)
	return func(ctn *service.Container) (interface{}, error) {
		return repository, nil
	}
}

// From retrieves the user repository in the given service container
func From(container *service.Container) (*Repository, error) {
	service, err := container.Service(ServiceName)
	if err != nil {
		return nil, errors.Wrapf(err, "error while retrieving '%s' service", ServiceName)
	}

	srv, ok := service.(*Repository)
	if !ok {
		return nil, errors.Errorf("retrieved service is not a valid '%s' service", ServiceName)
	}

	return srv, nil
}

// Must retrieves the user repository in the given service container or panic otherwise
func Must(container *service.Container) *Repository {
	srv, err := From(container)
	if err != nil {
		panic(err)
	}

	return srv
}
