package repository

import "time"

type HighscoreRepository interface {
	Create(appID AppID, userID UserID, score int64) (*Highscore, error)
	Delete(HighscoreID) error
	Get(HighscoreID) (*Highscore, error)
	List() ([]*Highscore, error)
	FindByAppID(appID AppID, userFilters ...UserID) ([]*Highscore, error)
	FindByUserID(userID UserID) ([]*Highscore, error)
}

type HighscoreID string
type Highscore struct {
	ID           HighscoreID
	Score        int64
	AppID        AppID
	UserID       UserID
	CreationTime time.Time
}
