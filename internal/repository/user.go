package repository

import (
	"time"

	"github.com/avct/uasurfer"
)

type UserRepository interface {
	Create() (*User, error)
	Save(*User) error
	Get(UserID) (*User, error)
	Delete(UserID) error
	Touch(UserID, string) error
	List() ([]*User, error)
	ListByID(...UserID) ([]*User, error)
}

type UserID string
type User struct {
	ID            UserID
	Nickname      string
	CreationTime  time.Time
	LastSeenTime  time.Time
	LastUserAgent *uasurfer.UserAgent
}

type UsersByLastSeen []*User

func (b UsersByLastSeen) Len() int {
	return len(b)
}

func (b UsersByLastSeen) Less(i, j int) bool {
	if b[i].LastSeenTime == b[j].LastSeenTime {
		return b[i].CreationTime.Before(b[j].CreationTime)
	}

	return b[i].LastSeenTime.Before(b[j].LastSeenTime)
}

func (b UsersByLastSeen) Swap(i, j int) {
	b[i], b[j] = b[j], b[i]
}
