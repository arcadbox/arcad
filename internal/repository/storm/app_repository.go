package storm

import (
	"sort"

	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/repository"

	"github.com/asdine/storm/v3"
)

type AppRepository struct {
	db *storm.DB
}

func (r *AppRepository) Init() error {
	if err := r.db.Init(&AppItem{}); err != nil {
		return errors.Wrap(err, "could not initialize app collection")
	}

	if err := r.db.ReIndex(&AppItem{}); err != nil {
		return errors.Wrap(err, "could not initialize app indexes")
	}

	return nil
}

type sortedByOrder []*AppItem

func (s sortedByOrder) Len() int      { return len(s) }
func (s sortedByOrder) Swap(i, j int) { s[i], s[j] = s[j], s[i] }
func (s sortedByOrder) Less(i, j int) bool {
	iOrder := s[i].Order
	if s[i].Override.Order != nil {
		iOrder = *s[i].Override.Order
	}

	jOrder := s[j].Order
	if s[j].Override.Order != nil {
		jOrder = *s[j].Override.Order
	}

	return iOrder < jOrder
}

func (r *AppRepository) List() ([]*repository.App, error) {
	appsItems := make([]*AppItem, 0)

	if err := r.db.All(&appsItems); err != nil {
		if errors.Is(err, storm.ErrNotFound) {
			return nil, errors.WithStack(repository.ErrNotFound)
		}

		return nil, errors.Wrap(err, "could not find apps")
	}

	sort.Sort(sortedByOrder(appsItems))

	apps := make([]*repository.App, 0, len(appsItems))
	for _, i := range appsItems {
		apps = append(apps, i.App())
	}

	return apps, nil
}

func (r *AppRepository) Get(id repository.AppID) (*repository.App, error) {
	appItem := &AppItem{}
	if err := r.db.One("ID", id, appItem); err != nil {
		if errors.Is(err, storm.ErrNotFound) {
			return nil, errors.WithStack(repository.ErrNotFound)
		}

		return nil, errors.Wrapf(err, "could not get app '%s'", id)
	}

	return appItem.App(), nil
}

func (r *AppRepository) Save(app *repository.App) error {
	appItem := &AppItem{}

	if app.ID != "" {
		if err := r.db.One("ID", app.ID, appItem); err != nil {
			if !errors.Is(err, storm.ErrNotFound) {
				return errors.Wrapf(err, "could not get app '%s'", app.ID)
			}
		}
	}

	appItem.ID = app.ID
	appItem.Manifest = app.Manifest
	appItem.Order = app.Order

	if err := r.db.Save(appItem); err != nil {
		return errors.Wrap(err, "could not update app")
	}

	return nil
}

func (r *AppRepository) Override(id repository.AppID, title, description *string, tags []string, order *int) error {
	appItem := &AppItem{}
	if err := r.db.One("ID", id, appItem); err != nil {
		if errors.Is(err, storm.ErrNotFound) {
			return errors.WithStack(repository.ErrNotFound)
		}

		return errors.Wrapf(err, "could not get app '%s'", id)
	}

	appItem.Override.Title = title
	appItem.Override.Description = description
	appItem.Override.Tags = tags
	appItem.Override.Order = order

	if err := r.db.Save(appItem); err != nil {
		return errors.Wrap(err, "could not update app")
	}

	return nil
}

func (r *AppRepository) Delete(id repository.AppID) error {
	appItem := &AppItem{
		ID: id,
	}

	if err := r.db.DeleteStruct(appItem); err != nil {
		if errors.Is(err, storm.ErrNotFound) {
			return errors.WithStack(repository.ErrNotFound)
		}

		return errors.Wrap(err, "could not delete app")
	}

	return nil
}

func (r *AppRepository) Publish(id repository.AppID) error {
	return r.setPublished(id, true)
}

func (r *AppRepository) Unpublish(id repository.AppID) error {
	return r.setPublished(id, false)
}

func (r *AppRepository) setPublished(id repository.AppID, published bool) error {
	appItem := &AppItem{ID: id}

	if err := r.db.UpdateField(appItem, "Published", published); err != nil {
		if errors.Is(err, storm.ErrNotFound) {
			return errors.WithStack(repository.ErrNotFound)
		}

		return errors.Wrap(err, "could not publish app")
	}

	return nil
}

func (r *AppRepository) IsPublished(id repository.AppID) (bool, error) {
	appItem := &AppItem{}
	if err := r.db.One("ID", id, appItem); err != nil {
		if errors.Is(err, storm.ErrNotFound) {
			return false, errors.WithStack(repository.ErrNotFound)
		}

		return false, errors.Wrapf(err, "could not get app '%s'", id)
	}

	return appItem.Published, nil
}

func NewAppRepository(db *storm.DB) *AppRepository {
	return &AppRepository{db}
}
