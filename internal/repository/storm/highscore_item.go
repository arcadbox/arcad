package storm

import (
	"time"

	"gitlab.com/arcadbox/arcad/internal/repository"
)

type HighscoreItem struct {
	ID           repository.HighscoreID `storm:"id"`
	CreationTime time.Time              `storm:"index"`
	AppID        repository.AppID       `storm:"index"`
	UserID       repository.UserID      `storm:"index"`
	Score        int64                  `storm:"index"`
}

func (i *HighscoreItem) Highscore() *repository.Highscore {
	return &repository.Highscore{
		ID:           i.ID,
		CreationTime: i.CreationTime,
		AppID:        i.AppID,
		UserID:       i.UserID,
		Score:        i.Score,
	}
}
