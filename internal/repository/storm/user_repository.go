package storm

import (
	"fmt"
	"time"

	"github.com/avct/uasurfer"

	"github.com/asdine/storm/v3/q"

	"github.com/pborman/uuid"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/repository"

	"github.com/asdine/storm/v3"
	"github.com/speps/go-hashids"
)

type UserRepository struct {
	db *storm.DB
	hd *hashids.HashIDData
}

func (r *UserRepository) Init() error {
	if err := r.db.Init(&UserItem{}); err != nil {
		return errors.Wrap(err, "could not initialize user collection")
	}

	if err := r.db.ReIndex(&UserItem{}); err != nil {
		return errors.Wrap(err, "could not initialize user indexes")
	}

	return nil
}

func (r *UserRepository) Create() (*repository.User, error) {
	tx, err := r.db.Begin(true)
	if err != nil {
		return nil, errors.Wrap(err, "could not start transaction")
	}

	defer func() {
		if err := tx.Rollback(); err != nil && err != storm.ErrNotInTransaction {
			panic(errors.Wrap(err, "could not rollback transaction"))
		}
	}()

	nickname, err := r.generateNickname(tx)
	if err != nil {
		return nil, errors.Wrap(err, "could not generate nickname")
	}

	uid := uuid.New()

	userItem := &UserItem{
		ID:           repository.UserID(uid),
		Nickname:     nickname,
		CreationTime: time.Now().UTC(),
	}

	if err := tx.Save(userItem); err != nil {
		return nil, errors.Wrap(err, "could not save user")
	}

	if err := tx.Commit(); err != nil {
		return nil, errors.Wrap(err, "could not commit transaction")
	}

	return userItem.User(), nil
}

func (r *UserRepository) Save(user *repository.User) error {
	userItem := &UserItem{
		ID:       user.ID,
		Nickname: user.Nickname,
	}
	if err := r.db.Update(userItem); err != nil {
		return errors.Wrap(err, "could not update user")
	}

	return nil
}

func (r *UserRepository) Get(id repository.UserID) (*repository.User, error) {
	userItem := &UserItem{}
	if err := r.db.One("ID", id, userItem); err != nil {
		if err.Error() == storm.ErrNotFound.Error() {
			return nil, repository.ErrNotFound
		}
		return nil, errors.Wrapf(err, "could not get user '%s'", id)
	}

	return userItem.User(), nil
}

func (r *UserRepository) List() ([]*repository.User, error) {
	userItems := make([]*UserItem, 0)

	if err := r.db.All(&userItems); err != nil {
		if err.Error() == storm.ErrNotFound.Error() {
			return []*repository.User{}, nil
		}

		return nil, errors.Wrap(err, "could not find users")
	}

	users := make([]*repository.User, 0, len(userItems))
	for _, u := range userItems {
		users = append(users, u.User())
	}

	return users, nil
}

func (r *UserRepository) ListByID(ids ...repository.UserID) ([]*repository.User, error) {
	userItems := make([]*UserItem, 0)

	matchers := make([]q.Matcher, 0, len(ids))
	for _, userID := range ids {
		matchers = append(matchers, q.Eq("ID", userID))
	}

	query := r.db.Select(q.Or(matchers...))
	if err := query.Find(&userItems); err != nil {
		if err.Error() == storm.ErrNotFound.Error() {
			return []*repository.User{}, nil
		}

		return nil, errors.Wrap(err, "could not find users")
	}

	users := make([]*repository.User, 0, len(userItems))
	for _, u := range userItems {
		users = append(users, u.User())
	}

	return users, nil
}

func (r *UserRepository) Delete(id repository.UserID) error {
	userItem := &UserItem{
		ID: id,
	}

	if err := r.db.DeleteStruct(userItem); err != nil {
		if err.Error() == storm.ErrNotFound.Error() {
			return repository.ErrNotFound
		}

		return errors.Wrap(err, "could not delete user")
	}

	return nil
}

func (r *UserRepository) Touch(id repository.UserID, rawUserAgent string) error {
	userAgent := uasurfer.Parse(rawUserAgent)

	userItem := &UserItem{
		ID:            id,
		LastSeenTime:  time.Now().UTC(),
		LastUserAgent: userAgent,
	}

	if err := r.db.Update(userItem); err != nil {
		return errors.Wrap(err, "could not update user")
	}

	return nil
}

func (r *UserRepository) generateNickname(tx storm.Node) (string, error) {
	var nickname string

	retries := 0

	for {
		total, err := tx.Count(&UserItem{})
		if err != nil {
			return "", errors.Wrap(err, "could not count users")
		}

		h, err := hashids.NewWithData(r.hd)
		if err != nil {
			return "", errors.Wrap(err, "could not initialize hashids encoder")
		}

		now := time.Now().UTC()
		hash, err := h.Encode([]int{
			total,
			now.Hour(),
			now.Minute(),
			now.Second(),
		})
		if err != nil {
			return "", errors.Wrap(err, "could not encode data")
		}

		nickname = fmt.Sprintf("Visiteur_%s", hash)

		exists, err := tx.Select(q.Eq("Nickname", nickname)).Count(&UserItem{})
		if err != nil {
			return "", errors.Wrap(err, "could not check nickname existence")
		}

		if exists == 0 {
			break
		}

		retries++

		if retries >= 10 {
			return "", errors.New("could not generate nickname")
		}
	}

	return nickname, nil
}

func NewUserRepository(db *storm.DB) *UserRepository {
	hd := hashids.NewData()
	return &UserRepository{db, hd}
}
