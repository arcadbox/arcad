package storm

import (
	"time"

	"github.com/avct/uasurfer"
	"gitlab.com/arcadbox/arcad/internal/repository"
)

type UserItem struct {
	ID            repository.UserID `storm:"id"`
	Nickname      string            `storm:"unique"`
	CreationTime  time.Time         `storm:"index"`
	LastSeenTime  time.Time         `storm:"index"`
	LastUserAgent *uasurfer.UserAgent
}

func (i *UserItem) User() *repository.User {
	return &repository.User{
		ID:            i.ID,
		Nickname:      i.Nickname,
		LastSeenTime:  i.LastSeenTime,
		CreationTime:  i.CreationTime,
		LastUserAgent: i.LastUserAgent,
	}
}
