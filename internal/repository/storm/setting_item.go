package storm

import (
	"gitlab.com/arcadbox/arcad/internal/setting"
)

type SettingItem struct {
	ID       setting.ID `storm:"id"`
	Value    interface{}
	metadata *setting.Metadata
}

func (i *SettingItem) Setting() *setting.Setting {
	return setting.NewSetting(i.ID, i.Value, i.metadata)
}
