package storm

import (
	"sync"

	"gitlab.com/arcadbox/arcad/internal/setting"

	"github.com/asdine/storm/v3"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/repository"
)

type SettingRepository struct {
	registry      map[setting.ID]*setting.Metadata
	registryMutex sync.RWMutex
	db            *storm.DB
}

func (r *SettingRepository) Init() error {
	if err := r.db.Init(&SettingItem{}); err != nil {
		return errors.Wrap(err, "could not initialize setting collection")
	}

	if err := r.db.ReIndex(&SettingItem{}); err != nil {
		return errors.Wrap(err, "could not initialize setting indexes")
	}

	return nil
}

func (r *SettingRepository) Register(id setting.ID, metadata *setting.Metadata) error {
	r.registryMutex.Lock()
	defer r.registryMutex.Unlock()

	r.registry[id] = metadata

	return nil
}

func (r *SettingRepository) ListMetadata() (map[setting.ID]*setting.Metadata, error) {
	return r.registry, nil
}

func (r *SettingRepository) GetMetadata(id setting.ID) (*setting.Metadata, error) {
	r.registryMutex.RLock()
	defer r.registryMutex.RUnlock()

	metadata, exists := r.registry[id]
	if !exists {
		return nil, repository.ErrNotFound
	}

	return metadata, nil
}

func (r *SettingRepository) List() ([]*setting.Setting, error) {
	settingItems := make([]*SettingItem, 0)

	if err := r.db.All(&settingItems); err != nil {
		if err.Error() == storm.ErrNotFound.Error() {
			return []*setting.Setting{}, nil
		}

		return nil, errors.Wrap(err, "could not find settings")
	}

	r.registryMutex.RLock()
	defer r.registryMutex.RUnlock()

	settings := make([]*setting.Setting, 0, len(settingItems))

	for _, s := range settingItems {
		s.metadata = r.registry[s.ID]
		settings = append(settings, s.Setting())
	}

	return settings, nil
}

func (r *SettingRepository) Get(id setting.ID) (*setting.Setting, error) {
	settingItem := &SettingItem{}
	if err := r.db.One("ID", id, settingItem); err != nil {
		if err.Error() == storm.ErrNotFound.Error() {
			metadata, err := r.GetMetadata(id)
			if err != nil {
				return nil, err
			}

			return setting.NewSetting(id, metadata.DefaultValue, metadata), nil
		}

		return nil, errors.Wrapf(err, "could not get setting '%s'", id)
	}

	return settingItem.Setting(), nil
}

func (r *SettingRepository) Has(id setting.ID) (bool, error) {
	settingItem := &SettingItem{}
	if err := r.db.One("ID", id, settingItem); err != nil {
		if err.Error() == storm.ErrNotFound.Error() {
			return false, nil
		}

		return false, errors.Wrapf(err, "could not get setting '%s'", id)
	}

	return true, nil
}

func (r *SettingRepository) Save(id setting.ID, value interface{}) error {
	settingItem := &SettingItem{id, value, nil}
	if err := r.db.Save(settingItem); err != nil {
		return errors.Wrapf(err, "could not save setting '%s'", id)
	}

	return nil
}

func (r *SettingRepository) Delete(id setting.ID) error {
	settingItem := &SettingItem{id, nil, nil}
	if err := r.db.DeleteStruct(settingItem); err != nil {
		if err.Error() == storm.ErrNotFound.Error() {
			return repository.ErrNotFound
		}

		return errors.Wrapf(err, "could not delete setting '%s'", id)
	}

	return nil
}

func NewSettingRepository(db *storm.DB) *SettingRepository {
	return &SettingRepository{
		registry: make(map[setting.ID]*setting.Metadata),
		db:       db,
	}
}
