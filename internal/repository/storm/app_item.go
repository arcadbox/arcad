package storm

import (
	"gitlab.com/arcadbox/arcad/internal/repository"
)

type AppOverride struct {
	Title       *string
	Description *string
	Tags        []string
	Order       *int
}

type AppItem struct {
	ID        repository.AppID `storm:"id"`
	Manifest  *repository.AppManifest
	Override  AppOverride
	Order     int `storm:"index"`
	Published bool
}

func (i *AppItem) App() *repository.App {
	manifest := &repository.AppManifest{
		ID:          i.Manifest.ID,
		Title:       i.Manifest.Title,
		Description: i.Manifest.Description,
		Tags:        i.Manifest.Tags,
		Options:     i.Manifest.Options,
	}

	if i.Override.Title != nil {
		manifest.Title = *i.Override.Title
	}

	if i.Override.Description != nil {
		manifest.Description = *i.Override.Description
	}

	if i.Override.Tags != nil {
		manifest.Tags = i.Override.Tags
	}

	order := i.Order
	if i.Override.Order != nil {
		order = *i.Override.Order
	}

	return &repository.App{
		ID:       i.ID,
		Manifest: manifest,
		Order:    order,
	}
}
