package storm

import (
	"time"

	"github.com/asdine/storm/v3/q"

	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/repository"

	"github.com/asdine/storm/v3"
	"github.com/pborman/uuid"
)

type HighscoreRepository struct {
	db *storm.DB
}

func (r *HighscoreRepository) Init() error {
	if err := r.db.Init(&HighscoreItem{}); err != nil {
		return errors.Wrap(err, "could not initialize highscore collection")
	}

	if err := r.db.ReIndex(&HighscoreItem{}); err != nil {
		return errors.Wrap(err, "could not initialize highscore indexes")
	}

	return nil
}

func (r *HighscoreRepository) Create(appID repository.AppID, userID repository.UserID, score int64) (*repository.Highscore, error) {
	uid := uuid.New()

	highscoreItem := &HighscoreItem{
		ID:           repository.HighscoreID(uid),
		AppID:        appID,
		UserID:       userID,
		Score:        score,
		CreationTime: time.Now().UTC(),
	}

	if err := r.db.Save(highscoreItem); err != nil {
		return nil, errors.Wrap(err, "could not save highscore")
	}

	return highscoreItem.Highscore(), nil
}

func (r *HighscoreRepository) Delete(id repository.HighscoreID) error {
	highscoreItem := &HighscoreItem{
		ID: id,
	}

	if err := r.db.DeleteStruct(highscoreItem); err != nil {
		if err.Error() == storm.ErrNotFound.Error() {
			return repository.ErrNotFound
		}

		return errors.Wrap(err, "could not delete highscore")
	}

	return nil
}

func (r *HighscoreRepository) FindByAppID(appID repository.AppID, userFilters ...repository.UserID) ([]*repository.Highscore, error) {
	highscoreItems := make([]*HighscoreItem, 0)

	matchers := []q.Matcher{
		q.Eq("AppID", appID),
	}
	for _, userID := range userFilters {
		matchers = append(matchers, q.Eq("UserID", userID))
	}

	query := r.db.Select(matchers...).OrderBy("Score", "CreationTime").Reverse()

	if err := query.Find(&highscoreItems); err != nil {
		if err == storm.ErrNotFound {
			return nil, repository.ErrNotFound
		}

		return nil, errors.Wrap(err, "could not find highscores")
	}

	highscores := make([]*repository.Highscore, 0, len(highscoreItems))
	for _, i := range highscoreItems {
		highscores = append(highscores, i.Highscore())
	}

	return highscores, nil
}

func (r *HighscoreRepository) Get(id repository.HighscoreID) (*repository.Highscore, error) {
	highscoreItem := &HighscoreItem{}
	if err := r.db.One("ID", id, highscoreItem); err != nil {
		if err.Error() == storm.ErrNotFound.Error() {
			return nil, repository.ErrNotFound
		}

		return nil, errors.Wrapf(err, "could not get highscore '%s'", id)
	}

	return highscoreItem.Highscore(), nil
}

func (r *HighscoreRepository) FindByUserID(userID repository.UserID) ([]*repository.Highscore, error) {
	highscoreItems := make([]*HighscoreItem, 0)

	if err := r.db.Find("UserID", userID, &highscoreItems); err != nil {
		if err.Error() == storm.ErrNotFound.Error() {
			return nil, repository.ErrNotFound
		}

		return nil, errors.Wrap(err, "could not find highscores")
	}

	highscores := make([]*repository.Highscore, 0, len(highscoreItems))
	for _, i := range highscoreItems {
		highscores = append(highscores, i.Highscore())
	}

	return highscores, nil
}

func (r *HighscoreRepository) List() ([]*repository.Highscore, error) {
	highscoreItems := make([]*HighscoreItem, 0)

	if err := r.db.All(&highscoreItems); err != nil {
		if err.Error() == storm.ErrNotFound.Error() {
			return []*repository.Highscore{}, nil
		}

		return nil, errors.Wrap(err, "could not find highscores")
	}

	highscores := make([]*repository.Highscore, 0, len(highscoreItems))
	for _, h := range highscoreItems {
		highscores = append(highscores, h.Highscore())
	}

	return highscores, nil
}

func NewHighscoreRepository(db *storm.DB) *HighscoreRepository {
	return &HighscoreRepository{db}
}
