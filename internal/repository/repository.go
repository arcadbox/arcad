package repository

import (
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/setting"
)

var (
	ErrNotFound = errors.New("not found")
)

type Repository struct {
	user      UserRepository
	highscore HighscoreRepository
	app       AppRepository
	setting   setting.Repository
}

func (r *Repository) User() UserRepository {
	return r.user
}

func (r *Repository) Highscore() HighscoreRepository {
	return r.highscore
}

func (r *Repository) App() AppRepository {
	return r.app
}

func (r *Repository) Setting() setting.Repository {
	return r.setting
}

func NewRepository(highscore HighscoreRepository, user UserRepository, app AppRepository, setting setting.Repository) *Repository {
	return &Repository{
		user:      user,
		highscore: highscore,
		app:       app,
		setting:   setting,
	}
}
