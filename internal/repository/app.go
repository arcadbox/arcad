package repository

type AppRepository interface {
	// Override overrides app attributes in the repository
	Override(id AppID, title, description *string, tags []string, order *int) error
	// Save saves an app in the repository
	Save(*App) error
	// List lists apps in the repository
	List() ([]*App, error)
	// Get returns an app by its id
	Get(AppID) (*App, error)
	// Delete removes an app from the repository
	Delete(AppID) error
	// Publish the given app
	Publish(AppID) error
	// Unpublish the given app
	Unpublish(AppID) error
	// isPublished checks if the given app is published
	IsPublished(AppID) (bool, error)
}

type AppID string

type AppManifest struct {
	ID          AppID      `yaml:"id"`
	Version     string     `yaml:"version"`
	Title       string     `yaml:"title"`
	Description string     `yaml:"description"`
	Tags        []string   `yaml:"tags"`
	Options     AppOptions `yaml:"options"`
}

type AppOptions struct {
	HighscoresEnabled bool `yaml:"highscoresEnabled"`
}

type App struct {
	ID       AppID
	Manifest *AppManifest
	Order    int
}
