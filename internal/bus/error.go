package bus

import "github.com/pkg/errors"

var (
	ErrPublishTimeout    = errors.New("publish timeout")
	ErrUnexpectedMessage = errors.New("unexpected message")
	ErrNoResponse        = errors.New("no response")
)
