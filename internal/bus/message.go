package bus

import (
	"strings"

	"github.com/pkg/errors"
)

type (
	MessageNamespace string
	MessageType      string
)

type Message interface {
	MessageNamespace() MessageNamespace
	MessageType() MessageType
}

func NewMessageNamespace(namespaces ...MessageNamespace) MessageNamespace {
	var sb strings.Builder

	for i, ns := range namespaces {
		if i != 0 {
			if _, err := sb.WriteString(":"); err != nil {
				panic(errors.Wrap(err, "could not build new message namespace"))
			}
		}

		if _, err := sb.WriteString(string(ns)); err != nil {
			panic(errors.Wrap(err, "could not build new message namespace"))
		}
	}

	return MessageNamespace(sb.String())
}
