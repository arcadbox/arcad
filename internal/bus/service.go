package bus

import (
	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/service"
)

const (
	ServiceName service.Name = "bus"
)

func ServiceProvider(bus Bus) service.Provider {
	return func(ctn *service.Container) (interface{}, error) {
		return bus, nil
	}
}

// From retrieves the bus service in the given container
func From(container *service.Container) (Bus, error) {
	service, err := container.Service(ServiceName)
	if err != nil {
		return nil, errors.Wrapf(err, "error while retrieving '%s' service", ServiceName)
	}

	srv, ok := service.(Bus)
	if !ok {
		return nil, errors.Errorf("retrieved service is not a valid '%s' service", ServiceName)
	}

	return srv, nil
}

// Must retrieves the bus service in the given container or panic otherwise
func Must(container *service.Container) Bus {
	srv, err := From(container)
	if err != nil {
		panic(err)
	}

	return srv
}
