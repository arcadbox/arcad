package memory

import (
	"testing"

	busTesting "gitlab.com/arcadbox/arcad/internal/bus/testing"
)

func TestMemoryBus(t *testing.T) {
	t.Parallel()

	t.Run("PublishSubscribe", func(t *testing.T) {
		t.Parallel()

		b := NewBus()
		busTesting.TestPublishSubscribe(t, b)
	})

	t.Run("RequestReply", func(t *testing.T) {
		t.Parallel()

		b := NewBus()
		busTesting.TestRequestReply(t, b)
	})
}
