package memory

type Option struct {
	BufferSize int64
}

type OptionFunc func(*Option)

func DefaultOption() *Option {
	return &Option{
		BufferSize: 16, // nolint: gomnd
	}
}

func WithBufferSize(size int64) OptionFunc {
	return func(o *Option) {
		o.BufferSize = size
	}
}
