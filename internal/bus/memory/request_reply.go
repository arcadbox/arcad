package memory

import (
	"context"
	"strconv"
	"sync/atomic"

	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/bus"
	"gitlab.com/wpetit/goweb/logger"
)

const (
	MessageNamespaceReqRep bus.MessageNamespace = "requestReply"
	MessageTypeRequest     bus.MessageType      = "request"
	MessageTypeReply       bus.MessageType      = "reply"
)

type RequestMessage struct {
	RequestID uint64

	Message bus.Message

	ns bus.MessageNamespace
}

func (m *RequestMessage) MessageType() bus.MessageType {
	return MessageTypeRequest
}

func (m *RequestMessage) MessageNamespace() bus.MessageNamespace {
	return m.ns
}

type ReplyMessage struct {
	RequestID uint64
	Message   bus.Message
	Error     error

	ns bus.MessageNamespace
}

func (m *ReplyMessage) MessageType() bus.MessageType {
	return MessageTypeReply
}

func (m *ReplyMessage) MessageNamespace() bus.MessageNamespace {
	return m.ns
}

func (b *Bus) Request(ctx context.Context, msg bus.Message) (bus.Message, error) {
	requestID := atomic.AddUint64(&b.nextRequestID, 1)

	ns := createRequestNamespace(msg.MessageNamespace(), msg.MessageType())

	req := &RequestMessage{
		RequestID: requestID,
		Message:   msg,
		ns:        ns,
	}

	replyNamespace := createReplyNamespace(requestID)

	replies, err := b.Subscribe(ctx, replyNamespace, MessageTypeReply)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	defer func() {
		b.Unsubscribe(ctx, replyNamespace, MessageTypeReply, replies)
	}()

	logger.Debug(ctx, "publishing request", logger.F("request", req))

	if err := b.Publish(ctx, req); err != nil {
		return nil, errors.WithStack(err)
	}

	for {
		select {
		case <-ctx.Done():
			return nil, errors.WithStack(ctx.Err())

		case msg, ok := <-replies:
			if !ok {
				return nil, errors.WithStack(bus.ErrNoResponse)
			}

			reply, ok := msg.(*ReplyMessage)
			if !ok {
				return nil, errors.WithStack(bus.ErrUnexpectedMessage)
			}

			if reply.Error != nil {
				return nil, errors.WithStack(err)
			}

			return reply.Message, nil
		}
	}
}

type RequestHandler func(evt bus.Message) (bus.Message, error)

func (b *Bus) Reply(ctx context.Context, msgNamespace bus.MessageNamespace, msgType bus.MessageType, h bus.RequestHandler) error {
	ns := createRequestNamespace(msgNamespace, msgType)

	requests, err := b.Subscribe(ctx, ns, MessageTypeRequest)
	if err != nil {
		return errors.WithStack(err)
	}

	defer func() {
		b.Unsubscribe(ctx, ns, MessageTypeRequest, requests)
	}()

	for {
		select {
		case <-ctx.Done():
			return errors.WithStack(ctx.Err())

		case msg, ok := <-requests:
			if !ok {
				return nil
			}

			request, ok := msg.(*RequestMessage)
			if !ok {
				return errors.WithStack(bus.ErrUnexpectedMessage)
			}

			logger.Debug(ctx, "handling request", logger.F("request", request))

			msg, err := h(request.Message)

			reply := &ReplyMessage{
				RequestID: request.RequestID,
				Message:   nil,
				Error:     nil,

				ns: createReplyNamespace(request.RequestID),
			}

			if err != nil {
				reply.Error = errors.WithStack(err)
			} else {
				reply.Message = msg
			}

			logger.Debug(ctx, "publishing reply", logger.F("reply", reply))

			if err := b.Publish(ctx, reply); err != nil {
				return errors.WithStack(err)
			}
		}
	}
}

func createRequestNamespace(msgNamespace bus.MessageNamespace, msgType bus.MessageType) bus.MessageNamespace {
	return bus.NewMessageNamespace(
		MessageNamespaceReqRep,
		msgNamespace,
		bus.MessageNamespace(msgType),
	)
}

func createReplyNamespace(requestID uint64) bus.MessageNamespace {
	return bus.NewMessageNamespace(
		MessageNamespaceReqRep,
		bus.MessageNamespace(strconv.FormatUint(requestID, 10)),
	)
}
