package memory

import (
	"context"

	cmap "github.com/orcaman/concurrent-map"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/bus"
	"gitlab.com/wpetit/goweb/logger"
)

type Bus struct {
	opt           *Option
	dispatchers   cmap.ConcurrentMap
	nextRequestID uint64
}

func (b *Bus) Subscribe(ctx context.Context, ns bus.MessageNamespace, messageType bus.MessageType) (<-chan bus.Message, error) {
	logger.Debug(
		ctx, "subscribing to messages",
		logger.F("messageNamespace", ns),
		logger.F("messageType", messageType),
	)

	dispatchers := b.getDispatchers(ns, messageType)
	d := newEventDispatcher(b.opt.BufferSize)

	go d.Run()

	dispatchers.Add(d)

	return d.Out(), nil
}

func (b *Bus) Unsubscribe(ctx context.Context, ns bus.MessageNamespace, messageType bus.MessageType, ch <-chan bus.Message) {
	logger.Debug(
		ctx, "unsubscribing from messages",
		logger.F("messageNamespace", ns),
		logger.F("messageType", messageType),
	)

	dispatchers := b.getDispatchers(ns, messageType)
	dispatchers.RemoveByOutChannel(ch)
}

func (b *Bus) Publish(ctx context.Context, msg bus.Message) error {
	dispatchers := b.getDispatchers(msg.MessageNamespace(), msg.MessageType())
	dispatchersList := dispatchers.List()

	logger.Debug(
		ctx, "publishing message",
		logger.F("dispatchers", len(dispatchersList)),
		logger.F("messageNamespace", msg.MessageNamespace()),
		logger.F("messageType", msg.MessageType()),
	)

	for _, d := range dispatchersList {
		if err := d.In(msg); err != nil {
			return errors.WithStack(err)
		}
	}

	return nil
}

func (b *Bus) getDispatchers(namespace bus.MessageNamespace, msgType bus.MessageType) *eventDispatcherSet {
	strNamespace := string(namespace)
	rawEventsMap, exists := b.dispatchers.Get(strNamespace)

	eventsMap, ok := rawEventsMap.(cmap.ConcurrentMap)
	if !exists || !ok {
		eventsMap = cmap.New()
		b.dispatchers.Set(strNamespace, eventsMap)
	}

	msgTypeStr := string(msgType)

	rawDispatchers, exists := eventsMap.Get(msgTypeStr)

	dispatchers, ok := rawDispatchers.(*eventDispatcherSet)
	if !exists || !ok {
		dispatchers = newEventDispatcherSet()
		eventsMap.Set(msgTypeStr, dispatchers)
	}

	return dispatchers
}

func NewBus(funcs ...OptionFunc) *Bus {
	opt := DefaultOption()

	for _, fn := range funcs {
		fn(opt)
	}

	return &Bus{
		opt:         opt,
		dispatchers: cmap.New(),
	}
}

// Check bus implementation.
var _ bus.Bus = NewBus()
