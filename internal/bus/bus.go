package bus

import "context"

type Bus interface {
	Subscribe(ctx context.Context, ns MessageNamespace, msgType MessageType) (<-chan Message, error)
	Unsubscribe(ctx context.Context, ns MessageNamespace, msgType MessageType, ch <-chan Message)
	Publish(ctx context.Context, msg Message) error
	Request(ctx context.Context, msg Message) (Message, error)
	Reply(ctx context.Context, ns MessageNamespace, msgType MessageType, h RequestHandler) error
}

type RequestHandler func(msg Message) (Message, error)
