package market

import (
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/market/client"
	"gitlab.com/arcadbox/arcad/internal/market/installer"
	"gitlab.com/wpetit/goweb/service"
)

const ServiceName service.Name = "market"

type Service struct {
	client    *client.Client
	installer *installer.Installer
}

func (s *Service) Client() *client.Client {
	return s.client
}

func (s *Service) Installer() *installer.Installer {
	return s.installer
}

// From retrieves the market service in the given container.
func From(container *service.Container) (*Service, error) {
	service, err := container.Service(ServiceName)
	if err != nil {
		return nil, errors.Wrapf(err, "error while retrieving '%s' service", ServiceName)
	}

	srv, ok := service.(*Service)
	if !ok {
		return nil, errors.Errorf("retrieved service is not a valid '%s' service", ServiceName)
	}

	return srv, nil
}

// Must retrieves the market service in the given container or panic otherwise.
func Must(container *service.Container) *Service {
	srv, err := From(container)
	if err != nil {
		panic(errors.WithStack(err))
	}

	return srv
}
