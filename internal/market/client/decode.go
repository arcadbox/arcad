package client

import (
	"reflect"
	"time"

	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
)

func toTimeHookFunc() mapstructure.DecodeHookFunc {
	return func(
		f reflect.Type,
		t reflect.Type,
		data interface{}) (interface{}, error) {
		if t != reflect.TypeOf(JSONTime{}) {
			return data, nil
		}

		switch f.Kind() {
		case reflect.String:
			t, err := time.Parse(time.RFC3339, data.(string))
			if err != nil {
				return nil, errors.WithStack(err)
			}

			return JSONTime{t}, nil
		default:
			return data, nil
		}
	}
}

func createDataDecoder(result interface{}) (*mapstructure.Decoder, error) {
	decoder, err := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		Metadata: nil,
		DecodeHook: mapstructure.ComposeDecodeHookFunc(
			toTimeHookFunc()),
		Result: result,
	})
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return decoder, nil
}
