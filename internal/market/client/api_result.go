package client

import (
	"time"

	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/bundle"
)

type SearchAppsResult struct {
	Apps []*AppEntry
}

type GetAppResult struct {
	App *AppEntry
}

type CreateAppResult struct {
	App *AppEntry
}

type ReleaseAppResult struct {
	App *AppEntry
}

type AppEntry struct {
	ID            int
	Name          string
	CreatedAt     JSONTime
	UpdatedAt     JSONTime
	LatestRelease struct {
		ID            int
		CID           string
		CreatedAt     JSONTime
		Version       string
		Description   string
		ArchiveFormat bundle.ArchiveExt
	}
	Owner struct {
		ID        int
		Username  string
		UpdatedAt JSONTime
		CreatedAt JSONTime
	}
}

type JSONTime struct {
	time.Time
}

// returns time.Now() no matter what!
func (t *JSONTime) UnmarshalJSON(b []byte) error {
	time, err := time.Parse(time.RFC3339, string(b))
	if err != nil {
		return errors.WithStack(err)
	}

	*t = JSONTime{time}

	return nil
}
