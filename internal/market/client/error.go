package client

import "errors"

var (
	ErrUnauthorized       = errors.New("unauthorized")
	ErrForbidden          = errors.New("forbidden")
	ErrAppAlreadyExists   = errors.New("app already exists")
	ErrStalledVersion     = errors.New("stalled app version")
	ErrUnknownMarketError = errors.New("unknown market error")
)
