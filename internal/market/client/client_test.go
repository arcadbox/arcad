package client

import (
	"context"
	"os"
	"testing"

	"github.com/davecgh/go-spew/spew"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/app"
	"gitlab.com/arcadbox/arcad/internal/bundle"
	"gitlab.com/wpetit/goweb/logger"
)

func TestClientDownloadApp(t *testing.T) {
	t.Parallel()

	logger.SetLevel(logger.LevelDebug)

	client := New()

	ctx := context.Background()

	bundle, filepath, err := client.DownloadApp(
		ctx,
		"QmPxQHC7aXsyUFTfRFquk428m6G9rK7EHokZNDcKq3LNbh",
		bundle.ExtTarGz,
	)
	if err != nil {
		t.Fatal(errors.WithStack(err))
	}

	if bundle == nil {
		t.Fatal("bundle should not be nil")
	}

	if filepath == "" {
		t.Fatal("filepath should not be nil")
	}

	defer os.Remove(filepath)

	appManifest, err := app.LoadAppManifest(bundle)
	if err != nil {
		t.Fatal(errors.WithStack(err))
	}

	spew.Dump(appManifest)
}
