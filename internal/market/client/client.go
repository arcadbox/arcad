package client

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/hashicorp/go-retryablehttp"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/bundle"
	"gitlab.com/wpetit/goweb/api"
	"gitlab.com/wpetit/goweb/logger"
)

const (
	DefaultTimeout        = 30 * time.Second
	DefaultRetryMax       = 3
	DefaultIPFSGatewayURL = "https://ipfs.io"
	DefaultMarketURL      = "http://localhost:3002"
)

type Option struct {
	IPFSGatewayURL string
	MarketURL      string
	Timeout        time.Duration
	RetryMax       int
}

type OptionFunc func(*Option)

type Client struct {
	ipfsGatewayURL string
	marketURL      string
	client         *retryablehttp.Client
}

func (c *Client) CreateApp(ctx context.Context, username, password, cid string, archiveFormat bundle.ArchiveExt) (*CreateAppResult, error) {
	url := c.getCreateAppURL()

	payload := struct {
		CID           string `json:"cid"`
		ArchiveFormat string `json:"archiveFormat"`
	}{
		CID:           cid,
		ArchiveFormat: string(archiveFormat),
	}

	rawPayload, err := json.Marshal(&payload)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	req, err := retryablehttp.NewRequest("POST", url, rawPayload)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	req.Header.Add("Content-Type", "application/json")
	req = req.WithContext(ctx)
	req.Close = true
	req.SetBasicAuth(username, password)

	res, err := c.client.Do(req)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	defer func() {
		if err := res.Body.Close(); err != nil {
			logger.Error(ctx, "could not close http client response body", logger.E(errors.WithStack(err)))
		}
	}()

	switch res.StatusCode {
	case http.StatusForbidden:
		return nil, errors.WithStack(ErrForbidden)
	case http.StatusUnauthorized:
		return nil, errors.WithStack(ErrUnauthorized)
	}

	decoder := json.NewDecoder(res.Body)
	apiResponse := &api.Response{}

	if err := decoder.Decode(apiResponse); err != nil {
		return nil, errors.WithStack(err)
	}

	if apiResponse.Error != nil {
		switch apiResponse.Error.Code {
		case ErrCodeAppAlreadyExists:
			return nil, errors.WithStack(ErrAppAlreadyExists)
		default:
			return nil, errors.Wrapf(
				ErrUnknownMarketError,
				"market responded with error code '%s'",
				apiResponse.Error.Code,
			)
		}
	}

	createAppResult := &CreateAppResult{}

	dataDecoder, err := createDataDecoder(createAppResult)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	if err := dataDecoder.Decode(apiResponse.Data); err != nil {
		return nil, errors.WithStack(err)
	}

	return createAppResult, nil
}

func (c *Client) ReleaseApp(ctx context.Context, username, password string, appID int, cid string, archiveFormat bundle.ArchiveExt) (*ReleaseAppResult, error) {
	url := c.getReleaseAppURL(appID)

	payload := struct {
		CID           string `json:"cid"`
		ArchiveFormat string `json:"archiveFormat"`
	}{
		CID:           cid,
		ArchiveFormat: string(archiveFormat),
	}

	rawPayload, err := json.Marshal(&payload)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	req, err := retryablehttp.NewRequest("POST", url, rawPayload)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	req.Header.Add("Content-Type", "application/json")
	req = req.WithContext(ctx)
	req.Close = true
	req.SetBasicAuth(username, password)

	res, err := c.client.Do(req)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	defer func() {
		if err := res.Body.Close(); err != nil {
			logger.Error(ctx, "could not close http client response body", logger.E(errors.WithStack(err)))
		}
	}()

	switch res.StatusCode {
	case http.StatusForbidden:
		return nil, errors.WithStack(ErrForbidden)
	case http.StatusUnauthorized:
		return nil, errors.WithStack(ErrUnauthorized)
	}

	decoder := json.NewDecoder(res.Body)
	apiResponse := &api.Response{}

	if err := decoder.Decode(apiResponse); err != nil {
		return nil, errors.WithStack(err)
	}

	if apiResponse.Error != nil {
		switch apiResponse.Error.Code {
		case ErrCodeAppAlreadyExists:
			return nil, errors.WithStack(ErrAppAlreadyExists)
		default:
			return nil, errors.Wrapf(
				ErrUnknownMarketError,
				"market responded with error code '%s'",
				apiResponse.Error.Code,
			)
		}
	}

	releaseAppResult := &ReleaseAppResult{}

	dataDecoder, err := createDataDecoder(releaseAppResult)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	if err := dataDecoder.Decode(apiResponse.Data); err != nil {
		return nil, errors.WithStack(err)
	}

	return releaseAppResult, nil
}

func (c *Client) GetApp(ctx context.Context, appID int) (*GetAppResult, error) {
	url := c.getAppURL(appID)

	logger.Debug(ctx, "get app", logger.F("appID", appID))

	req, err := retryablehttp.NewRequest("GET", url, nil)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	req = req.WithContext(ctx)
	req.Close = true

	res, err := c.client.Do(req)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	defer func() {
		if err := res.Body.Close(); err != nil {
			logger.Error(ctx, "could not close http client response body", logger.E(errors.WithStack(err)))
		}
	}()

	decoder := json.NewDecoder(res.Body)
	apiResponse := &api.Response{}

	if err := decoder.Decode(apiResponse); err != nil {
		return nil, errors.WithStack(err)
	}

	getAppResult := &GetAppResult{}

	dataDecoder, err := createDataDecoder(getAppResult)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	if err := dataDecoder.Decode(apiResponse.Data); err != nil {
		return nil, errors.WithStack(err)
	}

	return getAppResult, nil
}

func (c *Client) SearchApps(ctx context.Context, search string) (*SearchAppsResult, error) {
	url := c.getSearchURL(search)

	logger.Debug(ctx, "search apps", logger.F("url", url))

	req, err := retryablehttp.NewRequest("GET", url, nil)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	req = req.WithContext(ctx)
	req.Close = true

	res, err := c.client.Do(req)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	defer func() {
		if err := res.Body.Close(); err != nil {
			logger.Error(ctx, "could not close http client response body", logger.E(errors.WithStack(err)))
		}
	}()

	decoder := json.NewDecoder(res.Body)
	apiResponse := &api.Response{}

	if err := decoder.Decode(apiResponse); err != nil {
		return nil, errors.WithStack(err)
	}

	searchAppsResult := &SearchAppsResult{}

	dataDecoder, err := createDataDecoder(searchAppsResult)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	if err := dataDecoder.Decode(apiResponse.Data); err != nil {
		return nil, errors.WithStack(err)
	}

	return searchAppsResult, nil
}

func (c *Client) DownloadApp(ctx context.Context, cid string, archiveFormat bundle.ArchiveExt) (bundle.Bundle, string, error) {
	url := c.getArchiveURL(cid, archiveFormat)

	logger.Debug(ctx, "downloading app archive", logger.F("url", url))

	req, err := retryablehttp.NewRequest("GET", url, nil)
	if err != nil {
		return nil, "", errors.WithStack(err)
	}

	req = req.WithContext(ctx)
	req.Close = true

	res, err := c.client.Do(req)
	if err != nil {
		return nil, "", errors.WithStack(err)
	}

	defer func() {
		if err := res.Body.Close(); err != nil {
			logger.Error(ctx, "could not close http client response body", logger.E(errors.WithStack(err)))
		}
	}()

	file, err := ioutil.TempFile("", fmt.Sprintf("app.*.%s", archiveFormat))
	if err != nil {
		return nil, "", errors.WithStack(err)
	}

	if _, err := io.Copy(file, res.Body); err != nil {
		return nil, "", errors.WithStack(err)
	}

	filepath := file.Name()

	logger.Debug(ctx, "saved app archive on disk", logger.F("filepath", filepath))

	bundle, err := bundle.FromPath(filepath)
	if err != nil {
		return nil, "", errors.WithStack(err)
	}

	return bundle, filepath, nil
}

func (c *Client) getReleaseAppURL(appID int) string {
	return fmt.Sprintf("%s/api/v1/apps/%d/releases", c.marketURL, appID)
}

func (c *Client) getCreateAppURL() string {
	return fmt.Sprintf("%s/api/v1/apps", c.marketURL)
}

func (c *Client) getAppURL(appID int) string {
	return fmt.Sprintf("%s/api/v1/apps/%d", c.marketURL, appID)
}

func (c *Client) getSearchURL(search string) string {
	return fmt.Sprintf("%s/api/v1/apps?search=%s", c.marketURL, search)
}

func (c *Client) getArchiveURL(cid string, archiveFormat bundle.ArchiveExt) string {
	return fmt.Sprintf("%s/ipfs/%s?format=app.%s", c.ipfsGatewayURL, cid, archiveFormat)
}

func New(funcs ...OptionFunc) *Client {
	option := &Option{
		IPFSGatewayURL: DefaultIPFSGatewayURL,
		MarketURL:      DefaultMarketURL,
		RetryMax:       DefaultRetryMax,
		Timeout:        DefaultTimeout,
	}
	for _, fn := range funcs {
		fn(option)
	}

	client := retryablehttp.NewClient()
	client.HTTPClient.Timeout = option.Timeout
	client.RetryMax = option.RetryMax

	return &Client{
		ipfsGatewayURL: option.IPFSGatewayURL,
		marketURL:      option.MarketURL,
		client:         client,
	}
}

func WithTimeout(timeout time.Duration) OptionFunc {
	return func(option *Option) {
		option.Timeout = timeout
	}
}

func WithRetryMax(retryMax int) OptionFunc {
	return func(option *Option) {
		option.RetryMax = retryMax
	}
}

func WithIPFSGatewayURL(ipfsGatewayURL string) OptionFunc {
	return func(option *Option) {
		option.IPFSGatewayURL = ipfsGatewayURL
	}
}

func WithMarketURL(marketURL string) OptionFunc {
	return func(option *Option) {
		option.MarketURL = marketURL
	}
}
