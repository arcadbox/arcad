package market

import (
	"time"

	"gitlab.com/arcadbox/arcad/internal/market/client"
	"gitlab.com/arcadbox/arcad/internal/market/installer"
	"gitlab.com/wpetit/goweb/service"
)

type Option struct {
	IPFSGatewayURL string
	MarketURL      string
	RetryMax       int
	Timeout        time.Duration
	AppsDir        string
}

type OptionFunc func(*Option)

func ServiceProvider(funcs ...OptionFunc) service.Provider {
	option := &Option{
		IPFSGatewayURL: client.DefaultIPFSGatewayURL,
		MarketURL:      client.DefaultMarketURL,
		RetryMax:       client.DefaultRetryMax,
		Timeout:        client.DefaultTimeout,
	}
	for _, fn := range funcs {
		fn(option)
	}

	client := client.New(
		client.WithIPFSGatewayURL(option.IPFSGatewayURL),
		client.WithMarketURL(option.MarketURL),
		client.WithRetryMax(option.RetryMax),
		client.WithTimeout(option.Timeout),
	)

	installer := installer.New(client, option.AppsDir)

	srv := &Service{
		client:    client,
		installer: installer,
	}

	return func(ctn *service.Container) (interface{}, error) {
		return srv, nil
	}
}

func WithTimeout(timeout time.Duration) OptionFunc {
	return func(option *Option) {
		option.Timeout = timeout
	}
}

func WithRetryMax(retryMax int) OptionFunc {
	return func(option *Option) {
		option.RetryMax = retryMax
	}
}

func WithIPFSGatewayURL(ipfsGatewayURL string) OptionFunc {
	return func(option *Option) {
		option.IPFSGatewayURL = ipfsGatewayURL
	}
}

func WithMarketURL(marketURL string) OptionFunc {
	return func(option *Option) {
		option.MarketURL = marketURL
	}
}

func WithAppsDir(appsDir string) OptionFunc {
	return func(option *Option) {
		option.AppsDir = appsDir
	}
}
