package installer

import (
	"context"
	"fmt"
	"io"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/market/client"
	"gitlab.com/wpetit/goweb/logger"
)

func getAppOwnerDirectory(marketAppsDir string, ownerUsername string) (string, error) {
	marketAppsDirAbs, err := filepath.Abs(marketAppsDir)
	if err != nil {
		return "", errors.WithStack(err)
	}

	dirPath := path.Join(marketAppsDir, ownerUsername)

	dirPathAbs, err := filepath.Abs(dirPath)
	if err != nil {
		return "", errors.WithStack(err)
	}

	if !strings.HasPrefix(dirPathAbs, marketAppsDirAbs) {
		return "", errors.Errorf("directory '%s' is not a child of '%s'", dirPathAbs, marketAppsDirAbs)
	}

	return dirPathAbs, nil
}

func getAppPath(marketAppsDir string, app *client.AppEntry) (string, error) {
	appOwnerDir, err := getAppOwnerDirectory(marketAppsDir, app.Owner.Username)
	if err != nil {
		return "", errors.WithStack(err)
	}

	appPath := path.Join(appOwnerDir, fmt.Sprintf("%s.%s", app.Name, app.LatestRelease.ArchiveFormat))

	marketAppsDirAbs, err := filepath.Abs(marketAppsDir)
	if err != nil {
		return "", errors.WithStack(err)
	}

	appPathAbs, err := filepath.Abs(appPath)
	if err != nil {
		return "", errors.WithStack(err)
	}

	if !strings.HasPrefix(appPathAbs, marketAppsDirAbs) {
		return "", errors.Errorf("file '%s' is not a child of '%s'", appPathAbs, marketAppsDirAbs)
	}

	return appPathAbs, nil
}

func moveFile(sourcePath, destPath string) error {
	inputFile, err := os.Open(sourcePath)
	if err != nil {
		return errors.Wrapf(err, "couldn't open source file: %s", sourcePath)
	}

	defer func() {
		if err := inputFile.Close(); err != nil {
			logger.Error(context.Background(), "could not close file", logger.E(errors.WithStack(err)))
		}
	}()

	outputFile, err := os.Create(destPath)
	if err != nil {
		return errors.Wrapf(err, "couldn't open dest file: %s", destPath)
	}

	defer func() {
		if err := outputFile.Close(); err != nil {
			logger.Error(context.Background(), "could not close file", logger.E(errors.WithStack(err)))
		}
	}()

	_, err = io.Copy(outputFile, inputFile)

	if err != nil {
		return errors.Wrapf(err, "writing to output file failed: %s", destPath)
	}

	// The copy was successful, so now delete the original file
	err = os.Remove(sourcePath)
	if err != nil {
		if os.IsNotExist(err) {
			return nil
		}

		return errors.Wrapf(err, "failed removing original file: %s", sourcePath)
	}

	return nil
}
