package installer

import (
	"context"
	"os"

	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/market/client"
	"gitlab.com/wpetit/goweb/logger"
)

type Installer struct {
	client  *client.Client
	appsDir string
}

func (i *Installer) UninstallApp(ctx context.Context, appID int) error {
	logger.Info(ctx, "retrieving app informations", logger.F("appID", appID))

	result, err := i.client.GetApp(ctx, appID)
	if err != nil {
		return errors.Wrap(err, "could not execute search")
	}

	if result.App == nil {
		return errors.Errorf("could not find app '%d'", appID)
	}

	loggerCtx := logger.With(
		ctx,
		logger.F("appID", appID),
		logger.F("owner", result.App.Owner.Username),
	)

	logger.Info(loggerCtx, "uninstalling app")

	appPath, err := getAppPath(i.appsDir, result.App)
	if err != nil {
		return errors.WithStack(err)
	}

	if err := os.Remove(appPath); err != nil {
		if os.IsNotExist(err) {
			logger.Info(loggerCtx, "app is not installed")

			return nil
		}

		return errors.Wrapf(err, "could not remove file '%s'", appPath)
	}

	logger.Info(loggerCtx, "app uninstalled", logger.F("path", appPath))

	return nil
}

func (i *Installer) InstallAppLatestVersion(ctx context.Context, appID int) (string, error) {
	result, err := i.client.GetApp(ctx, appID)
	if err != nil {
		return "", errors.Wrap(err, "could not fetch app informations")
	}

	_, tmpPath, err := i.client.DownloadApp(ctx, result.App.LatestRelease.CID, result.App.LatestRelease.ArchiveFormat)
	if err != nil {
		return "", errors.Wrap(err, "could not download app")
	}

	// Cleanup tmp file on error
	defer func() {
		if err := os.Remove(tmpPath); err != nil {
			if os.IsNotExist(err) {
				return
			}

			panic(errors.WithStack(err))
		}
	}()

	appOwnerDir, err := getAppOwnerDirectory(i.appsDir, result.App.Owner.Username)
	if err != nil {
		return "", errors.WithStack(err)
	}

	if err := os.MkdirAll(appOwnerDir, 0755); err != nil {
		return "", errors.Wrapf(err, "could not create directory '%s'", appOwnerDir)
	}

	appPath, err := getAppPath(i.appsDir, result.App)
	if err != nil {
		return "", errors.WithStack(err)
	}

	if err := moveFile(tmpPath, appPath); err != nil {
		return "", errors.Wrap(err, "could not install app")
	}

	logger.Info(
		ctx,
		"app installed",
		logger.F("path", appPath),
	)

	return appPath, nil
}

func New(client *client.Client, appsDir string) *Installer {
	return &Installer{
		client:  client,
		appsDir: appsDir,
	}
}
