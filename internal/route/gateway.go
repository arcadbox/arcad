package route

import (
	"net/http"
	"net/http/httputil"
	"regexp"

	"forge.cadoles.com/wpetit/go-tunnel"

	"gitlab.com/arcadbox/arcad/internal/config"
	"gitlab.com/arcadbox/arcad/internal/vfs"

	"github.com/pkg/errors"
	gateway "gitlab.com/arcadbox/arcad/internal/tunnel/server"
	"gitlab.com/wpetit/goweb/middleware/container"
	"gitlab.com/wpetit/goweb/service"
	"gitlab.com/wpetit/goweb/service/template"
)

func createGatewayHandlerFunc(conf *config.Config, notFoundHandler http.Handler) (http.Handler, error) {
	clients := make(map[string]config.GatewayRemoteClientConfig)
	for _, c := range conf.GatewayServer.Clients {
		clients[c.Match] = c
	}

	matchRegExp, err := regexp.Compile(conf.GatewayServer.MatchRegExp)
	if err != nil {
		return nil, errors.Wrap(err, "could not compile gateway server match regexp")
	}

	matchIndex := -1
	for i, submatch := range matchRegExp.SubexpNames() {
		if submatch == "Match" {
			matchIndex = i

			break
		}
	}

	if matchIndex == -1 {
		return nil, errors.Wrap(err, "could not find 'Match' capturing group in gateway server regexp")
	}

	publicDirs := conf.Theme.ThemedDirectories("public", conf.HTTP.PublicDir)
	publicFs := vfs.NewLayeredFilesystem(publicDirs...)
	httpDir := vfs.HTTPDir(publicFs, "", notFoundHandler)

	return tunnel.ProxyHandler(
		conf.GatewayServer.ProxyTargetURL,
		func(w http.ResponseWriter, r *http.Request) (*tunnel.RemoteClient, error) {
			ctx := r.Context()

			ctn, err := container.From(ctx)
			if err != nil {
				return nil, errors.WithStack(err)
			}

			matches := matchRegExp.FindStringSubmatch(r.Host)
			if len(matches) < matchIndex {
				renderBadGatewayPage(httpDir, ctn, w, r)

				return nil, tunnel.ErrAbortProxy
			}

			clientMatch := matches[matchIndex]

			clientConfig, exists := clients[clientMatch]
			if !exists {
				renderBadGatewayPage(httpDir, ctn, w, r)

				return nil, tunnel.ErrAbortProxy
			}

			gateway := gateway.Must(ctn)

			remoteClient := gateway.Clients().Get(clientConfig.ID)
			if remoteClient == nil {
				renderBadGatewayPage(httpDir, ctn, w, r)

				return nil, tunnel.ErrAbortProxy
			}

			return remoteClient, nil
		},
		tunnel.WithProxyConfigure(func(reverse *httputil.ReverseProxy) error {
			if !conf.GatewayServer.Cache.Enabled {
				return nil
			}

			reverse.Transport = gateway.NewETagCachingTransport(
				reverse.Transport,
				conf.GatewayServer.Cache.MaxSize,
				conf.GatewayServer.Cache.TTL,
				conf.GatewayServer.Cache.RefreshTimeout,
			)

			return nil
		}),
	)
}

func renderBadGatewayPage(httpDir http.Handler, ctn *service.Container, w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" && r.URL.Path != "" {
		httpDir.ServeHTTP(w, r)

		return
	}

	tmpl := template.Must(ctn)
	data := extendTemplateData(w, r, template.Data{})

	w.WriteHeader(http.StatusBadGateway)

	if err := tmpl.RenderPage(w, "bad_gateway.html.tmpl", data); err != nil {
		panic(errors.Wrapf(err, "could not render '%s' page", r.URL.Path))
	}
}
