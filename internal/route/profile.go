package route

import (
	"net/http"

	"github.com/asdine/storm"

	"gitlab.com/arcadbox/arcad/internal/middleware"
	"gitlab.com/arcadbox/arcad/internal/repository"

	"github.com/gorilla/csrf"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/config"
	"gitlab.com/wpetit/goweb/middleware/container"
	"gitlab.com/wpetit/goweb/service/session"
	"gitlab.com/wpetit/goweb/service/template"
)

func serveProfilePage(w http.ResponseWriter, r *http.Request) {
	ctn := container.Must(r.Context())

	user, err := middleware.FindOrCreateAuthenticatedUser(w, r)
	if err != nil {
		panic(errors.Wrap(err, "could not find or create authenticated user"))
	}

	config := config.Must(ctn)

	data := extendTemplateData(w, r, template.Data{
		csrf.TemplateTag: csrf.TemplateField(r),
		"Contact":        config.Contact,
		"User":           user,
	})

	tmpl := template.Must(ctn)

	if err := tmpl.RenderPage(w, "profile.html.tmpl", data); err != nil {
		panic(errors.Wrap(err, "could not render page 'profile.html.tmpl'"))
	}
}

func handleProfileChange(w http.ResponseWriter, r *http.Request) {
	ctn := container.Must(r.Context())
	tmpl := template.Must(ctn)

	sess, err := session.Must(ctn).Get(w, r)
	if err != nil {
		panic(errors.Wrap(err, "could not retrieve session"))
	}

	renderError := func(message string) {
		sess.AddFlash(session.FlashError, message)
		data := extendTemplateData(w, r, template.Data{
			csrf.TemplateTag: csrf.TemplateField(r),
			"Form":           r.Form,
		})
		if err := tmpl.RenderPage(w, "profile.html.tmpl", data); err != nil {
			panic(errors.Wrap(err, "could not render page 'profile.html.tmpl'"))
		}
	}

	if err := r.ParseForm(); err != nil {
		panic(errors.Wrap(err, "could not parse form"))
	}

	newNickname := r.Form.Get("nickname")

	if newNickname == "" {
		renderError("Votre pseudonyme ne peut pas être vide.")
		return
	}

	repo := repository.Must(ctn)

	user, err := middleware.FindOrCreateAuthenticatedUser(w, r)
	if err != nil {
		panic(errors.Wrap(err, "could not retrieve or create authenticated user"))
	}

	user.Nickname = newNickname

	if err := repo.User().Save(user); err != nil {
		cause := errors.Cause(err)
		if cause.Error() == storm.ErrAlreadyExists.Error() {
			renderError("Ce pseudonyme est déjà utilisé.")
			return
		}

		panic(errors.Wrap(err, "could not save user"))
	}

	sess.AddFlash(session.FlashSuccess, "Votre pseudonyme a été modifié.")

	if err := sess.Save(w, r); err != nil {
		panic(errors.Wrap(err, "could not save session"))
	}

	http.Redirect(w, r, "/profile", http.StatusSeeOther)
}
