package route

import (
	"net/http"

	"gitlab.com/wpetit/goweb/service/session"

	"github.com/gorilla/csrf"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/repository"
	"gitlab.com/arcadbox/arcad/internal/setting"
	"gitlab.com/wpetit/goweb/middleware/container"
	"gitlab.com/wpetit/goweb/service/template"
)

func serveAdminRegisterPage(w http.ResponseWriter, r *http.Request) {
	ctn := container.Must(r.Context())
	tmpl := template.Must(ctn)
	settingRepo := repository.Must(ctn).Setting()

	hasAdminPassword, err := settingRepo.Has(setting.AdminPassword)
	if err != nil {
		panic(errors.Wrap(err, "could not check admin status"))
	}

	if hasAdminPassword {
		http.Redirect(w, r, "/admin/login", http.StatusSeeOther)

		return
	}

	data := extendTemplateData(w, r, template.Data{
		csrf.TemplateTag: csrf.TemplateField(r),
	})

	if err := tmpl.RenderPage(w, "admin_register.html.tmpl", data); err != nil {
		panic(errors.Wrapf(err, "could not render '%s' page", r.URL.Path))
	}
}

func handleAdminRegister(w http.ResponseWriter, r *http.Request) {
	ctn := container.Must(r.Context())
	tmpl := template.Must(ctn)
	settingRepo := repository.Must(ctn).Setting()

	hasAdminPassword, err := settingRepo.Has(setting.AdminPassword)
	if err != nil {
		panic(errors.Wrap(err, "could not check admin status"))
	}

	if hasAdminPassword {
		http.Redirect(w, r, "/admin/login", http.StatusSeeOther)

		return
	}

	sess, err := session.Must(ctn).Get(w, r)
	if err != nil {
		panic(errors.Wrap(err, "could not retrieve session"))
	}

	if err := r.ParseForm(); err != nil {
		panic(errors.Wrap(err, "could not parse form"))
	}

	meta, err := settingRepo.GetMetadata(setting.AdminPassword)
	if err != nil {
		panic(errors.Wrap(err, "could not retrieve admin password setting metadata"))
	}

	err = validateAndSaveSetting(settingRepo, setting.AdminPassword, r.Form, meta)
	if err != nil {
		cause := errors.Cause(err)
		if validationMessage, ok := cause.(setting.ValidationMessage); ok {
			sess.AddFlash(session.FlashError, validationMessage.ValidationMessage())

			data := extendTemplateData(w, r, template.Data{
				csrf.TemplateTag: csrf.TemplateField(r),
			})

			if err := tmpl.RenderPage(w, "admin_register.html.tmpl", data); err != nil {
				panic(errors.Wrapf(err, "could not render page '%s'", r.URL.Path))
			}

			return
		}

		panic(errors.Wrapf(err, "could not validate and save setting '%s'", setting.AdminPassword))
	}

	http.Redirect(w, r, "/admin/login", http.StatusSeeOther)
}
