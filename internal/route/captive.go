package route

import (
	"net/http"

	"forge.cadoles.com/wpetit/go-captiveportal"
	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/middleware/container"
	"gitlab.com/wpetit/goweb/service/template"
)

func serveCaptivePage(w http.ResponseWriter, r *http.Request) {
	ctn := container.Must(r.Context())
	tmpl := template.Must(ctn)
	capPort := captiveportal.Must(ctn)

	escape := r.URL.Query().Get("escape") == "1"

	clientOS, err := capPort.ClientOS(r)
	if err != nil {
		panic(errors.Wrap(err, "could not retrieve captive portal client os"))
	}

	if escape {
		if err := capPort.Release(r); err != nil {
			panic(errors.Wrap(err, "could not release captive portal client"))
		}

		if clientOS == captiveportal.OSApple {
			if err := capPort.Lie(r); err != nil {
				panic(errors.Wrap(err, "could not lie ti captive portal client"))
			}
		}

		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)

		return
	}

	data := extendTemplateData(w, r, template.Data{
		"IsAndroid": clientOS == captiveportal.OSAndroid,
		"IsApple":   clientOS == captiveportal.OSApple,
	})

	if err := tmpl.RenderPage(w, "captive_portal.html.tmpl", data); err != nil {
		panic(errors.Wrapf(err, "could not render '%s' page", r.URL.Path))
	}
}
