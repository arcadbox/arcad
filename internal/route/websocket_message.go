package route

import (
	"encoding/json"

	"gitlab.com/arcadbox/arcad/internal/repository"
)

const (
	WebsocketMessageTypeInit  = "init"
	WebsocketMessageTypeEvent = "event"
)

type WebsocketMessage struct {
	Type    string          `json:"t"`
	Payload json.RawMessage `json:"p"`
}

type WebsocketMessageInitPayload struct {
	AppID repository.AppID `json:"id"`
}

type WebsocketMessageEventPayload struct {
	Data map[string]interface{} `json:"d"`
}

func NewWebsocketMessage(dataType string, payload json.RawMessage) *WebsocketMessage {
	return &WebsocketMessage{
		Type:    dataType,
		Payload: payload,
	}
}
