package route

import (
	"fmt"
	"image/png"
	"net/http"

	"gitlab.com/arcadbox/arcad/internal/middleware"
	"gitlab.com/arcadbox/arcad/internal/repository"

	"github.com/gorilla/csrf"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/member"
	"gitlab.com/wpetit/goweb/logger"
	"gitlab.com/wpetit/goweb/middleware/container"
	"gitlab.com/wpetit/goweb/service/session"
	"gitlab.com/wpetit/goweb/service/template"
)

func handleLogout(w http.ResponseWriter, r *http.Request) {
	ctn := container.Must(r.Context())

	sess, err := session.Must(ctn).Get(w, r)
	if err != nil {
		panic(errors.Wrap(err, "retrieve session"))
	}

	if err := sess.Delete(w, r); err != nil {
		panic(errors.Wrap(err, "delete session"))
	}

	http.Redirect(w, r, "/profile", http.StatusTemporaryRedirect)
}

func handleMemberLoginForm(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		panic(errors.Wrap(err, "could not parse form"))
	}

	file, _, err := r.FormFile("memberCard")
	if err != nil {
		panic(errors.Wrap(err, "could not retrieve member card"))
	}
	defer file.Close()

	ctn := container.Must(r.Context())
	tmplSrv := template.Must(ctn)

	sess, err := session.Must(ctn).Get(w, r)
	if err != nil {
		panic(errors.Wrap(err, "could not retrieve session"))
	}

	renderInvalidMemberCard := func() {
		sess.AddFlash(session.FlashError, "Carte de membre non reconnue.")
		data := extendTemplateData(w, r, template.Data{
			csrf.TemplateTag: csrf.TemplateField(r),
		})

		if err := tmplSrv.RenderPage(w, "profile.html.tmpl", data); err != nil {
			panic(errors.Wrap(err, "could not render page 'profile.html.tmpl'"))
		}
	}

	img, err := png.Decode(file)
	if err != nil {
		logger.Error(
			r.Context(),
			"unexpected error",
			logger.E(err),
		)
		renderInvalidMemberCard()

		return
	}

	member := member.Must(ctn)

	userID, err := member.ValidateCard(img)
	if err != nil {
		logger.Error(
			r.Context(),
			"unexpected error",
			logger.E(err),
		)
		renderInvalidMemberCard()

		return
	}

	repo := repository.Must(ctn)

	u, err := repo.User().Get(userID)
	if err != nil {
		if err == repository.ErrNotFound {
			renderInvalidMemberCard()

			return
		}

		panic(errors.Wrapf(err, "could not retrieve user '%s'", userID))
	}

	sess.Set(middleware.SessionKeyUserID, string(userID))
	sess.AddFlash(session.FlashSuccess, fmt.Sprintf("Bienvenue %s !", u.Nickname))

	if err := sess.Save(w, r); err != nil {
		panic(errors.Wrap(err, "could not save session"))
	}

	logger.Debug(
		r.Context(),
		"successful authentication",
		logger.F("userID", userID),
	)

	http.Redirect(w, r, "/profile", http.StatusSeeOther)
}
