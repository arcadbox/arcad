package route

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/repository"
	"gitlab.com/wpetit/goweb/middleware/container"
	"gitlab.com/wpetit/goweb/service/template"
)

func serveAppHighscores(w http.ResponseWriter, r *http.Request) {
	ctn := container.Must(r.Context())
	tmpl := template.Must(ctn)
	repo := repository.Must(ctn)

	appID := repository.AppID(chi.URLParam(r, "appID"))

	app, err := repo.App().Get(appID)
	if err != nil {
		if err == repository.ErrNotFound {
			http.NotFound(w, r)
			return
		}

		panic(err)
	}

	if !app.Manifest.Options.HighscoresEnabled {
		http.NotFound(w, r)
		return
	}

	highscores, err := repo.Highscore().FindByAppID(appID)
	if err != nil {
		if err == repository.ErrNotFound {
			highscores = make([]*repository.Highscore, 0)
		} else {
			panic(err)
		}
	}

	users := make(map[repository.UserID]*repository.User)
	userIDs := make([]repository.UserID, 0)

	for _, h := range highscores {
		if _, exists := users[h.UserID]; exists {
			continue
		}

		users[h.UserID] = nil

		userIDs = append(userIDs, h.UserID)
	}

	foundUsers, err := repo.User().ListByID(userIDs...)
	if err != nil {
		panic(errors.Wrap(err, "could not find highscore users"))
	}

	for _, u := range foundUsers {
		users[u.ID] = u
	}

	data := extendTemplateData(w, r, template.Data{
		"App":        app,
		"Highscores": highscores,
		"Users":      users,
	})

	if err := tmpl.RenderPage(w, "highscore.html.tmpl", data); err != nil {
		panic(errors.Wrap(err, "render page"))
	}
}
