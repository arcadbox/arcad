package route

import (
	"context"
	"time"

	"forge.cadoles.com/wpetit/go-captiveportal"
	"forge.cadoles.com/wpetit/go-captiveportal/rfc8908"
	"github.com/go-chi/chi"
	"github.com/gorilla/csrf"
	"github.com/igm/sockjs-go/v3/sockjs"
	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/arcadbox/arcad/internal/config"
	"gitlab.com/arcadbox/arcad/internal/hook"
	"gitlab.com/arcadbox/arcad/internal/middleware"
	"gitlab.com/arcadbox/arcad/internal/vfs"
	"gitlab.com/wpetit/goweb/logger"
	"gitlab.com/wpetit/goweb/middleware/container"
	"gitlab.com/wpetit/goweb/service"
)

func Mount(r *chi.Mux, config *config.Config, ctn *service.Container) {
	csrfSecret, err := generateRandomBytes(32)
	if err != nil {
		panic(errors.Wrap(err, "error while generating CSRF secret"))
	}

	csrfMiddleware := csrf.Protect(
		csrfSecret,
		csrf.Secure(false),
	)

	publishedAppOnly := middleware.PublishedAppOnly("appID")

	notFoundHandler := r.NotFoundHandler()

	r.Group(func(r chi.Router) {
		var captivePortal *captiveportal.Service

		if config.CaptivePortal.Enabled {
			captivePortal = captiveportal.Must(ctn)

			r.Use(captivePortal.Middleware())
		}

		r.Use(middleware.Authenticate)

		// CSRF protected routes
		r.Group(func(r chi.Router) {
			r.Use(csrfMiddleware)

			r.Post("/login", handleMemberLoginForm)

			r.With(middleware.ETag).Get("/profile", serveProfilePage)
			r.Post("/profile", handleProfileChange)

			r.With(middleware.ETag).Get("/admin/register", serveAdminRegisterPage)
			r.Post("/admin/register", handleAdminRegister)

			r.With(middleware.ETag).Get("/admin/login", serveAdminLoginPage)
			r.Post("/admin/login", handleAdminLogin)
		})

		r.With(middleware.ETag).Get("/", serveHomePage)
		r.With(middleware.ETag).Get("/captive", serveCaptivePage)
		r.With(middleware.ETag).Get("/captive/api", rfc8908.Handler(
			captivePortal.IsCaptive,
			rfc8908.WithUserPortalURL(config.CaptivePortal.RedirectURL),
			rfc8908.WithVenueInfoURL(config.CaptivePortal.RedirectURL),
		))

		r.With(middleware.ETag).Get("/logout", handleLogout)
		r.With(middleware.ETag, publishedAppOnly).Get("/apps/{appID}", serveAppWrapper)

		r.With(publishedAppOnly).Post("/apps/{appID}/upload", handleAppUpload)
		r.With(middleware.ETag, publishedAppOnly).Get("/apps/{appID}/download/{fileID}", handleAppDownload)

		r.With(middleware.ETag, publishedAppOnly).Get("/apps/{appID}/*", serveApp)
		r.With(middleware.ETag, publishedAppOnly).Get("/highscores/{appID}", serveAppHighscores)

		r.With(middleware.ETag).Get("/profile/card", serveMemberCard)

		sockjsOptions := func() sockjs.Options {
			return sockjs.DefaultOptions
		}()

		sockjsOptions.DisconnectDelay = 10 * time.Second

		handler := sockjs.NewHandler("/sock", sockjsOptions, handleSockSession)

		r.With(withUser, withIsAdmin).Handle("/sock/*", handler)

		r.With(middleware.ETag).Get("/admin/logout", handleAdminLogout)

		r.With(middleware.ETag).Get("/metrics", promhttp.Handler().ServeHTTP)

		r.Group(func(r chi.Router) {
			r.Use(middleware.Authorize)

			r.Route("/admin", func(r chi.Router) {
				r.With(middleware.ETag).Get("/", serveAdminHomePage)

				// CSRF protected routes
				r.Group(func(r chi.Router) {
					r.Use(csrfMiddleware)

					r.With(middleware.ETag).Get("/settings", serveAdminSettingsPage)
					r.Post("/settings", handleAdminSettingsForm)

					r.With(middleware.ETag).Get("/users/{userID}", serveAdminUserInfoPage)
					r.Post("/users/{userID}", handleAdminUserUpdate)

					r.With(middleware.ETag).Get("/apps/{appID}", serveAdminAppEditPage)
					r.Post("/apps/{appID}", handleAdminAppUpdate)

					r.With(middleware.ETag).Get("/users/{userID}/delete", serveAdminUserDeletePage)
					r.Post("/users/{userID}/delete", handleAdminUserDelete)
				})

				r.With(middleware.ETag).Get("/users", serveAdminUsersListPage)
				r.With(middleware.ETag).Get("/users/{userID}/card", serveAdminUserMemberCard)
				r.With(middleware.ETag).Get("/apps", serveAdminAppsListPage)

				r.With(middleware.ETag).Get("/market", serveAdminMarketSearchPage)
				r.With(middleware.ETag).Get("/market/{appID}/install", serveAdminMarketAppInstallPage)
				r.Post("/market/{appID}/install", handleAdminMarketAppInstallRequest)

				r.Route("/api/v1", func(r chi.Router) {
					r.Post("/clock-sync", handleClockSync)
				})
			})
		})

		// Mount plugins routes
		r.Group(func(r chi.Router) {
			ctx := container.WithContainer(context.Background(), ctn)

			if err := hook.InvokeMountRoutesHook(ctx, r); err != nil {
				logger.Fatal(ctx, "could not mount plugins routes", logger.E(errors.WithStack(err)))
			}
		})

		publicDirs := config.Theme.ThemedDirectories("public", config.HTTP.PublicDir)
		publicFs := vfs.NewLayeredFilesystem(publicDirs...)

		r.With(middleware.ETag).Get("/*", vfs.HTTPDir(publicFs, "", notFoundHandler))
	})
}

func MountGateway(r *chi.Mux, conf *config.Config, ctn *service.Container) {
	serveGateway, err := createGatewayHandlerFunc(conf, r.NotFoundHandler())
	if err != nil {
		panic(errors.Wrap(err, "could not create gateway handler func"))
	}

	r.Handle("/*", serveGateway)
}
