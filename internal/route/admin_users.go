package route

import (
	"fmt"
	"net/http"
	"sort"

	"github.com/asdine/storm"
	"github.com/go-chi/chi"
	"github.com/gorilla/csrf"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/member"
	"gitlab.com/arcadbox/arcad/internal/repository"
	"gitlab.com/wpetit/goweb/middleware/container"
	"gitlab.com/wpetit/goweb/service/session"
	"gitlab.com/wpetit/goweb/service/template"
)

func serveAdminUsersListPage(w http.ResponseWriter, r *http.Request) {
	ctn := container.Must(r.Context())
	tmpl := template.Must(ctn)
	repo := repository.Must(ctn)

	users, err := repo.User().List()
	if err != nil {
		panic(err)
	}

	sort.Sort(sort.Reverse(repository.UsersByLastSeen(users)))

	data := extendTemplateData(w, r, template.Data{
		"Users":      users,
		"UsersCount": len(users),
	})

	if err := tmpl.RenderPage(w, "admin_users_list.html.tmpl", data); err != nil {
		panic(errors.Wrapf(err, "could not render '%s' page", r.URL.Path))
	}
}

func serveAdminUserInfoPage(w http.ResponseWriter, r *http.Request) {
	ctn := container.Must(r.Context())
	tmpl := template.Must(ctn)
	repo := repository.Must(ctn)

	userID := repository.UserID(chi.URLParam(r, "userID"))

	user, err := repo.User().Get(userID)
	if err != nil {
		if err == repository.ErrNotFound {
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)

			return
		}

		panic(err)
	}

	data := extendTemplateData(w, r, template.Data{
		csrf.TemplateTag: csrf.TemplateField(r),
		"User":           user,
	})

	if err := tmpl.RenderPage(w, "admin_user_info.html.tmpl", data); err != nil {
		panic(errors.Wrapf(err, "could not render '%s' page", r.URL.Path))
	}
}

func handleAdminUserUpdate(w http.ResponseWriter, r *http.Request) {
	ctn := container.Must(r.Context())
	tmpl := template.Must(ctn)
	repo := repository.Must(ctn)

	sess, err := session.Must(ctn).Get(w, r)
	if err != nil {
		panic(errors.Wrap(err, "could not retrieve session"))
	}

	userID := repository.UserID(chi.URLParam(r, "userID"))

	user, err := repo.User().Get(userID)
	if err != nil {
		if err == repository.ErrNotFound {
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)

			return
		}

		panic(errors.Wrap(err, "could not retrieve user"))
	}

	renderError := func(message string) {
		sess.AddFlash(session.FlashError, message)

		data := extendTemplateData(w, r, template.Data{
			csrf.TemplateTag: csrf.TemplateField(r),
			"User":           user,
			"Form":           r.Form,
		})

		if err := tmpl.RenderPage(w, "admin_user_info.html.tmpl", data); err != nil {
			panic(errors.Wrapf(err, "could not render '%s' page", r.URL.Path))
		}
	}

	if err := r.ParseForm(); err != nil {
		panic(errors.Wrap(err, "could not parse form"))
	}

	newNickname := r.Form.Get("nickname")

	if newNickname == "" {
		renderError("Le pseudonyme ne peut pas être vide.")
		return
	}

	user.Nickname = newNickname

	if err := repo.User().Save(user); err != nil {
		cause := errors.Cause(err)
		if cause.Error() == storm.ErrAlreadyExists.Error() {
			renderError("Ce pseudonyme est déjà utilisé.")
			return
		}

		panic(errors.Wrap(err, "could not save user"))
	}

	sess.AddFlash(session.FlashSuccess, "Le pseudonyme de l'utilisateur a été modifié.")

	if err := sess.Save(w, r); err != nil {
		panic(errors.Wrap(err, "could not save session"))
	}

	http.Redirect(w, r, fmt.Sprintf("/admin/users/%s", user.ID), http.StatusSeeOther)
}

func serveAdminUserMemberCard(w http.ResponseWriter, r *http.Request) {
	ctn := container.Must(r.Context())
	repo := repository.Must(ctn)

	userID := repository.UserID(chi.URLParam(r, "userID"))

	user, err := repo.User().Get(userID)
	if err != nil {
		if err == repository.ErrNotFound {
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)

			return
		}

		panic(errors.Wrap(err, "could not find user"))
	}

	member := member.Must(ctn)

	img, err := member.GenerateCard(user)
	if err != nil {
		panic(errors.Wrap(err, "could not render member card"))
	}

	data, err := member.EncodeUserID(img, user.ID)
	if err != nil {
		panic(errors.Wrap(err, "could not encode user id"))
	}

	w.Header().Set("Content/Type", "image/png")
	w.WriteHeader(http.StatusOK)

	if _, err := data.WriteTo(w); err != nil {
		panic(errors.Wrap(err, "could not send member card"))
	}
}

func serveAdminUserDeletePage(w http.ResponseWriter, r *http.Request) {
	ctn := container.Must(r.Context())
	tmpl := template.Must(ctn)
	repo := repository.Must(ctn)

	userID := repository.UserID(chi.URLParam(r, "userID"))

	user, err := repo.User().Get(userID)
	if err != nil {
		if err == repository.ErrNotFound {
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)

			return
		}

		panic(errors.Wrap(err, "could not find user"))
	}

	data := extendTemplateData(w, r, template.Data{
		csrf.TemplateTag: csrf.TemplateField(r),
		"User":           user,
	})

	if err := tmpl.RenderPage(w, "admin_user_delete.html.tmpl", data); err != nil {
		panic(errors.Wrapf(err, "could not render '%s' page", r.URL.Path))
	}
}

func handleAdminUserDelete(w http.ResponseWriter, r *http.Request) {
	ctn := container.Must(r.Context())
	repo := repository.Must(ctn)

	sess, err := session.Must(ctn).Get(w, r)
	if err != nil {
		panic(errors.Wrap(err, "could not retrieve session"))
	}

	userID := repository.UserID(chi.URLParam(r, "userID"))

	if err := repo.User().Delete(userID); err != nil {
		if err == repository.ErrNotFound {
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)

			return
		}

		panic(errors.Wrap(err, "could not retrieve user"))
	}

	sess.AddFlash(session.FlashSuccess, "Le compte de l'utilisateur a été supprimé.")

	if err := sess.Save(w, r); err != nil {
		panic(errors.Wrap(err, "could not save session"))
	}

	http.Redirect(w, r, "/admin/users", http.StatusSeeOther)
}
