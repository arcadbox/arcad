package route

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"sync"
	"sync/atomic"

	"gitlab.com/wpetit/goweb/logger"

	"gitlab.com/arcadbox/arcad/internal/app"
	"gitlab.com/arcadbox/arcad/internal/app/module"
	"gitlab.com/arcadbox/arcad/internal/repository"

	"gitlab.com/arcadbox/arcad/internal/bus"

	"gitlab.com/arcadbox/arcad/internal/bundle"
	"gitlab.com/arcadbox/arcad/internal/middleware"

	"github.com/go-chi/chi"
	"github.com/pkg/errors"

	"github.com/igm/sockjs-go/v3/sockjs"
	"gitlab.com/wpetit/goweb/middleware/container"
	"gitlab.com/wpetit/goweb/service/template"
)

func serveAppWrapper(w http.ResponseWriter, r *http.Request) {
	ctn := container.Must(r.Context())
	tmpl := template.Must(ctn)
	repo := repository.Must(ctn)

	appID := repository.AppID(chi.URLParam(r, "appID"))

	app, err := repo.App().Get(appID)
	if err != nil {
		if errors.Is(err, repository.ErrNotFound) {
			http.NotFound(w, r)
			return
		}

		panic(errors.Wrap(err, "could not retrieve app"))
	}

	data := extendTemplateData(w, r, template.Data{
		"App": app,
	})

	if err := tmpl.RenderPage(w, "app.html.tmpl", data); err != nil {
		panic(errors.Wrap(err, "render page"))
	}
}

func serveApp(w http.ResponseWriter, r *http.Request) {
	ctn := container.Must(r.Context())
	appManager := app.Must(ctn)

	appID := repository.AppID(chi.URLParam(r, "appID"))

	appBundle, err := appManager.GetBundle(appID)
	if err != nil {
		if err == app.ErrBundleNotFound {
			http.NotFound(w, r)
			return
		}

		panic(err)
	}

	fs := bundle.NewFileSystem("public", appBundle)
	prefix := fmt.Sprintf("/apps/%s/", appID)
	handler := http.StripPrefix(prefix, http.FileServer(fs))

	handler.ServeHTTP(w, r)
}

type contextKey string

const (
	contextKeyUser    contextKey = "user"
	contextKeyIsAdmin contextKey = "isAdmin"
)

func withIsAdmin(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		isAdmin, err := middleware.IsAdmin(w, r)
		if err != nil {
			panic(errors.Wrap(err, "could not identify user authorization level"))
		}

		r = r.WithContext(context.WithValue(r.Context(), contextKeyIsAdmin, isAdmin))

		next.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}

func withUser(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		user, err := middleware.FindOrCreateAuthenticatedUser(w, r)
		if err != nil {
			panic(errors.Wrap(err, "could not find or create authenticated user"))
		}

		r = r.WithContext(context.WithValue(r.Context(), contextKeyUser, user))

		next.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}

func ctxUser(ctx context.Context) (*repository.User, error) {
	user, ok := ctx.Value(contextKeyUser).(*repository.User)
	if !ok {
		return nil, errors.New("could not find user in context")
	}

	return user, nil
}

func ctxIsAdmin(ctx context.Context) (bool, error) {
	isAdmin, ok := ctx.Value(contextKeyIsAdmin).(bool)
	if !ok {
		return false, errors.New("could not find isAdmin in context")
	}

	return isAdmin, nil
}

const (
	statusChannelClosed = iota
)

func handleSockSession(sess sockjs.Session) {
	reqCtx := sess.Request().Context()

	user, err := ctxUser(reqCtx)
	if err != nil {
		panic(errors.WithStack(err))
	}

	isAdmin, err := ctxIsAdmin(reqCtx)
	if err != nil {
		panic(errors.WithStack(err))
	}

	ctn := container.Must(reqCtx)
	bus := bus.Must(ctn)

	ctx := logger.With(sess.Context(),
		logger.F("sessionID", sess.ID()),
		logger.F("isAdmin", isAdmin),
		logger.F("userID", user.ID),
	)

	logger.Debug(ctx, "new sockjs session")

	var (
		once        sync.Once
		atomicAppID atomic.Value
	)

	getAppID := func() (repository.AppID, bool) {
		appID, ok := atomicAppID.Load().(repository.AppID)

		return appID, ok
	}
	setAppID := func(appID repository.AppID) {
		atomicAppID.Store(appID)

		// Start listening for backend messages
		// once the appID is known
		once.Do(func() {
			go handleBackendMessages(ctx, bus, user.ID, sess, getAppID)
		})
	}

	defer func() {
		if sess.GetSessionState() == sockjs.SessionActive {
			if err := sess.Close(statusChannelClosed, "channel closed"); err != nil {
				logger.Error(ctx, "could not close sockjs session", logger.E(errors.WithStack(err)))
			}
		}

		// If appID is set, dispatch player disconnected message
		appID, ok := getAppID()
		if ok {
			logger.Debug(
				ctx, "publishing player disconnected",
				logger.F("appID", appID),
			)
			msg := module.NewMessageUserDisconnected(appID, user.ID)

			if err := bus.Publish(ctx, msg); err != nil {
				logger.Error(ctx, "could not publish message",
					logger.E(errors.WithStack(err)),
					logger.F("message", msg),
				)
			}
		}
	}()

	handleFrontendMessages(bus, user.ID, sess, setAppID, getAppID, isAdmin)
}

func handleBackendMessages(ctx context.Context, b bus.Bus, userID repository.UserID, sess sockjs.Session, getAppID func() (repository.AppID, bool)) {
	appID, ok := getAppID()
	if !ok {
		logger.Error(ctx, "could not retrieve app id")
	}

	ctx = logger.With(ctx, logger.F("appID", appID))

	ns := module.AppMessageNamespace(appID)

	messages, err := b.Subscribe(ctx, ns, module.MessageTypeBackendMessage)
	if err != nil {
		panic(errors.WithStack(err))
	}

	defer func() {
		// Close messages subscriber
		b.Unsubscribe(ctx, ns, module.MessageTypeBackendMessage, messages)

		logger.Debug(ctx, "unsubscribed")

		if sess.GetSessionState() != sockjs.SessionActive {
			return
		}

		if err := sess.Close(statusChannelClosed, "channel closed"); err != nil {
			logger.Error(ctx, "could not close sockjs session", logger.E(errors.WithStack(err)))
		}
	}()

	for {
		select {
		case <-ctx.Done():
			return

		case msg := <-messages:
			backendMessage, ok := msg.(*module.MessageBackendMessage)
			if !ok {
				logger.Error(
					ctx,
					"unexpected backend message",
					logger.F("message", msg),
				)

				continue
			}

			// Ignore non broadcast or other users message
			if backendMessage.UserID != "" && backendMessage.UserID != userID {
				continue
			}

			payload, err := json.Marshal(backendMessage.Data)
			if err != nil {
				logger.Error(
					ctx,
					"could not encode message",
					logger.E(err),
				)

				continue
			}

			message := NewWebsocketMessage(
				WebsocketMessageTypeEvent,
				json.RawMessage(payload),
			)

			data, err := json.Marshal(message)
			if err != nil {
				logger.Error(
					ctx,
					"could not encode message",
					logger.E(err),
				)

				continue
			}

			logger.Debug(ctx, "sending message")

			// Send message
			if err := sess.Send(string(data)); err != nil {
				logger.Error(
					ctx,
					"could not send message",
					logger.E(err),
				)
			}
		}
	}
}

func handleFrontendMessages(bus bus.Bus, userID repository.UserID, sess sockjs.Session, setAppID func(appID repository.AppID), getAppID func() (repository.AppID, bool), isAdmin bool) {
	ctx := sess.Context()
	for {
		select {
		case <-ctx.Done():
			logger.Debug(ctx, "context done")
			return

		default:
			logger.Debug(ctx, "waiting for websocket data")
			data, err := sess.RecvCtx(ctx)
			if err != nil {
				logger.Error(
					ctx,
					"could not read message",
					logger.E(errors.WithStack(err)),
				)

				break
			}

			logger.Debug(ctx, "websocket data received", logger.F("data", data))

			message := &WebsocketMessage{}
			if err := json.Unmarshal([]byte(data), message); err != nil {
				logger.Error(
					ctx,
					"could not decode message",
					logger.E(errors.WithStack(err)),
				)

				break
			}

			switch {
			case message.Type == WebsocketMessageTypeInit:
				payload := &WebsocketMessageInitPayload{}

				if err := json.Unmarshal(message.Payload, payload); err != nil {
					logger.Error(
						ctx,
						"could not decode payload",
						logger.E(errors.WithStack(err)),
					)

					return
				}

				setAppID(payload.AppID)

				userConnectedMsg := module.NewMessageUserConnected(payload.AppID, userID, isAdmin)

				logger.Debug(ctx, "publishing player connected", logger.F("userID", userID))

				if err := bus.Publish(ctx, userConnectedMsg); err != nil {
					logger.Error(ctx, "could not publish message",
						logger.E(errors.WithStack(err)),
						logger.F("message", userConnectedMsg),
					)

					return
				}

			case message.Type == WebsocketMessageTypeEvent:
				appID, ok := getAppID()
				if !ok {
					logger.Error(
						ctx,
						"app id not received yet",
					)

					return
				}

				payload := &WebsocketMessageEventPayload{}
				if err := json.Unmarshal(message.Payload, payload); err != nil {
					logger.Error(
						ctx,
						"could not decode payload",
						logger.E(errors.WithStack(err)),
					)

					return
				}

				ctx := logger.With(ctx, logger.F("userID", userID), logger.F("data", payload.Data))

				frontendMessage := module.NewMessageFrontedMessage(appID, userID, payload.Data)

				logger.Debug(ctx, "publishing new fronted message", logger.F("message", frontendMessage))

				if err := bus.Publish(ctx, frontendMessage); err != nil {
					logger.Error(ctx, "could not publish message",
						logger.E(errors.WithStack(err)),
						logger.F("message", frontendMessage),
					)

					return
				}

				logger.Debug(ctx, "new fronted message published", logger.F("message", frontendMessage))

			default:
				logger.Error(
					ctx,
					"unsupported message type",
					logger.F("messageType", message.Type),
				)
			}
		}
	}
}
