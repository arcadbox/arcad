package route

import (
	"net/http"
	"net/url"

	"github.com/gorilla/csrf"

	"gitlab.com/arcadbox/arcad/internal/setting"

	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/repository"
	"gitlab.com/wpetit/goweb/middleware/container"
	"gitlab.com/wpetit/goweb/service/session"
	"gitlab.com/wpetit/goweb/service/template"
)

type settingList []*setting.Setting

func (l settingList) Get(id setting.ID) *setting.Setting {
	for _, s := range l {
		if s.ID() == id {
			return s
		}
	}

	return nil
}

func serveAdminHomePage(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/admin/settings", http.StatusTemporaryRedirect)
}

func serveAdminSettingsPage(w http.ResponseWriter, r *http.Request) {
	ctn := container.Must(r.Context())
	tmpl := template.Must(ctn)
	repo := repository.Must(ctn)

	metadatas, err := repo.Setting().ListMetadata()
	if err != nil {
		panic(err)
	}

	settings, err := repo.Setting().List()
	if err != nil {
		panic(err)
	}

	data := extendTemplateData(w, r, template.Data{
		csrf.TemplateTag: csrf.TemplateField(r),
		"Metadatas":      metadatas,
		"Settings":       settingList(settings),
	})

	if err := tmpl.RenderPage(w, "admin_settings.html.tmpl", data); err != nil {
		panic(errors.Wrapf(err, "could not render '%s' page", r.URL.Path))
	}
}

func handleAdminSettingsForm(w http.ResponseWriter, r *http.Request) {
	ctn := container.Must(r.Context())
	tmpl := template.Must(ctn)
	settingRepo := repository.Must(ctn).Setting()

	sess, err := session.Must(ctn).Get(w, r)
	if err != nil {
		panic(errors.Wrap(err, "could not retrieve session"))
	}

	metadatas, err := settingRepo.ListMetadata()
	if err != nil {
		panic(err)
	}

	settings, err := settingRepo.List()
	if err != nil {
		panic(err)
	}

	renderError := func(message string) {
		sess.AddFlash(session.FlashError, message)

		data := extendTemplateData(w, r, template.Data{
			csrf.TemplateTag: csrf.TemplateField(r),
			"Metadatas":      metadatas,
			"Settings":       settingList(settings),
		})

		if err := tmpl.RenderPage(w, "admin_settings.html.tmpl", data); err != nil {
			panic(errors.Wrapf(err, "could not render page '%s'", r.URL.Path))
		}
	}

	if err := r.ParseForm(); err != nil {
		panic(errors.Wrap(err, "could not parse form"))
	}

	saved := false

	for id, m := range metadatas {
		if err := validateAndSaveSetting(settingRepo, id, r.Form, m); err != nil {
			cause := errors.Cause(err)
			if validationMessage, ok := cause.(setting.ValidationMessage); ok {
				renderError(validationMessage.ValidationMessage())

				return
			}

			panic(errors.Wrapf(err, "could not validate and save setting '%s'", id))
		}

		saved = true
	}

	if saved {
		sess.AddFlash(session.FlashSuccess, "Modifications enregistrées.")

		if err := sess.Save(w, r); err != nil {
			panic(errors.Wrap(err, "could not save session"))
		}
	}

	http.Redirect(w, r, r.URL.Path, http.StatusSeeOther)
}

func validateAndSaveSetting(settingRepo setting.Repository, id setting.ID, form url.Values, m *setting.Metadata) error {
	value := form.Get(string(id))

	if value == m.DefaultValue {
		return nil
	}

	if err := m.Validate(id, value, form); err != nil {
		return errors.Wrapf(err, "could not validate setting '%s'", id)
	}

	transformedValue, err := m.Transform(id, value)
	if err != nil {
		return errors.Wrapf(err, "could not transform setting '%s'", id)
	}

	if err := settingRepo.Save(id, transformedValue); err != nil {
		return errors.Wrapf(err, "could not save setting '%s'", id)
	}

	return nil
}
