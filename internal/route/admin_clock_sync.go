package route

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os/exec"
	"strings"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/config"
	"gitlab.com/wpetit/goweb/logger"
	"gitlab.com/wpetit/goweb/middleware/container"
)

func handleClockSync(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)

		return
	}

	payload := struct {
		Timestamp time.Time `json:"t"`
	}{}

	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	if err := decoder.Decode(&payload); err != nil {
		panic(errors.Wrap(err, "could not decode payload"))
	}

	if err := updateSystemDate(r.Context(), payload.Timestamp); err != nil {
		panic(errors.Wrap(err, "could not update system date"))
	}

	encoder := json.NewEncoder(w)

	res := struct {
		Result string `json:"r"`
	}{}

	if err := encoder.Encode(&res); err != nil {
		panic(errors.Wrap(err, "could not encode response"))
	}
}

func updateSystemDate(ctx context.Context, date time.Time) error {
	ctn, err := container.From(ctx)
	if err != nil {
		return errors.WithStack(err)
	}

	conf, err := config.From(ctn)
	if err != nil {
		return errors.WithStack(err)
	}

	clockSync := conf.ClockSync

	if !clockSync.Enabled {
		logger.Debug(ctx, "clock synchronization is disabled, ignoring request")

		return nil
	}

	var args []string
	if len(clockSync.Command) > 1 {
		args = clockSync.Command[1:]
	} else {
		args = make([]string, 0)
	}

	formattedDate := date.Format(clockSync.Format)

	for i, a := range args {
		if strings.Count(a, "%s") != 1 {
			continue
		}

		args[i] = fmt.Sprintf(a, formattedDate)
	}

	logger.Debug(
		ctx, "updating system date",
		logger.F("command", clockSync.Command[0]),
		logger.F("args", args),
	)

	// #nosec G204
	cmd := exec.CommandContext(ctx, clockSync.Command[0], args...)

	if out, err := cmd.CombinedOutput(); err != nil {
		logger.Debug(
			ctx, "could not execute clock sync command",
			logger.F("output", string(out)),
		)

		return errors.Wrap(err, "could not update system date")
	}

	return nil
}
