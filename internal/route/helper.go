package route

import (
	"crypto/rand"
	"net/http"

	"github.com/pkg/errors"
	arcadTemplate "gitlab.com/arcadbox/arcad/internal/template"
	"gitlab.com/wpetit/goweb/middleware/container"
	"gitlab.com/wpetit/goweb/service/template"
	"gitlab.com/wpetit/goweb/template/html"
)

func extendTemplateData(w http.ResponseWriter, r *http.Request, data template.Data) template.Data {
	ctn := container.Must(r.Context())
	data, err := template.Extend(data,
		html.WithFlashes(w, r, ctn),
		arcadTemplate.WithBuildInfo(w, r, ctn),
		arcadTemplate.WithIsAdmin(w, r, ctn),
		arcadTemplate.WithJSConfig(w, r, ctn),
		arcadTemplate.WithFromGateway(r),
	)
	if err != nil {
		panic(errors.Wrap(err, "could not extend template data"))
	}
	return data
}

func generateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}
	return b, nil
}
