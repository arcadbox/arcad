package route

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/arcadbox/arcad/internal/repository"

	"github.com/go-chi/chi"
	"github.com/gorilla/csrf"
	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/middleware/container"
	"gitlab.com/wpetit/goweb/service/session"
	"gitlab.com/wpetit/goweb/service/template"
)

type publishableApp struct {
	*repository.App
	Published bool
}

func serveAdminAppsListPage(w http.ResponseWriter, r *http.Request) {
	ctn := container.Must(r.Context())
	tmpl := template.Must(ctn)
	repo := repository.Must(ctn)

	apps, err := repo.App().List()
	if err != nil {
		panic(err)
	}

	publishableApps := make([]*publishableApp, 0, len(apps))

	for _, a := range apps {
		isPublished, err := repo.App().IsPublished(a.ID)
		if err != nil {
			if errors.Is(err, repository.ErrNotFound) {
				http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)

				return
			}

			panic(errors.Wrap(err, "could not retrieve app publication status"))
		}

		publishableApps = append(publishableApps, &publishableApp{
			App:       a,
			Published: isPublished,
		})
	}

	data := extendTemplateData(w, r, template.Data{
		"Apps":      publishableApps,
		"AppsCount": len(apps),
	})

	if err := tmpl.RenderPage(w, "admin_apps_list.html.tmpl", data); err != nil {
		panic(errors.Wrapf(err, "could not render '%s' page", r.URL.Path))
	}
}

func serveAdminAppEditPage(w http.ResponseWriter, r *http.Request) {
	ctn := container.Must(r.Context())
	tmpl := template.Must(ctn)
	repo := repository.Must(ctn)

	appID := repository.AppID(chi.URLParam(r, "appID"))

	app, err := repo.App().Get(appID)
	if err != nil {
		if errors.Is(err, repository.ErrNotFound) {
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)

			return
		}

		panic(errors.Wrap(err, "could not retrieve app"))
	}

	isPublished, err := repo.App().IsPublished(appID)
	if err != nil {
		if errors.Is(err, repository.ErrNotFound) {
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)

			return
		}

		panic(errors.Wrap(err, "could not retrieve app publication status"))
	}

	data := extendTemplateData(w, r, template.Data{
		csrf.TemplateTag: csrf.TemplateField(r),
		"App":            app,
		"IsPublished":    isPublished,
	})

	if err := tmpl.RenderPage(w, "admin_app_edit.html.tmpl", data); err != nil {
		panic(errors.Wrapf(err, "could not render '%s' page", r.URL.Path))
	}
}

func handleAdminAppUpdate(w http.ResponseWriter, r *http.Request) {
	ctn := container.Must(r.Context())
	tmpl := template.Must(ctn)
	repo := repository.Must(ctn)

	sess, err := session.Must(ctn).Get(w, r)
	if err != nil {
		panic(errors.Wrap(err, "could not retrieve session"))
	}

	appID := repository.AppID(chi.URLParam(r, "appID"))

	app, err := repo.App().Get(appID)
	if err != nil {
		if errors.Is(err, repository.ErrNotFound) {
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)

			return
		}

		panic(errors.Wrap(err, "could not retrieve app"))
	}

	isPublished, err := repo.App().IsPublished(appID)
	if err != nil {
		if errors.Is(err, repository.ErrNotFound) {
			http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)

			return
		}

		panic(errors.Wrap(err, "could not retrieve app publication status"))
	}

	renderError := func(message string) {
		sess.AddFlash(session.FlashError, message)

		data := extendTemplateData(w, r, template.Data{
			csrf.TemplateTag: csrf.TemplateField(r),
			"App":            app,
			"Form":           r.Form,
			"IsPublished":    isPublished,
		})

		if err := tmpl.RenderPage(w, "admin_app_edit.html.tmpl", data); err != nil {
			panic(errors.Wrapf(err, "could not render '%s' page", r.URL.Path))
		}
	}

	if err := r.ParseForm(); err != nil {
		panic(errors.Wrap(err, "could not parse form"))
	}

	var newTitle *string

	if r.Form.Get("resetTitle") != "" {
		newTitle = nil
	} else {
		formTitle := r.Form.Get("title")
		newTitle = &formTitle
	}

	var newDescription *string

	if r.Form.Get("resetDescription") != "" {
		newDescription = nil
	} else {
		formDescription := r.Form.Get("description")
		newDescription = &formDescription
	}

	var newTags []string

	if r.Form.Get("resetTags") != "" {
		newTags = nil
	} else {
		formTags := r.Form.Get("tags")
		newTags = strings.Split(formTags, ",")

		for i, t := range newTags {
			newTags[i] = strings.ToLower(strings.Trim(t, " \r\n"))
		}
	}

	var newOrder *int

	if r.Form.Get("resetOrder") != "" {
		newOrder = nil
	} else {
		formOrder := r.Form.Get("order")

		order, err := strconv.ParseInt(formOrder, 10, 32)
		if err != nil {
			renderError("L'ordre d'affichage doit être un nombre entier.")

			return
		}

		intOrder := int(order)
		newOrder = &intOrder
	}

	if err := repo.App().Override(appID, newTitle, newDescription, newTags, newOrder); err != nil {
		panic(errors.Wrap(err, "could not override app"))
	}

	formPublished := r.Form.Get("published")

	if formPublished == "on" {
		if err := repo.App().Publish(appID); err != nil {
			panic(errors.Wrap(err, "could not publish app"))
		}
	} else {
		if err := repo.App().Unpublish(appID); err != nil {
			panic(errors.Wrap(err, "could not unpublish app"))
		}
	}

	sess.AddFlash(session.FlashSuccess, "L'app a été modifiée.")

	if err := sess.Save(w, r); err != nil {
		panic(errors.Wrap(err, "could not save session"))
	}

	http.Redirect(w, r, fmt.Sprintf("/admin/apps/%s", app.ID), http.StatusSeeOther)
}
