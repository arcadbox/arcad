package route

import (
	"net/http"

	"golang.org/x/crypto/bcrypt"

	"gitlab.com/arcadbox/arcad/internal/setting"

	"gitlab.com/arcadbox/arcad/internal/repository"

	"gitlab.com/arcadbox/arcad/internal/middleware"

	"github.com/gorilla/csrf"
	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/middleware/container"
	"gitlab.com/wpetit/goweb/service/session"
	"gitlab.com/wpetit/goweb/service/template"
)

func serveAdminLoginPage(w http.ResponseWriter, r *http.Request) {
	ctn := container.Must(r.Context())
	tmpl := template.Must(ctn)
	settingRepo := repository.Must(ctn).Setting()

	hasAdminPassword, err := settingRepo.Has(setting.AdminPassword)
	if err != nil {
		panic(errors.Wrap(err, "could not check admin status"))
	}

	if !hasAdminPassword {
		http.Redirect(w, r, "/admin/register", http.StatusSeeOther)

		return
	}

	data := extendTemplateData(w, r, template.Data{
		csrf.TemplateTag: csrf.TemplateField(r),
	})

	if err := tmpl.RenderPage(w, "admin_login.html.tmpl", data); err != nil {
		panic(errors.Wrapf(err, "could not render '%s' page", r.URL.Path))
	}
}

func handleAdminLogin(w http.ResponseWriter, r *http.Request) {
	ctn := container.Must(r.Context())
	tmpl := template.Must(ctn)
	settingRepo := repository.Must(ctn).Setting()

	sess, err := session.Must(ctn).Get(w, r)
	if err != nil {
		panic(errors.Wrap(err, "could not retrieve session"))
	}

	renderError := func(message string) {
		sess.AddFlash(session.FlashError, message)

		data := extendTemplateData(w, r, template.Data{
			csrf.TemplateTag: csrf.TemplateField(r),
		})

		if err := tmpl.RenderPage(w, "admin_login.html.tmpl", data); err != nil {
			panic(errors.Wrapf(err, "could not render page '%s'", r.URL.Path))
		}
	}

	adminPasswordSetting, err := settingRepo.Get(setting.AdminPassword)
	if err != nil {
		panic(errors.Wrap(err, "could not retrieve admin password setting"))
	}

	if err := r.ParseForm(); err != nil {
		panic(errors.Wrap(err, "could not parse form"))
	}

	password := r.Form.Get("password")

	adminPassword, err := adminPasswordSetting.AsString()
	if err != nil {
		panic(errors.Wrap(err, "unexpected admin password setting value"))
	}

	if err := checkPassword(password, adminPassword); err != nil {
		if err == bcrypt.ErrMismatchedHashAndPassword {
			renderError("Mot de passe invalide.")

			return
		}

		panic(errors.Wrap(err, "could not compare passwords"))
	}

	if err := middleware.PromoteSession(w, r); err != nil {
		panic(errors.Wrap(err, "could not promote session"))
	}

	sess.AddFlash(session.FlashSuccess, "Bienvenue administrateur.")

	if err := sess.Save(w, r); err != nil {
		panic(errors.Wrap(err, "could not save session"))
	}

	http.Redirect(w, r, "/admin", http.StatusSeeOther)
}

func handleAdminLogout(w http.ResponseWriter, r *http.Request) {
	if err := middleware.DemoteSession(w, r); err != nil {
		panic(errors.Wrap(err, "could not demote session"))
	}

	http.Redirect(w, r, "/admin", http.StatusSeeOther)
}

func checkPassword(password, hash string) error {
	return bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
}
