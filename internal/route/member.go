package route

import (
	"net/http"

	"gitlab.com/arcadbox/arcad/internal/member"

	"gitlab.com/arcadbox/arcad/internal/middleware"

	"github.com/pkg/errors"

	"gitlab.com/wpetit/goweb/middleware/container"
)

func serveMemberCard(w http.ResponseWriter, r *http.Request) {
	ctn := container.Must(r.Context())
	member := member.Must(ctn)

	user, err := middleware.FindOrCreateAuthenticatedUser(w, r)
	if err != nil {
		panic(errors.Wrap(err, "could not find or create authenticated user"))
	}

	img, err := member.GenerateCard(user)
	if err != nil {
		panic(errors.Wrap(err, "could not render member card"))
	}

	data, err := member.EncodeUserID(img, user.ID)
	if err != nil {
		panic(errors.Wrap(err, "could not encode user id"))
	}

	w.Header().Set("Content/Type", "image/png")
	w.Header().Set("Cache-Control", "no-cache")
	w.WriteHeader(http.StatusOK)

	if _, err := data.WriteTo(w); err != nil {
		panic(errors.Wrap(err, "could not send member card"))
	}
}
