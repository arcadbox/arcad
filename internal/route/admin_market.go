package route

import (
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/market"
	"gitlab.com/arcadbox/arcad/internal/market/client"
	"gitlab.com/wpetit/goweb/api"
	"gitlab.com/wpetit/goweb/logger"
	"gitlab.com/wpetit/goweb/middleware/container"
	"gitlab.com/wpetit/goweb/service/session"
	"gitlab.com/wpetit/goweb/service/template"
)

func serveAdminMarketSearchPage(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	ctn := container.Must(ctx)
	tmpl := template.Must(ctn)
	market := market.Must(ctn)

	search := r.URL.Query().Get("search")

	results, err := market.Client().SearchApps(ctx, search)
	if err != nil {
		logger.Error(ctx, "could not search apps", logger.E(errors.WithStack(err)))

		sess, err := session.Must(ctn).Get(w, r)
		if err != nil {
			panic(errors.Wrap(err, "could not retrieve session"))
		}

		sess.AddFlash(session.FlashError, "Impossible de contacter la place de marché !")

		results = &client.SearchAppsResult{
			Apps: make([]*client.AppEntry, 0),
		}
	}

	data := extendTemplateData(w, r, template.Data{
		"Results":      results.Apps,
		"ResultsCount": len(results.Apps),
		"Search":       search,
	})

	if err := tmpl.RenderPage(w, "admin_market_search.html.tmpl", data); err != nil {
		panic(errors.Wrapf(err, "could not render '%s' page", r.URL.Path))
	}
}

func serveAdminMarketAppInstallPage(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	rawAppID := chi.URLParam(r, "appID")

	appID, err := strconv.ParseInt(rawAppID, 10, 32)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)

		return
	}

	ctn := container.Must(ctx)
	tmpl := template.Must(ctn)
	market := market.Must(ctn)

	result, err := market.Client().GetApp(ctx, int(appID))
	if err != nil {
		logger.Error(ctx, "could not search apps", logger.E(errors.WithStack(err)))

		sess, err := session.Must(ctn).Get(w, r)
		if err != nil {
			panic(errors.Wrap(err, "could not retrieve session"))
		}

		sess.AddFlash(session.FlashError, "Impossible de contacter la place de marché !")

		result = &client.GetAppResult{}
	}

	data := extendTemplateData(w, r, template.Data{
		"App": result.App,
	})

	if err := tmpl.RenderPage(w, "admin_market_app_install.html.tmpl", data); err != nil {
		panic(errors.Wrapf(err, "could not render '%s' page", r.URL.Path))
	}
}

const (
	ErrCodeMarketError   api.ErrorCode = "market-error"
	ErrCodeInstallFailed api.ErrorCode = "install-failed"
)

func handleAdminMarketAppInstallRequest(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	rawAppID := chi.URLParam(r, "appID")

	appID, err := strconv.ParseInt(rawAppID, 10, 32)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)

		return
	}

	ctn := container.Must(ctx)
	market := market.Must(ctn)

	_, err = market.Installer().InstallAppLatestVersion(ctx, int(appID))
	if err != nil {
		logger.Error(ctx, "could not install app", logger.E(errors.WithStack(err)))

		api.ErrorResponse(w, http.StatusInternalServerError, ErrCodeInstallFailed, nil)

		return
	}

	api.DataResponse(w, http.StatusOK, struct {
		AppID int
	}{
		AppID: int(appID),
	})

	go func() {
		time.Sleep(time.Second)
		os.Exit(0)
	}()
}
