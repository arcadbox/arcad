package route

import (
	"encoding/json"
	"io"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/app/module"
	busSrv "gitlab.com/arcadbox/arcad/internal/bus"
	"gitlab.com/arcadbox/arcad/internal/config"
	"gitlab.com/arcadbox/arcad/internal/middleware"
	"gitlab.com/arcadbox/arcad/internal/repository"
	"gitlab.com/wpetit/goweb/logger"
	"gitlab.com/wpetit/goweb/middleware/container"
)

type uploadResponse struct {
	FileID string `json:"fileId"`
}

func handleAppUpload(w http.ResponseWriter, r *http.Request) {
	user, err := middleware.FindOrCreateAuthenticatedUser(w, r)
	if err != nil {
		panic(errors.Wrap(err, "could not find or create authenticated user"))
	}

	ctx := r.Context()
	ctn := container.Must(ctx)
	bus := busSrv.Must(ctn)
	conf := config.Must(ctn)

	r.Body = http.MaxBytesReader(w, r.Body, conf.App.MaxFileUploadSize)

	if err := r.ParseMultipartForm(conf.App.MaxFileUploadSize); err != nil {
		logger.Error(ctx, "could not parse multipart form", logger.E(errors.WithStack(err)))
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)

		return
	}

	file, header, err := r.FormFile("file")
	if err != nil {
		logger.Error(ctx, "could not read form file", logger.E(errors.WithStack(err)))
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)

		return
	}

	defer file.Close()

	var metadata map[string]interface{}

	rawMetadata := r.Form.Get("metadata")
	if rawMetadata != "" {
		if err := json.Unmarshal([]byte(rawMetadata), &metadata); err != nil {
			logger.Error(ctx, "could not parse metadata", logger.E(errors.WithStack(err)))
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)

			return
		}
	}

	appID := repository.AppID(chi.URLParam(r, "appID"))

	requestMsg := module.NewMessageUploadRequest(appID, user.ID, header, file, metadata)

	reply, err := bus.Request(ctx, requestMsg)
	if err != nil {
		logger.Error(ctx, "could not retrieve file", logger.E(errors.WithStack(err)))
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	logger.Debug(ctx, "upload reply", logger.F("reply", reply))

	responseMsg, ok := reply.(*module.MessageUploadResponse)
	if !ok {
		logger.Error(
			ctx, "unexpected upload response message",
			logger.F("message", reply),
		)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	if !responseMsg.Allow {
		http.Error(w, http.StatusText(http.StatusForbidden), http.StatusForbidden)

		return
	}

	encoder := json.NewEncoder(w)
	res := &uploadResponse{
		FileID: responseMsg.FileID,
	}

	if err := encoder.Encode(res); err != nil {
		panic(errors.Wrap(err, "could not encode upload response"))
	}
}

func handleAppDownload(w http.ResponseWriter, r *http.Request) {
	user, err := middleware.FindOrCreateAuthenticatedUser(w, r)
	if err != nil {
		panic(errors.Wrap(err, "could not find or create authenticated user"))
	}

	ctx := r.Context()
	ctn := container.Must(ctx)
	bus := busSrv.Must(ctn)

	appID := repository.AppID(chi.URLParam(r, "appID"))
	fileID := chi.URLParam(r, "fileID")

	requestMsg := module.NewMessageDownloadRequest(appID, user.ID, fileID)

	reply, err := bus.Request(ctx, requestMsg)
	if err != nil {
		logger.Error(ctx, "could not retrieve file", logger.E(errors.WithStack(err)))
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	}

	replyMsg, ok := reply.(*module.MessageDownloadResponse)
	if !ok {
		logger.Error(
			ctx, "unexpected download response message",
			logger.E(errors.WithStack(busSrv.ErrUnexpectedMessage)),
			logger.F("message", reply),
		)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)

		return
	}

	if !replyMsg.Allow {
		http.Error(w, http.StatusText(http.StatusForbidden), http.StatusForbidden)

		return
	}

	if replyMsg.File == nil {
		http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)

		return
	}

	defer replyMsg.File.Close()

	w.Header().Add("Content-Type", replyMsg.ContentType)
	w.Header().Add("Content-Length", strconv.FormatInt(replyMsg.Size, 10))
	w.Header().Add("Cache-Control", "public, immutable")

	if _, err := io.Copy(w, replyMsg.File); err != nil {
		logger.Error(ctx, "could not send file", logger.E(errors.WithStack(err)))
	}
}
