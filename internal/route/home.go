package route

import (
	"net/http"

	"gitlab.com/arcadbox/arcad/internal/middleware"
	"gitlab.com/arcadbox/arcad/internal/setting"

	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/repository"
	"gitlab.com/wpetit/goweb/middleware/container"
	"gitlab.com/wpetit/goweb/service/template"
)

func serveHomePage(w http.ResponseWriter, r *http.Request) {
	isAdmin, err := middleware.IsAdmin(w, r)
	if err != nil {
		panic(errors.Wrap(err, "could not check user authorization"))
	}

	ctn := container.Must(r.Context())
	tmpl := template.Must(ctn)
	repo := repository.Must(ctn)

	apps, err := repo.App().List()
	if err != nil {
		panic(errors.Wrap(err, "could not retrieve apps list"))
	}

	publishedApps := make([]*repository.App, 0, len(apps))

	for _, app := range apps {
		isPublished, err := repo.App().IsPublished(app.ID)
		if err != nil {
			panic(errors.Wrap(err, "could not retrieve app publication status"))
		}

		if !isPublished && !isAdmin {
			continue
		}

		publishedApps = append(publishedApps, app)
	}

	welcomeMessage, err := repo.Setting().Get(setting.WelcomeMessage)
	if err != nil {
		panic(errors.Wrap(err, "could not retrieve welcome message"))
	}

	data := extendTemplateData(w, r, template.Data{
		"Apps":           publishedApps,
		"WelcomeMessage": welcomeMessage,
	})

	if err := tmpl.RenderPage(w, "home.html.tmpl", data); err != nil {
		panic(errors.Wrapf(err, "could not render '%s' page", r.URL.Path))
	}
}
