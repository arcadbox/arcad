package hook

import (
	"context"

	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/logger"
	"gitlab.com/wpetit/goweb/plugin"
)

// HookMountRoutes is an interface allowing compatible plugins
// to mount additional http routes.
type MountRoutes interface {
	// HookMountRoutes handles the chi.Router used by the http server
	HookMountRoutes(ctx context.Context, r chi.Router) error
}

func InvokeMountRoutesHook(ctx context.Context, r chi.Router) error {
	hookService, err := getHookService(ctx)
	if err != nil {
		return errors.WithStack(err)
	}

	err = hookService.Plugins().Each(func(p plugin.Plugin) error {
		hook, ok := p.(MountRoutes)
		if !ok {
			return nil
		}

		pluginName := p.PluginName()
		pluginVersion := p.PluginVersion()

		logger.Debug(
			ctx, "invoking plugin HookMountRoutes()",
			logger.F("pluginName", pluginName),
			logger.F("pluginVersion", pluginVersion),
		)

		if err = hook.HookMountRoutes(ctx, r); err != nil {
			return errors.Wrapf(
				err,
				"could not mount routes from plugin '%s:%s'",
				pluginName, pluginVersion,
			)
		}

		return nil
	})
	if err != nil {
		return errors.WithStack(err)
	}

	return nil
}
