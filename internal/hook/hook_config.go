package hook

import (
	"context"

	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/logger"
	"gitlab.com/wpetit/goweb/plugin"
)

// Config is an interface allowing compatible plugins
// to receive configuration options on application startup.
type Config interface {
	// ConfigureHook handles plugins configuration options given by the host process.
	HookConfig(ctx context.Context, options map[string]interface{}) error
}

// InvokeConfigHook invokes plugins implementing the
// Config interface with their associated configuration options.
func InvokeConfigHook(ctx context.Context, reg *plugin.Registry, config map[string]map[string]interface{}) error {
	err := reg.Each(func(p plugin.Plugin) error {
		hook, ok := p.(Config)
		if !ok {
			return nil
		}

		pluginName := p.PluginName()
		pluginVersion := p.PluginVersion()

		pluginConfig, exists := config[pluginName]
		if !exists {
			pluginConfig = make(map[string]interface{})
		}

		logger.Debug(
			ctx, "invoke plugin HookConfig()",
			logger.F("pluginName", pluginName),
			logger.F("pluginVersion", pluginVersion),
		)

		if err := hook.HookConfig(ctx, pluginConfig); err != nil {
			return errors.Wrapf(err, "could not configure plugin '%s:%s'", pluginName, pluginVersion)
		}

		return nil
	})
	if err != nil {
		return errors.WithStack(err)
	}

	return nil
}
