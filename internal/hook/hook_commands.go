package hook

import (
	"context"

	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
	"gitlab.com/wpetit/goweb/logger"
	"gitlab.com/wpetit/goweb/plugin"
)

// Commands is an interface allowing compatible plugins
// to modify available commands.
type Commands interface {
	// HookCommands handles default commands
	// returns a modified slice of commands, if necessary.
	HookCommands(ctx context.Context, defaultCommands []*cli.Command) ([]*cli.Command, error)
}

func InvokeCommandsHook(ctx context.Context, reg *plugin.Registry, commands []*cli.Command) ([]*cli.Command, error) {
	err := reg.Each(func(p plugin.Plugin) error {
		hook, ok := p.(Commands)
		if !ok {
			return nil
		}

		pluginName := p.PluginName()
		pluginVersion := p.PluginVersion()

		logger.Debug(
			ctx, "invoking plugin HookCommands()",
			logger.F("pluginName", pluginName),
			logger.F("pluginVersion", pluginVersion),
		)

		var err error

		commands, err = hook.HookCommands(ctx, commands)
		if err != nil {
			return errors.Wrapf(
				err,
				"could not retrieve commands from plugin '%s:%s'",
				pluginName, pluginVersion,
			)
		}

		return nil
	})
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return commands, nil
}
