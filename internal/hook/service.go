package hook

import (
	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/plugin"
	"gitlab.com/wpetit/goweb/service"
)

const (
	ServiceName service.Name = "hook"
)

type Service struct {
	registry *plugin.Registry
}

func (s *Service) Plugins() *plugin.Registry {
	return s.registry
}

func ServiceProvider(registry *plugin.Registry) service.Provider {
	srv := &Service{registry}

	return func(ctn *service.Container) (interface{}, error) {
		return srv, nil
	}
}

// From retrieves the hook service in the given service container
func From(container *service.Container) (*Service, error) {
	service, err := container.Service(ServiceName)
	if err != nil {
		return nil, errors.Wrapf(err, "error while retrieving '%s' service", ServiceName)
	}

	srv, ok := service.(*Service)
	if !ok {
		return nil, errors.Errorf("retrieved service is not a valid '%s' service", ServiceName)
	}

	return srv, nil
}

// Must retrieves the hook service in the given service container or panic otherwise
func Must(container *service.Container) *Service {
	srv, err := From(container)
	if err != nil {
		panic(err)
	}

	return srv
}
