package hook

import (
	"context"

	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/app"
	"gitlab.com/wpetit/goweb/logger"
	"gitlab.com/wpetit/goweb/plugin"
)

// HookBackendModules is an interface allowing compatible plugins
// to modify apps backend exposed modules.
type BackendModules interface {
	// HookBackendModules handles backend modules factories and
	// returns a modified version, if necessary.
	HookBackendModules(ctx context.Context, factories []app.BackendModuleFactory) ([]app.BackendModuleFactory, error)
}

// InvokeBackendModulesHookWithContext extracts hook.Service from the given context
// and returns the InvokeBackendModulesHook result.
func InvokeBackendModulesHookWithContext(ctx context.Context, factories []app.BackendModuleFactory) ([]app.BackendModuleFactory, error) {
	hookService, err := getHookService(ctx)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return InvokeBackendModulesHook(ctx, hookService.Plugins(), factories)
}

// InvokeBackendModulesHook invokes the registered plugins implementing the BackendModules
// interface and extending backend modules factories.
func InvokeBackendModulesHook(ctx context.Context, reg *plugin.Registry, factories []app.BackendModuleFactory) ([]app.BackendModuleFactory, error) {
	var err error

	err = reg.Each(func(p plugin.Plugin) error {
		hook, ok := p.(BackendModules)
		if !ok {
			return nil
		}

		pluginName := p.PluginName()
		pluginVersion := p.PluginVersion()

		logger.Debug(
			ctx, "invoking plugin HookBackendModules()",
			logger.F("pluginName", pluginName),
			logger.F("pluginVersion", pluginVersion),
		)

		factories, err = hook.HookBackendModules(ctx, factories)
		if err != nil {
			return errors.Wrapf(
				err,
				"could not retrieve backend modules factories from plugin '%s:%s'",
				pluginName, pluginVersion,
			)
		}

		return nil
	})
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return factories, nil
}
