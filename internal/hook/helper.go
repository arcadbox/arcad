package hook

import (
	"context"

	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/middleware/container"
)

func getHookService(ctx context.Context) (*Service, error) {
	ctn, err := container.From(ctx)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	hookService, err := From(ctn)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return hookService, nil
}
