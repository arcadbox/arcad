package hook

import (
	"context"

	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/config"
	"gitlab.com/wpetit/goweb/logger"
	"gitlab.com/wpetit/goweb/plugin"
)

// HookConfig is an interface allowing compatible plugins
// to dump configuration options on 'dump-config' command execution.
type DumpConfig interface {
	// HookDumpConfig handles configuration file dump
	HookDumpConfig(ctx context.Context) (map[string]interface{}, error)
}

// InvokeDumpConfigHook invokes plugins implementing the
// DumpConfig interface.
func InvokeDumpConfigHook(ctx context.Context, config *config.Config) error {
	hookService, err := getHookService(ctx)
	if err != nil {
		return errors.WithStack(err)
	}

	err = hookService.Plugins().Each(func(p plugin.Plugin) error {
		hook, ok := p.(DumpConfig)
		if !ok {
			return nil
		}

		pluginName := p.PluginName()
		pluginVersion := p.PluginVersion()

		logger.Debug(
			ctx, "invoke plugin HookDumpConfig()",
			logger.F("pluginName", pluginName),
			logger.F("pluginVersion", pluginVersion),
		)

		pluginConfig, err := hook.HookDumpConfig(ctx)
		if err != nil {
			return errors.Wrapf(err, "could not dump plugin '%s:%s' config", pluginName, pluginVersion)
		}

		config.Plugins[pluginName] = pluginConfig

		return nil
	})
	if err != nil {
		return errors.WithStack(err)
	}

	return nil
}
