package middleware

import (
	"net/http"

	"gitlab.com/arcadbox/arcad/internal/repository"

	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/middleware/container"
	"gitlab.com/wpetit/goweb/service/session"
)

const (
	SessionKeyUserID = "uid"
	SessionKeyAdmin  = "admin"
)

// Authenticate checks that a user is associated with the request session.
// If no user can be found, a new one will be created
func Authenticate(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		ctn := container.Must(r.Context())
		repo := repository.Must(ctn)

		user, err := FindOrCreateAuthenticatedUser(w, r)
		if err != nil {
			panic(errors.Wrap(err, "could not find or create authenticated user"))
		}

		if err := repo.User().Touch(user.ID, r.UserAgent()); err != nil {
			panic(errors.Wrap(err, "could not update user last seen time"))
		}

		next.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}

// Authorize checks that the authenticated user has an admin level authorization
func Authorize(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		isAdmin, err := IsAdmin(w, r)
		if err != nil {
			panic(errors.Wrap(err, "could not check authorization level"))
		}

		if !isAdmin {
			http.Redirect(w, r, "/admin/login", http.StatusSeeOther)

			return
		}

		next.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}

// FindOrCreateAuthenticatedUser retrieves the user associated with the request session.
// If no user can be found, a new one will be created and returned
func FindOrCreateAuthenticatedUser(w http.ResponseWriter, r *http.Request) (*repository.User, error) {
	ctn := container.Must(r.Context())

	sess, err := session.Must(ctn).Get(w, r)
	if err != nil {
		return nil, errors.Wrap(err, "could not retrieve session")
	}

	repo := repository.Must(ctn)

	createUser := func() (*repository.User, error) {
		newUser, err := repo.User().Create()
		if err != nil {
			return nil, errors.Wrap(err, "could not create user")
		}

		sess.Set(SessionKeyUserID, string(newUser.ID))

		if err := sess.Save(w, r); err != nil {
			return nil, errors.Wrap(err, "could not save session")
		}

		return newUser, nil
	}

	uid, ok := sess.Get(SessionKeyUserID).(string)

	if !ok || uid == "" {
		newUser, err := createUser()
		if err != nil {
			return nil, err
		}

		return newUser, nil
	}

	existingUser, err := repo.User().Get(repository.UserID(uid))
	if err != nil {
		if err == repository.ErrNotFound {
			newUser, err := createUser()
			if err != nil {
				return nil, err
			}

			return newUser, nil
		}

		return nil, errors.Wrapf(err, "could not find user '%s'", uid)
	}

	return existingUser, nil
}

// IsAdmin checks if the session associated with the given request
// has the admin authorization level
func IsAdmin(w http.ResponseWriter, r *http.Request) (bool, error) {
	ctn := container.Must(r.Context())

	sess, err := session.Must(ctn).Get(w, r)
	if err != nil {
		return false, errors.Wrap(err, "could not retrieve session")
	}

	isAdmin, ok := sess.Get(SessionKeyAdmin).(bool)

	if !ok {
		return false, nil
	}

	return isAdmin, nil
}

// PromoteSession promotes the current request session to the admin authorization level
func PromoteSession(w http.ResponseWriter, r *http.Request) error {
	ctn := container.Must(r.Context())

	sess, err := session.Must(ctn).Get(w, r)
	if err != nil {
		return errors.Wrap(err, "could not retrieve session")
	}

	sess.Set(SessionKeyAdmin, true)

	if err := sess.Save(w, r); err != nil {
		return errors.Wrap(err, "could not save session")
	}

	return nil
}

// DemoteSession demotes the current request session to base user authorization level
func DemoteSession(w http.ResponseWriter, r *http.Request) error {
	ctn := container.Must(r.Context())

	sess, err := session.Must(ctn).Get(w, r)
	if err != nil {
		return errors.Wrap(err, "could not retrieve session")
	}

	sess.Unset(SessionKeyAdmin)

	if err := sess.Save(w, r); err != nil {
		return errors.Wrap(err, "could not save session")
	}

	return nil
}
