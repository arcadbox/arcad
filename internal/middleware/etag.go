package middleware

import (
	"net/http"

	"github.com/go-http-utils/etag"
)

func ETag(h http.Handler) http.Handler {
	return etag.Handler(h, false)
}
