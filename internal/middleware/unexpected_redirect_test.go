package middleware

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"
)

func TestUnexpectedRedirect(t *testing.T) {
	t.Parallel()

	publicHost := "public-host"
	invalidHost := "invalid-host"

	publicURL := fmt.Sprintf("http://%s/", publicHost)
	invalidURL := fmt.Sprintf("http://%s/foo", invalidHost)

	getHelloWorld := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if _, err := w.Write([]byte("Hello world !")); err != nil {
			t.Error(err)
		}
	})

	router := chi.NewRouter()

	router.Use(UnexpectedRedirect(publicHost, false))
	router.Get("/", getHelloWorld)

	r := httptest.NewRequest("GET", invalidURL, nil)
	w := httptest.NewRecorder()

	router.ServeHTTP(w, r)

	if g, e := w.Code, http.StatusTemporaryRedirect; g != e {
		t.Errorf("w.Code: got '%d', expected '%d", g, e)
	}

	location := w.Header().Get("Location")
	if g, e := location, publicURL; g != e {
		t.Errorf("location: got '%v', expected '%v", g, e)
	}
}
