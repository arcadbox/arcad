package middleware

import "net/http"

// UnexpectedRedirect configures the server to automatically
// redirect invalid host to the correct host/scheme
func UnexpectedRedirect(host string, useTLS bool) func(http.Handler) http.Handler {
	middleware := func(handler http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			invalidScheme := (useTLS && r.TLS == nil) || (!useTLS && r.TLS != nil)
			invalidHost := host != r.Host

			if invalidHost || invalidScheme {
				if host != "" {
					r.URL.Host = host
				}

				if useTLS && r.TLS == nil {
					r.URL.Scheme = "https"
				} else if !useTLS && r.TLS != nil {
					r.URL.Scheme = "http"
				}

				r.URL.Path = "/"

				http.Redirect(w, r, r.URL.String(), http.StatusTemporaryRedirect)

				return
			}

			handler.ServeHTTP(w, r)
		}

		return http.HandlerFunc(fn)
	}

	return middleware
}
