package middleware

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/repository"

	"gitlab.com/wpetit/goweb/middleware/container"
)

// PublishedAppOnly intercepts apps requests and returns a 404 HTTP error
// if the app is not published AND the user is not an admin.
func PublishedAppOnly(appIDParam string) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			ctn := container.Must(r.Context())
			repo := repository.Must(ctn)

			appID := repository.AppID(chi.URLParam(r, appIDParam))

			isPublished, err := repo.App().IsPublished(appID)
			if err != nil {
				if errors.Is(err, repository.ErrNotFound) {
					http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)

					return
				}

				panic(errors.Wrap(err, "could not retrieve app publication status"))
			}

			isAdmin, err := IsAdmin(w, r)
			if err != nil {
				panic(errors.Wrap(err, "could not identify user authorization level"))
			}

			if !isPublished && !isAdmin {
				http.NotFound(w, r)

				return
			}

			next.ServeHTTP(w, r)
		}

		return http.HandlerFunc(fn)
	}
}
