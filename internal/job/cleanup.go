package job

import (
	"time"

	"github.com/pkg/errors"

	"gitlab.com/arcadbox/arcad/internal/repository"

	"gitlab.com/wpetit/goweb/service"
)

type CleanupJob struct {
	ctn *service.Container
}

func (j *CleanupJob) Run() {
	if err := j.CleanupUsers(); err != nil {
		panic(errors.Wrap(err, "could not cleanup users"))
	}

	if err := j.CleanupHighscores(); err != nil {
		panic(errors.Wrap(err, "could not cleanup highscores"))
	}
}

func (j *CleanupJob) CleanupUsers() error {
	repo := repository.Must(j.ctn)

	userRepo := repo.User()
	highscoreRepo := repo.Highscore()

	users, err := userRepo.List()
	if err != nil {
		return errors.Wrap(err, "could not list users")
	}

	for _, u := range users {
		_, err := highscoreRepo.FindByUserID(u.ID)
		if err == nil {
			continue
		}

		if err != repository.ErrNotFound {
			return errors.Wrap(err, "could not list user's highscores")
		}

		// User has no highscores

		if time.Since(u.CreationTime.UTC()) < 48*60*time.Minute {
			continue
		}

		// User's creation is older than 48 hours

		creationLastConnectionDiff := u.CreationTime.UTC().Sub(u.CreationTime.UTC())
		if creationLastConnectionDiff > 5*time.Second {
			continue
		}

		// User's creationTime and last connection are the ~same

		// This account is trashable, delete it
		if err := userRepo.Delete(u.ID); err != nil {
			return errors.Wrapf(err, "could not delete user '%s'", u.ID)
		}
	}

	return nil
}

func (j *CleanupJob) CleanupHighscores() error {
	repo := repository.Must(j.ctn)

	userRepo := repo.User()
	highscoreRepo := repo.Highscore()

	highscores, err := highscoreRepo.List()
	if err != nil {
		return errors.Wrap(err, "could not list highscores")
	}

	for _, h := range highscores {
		u, err := userRepo.Get(h.UserID)
		if err == nil {
			continue
		}

		if err != repository.ErrNotFound {
			return errors.Wrapf(err, "could not find highscore '%s' user '%s'", h.ID, u.ID)
		}

		// User associated with the highscore does not exist anymore

		// Delete highscore
		if err := highscoreRepo.Delete(h.ID); err != nil {
			return errors.Wrapf(err, "could not delete highscore '%s'", h.ID)
		}
	}

	return nil
}

func NewCleanupJob(ctn *service.Container) *CleanupJob {
	return &CleanupJob{
		ctn: ctn,
	}
}
