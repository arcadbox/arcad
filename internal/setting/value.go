package setting

type ValueType string

const (
	ValueTypeText     ValueType = "text"
	ValueTypeMarkdown ValueType = "markdown"
	ValueTypePassword ValueType = "password"
)
