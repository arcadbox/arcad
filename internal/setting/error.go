package setting

import "errors"

var (
	ErrInvalidSettingType  = errors.New("invalid setting type")
	ErrInvalidSettingValue = errors.New("invalid setting value")
)

type validationError struct {
	error
	message string
}

func (e *validationError) ValidationMessage() string {
	return e.message
}

func NewValidationError(err error, message string) error {
	return &validationError{err, message}
}

type ValidationMessage interface {
	error

	ValidationMessage() string
}
