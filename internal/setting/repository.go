package setting

type Repository interface {
	Register(id ID, metadata *Metadata) error
	ListMetadata() (map[ID]*Metadata, error)
	GetMetadata(id ID) (*Metadata, error)
	List() ([]*Setting, error)
	Get(id ID) (*Setting, error)
	Has(id ID) (bool, error)
	Save(id ID, value interface{}) error
	Delete(id ID) error
}
