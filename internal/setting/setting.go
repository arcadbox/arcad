package setting

type ID string

const (
	AdminPassword  ID = "adminPassword"
	WelcomeMessage ID = "welcomeMessage"
)

type Setting struct {
	id       ID
	value    interface{}
	metadata *Metadata
}

func (s *Setting) ID() ID {
	return s.id
}

func (s *Setting) Metadata() *Metadata {
	return s.metadata
}

func (s *Setting) Value() interface{} {
	return s.value
}

func (s *Setting) AsString() (string, error) {
	val, ok := s.value.(string)
	if !ok {
		return "", ErrInvalidSettingType
	}

	return val, nil
}

func NewSetting(id ID, value interface{}, metadata *Metadata) *Setting {
	return &Setting{id, value, metadata}
}
