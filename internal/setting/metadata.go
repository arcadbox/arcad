package setting

import (
	"fmt"
	"net/url"

	"github.com/pkg/errors"

	"golang.org/x/crypto/bcrypt"
)

type ValidateFunc func(id ID, value interface{}, metadata *Metadata, form url.Values) error
type TransformFunc func(id ID, value interface{}, metadata *Metadata) (interface{}, error)

type Metadata struct {
	ValueType    ValueType
	Label        string
	DefaultValue interface{}
	validate     ValidateFunc
	transform    TransformFunc
}

func (m *Metadata) Transform(id ID, value interface{}) (interface{}, error) {
	if m.transform == nil {
		return value, nil
	}

	return m.transform(id, value, m)
}

func (m *Metadata) Validate(id ID, value interface{}, form url.Values) error {
	if m.validate == nil {
		return nil
	}

	return m.validate(id, value, m, form)
}

func NewBaseMetadata(valueType ValueType, label string, defaultValue interface{}) *Metadata {
	return &Metadata{
		ValueType:    valueType,
		Label:        label,
		DefaultValue: defaultValue,
	}
}

func NewPasswordMetadata(label string) *Metadata {
	return &Metadata{
		ValueType:    ValueTypePassword,
		Label:        label,
		DefaultValue: "",
		validate:     validatePassword,
		transform:    transformPassword,
	}
}

func validatePassword(id ID, value interface{}, metadata *Metadata, form url.Values) error {
	passwordConfirmField := fmt.Sprintf("%s-confirm", id)
	passwordConfirm := form.Get(passwordConfirmField)

	if passwordConfirm == "" {
		return NewValidationError(ErrInvalidSettingValue, "La confirmation du mot de passe ne peut pas être vide.")
	}

	password, ok := value.(string)
	if !ok {
		return NewValidationError(ErrInvalidSettingValue, "Valeur invalide pour le mot de passe.")
	}

	if password != passwordConfirm {
		return NewValidationError(ErrInvalidSettingValue, "Le mot de passe et sa confirmation diffèrent.")
	}

	return nil
}

func transformPassword(id ID, value interface{}, metadata *Metadata) (interface{}, error) {
	password, ok := value.(string)
	if !ok {
		return nil, ErrInvalidSettingValue
	}

	passwordBytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return nil, errors.Wrap(err, "could not generate password hash")
	}

	return string(passwordBytes), nil
}

func NewMarkdownMetadata(label, defaultValue string) *Metadata {
	return NewBaseMetadata(ValueTypeMarkdown, label, defaultValue)
}
