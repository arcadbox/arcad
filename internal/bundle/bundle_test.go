package bundle

import (
	"fmt"
	"testing"

	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/logger"
)

func TestBundle(t *testing.T) {
	t.Parallel()

	logger.SetLevel(logger.LevelDebug)

	bundles := []Bundle{
		NewDirectoryBundle("testdata/bundle"),
		NewTarBundle("testdata/bundle.tar.gz"),
		NewZipBundle("testdata/bundle.zip"),
	}

	for _, b := range bundles {
		func(b Bundle) {
			t.Run(fmt.Sprintf("'%T'", b), func(t *testing.T) {
				t.Parallel()

				reader, info, err := b.File("data/test/foo.txt")
				if err != nil {
					t.Error(err)
				}

				if reader == nil {
					t.Fatal("File(data/test/foo.txt): reader should not be nil")
				}

				defer func() {
					if err := reader.Close(); err != nil {
						t.Error(errors.WithStack(err))
					}
				}()

				if info == nil {
					t.Error("File(data/test/foo.txt): info should not be nil")
				}

				files, err := b.Dir("data")
				if err != nil {
					t.Error(err)
				}

				if e, g := 1, len(files); e != g {
					t.Errorf("len(files): expected '%v', got '%v'", e, g)
				}

				files, err = b.Dir("data/test")
				if err != nil {
					t.Error(err)
				}

				if e, g := 1, len(files); e != g {
					t.Fatalf("len(files): expected '%v', got '%v'", e, g)
				}

				if e, g := "foo.txt", files[0].Name(); e != g {
					t.Errorf("files[0].Name(): expected '%v', got '%v'", e, g)
				}
			})
		}(b)
	}
}
