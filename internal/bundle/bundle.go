package bundle

import (
	"io"
	"os"
)

type Bundle interface {
	File(string) (io.ReadCloser, os.FileInfo, error)
	Dir(string) ([]os.FileInfo, error)
}
