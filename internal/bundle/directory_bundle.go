package bundle

import (
	"context"
	"io"
	"io/ioutil"
	"os"
	"path"

	"gitlab.com/wpetit/goweb/logger"

	"github.com/pkg/errors"
)

type DirectoryBundle struct {
	baseDir string
}

func (b *DirectoryBundle) File(filename string) (io.ReadCloser, os.FileInfo, error) {
	ctx := context.Background()

	fullPath := path.Join(b.baseDir, filename)

	logger.Debug(ctx, "accessing bundle file", logger.F("file", fullPath))

	info, err := os.Stat(fullPath)
	if err != nil {
		if os.IsNotExist(err) {
			return nil, nil, err
		}

		return nil, nil, errors.Wrapf(err, "stat '%s'", fullPath)
	}

	reader, err := os.Open(fullPath)
	if err != nil {
		return nil, nil, errors.Wrapf(err, "open '%s'", fullPath)
	}

	return reader, info, nil
}

func (b *DirectoryBundle) Dir(dirname string) ([]os.FileInfo, error) {
	fullPath := path.Join(b.baseDir, dirname)
	ctx := context.Background()
	logger.Debug(ctx, "accessing bundle directory", logger.F("file", fullPath))
	return ioutil.ReadDir(fullPath)
}

func NewDirectoryBundle(baseDir string) *DirectoryBundle {
	return &DirectoryBundle{
		baseDir: baseDir,
	}
}
