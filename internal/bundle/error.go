package bundle

import "errors"

var ErrUnknownBundleArchiveExt = errors.New("unknown bundle archive extension")
