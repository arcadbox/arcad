package gateway

type Credentials struct {
	ID     string `json:"i" mapstructure:"i"`
	Secret string `json:"s" mapstructure:"j"`
}

func NewCredentials(id, secret string) *Credentials {
	return &Credentials{
		ID:     id,
		Secret: secret,
	}
}
