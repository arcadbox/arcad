package client

import (
	"forge.cadoles.com/wpetit/go-tunnel"
	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/service"
)

const (
	ServiceName service.Name = "tunnel-client"
)

type Service struct {
	client *tunnel.Client
}

func (s *Service) Client() *tunnel.Client {
	return s.client
}

func ServiceProvider(funcs ...tunnel.ClientConfigFunc) service.Provider {
	client := tunnel.NewClient(funcs...)

	srv := &Service{client}

	return func(ctn *service.Container) (interface{}, error) {
		return srv, nil
	}
}

// From retrieves the gateway client service in the given service container.
func From(container *service.Container) (*Service, error) {
	service, err := container.Service(ServiceName)
	if err != nil {
		return nil, errors.Wrapf(err, "error while retrieving '%s' service", ServiceName)
	}

	srv, ok := service.(*Service)
	if !ok {
		return nil, errors.Errorf("retrieved service is not a valid '%s' service", ServiceName)
	}

	return srv, nil
}

// Must retrieves the gateway client service in the given service container or panic otherwise.
func Must(container *service.Container) *Service {
	srv, err := From(container)
	if err != nil {
		panic(err)
	}

	return srv
}
