package client

import (
	"math/rand"
	"time"
)

type BackoffStrategy struct {
	factor float32
	base   time.Duration
	delay  time.Duration
}

func (s *BackoffStrategy) Sleep() {
	time.Sleep(s.delay)
	s.delay = s.delay*time.Duration(s.factor) + (time.Duration(rand.Intn(int(s.base))))
}

func (s *BackoffStrategy) Delay() time.Duration {
	return s.delay
}

func NewBackoffStrategy(base time.Duration, exponentialFactor float32) *BackoffStrategy {
	return &BackoffStrategy{
		factor: exponentialFactor,
		base:   base,
		delay:  base,
	}
}
