package server

import (
	"forge.cadoles.com/wpetit/go-tunnel"
	cmap "github.com/streamrail/concurrent-map"
)

type ClientRegistry struct {
	clientIDByRemoteAddr cmap.ConcurrentMap
	clientByClientID     cmap.ConcurrentMap
}

func (r *ClientRegistry) Get(clientID string) *tunnel.RemoteClient {
	rawRemoteClient, exists := r.clientByClientID.Get(clientID)
	if !exists {
		return nil
	}

	remoteClient, ok := rawRemoteClient.(*tunnel.RemoteClient)
	if !ok {
		return nil
	}

	return remoteClient
}

func (r *ClientRegistry) Add(clientID, remoteAddr string, rc *tunnel.RemoteClient) {
	r.clientByClientID.Set(clientID, rc)
	r.clientIDByRemoteAddr.Set(remoteAddr, clientID)
}

func (r *ClientRegistry) RemoveByRemoteAddr(remoteAddr string) {
	rawClientID, exists := r.clientIDByRemoteAddr.Get(remoteAddr)
	if !exists {
		return
	}

	r.clientIDByRemoteAddr.Remove(remoteAddr)

	clientID, ok := rawClientID.(string)
	if !ok {
		return
	}

	r.clientByClientID.Remove(clientID)
}

func NewClientRegistry() *ClientRegistry {
	return &ClientRegistry{
		clientIDByRemoteAddr: cmap.New(),
		clientByClientID:     cmap.New(),
	}
}
