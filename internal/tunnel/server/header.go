package server

import "net/http"

const GatewayHeaderKey = "X-Arcad-Gateway"

func AddGatewayHeader(r *http.Request) {
	r.Header.Set(GatewayHeaderKey, "1")
}

func HasGatewayHeader(r *http.Request) bool {
	return r.Header.Get(GatewayHeaderKey) == "1"
}
