package server

import (
	"forge.cadoles.com/wpetit/go-tunnel"
	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/service"
)

const (
	ServiceName service.Name = "tunnel-server"
)

type Service struct {
	clientRegistry *ClientRegistry
	server         *tunnel.Server
}

func (s *Service) Server() *tunnel.Server {
	return s.server
}

func (s *Service) Clients() *ClientRegistry {
	return s.clientRegistry
}

func ServiceProvider(funcs ...tunnel.ServerConfigFunc) service.Provider {
	server := tunnel.NewServer(funcs...)

	srv := &Service{
		clientRegistry: NewClientRegistry(),
		server:         server,
	}

	return func(ctn *service.Container) (interface{}, error) {
		return srv, nil
	}
}

// From retrieves the gateway server service in the given service container.
func From(container *service.Container) (*Service, error) {
	service, err := container.Service(ServiceName)
	if err != nil {
		return nil, errors.Wrapf(err, "error while retrieving '%s' service", ServiceName)
	}

	srv, ok := service.(*Service)
	if !ok {
		return nil, errors.Errorf("retrieved service is not a valid '%s' service", ServiceName)
	}

	return srv, nil
}

// Must retrieves the gateway server in the given service container or panic otherwise.
func Must(container *service.Container) *Service {
	srv, err := From(container)
	if err != nil {
		panic(err)
	}

	return srv
}
