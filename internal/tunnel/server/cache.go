package server

import (
	cmap "github.com/orcaman/concurrent-map"
	"github.com/pkg/errors"
)

type Cache interface {
	Get(key string) ([]byte, error)
	Has(key string) bool
	Store(key string, data []byte) error
}

type MemoryCache struct {
	store cmap.ConcurrentMap
}

func (c *MemoryCache) Get(key string) ([]byte, error) {
	raw, found := c.store.Get(key)
	if !found {
		return nil, errors.WithStack(ErrKeyNotFound)
	}

	res, ok := raw.([]byte)
	if !ok {
		return nil, errors.WithStack(ErrUnexpectedStoredValue)
	}

	return res, nil
}

func (c *MemoryCache) Has(key string) bool {
	return c.store.Has(key)
}

func (c *MemoryCache) Store(key string, data []byte) error {
	c.store.Set(key, data)

	return nil
}

func NewMemoryCache() *MemoryCache {
	return &MemoryCache{
		store: cmap.New(),
	}
}
