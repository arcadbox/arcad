package server

import (
	"bufio"
	"bytes"
	"context"
	"crypto/sha256"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"strings"
	"time"

	"github.com/karlseguin/ccache/v2"
	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/logger"
)

const (
	etagNone = "none"
)

type EtagCachingTransport struct {
	upstream       http.RoundTripper
	cache          *ccache.LayeredCache
	ttl            time.Duration
	refreshTimeout time.Duration
}

func (t *EtagCachingTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	ctx := req.Context()

	ctx = logger.With(
		ctx,
		logger.F("url", req.URL.String()),
	)

	primaryCacheKey, secondaryCacheKey, err := t.getCacheKey(req)
	if err != nil {
		logger.Error(
			ctx,
			"could not generate cache key",
			logger.E(errors.WithStack(err)),
		)
	}

	if err == nil {
		ctx = logger.With(
			ctx,
			logger.F("primaryCacheKey", primaryCacheKey),
			logger.F("secondaryCacheKey", secondaryCacheKey),
		)

		isCached := t.isCached(primaryCacheKey, secondaryCacheKey)

		ctx = logger.With(
			ctx,
			logger.F("isCached", isCached),
		)

		if isCached {
			res, err := t.fromCache(primaryCacheKey, secondaryCacheKey, req)
			if err != nil {
				return nil, errors.WithStack(err)
			}

			if res != nil {
				logger.Debug(ctx, "serving resource from cache")

				return res, nil
			}
		}
	}

	return t.fetchUpstreamAndCache(ctx, req, true)
}

func (t *EtagCachingTransport) fetchUpstreamAndCache(ctx context.Context, req *http.Request, withBackgroundRefresh bool) (*http.Response, error) {
	isRequestCacheable := t.isRequestCacheable(req)

	ctx = logger.With(
		ctx,
		logger.F("isRequestCacheable", isRequestCacheable),
	)

	logger.Debug(ctx, "fetching upstream resource")

	res, err := t.upstream.RoundTrip(req)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	ctx = logger.With(
		ctx,
		logger.F("responseStatusCode", res.StatusCode),
	)

	isResponseCacheable := t.isResponseCacheable(res)
	ctx = logger.With(
		ctx,
		logger.F("isResponseCacheable", isResponseCacheable),
	)

	logger.Debug(ctx, "upstream response received")

	if !isResponseCacheable {
		logger.Debug(ctx, "cannot cache upstream response")

		triggerRefresh := withBackgroundRefresh &&
			isRequestCacheable &&
			res.StatusCode == http.StatusNotModified

		if triggerRefresh {
			go t.refreshUpstreamResource(req)
		}

		return res, nil
	}

	res.Body = &cachingReadCloser{
		R: res.Body,
		OnEOF: func(r io.Reader) {
			res.Body = ioutil.NopCloser(r)

			data, err := httputil.DumpResponse(res, true)
			if err != nil {
				logger.Error(
					ctx, "could not dump response",
					logger.E(errors.WithStack(err)),
				)

				return
			}

			primaryCacheKey, _, err := t.getCacheKey(req)
			if err != nil {
				logger.Error(
					ctx, "could not generate cache key",
					logger.E(errors.WithStack(err)),
				)
			}

			secondaryCacheKey := etagNone
			if etag := res.Header.Get("Etag"); etag != "" {
				secondaryCacheKey = etag
			}

			ctx = logger.With(
				ctx,
				logger.F("primaryCacheKey", primaryCacheKey),
				logger.F("secondaryCacheKey", secondaryCacheKey),
				logger.F("ttl", t.ttl),
			)

			logger.Debug(ctx, "caching upstream http response")

			cachedItem := t.cache.Get(primaryCacheKey, etagNone)
			if cachedItem == nil {
				t.cache.Set(primaryCacheKey, etagNone, cachedResponse(data), t.ttl)
			}

			if secondaryCacheKey != etagNone {
				t.cache.Set(primaryCacheKey, secondaryCacheKey, cachedResponse(data), t.ttl)
			}
		},
	}

	return res, nil
}

func (t *EtagCachingTransport) refreshUpstreamResource(req *http.Request) {
	backgroundCtx, cancel := context.WithTimeout(
		context.Background(),
		t.refreshTimeout,
	)
	defer cancel()

	backgroundCtx = logger.With(
		backgroundCtx,
		logger.F("isBackground", true),
		logger.F("url", req.URL.String()),
		logger.F("method", req.Method),
	)

	logger.Debug(backgroundCtx, "fetching upstream resource in background")

	backgroundReq := req.Clone(backgroundCtx)

	// Delete cache control headers
	for k := range backgroundReq.Header {
		if strings.HasPrefix(k, "If-") {
			backgroundReq.Header.Del(k)
		}
	}

	res, err := t.fetchUpstreamAndCache(backgroundCtx, backgroundReq, false)
	if err != nil {
		logger.Error(backgroundCtx, "could not fetch upstream ressource", logger.E(errors.WithStack(err)))

		return
	}

	defer func() {
		if err := res.Body.Close(); err != nil {
			logger.Error(backgroundCtx, "could not close upstream response body", logger.E(errors.WithStack(err)))
		}
	}()

	if _, err := io.Copy(ioutil.Discard, res.Body); err != nil {
		logger.Error(backgroundCtx, "could not read upstream response body", logger.E(errors.WithStack(err)))
	}
}

func (t *EtagCachingTransport) fromCache(primaryKey, secondaryKey string, req *http.Request) (*http.Response, error) {
	item := t.cache.Get(primaryKey, secondaryKey)
	if item == nil || item.Expired() {
		return nil, nil
	}

	cachedResponse, ok := item.Value().(cachedResponse)
	if !ok {
		return nil, errors.WithStack(ErrUnexpectedStoredValue)
	}

	b := bytes.NewBuffer([]byte(cachedResponse))

	res, err := http.ReadResponse(bufio.NewReader(b), req)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	res.Header.Set("X-From-Cache", "1")

	return res, nil
}

func (t *EtagCachingTransport) isCached(primary, secondary string) bool {
	item := t.cache.Get(primary, secondary)

	return item != nil && !item.Expired()
}

func (t *EtagCachingTransport) getCacheKey(req *http.Request) (string, string, error) {
	hash := sha256.New()

	if _, err := hash.Write([]byte(req.Method)); err != nil {
		return "", "", errors.WithStack(err)
	}

	if _, err := hash.Write([]byte(req.URL.Path)); err != nil {
		return "", "", errors.WithStack(err)
	}

	primary := fmt.Sprintf("%x", hash.Sum(nil))
	secondary := "none"

	if etag := req.Header.Get("If-None-Match"); etag != "" {
		secondary = etag
	}

	return primary, secondary, nil
}

func (t *EtagCachingTransport) isRequestCacheable(req *http.Request) bool {
	return req.Method == http.MethodGet &&
		req.URL.RawQuery == "" &&
		!strings.Contains(req.Header.Get("Upgrade"), "websocket") &&
		!strings.Contains(req.Header.Get("Cache-Control"), "no-cache")
}

func (t *EtagCachingTransport) isResponseCacheable(res *http.Response) bool {
	return res.Header.Get("Set-Cookie") == "" &&
		(res.StatusCode >= http.StatusOK && res.StatusCode < http.StatusMultipleChoices) &&
		!strings.Contains(res.Header.Get("Cache-Control"), "no-cache")
}

func NewETagCachingTransport(upstream http.RoundTripper, maxSize int64, ttl, refreshTimeout time.Duration) *EtagCachingTransport {
	return &EtagCachingTransport{
		upstream: upstream,
		cache: ccache.Layered(
			ccache.Configure().MaxSize(maxSize),
		),
		ttl:            ttl,
		refreshTimeout: refreshTimeout,
	}
}

type cachedResponse []byte

func (r cachedResponse) Size() int64 {
	return int64(len(r))
}

// From https://github.com/gregjones/httpcache/blob/master/httpcache.go

// cachingReadCloser is a wrapper around ReadCloser R that calls OnEOF
// handler with a full copy of the content read from R when EOF is
// reached.
type cachingReadCloser struct {
	// Underlying ReadCloser.
	R io.ReadCloser
	// OnEOF is called with a copy of the content of R when EOF is reached.
	OnEOF func(io.Reader)

	buf bytes.Buffer // buf stores a copy of the content of R.
}

// Read reads the next len(p) bytes from R or until R is drained. The
// return value n is the number of bytes read. If R has no data to
// return, err is io.EOF and OnEOF is called with a full copy of what
// has been read so far.
func (r *cachingReadCloser) Read(p []byte) (n int, err error) {
	n, err = r.R.Read(p)
	r.buf.Write(p[:n])

	if errors.Is(err, io.EOF) {
		r.OnEOF(bytes.NewReader(r.buf.Bytes()))
	}

	return n, err
}

func (r *cachingReadCloser) Close() error {
	if err := r.R.Close(); err != nil {
		return errors.WithStack(err)
	}

	return nil
}
