package server

import "errors"

var (
	ErrKeyNotFound           = errors.New("key not found")
	ErrUnexpectedStoredValue = errors.New("unexpected stored value")
)
