package template

import (
	"encoding/json"
	"html/template"
	"time"

	"github.com/pkg/errors"

	"github.com/goodsign/monday"
	markdown "gitlab.com/golang-commonmark/markdown"
)

func AddInt(a, b int) int {
	return a + b
}

func LocaleTimeFormat(dt time.Time, layout string, locale string) string {
	return monday.Format(dt.Local(), layout, monday.Locale(locale))
}

func CreateMap(args ...interface{}) (map[string]interface{}, error) {
	data := make(map[string]interface{})

	var (
		key string
		ok  bool
	)

	for i, a := range args {
		isEven := i%2 == 0
		if isEven {
			key, ok = a.(string)
			if !ok {
				return nil, errors.Errorf("arg #%d: unexpected key type", i)
			}

			continue
		}

		data[key] = a
	}

	return data, nil
}

func Markdown(src string) template.HTML {
	md := markdown.New(
		markdown.Nofollow(true),
	)

	// #nosec G203
	return template.HTML(md.RenderToString([]byte(src)))
}

func ToJSON(data interface{}) (string, error) {
	json, err := json.Marshal(data)
	if err != nil {
		return "", err
	}

	return string(json), nil
}
