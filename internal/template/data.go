package template

import (
	"net/http"

	"gitlab.com/arcadbox/arcad/internal/config"
	"gitlab.com/arcadbox/arcad/internal/tunnel/server"

	"gitlab.com/arcadbox/arcad/internal/middleware"

	"github.com/pkg/errors"

	"gitlab.com/arcadbox/arcad/internal/build"
	"gitlab.com/wpetit/goweb/service"
	"gitlab.com/wpetit/goweb/service/template"
)

func WithBuildInfo(w http.ResponseWriter, r *http.Request, ctn *service.Container) template.DataExtFunc {
	return func(data template.Data) (template.Data, error) {
		info, err := build.From(ctn)
		if err != nil {
			return nil, errors.Wrap(err, "could not retrieve build service")
		}
		data["BuildInfo"] = info
		return data, nil
	}
}

func WithIsAdmin(w http.ResponseWriter, r *http.Request, ctn *service.Container) template.DataExtFunc {
	return func(data template.Data) (template.Data, error) {
		isAdmin, err := middleware.IsAdmin(w, r)
		if err != nil {
			return nil, errors.Wrap(err, "could not check authorization level")
		}

		data["IsAdmin"] = isAdmin
		return data, nil
	}
}

func WithFromGateway(r *http.Request) template.DataExtFunc {
	return func(data template.Data) (template.Data, error) {
		hasGatewayHeader := server.HasGatewayHeader(r)

		data["FromGateway"] = hasGatewayHeader

		return data, nil
	}
}

func WithJSConfig(w http.ResponseWriter, r *http.Request, ctn *service.Container) template.DataExtFunc {
	return func(data template.Data) (template.Data, error) {
		conf, err := config.From(ctn)
		if err != nil {
			return nil, errors.Wrap(err, "could not retrieve config service")
		}

		data["JSConfig"] = map[string]interface{}{
			"SENTRY_DSN":               conf.Sentry.DSN,
			"SENTRY_ENVIRONMENT":       conf.Sentry.Environment,
			"SENTRY_TRACE_SAMPLE_RATE": conf.Sentry.TraceSampleRate,
			"DEBUG":                    conf.Debug,
		}

		return data, nil
	}
}
