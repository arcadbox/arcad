package command

import (
	"context"

	"forge.cadoles.com/wpetit/go-tunnel"
	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
	"gitlab.com/arcadbox/arcad/internal/command/bootstrap"
	"gitlab.com/arcadbox/arcad/internal/config"
	gateway "gitlab.com/arcadbox/arcad/internal/tunnel"
	gatewayClient "gitlab.com/arcadbox/arcad/internal/tunnel/client"
	"gitlab.com/wpetit/goweb/logger"
	"gitlab.com/wpetit/goweb/service"
)

func GatewayClientCommand() *cli.Command {
	return &cli.Command{
		Name:  "gateway-client",
		Usage: "start the gateway client",
		Action: func(ctx *cli.Context) error {
			conf, ctn, err := bootstrap.StartUp(ctx)
			if err != nil {
				return errors.WithStack(err)
			}

			// Configure service container
			err = bootstrap.ConfigureContainer(ctx.Context, ctn, conf,
				configureGatewayClientService,
			)
			if err != nil {
				return errors.Wrap(err, "could not configure service container")
			}

			gateway, err := gatewayClient.From(ctn)
			if err != nil {
				return errors.WithStack(err)
			}

			client := gateway.Client()

			backoff := gatewayClient.NewBackoffStrategy(
				conf.GatewayClient.BackoffBaseDelay,
				conf.GatewayClient.BackoffFactor,
			)

			sleep := func() {
				logger.Info(ctx.Context, "sleeping", logger.F("delay", backoff.Delay()))
				backoff.Sleep()
			}

			for {
				logger.Info(
					ctx.Context, "connecting to gateway server",
					logger.F("serverAddress", conf.GatewayClient.ServerAddress),
				)

				if err := client.Connect(ctx.Context); err != nil {
					logger.Error(ctx.Context, "could not connect", logger.E(err))
					sleep()

					continue
				}

				logger.Info(ctx.Context, "waiting for server instructions")

				if err := client.Listen(ctx.Context); err != nil {
					logger.Error(ctx.Context, "error while listening", logger.E(err))
				}

				sleep()
			}
		},
	}
}

func configureGatewayClientService(ctx context.Context, conf *config.Config, ctn *service.Container) error {
	ctn.Provide(gatewayClient.ServiceName, gatewayClient.ServiceProvider(
		tunnel.WithClientServerAddress(conf.GatewayClient.ServerAddress),
		tunnel.WithClientBlockCrypt(
			"salsa20",
			conf.GatewayClient.SharedSecret,
			conf.GatewayClient.SharedSalt,
			2048,
			32,
		),
		tunnel.WithClientCredentials(gateway.NewCredentials(
			conf.GatewayClient.ID,
			conf.GatewayClient.Secret,
		)),
	))

	return nil
}
