package command

import (
	"context"

	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
	"gitlab.com/arcadbox/arcad/internal/command/bootstrap"
	"gitlab.com/arcadbox/arcad/internal/config"
	"gitlab.com/arcadbox/arcad/internal/repository"
	"gitlab.com/arcadbox/arcad/internal/setting"
	"gitlab.com/wpetit/goweb/logger"
	"gitlab.com/wpetit/goweb/service"
)

func ResetAdminCommand() *cli.Command {
	return &cli.Command{
		Name:  "reset-admin",
		Usage: "Reset the administration account",
		Action: func(ctx *cli.Context) error {
			conf, err := config.FromContext(ctx.Context)
			if err != nil {
				return errors.WithStack(err)
			}

			gitRef := ctx.String("gitRef")
			projectVersion := ctx.String("projectVersion")
			buildDate := ctx.String("buildDate")

			// Initialize and configure service container
			ctn := service.NewContainer()

			// Create service container
			err = bootstrap.ConfigureContainer(ctx.Context, ctn, conf,
				bootstrap.WithConfigureBuildInfo(projectVersion, gitRef, buildDate),
				bootstrap.ConfigureRepositoryService,
			)
			if err != nil {
				return errors.Wrap(err, "could not initialize service container")
			}

			if err := deleteAdminPasswordSetting(ctx.Context, ctn); err != nil {
				return errors.WithStack(err)
			}

			return nil
		},
	}
}

func deleteAdminPasswordSetting(ctx context.Context, ctn *service.Container) error {
	logger.Info(ctx, "resetting administrator password")

	repo, err := repository.From(ctn)
	if err != nil {
		return errors.Wrap(err, "could not retrieve repository service")
	}

	if err := repo.Setting().Delete(setting.AdminPassword); err != nil {
		if errors.Cause(err) != repository.ErrNotFound {
			return errors.Wrap(err, "could not reset admin password")
		}
	}

	logger.Info(ctx, "done")

	return nil
}
