package command

import (
	"context"
	"net/http"
	"os"
	"time"

	"forge.cadoles.com/wpetit/go-captiveportal"
	"forge.cadoles.com/wpetit/go-captiveportal/arp"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
	"gitlab.com/arcadbox/arcad/internal/app"
	"gitlab.com/arcadbox/arcad/internal/bus"
	"gitlab.com/arcadbox/arcad/internal/bus/memory"
	"gitlab.com/arcadbox/arcad/internal/command/bootstrap"
	"gitlab.com/arcadbox/arcad/internal/config"
	"gitlab.com/arcadbox/arcad/internal/member"
	"gitlab.com/arcadbox/arcad/internal/route"
	"gitlab.com/wpetit/goweb/logger"
	"gitlab.com/wpetit/goweb/middleware/container"
	"gitlab.com/wpetit/goweb/service"

	sentryhttp "github.com/getsentry/sentry-go/http"
	jwtMember "gitlab.com/arcadbox/arcad/internal/member/jwt"
)

const arpWatcherRestartMaxDelay = 30 * time.Second

func ServeCommand() *cli.Command {
	return &cli.Command{
		Name:  "serve",
		Usage: "Start the app server service",
		Action: func(ctx *cli.Context) error {
			conf, ctn, err := bootstrap.StartUp(ctx)
			if err != nil {
				return errors.WithStack(err)
			}

			// Configure services
			err = bootstrap.ConfigureContainer(ctx.Context, ctn, conf,
				bootstrap.ConfigureConfigService,
				bootstrap.ConfigureSessionService,
				bootstrap.ConfigureTemplateService,
				bootstrap.ConfigureMarketClientService,
				bootstrap.ConfigureRepositoryService,
				configureServeServices,
			)
			if err != nil {
				return errors.WithStack(err)
			}

			// Load apps
			if err := bootstrap.LoadApps(ctx.Context, conf, ctn); err != nil {
				return errors.Wrap(err, "could not load apps")
			}

			// Start jobs runner
			jobRunner, err := startJobRunner(ctx.Context, conf, ctn)
			if err != nil {
				return errors.Wrap(err, "could not start job runner")
			}

			defer jobRunner.Stop()

			r := chi.NewRouter()

			// Define base middlewares
			r.Use(middleware.Logger)
			r.Use(middleware.Recoverer)

			if conf.Sentry.DSN != "" {
				sentryHandler := sentryhttp.New(sentryhttp.Options{
					Repanic:         true,
					WaitForDelivery: false,
					Timeout:         time.Second * 30, // nolint: gomnd
				})

				r.Use(sentryHandler.Handle)
			}

			r.Use(middleware.DefaultCompress)

			// Expose service container on router
			r.Use(container.ServiceContainer(ctn))

			// Define routes
			route.Mount(r, conf, ctn)

			logger.Info(ctx.Context, "listening", logger.F("address", conf.HTTP.Address))

			if err := http.ListenAndServe(conf.HTTP.Address, r); err != nil {
				return errors.Wrap(err, "could not listen")
			}

			return nil
		},
	}
}

func configureServeServices(ctx context.Context, conf *config.Config, ctn *service.Container) error {
	jwtMemberService := jwtMember.NewService(
		conf.MemberCard.LayoutPath,
		conf.MemberCard.FontPath,
		jwtMember.TextLayout{
			FontSize:    conf.MemberCard.NicknameFontSize,
			AnchorX:     conf.MemberCard.NicknameAnchorX,
			AnchorY:     conf.MemberCard.NicknameAnchorY,
			LineSpacing: conf.MemberCard.NicknameLineSpacing,
		},
	)

	signingKeyPath := conf.MemberCard.SigningKeyPath

	if _, err := os.Stat(signingKeyPath); os.IsNotExist(err) {
		logger.Debug(ctx, "generating member card signing key", logger.F("path", signingKeyPath))

		if err := jwtMemberService.GenerateSigningKey(signingKeyPath); err != nil {
			return errors.Wrap(err, "could not generate signing key")
		}
	}

	logger.Debug(ctx, "loading member card signing key", logger.F("path", signingKeyPath))

	if err := jwtMemberService.LoadSigningKey(signingKeyPath); err != nil {
		return errors.Wrap(err, "could not load signing key")
	}

	// Create and expose member service provider
	ctn.Provide(member.ServiceName, member.ServiceProvider(jwtMemberService))

	// Create and expose bus service provider
	messageBus := memory.NewBus()
	ctn.Provide(bus.ServiceName, bus.ServiceProvider(messageBus))

	// Create and expose app manager provider
	ctn.Provide(app.ServiceName, app.ServiceProvider())

	arpIdentifier := arp.NewIdentifier()

	if conf.CaptivePortal.Enabled {
		for _, arpConfig := range conf.CaptivePortal.ARPWatcher {
			logger.Info(ctx, "start watching arp changes",
				logger.F("hostIP", arpConfig.HostIP),
				logger.F("routerIP", arpConfig.RouterIP),
				logger.F("routerNetwork", arpConfig.RouterNetwork),
				logger.F("probeInterval", arpConfig.ProbeInterval),
				logger.F("offlineDeadline", arpConfig.OfflineDeadline),
				logger.F("purgeDeadline", arpConfig.PurgeDeadline),
			)

			go func(arpConfig config.ARPWatcherConfig) {
				delay := time.Second
				for {
					if err := arpIdentifier.Watch(ctx, toWatchConfig(arpConfig)); err != nil {
						logger.Error(ctx, "error while watching arp tables", logger.E(errors.WithStack(err)))
					}

					logger.Error(ctx, "waiting before restart arp watcher", logger.F("delay", delay))
					time.Sleep(delay)

					if delay < arpWatcherRestartMaxDelay {
						delay *= 2
					}
				}
			}(arpConfig)
		}
	}

	ctn.Provide(captiveportal.ServiceName, captiveportal.ServiceProvider(
		conf.CaptivePortal.RedirectURL,
		arpIdentifier,
	))

	return nil
}

func toWatchConfig(c config.ARPWatcherConfig) arp.WatchConfig {
	return arp.WatchConfig{
		HostIP:          c.HostIP,
		NIC:             c.NIC,
		OfflineDeadline: c.OfflineDeadline,
		ProbeInterval:   c.ProbeInterval,
		PurgeDeadline:   c.PurgeDeadline,
		RouterIP:        c.RouterIP,
		RouterNetwork:   c.RouterNetwork,
	}
}
