package command

import (
	"context"
	"crypto/subtle"
	"net/http"

	"forge.cadoles.com/wpetit/go-tunnel"

	sentryhttp "github.com/getsentry/sentry-go/http"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
	"gitlab.com/arcadbox/arcad/internal/command/bootstrap"
	"gitlab.com/arcadbox/arcad/internal/config"
	"gitlab.com/arcadbox/arcad/internal/route"
	gateway "gitlab.com/arcadbox/arcad/internal/tunnel"
	gatewayServer "gitlab.com/arcadbox/arcad/internal/tunnel/server"
	"gitlab.com/wpetit/goweb/logger"
	"gitlab.com/wpetit/goweb/middleware/container"
	"gitlab.com/wpetit/goweb/service"
)

func GatewayServerCommand() *cli.Command {
	return &cli.Command{
		Name:  "gateway-server",
		Usage: "start the gateway server",
		Action: func(ctx *cli.Context) error {
			conf, ctn, err := bootstrap.StartUp(ctx)
			if err != nil {
				return errors.WithStack(err)
			}

			// Configure service container
			err = bootstrap.ConfigureContainer(ctx.Context, ctn, conf,
				bootstrap.ConfigureConfigService,
				bootstrap.ConfigureSessionService,
				bootstrap.ConfigureTemplateService,
				configureGatewayServerService,
			)
			if err != nil {
				return errors.Wrap(err, "could not configure service container")
			}

			gateway, err := gatewayServer.From(ctn)
			if err != nil {
				return errors.WithStack(err)
			}

			go func() {
				// Start listening for gateway clients
				logger.Info(ctx.Context, "listening for gateway clients", logger.F("address", conf.GatewayServer.TunnelAddress))

				if err := gateway.Server().Listen(ctx.Context); err != nil {
					logger.Fatal(ctx.Context, "could not listen", logger.E(err))
				}
			}()

			r := chi.NewRouter()

			// Define base middlewares
			r.Use(middleware.Logger)
			r.Use(middleware.Recoverer)

			sentryHandler := sentryhttp.New(sentryhttp.Options{
				Repanic: true,
			})

			r.Use(sentryHandler.Handle)

			// Expose service container on router
			r.Use(container.ServiceContainer(ctn))

			// Define routes
			route.MountGateway(r, conf, ctn)

			logger.Info(ctx.Context, "listening for http requests", logger.F("address", conf.GatewayServer.HTTPAddress))

			if err := http.ListenAndServe(conf.GatewayServer.HTTPAddress, r); err != nil {
				return errors.Wrap(err, "could not listen")
			}

			return nil
		},
	}
}

func configureGatewayServerService(ctx context.Context, conf *config.Config, ctn *service.Container) error {
	ctn.Provide(gatewayServer.ServiceName, gatewayServer.ServiceProvider(
		tunnel.WithServerAddress(conf.GatewayServer.TunnelAddress),
		tunnel.WithServerBlockCrypt(
			"salsa20",
			conf.GatewayServer.SharedSecret,
			conf.GatewayServer.SharedSalt,
			2048,
			32,
		),
		tunnel.WithServerOnClientAuth(createOnClientAuthHook(ctn, conf.GatewayServer)),
		tunnel.WithServerOnClientDisconnect(createOnClientDisconnectHook(ctn, conf.GatewayServer)),
	))

	return nil
}

func createOnClientAuthHook(ctn *service.Container, conf config.GatewayServerConfig) tunnel.OnClientAuthFunc {
	clientsByID := make(map[string]config.GatewayRemoteClientConfig)

	for _, c := range conf.Clients {
		clientsByID[c.ID] = c
	}

	return func(ctx context.Context, remoteClient *tunnel.RemoteClient, rawCredentials interface{}) (bool, error) {
		remoteAddr := remoteClient.RemoteAddr().String()

		logger.Info(ctx, "new gateway client auth request", logger.F("remoteAddress", remoteAddr))

		credentials := &gateway.Credentials{}
		if err := mapstructure.Decode(rawCredentials, credentials); err != nil {
			if err != nil {
				return false, errors.Wrap(err, "could not decode credentials")
			}
		}

		if credentials.ID == "" {
			return false, nil
		}

		clientConf, exists := clientsByID[credentials.ID]
		if !exists {
			return false, nil
		}

		secretMatches := subtle.ConstantTimeCompare([]byte(clientConf.Secret), []byte(credentials.Secret)) != 1

		if !secretMatches {
			return false, nil
		}

		gateway := gatewayServer.Must(ctn)

		gateway.Clients().Add(credentials.ID, remoteAddr, remoteClient)

		return true, nil
	}
}

func createOnClientDisconnectHook(ctn *service.Container, conf config.GatewayServerConfig) tunnel.OnClientDisconnectFunc {
	return func(ctx context.Context, remoteClient *tunnel.RemoteClient) error {
		remoteAddr := remoteClient.RemoteAddr().String()

		logger.Info(ctx, "gateway client disconnected", logger.F("remoteAddress", remoteAddr))

		gateway := gatewayServer.Must(ctn)

		gateway.Clients().RemoveByRemoteAddr(remoteAddr)

		return nil
	}
}
