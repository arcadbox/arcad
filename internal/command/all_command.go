package command

import (
	"context"

	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
	"gitlab.com/arcadbox/arcad/internal/command/apps"
	"gitlab.com/arcadbox/arcad/internal/hook"
	"gitlab.com/arcadbox/arcad/plugin"
	"gitlab.com/wpetit/goweb/logger"
)

func Commands() []*cli.Command {
	commands := []*cli.Command{
		ServeCommand(),
		VersionCommand(),
		DumpConfigCommand(),
		ResetAdminCommand(),
		GatewayServerCommand(),
		GatewayClientCommand(),
		TunnelServerCommand(),
		TunnelClientCommand(),
		apps.Command(),
	}

	ctx := context.Background()
	commands, err := hook.InvokeCommandsHook(ctx, plugin.Registry(), commands)
	if err != nil {
		logger.Fatal(ctx, "could not invoke commands hook", logger.E(errors.WithStack(err)))
	}

	return commands
}
