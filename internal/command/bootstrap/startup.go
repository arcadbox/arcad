package bootstrap

import (
	"context"
	"time"

	"gitlab.com/wpetit/goweb/logger"
	"gitlab.com/wpetit/goweb/middleware/container"

	"gitlab.com/arcadbox/arcad/internal/app"
	"gitlab.com/arcadbox/arcad/internal/app/module"
	"gitlab.com/arcadbox/arcad/internal/hook"

	"gitlab.com/arcadbox/arcad/internal/repository"

	"gitlab.com/arcadbox/arcad/internal/bus"

	"github.com/getsentry/sentry-go"
	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
	"gitlab.com/arcadbox/arcad/internal/config"
	"gitlab.com/wpetit/goweb/service"
)

func CreateBaseServiceContainer(ctx context.Context, conf *config.Config) (*service.Container, error) {
	ctn := service.NewContainer()

	// Configure base service container
	err := ConfigureContainer(ctx, ctn, conf,
		WithConfigureHook(),
	)
	if err != nil {
		return nil, errors.Wrap(err, "could not create service container")
	}

	return ctn, nil
}

func StartUp(ctx *cli.Context) (*config.Config, *service.Container, error) {
	conf, err := config.FromContext(ctx.Context)
	if err != nil {
		return nil, nil, errors.WithStack(err)
	}

	gitRef := ctx.String("gitRef")
	projectVersion := ctx.String("projectVersion")
	buildDate := ctx.String("buildDate")

	logger.Debug(
		ctx.Context,
		"starting",
		logger.F("gitRef", gitRef),
		logger.F("projectVersion", projectVersion),
		logger.F("buildDate", buildDate),
	)

	if conf.Sentry.DSN != "" {
		logger.Debug(
			ctx.Context,
			"initializing sentry client",
			logger.F("dsn", conf.Sentry.DSN),
			logger.F("debug", conf.Debug),
			logger.F("environment", conf.Sentry.Environment),
		)

		err = sentry.Init(sentry.ClientOptions{
			Dsn:              conf.Sentry.DSN,
			Debug:            conf.Debug,
			Environment:      conf.Sentry.Environment,
			AttachStacktrace: true,
			TracesSampleRate: conf.Sentry.TraceSampleRate,
		})
		if err != nil {
			return nil, nil, errors.Wrap(err, "could not initialize sentry")
		}

		defer sentry.Flush(2 * time.Second)
	}

	ctn, err := CreateBaseServiceContainer(ctx.Context, conf)
	if err != nil {
		return nil, nil, errors.Wrap(err, "could not create base service container")
	}

	// Configure service container
	err = ConfigureContainer(ctx.Context, ctn, conf,
		WithConfigureBuildInfo(projectVersion, gitRef, buildDate),
		WithConfigureHook(),
	)
	if err != nil {
		return nil, nil, errors.Wrap(err, "could not configure service container")
	}

	return conf, ctn, nil
}

func LoadApps(ctx context.Context, conf *config.Config, ctn *service.Container) error {
	eventBus, err := bus.From(ctn)
	if err != nil {
		return errors.WithStack(err)
	}

	appManager, err := app.From(ctn)
	if err != nil {
		return errors.WithStack(err)
	}

	repo, err := repository.From(ctn)
	if err != nil {
		return errors.WithStack(err)
	}

	// Create filesystem app loader
	appLoader := app.NewFilesystemLoader(conf.App.SearchPatterns...)

	// Load apps from filesystem
	loadedApps, err := appLoader.Load(ctx)
	if err != nil {
		return errors.Wrap(err, "could not load apps")
	}

	appRepo := repo.App()

	// Create backend modules
	backendModules := []app.BackendModuleFactory{
		module.LifecycleModuleFactory(eventBus),
		module.ConsoleModuleFactory(),
		module.NetModuleFactory(eventBus),
		module.HighscoreModuleFactory(repo),
		module.RPCModuleFactory(eventBus),
		module.TimerModuleFactory(),
		module.UserModuleFactory(repo.User()),
		module.StoreModuleFactory(conf.App.DataDir),
		module.AuthorizationModuleFactory(eventBus),
		module.FileModuleFactory(conf.App.DataDir, eventBus),
	}

	ctx = container.WithContainer(ctx, ctn)

	backendModules, err = hook.InvokeBackendModulesHookWithContext(ctx, backendModules)
	if err != nil {
		return errors.Wrap(err, "could not hook backend modules definition")
	}

	existingApps := make(map[repository.AppID]struct{})

	for i, g := range loadedApps {

		g.App.Order = i

		logger.Debug(
			ctx,
			"loading app",
			logger.F("appID", g.App.ID),
			logger.F("order", g.App.Order),
		)

		// Update app from loaded data
		if err := appRepo.Save(g.App); err != nil {
			return errors.Wrapf(err, "could not save app '%s'", g.App.ID)
		}

		appManager.SetBundle(g.App.ID, g.Bundle)

		backend, err := appManager.GetBackend(g.App.ID, backendModules...)

		if err == app.ErrNoBackend {
			logger.Debug(ctx, "no backend defined", logger.F("appID", g.App.ID))
			existingApps[g.App.ID] = struct{}{}
			continue
		}

		if err != nil {
			return errors.Wrapf(err, "could not get app '%s' backend", g.App.ID)
		}

		logger.Info(ctx, "starting app", logger.F("appID", g.App.ID))

		backend.Start()

		if err := backend.OnInit(); err != nil {
			return errors.Wrapf(err, "could not run app init '%s'", g.App.ID)
		}

		existingApps[g.App.ID] = struct{}{}
	}

	// Cleanup removed apps
	apps, err := appRepo.List()
	if err != nil {
		return errors.Wrap(err, "could not list apps")
	}

	for _, g := range apps {
		if _, exists := existingApps[g.ID]; exists {
			continue
		}

		logger.Debug(
			ctx,
			"deleting app",
			logger.F("appID", g.ID),
		)

		if err := appRepo.Delete(g.ID); err != nil {
			return errors.Wrapf(err, "could not delete app '%s'", g.ID)
		}
	}

	return nil
}
