package bootstrap

import (
	"context"
	"net/http"
	"time"

	"github.com/asdine/storm/v3"
	"github.com/gorilla/sessions"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/build"
	"gitlab.com/arcadbox/arcad/internal/config"
	"gitlab.com/arcadbox/arcad/internal/cookie"
	"gitlab.com/arcadbox/arcad/internal/hook"
	"gitlab.com/arcadbox/arcad/internal/market"
	"gitlab.com/arcadbox/arcad/internal/repository"
	stormRepository "gitlab.com/arcadbox/arcad/internal/repository/storm"
	arcadTemplate "gitlab.com/arcadbox/arcad/internal/template"
	"gitlab.com/arcadbox/arcad/internal/vfs"
	"gitlab.com/arcadbox/arcad/plugin"
	"gitlab.com/wpetit/goweb/logger"
	"gitlab.com/wpetit/goweb/service"
	"gitlab.com/wpetit/goweb/service/session"
	"gitlab.com/wpetit/goweb/service/template"
	"gitlab.com/wpetit/goweb/session/gorilla"
	"gitlab.com/wpetit/goweb/template/html"
	"go.etcd.io/bbolt"
)

type configureContainerFunc func(ctx context.Context, conf *config.Config, ctn *service.Container) error

func ConfigureContainer(ctx context.Context, ctn *service.Container, conf *config.Config, funcs ...configureContainerFunc) error {
	logger.Debug(ctx, "configuring services")
	for _, fn := range funcs {
		if err := fn(ctx, conf, ctn); err != nil {
			return errors.WithStack(err)
		}
	}
	logger.Debug(ctx, "services configured")

	return nil
}

func WithConfigureBuildInfo(projectVersion, gitRef, buildDate string) configureContainerFunc {
	return func(ctx context.Context, conf *config.Config, ctn *service.Container) error {
		ctn.Provide(build.ServiceName, build.ServiceProvider(projectVersion, gitRef, buildDate))

		return nil
	}
}

func WithConfigureHook() configureContainerFunc {
	return func(ctx context.Context, conf *config.Config, ctn *service.Container) error {
		registry := plugin.Registry()
		ctn.Provide(hook.ServiceName, hook.ServiceProvider(registry))

		return nil
	}
}

func ConfigureTemplateService(ctx context.Context, conf *config.Config, ctn *service.Container) error {
	// Create layered filesystem for theming purpose
	templateDirs := conf.Theme.ThemedDirectories("template", conf.HTTP.TemplateDir)

	logger.Debug(ctx, "using template directories", logger.F("directories", templateDirs))

	templateFs := vfs.NewLayeredFilesystem(templateDirs...)

	// Create and expose template service provider
	ctn.Provide(template.ServiceName, html.ServiceProvider(
		vfs.NewTemplateLoader(templateFs),
		html.WithHelper("localeTimeFormat", arcadTemplate.LocaleTimeFormat),
		html.WithHelper("addInt", arcadTemplate.AddInt),
		html.WithHelper("createMap", arcadTemplate.CreateMap),
		html.WithHelper("markdown", arcadTemplate.Markdown),
		html.WithHelper("toJSON", arcadTemplate.ToJSON),
		html.WithDevMode(conf.Debug),
	))

	return nil
}

func ConfigureSessionService(ctx context.Context, conf *config.Config, ctn *service.Container) error {
	// Generate random cookie authentication key if none is set
	if conf.HTTP.CookieAuthenticationKey == "" {
		logger.Info(ctx, "could not find cookie authentication key. generating one...")
		cookieAuthenticationKey, err := cookie.GenerateRandomBytes(64)
		if err != nil {
			return errors.Wrap(err, "could not generate cookie authentication key")
		}

		conf.HTTP.CookieAuthenticationKey = string(cookieAuthenticationKey)
	}

	// Generate random cookie encryption key if none is set
	if conf.HTTP.CookieEncryptionKey == "" {
		logger.Info(ctx, "could not find cookie encryption key. generating one...")
		cookieEncryptionKey, err := cookie.GenerateRandomBytes(32)
		if err != nil {
			return errors.Wrap(err, "could not generate cookie encryption key")
		}

		conf.HTTP.CookieEncryptionKey = string(cookieEncryptionKey)
	}

	// Create and initialize HTTP session service provider
	cookieStore := cookie.NewStore(
		[]byte(conf.HTTP.CookieAuthenticationKey),
		[]byte(conf.HTTP.CookieEncryptionKey),
		&sessions.Options{
			Path:     "/",
			HttpOnly: false,
			MaxAge:   conf.HTTP.CookieMaxAge,
			SameSite: http.SameSiteLaxMode,
		},
	)

	ctn.Provide(
		session.ServiceName,
		gorilla.ServiceProvider("arcad", cookieStore),
	)

	return nil
}

func ConfigureConfigService(ctx context.Context, conf *config.Config, ctn *service.Container) error {
	// Create and expose config service provider
	ctn.Provide(config.ServiceName, config.ServiceProvider(conf))

	return nil
}

func ConfigureMarketClientService(ctx context.Context, conf *config.Config, ctn *service.Container) error {
	ctn.Provide(market.ServiceName, market.ServiceProvider(
		market.WithIPFSGatewayURL(conf.MarketClient.IPFSGatewayURL),
		market.WithMarketURL(conf.MarketClient.MarketURL),
		market.WithRetryMax(conf.MarketClient.RetryMax),
		market.WithTimeout(conf.MarketClient.Timeout),
		market.WithAppsDir(conf.MarketClient.AppsDir),
	))

	return nil
}

func ConfigureRepositoryService(ctx context.Context, conf *config.Config, ctn *service.Container) error {
	logger.Debug(ctx, "opening storm database", logger.F("path", conf.Data.DBPath))

	// Initialise Storm database
	db, err := storm.Open(
		conf.Data.DBPath,
		storm.BoltOptions(0600, &bbolt.Options{Timeout: 1 * time.Second}),
	)
	if err != nil {
		return errors.Wrapf(err, "could not open database file '%s'", conf.Data.DBPath)
	}

	// Create repositories

	userRepo := stormRepository.NewUserRepository(db)
	if err := userRepo.Init(); err != nil {
		return errors.Wrap(err, "could not initialize user repository")
	}

	highscoreRepo := stormRepository.NewHighscoreRepository(db)
	if err := highscoreRepo.Init(); err != nil {
		return errors.Wrap(err, "could not initialize highscore repository")
	}

	appRepo := stormRepository.NewAppRepository(db)
	if err := appRepo.Init(); err != nil {
		return errors.Wrap(err, "could not initialize app repository")
	}

	settingRepo := stormRepository.NewSettingRepository(db)
	if err := settingRepo.Init(); err != nil {
		return errors.Wrap(err, "could not initialize setting repository")
	}

	if err := initializeSettings(settingRepo); err != nil {
		return errors.Wrap(err, "could not initialize settings")
	}

	// Create and expose repository service provider
	ctn.Provide(
		repository.ServiceName,
		repository.ServiceProvider(highscoreRepo, userRepo, appRepo, settingRepo),
	)

	return nil
}
