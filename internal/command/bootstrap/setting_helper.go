package bootstrap

import (
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/setting"
)

func initializeSettings(settingRepo setting.Repository) error {
	adminPasswordMetadata := setting.NewPasswordMetadata("Mot de passe du compte administrateur")
	if err := settingRepo.Register(setting.AdminPassword, adminPasswordMetadata); err != nil {
		return errors.Wrapf(err, "could not initialize '%s' setting", setting.AdminPassword)
	}

	welcomeMessageMetadata := setting.NewMarkdownMetadata("Message d'accueil", "")
	if err := settingRepo.Register(setting.WelcomeMessage, welcomeMessageMetadata); err != nil {
		return errors.Wrapf(err, "could not initialize '%s' setting", setting.WelcomeMessage)
	}

	return nil
}
