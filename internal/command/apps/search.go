package apps

import (
	"os"
	"strconv"
	"time"

	"github.com/olekukonko/tablewriter"
	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
	"gitlab.com/arcadbox/arcad/internal/command/bootstrap"
	"gitlab.com/arcadbox/arcad/internal/market"
)

func SearchCommand() *cli.Command {
	return &cli.Command{
		Name:      "search",
		ArgsUsage: "<search>",
		Usage:     "search the marketplace for apps",
		Action: func(ctx *cli.Context) error {
			search := ctx.Args().First()

			conf, ctn, err := bootstrap.StartUp(ctx)
			if err != nil {
				return errors.WithStack(err)
			}

			// Configure service container
			err = bootstrap.ConfigureContainer(
				ctx.Context, ctn, conf,
				bootstrap.ConfigureMarketClientService,
			)
			if err != nil {
				return errors.Wrap(err, "could not configure service container")
			}

			market, err := market.From(ctn)
			if err != nil {
				return errors.WithStack(err)
			}

			results, err := market.Client().SearchApps(ctx.Context, search)
			if err != nil {
				return errors.Wrap(err, "could not execute search")
			}

			data := make([][]string, 0, len(results.Apps))

			for _, r := range results.Apps {
				data = append(data, []string{
					strconv.FormatInt(int64(r.ID), 10),
					r.Name,
					r.Owner.Username,
					r.LatestRelease.Description,
					r.LatestRelease.Version,
					r.UpdatedAt.Format(time.RFC822),
				})
			}

			table := tablewriter.NewWriter(os.Stdout)
			table.SetHeader([]string{"ID", "Name", "Owner", "Description", "Version", "Last Update"})
			table.SetBorder(true)
			table.AppendBulk(data)
			table.Render()

			return nil
		},
	}
}
