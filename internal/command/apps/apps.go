package apps

import (
	"github.com/urfave/cli/v2"
)

func Command() *cli.Command {
	return &cli.Command{
		Name:  "apps",
		Usage: "Apps related actions",
		Subcommands: []*cli.Command{
			SearchCommand(),
			InstallCommand(),
			UninstallCommand(),
		},
	}
}
