package apps

import (
	"strconv"

	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
	"gitlab.com/arcadbox/arcad/internal/command/bootstrap"
	"gitlab.com/arcadbox/arcad/internal/market"
)

func InstallCommand() *cli.Command {
	return &cli.Command{
		Name:      "install",
		Usage:     "install the specified app from the marketplace",
		ArgsUsage: "<appID>",
		Action: func(ctx *cli.Context) error {
			rawAppID := ctx.Args().First()

			appID, err := strconv.ParseInt(rawAppID, 10, 32)
			if err != nil {
				return errors.Wrap(err, "invalid appID format")
			}

			conf, ctn, err := bootstrap.StartUp(ctx)
			if err != nil {
				return errors.WithStack(err)
			}

			// Configure service container
			err = bootstrap.ConfigureContainer(
				ctx.Context, ctn, conf,
				bootstrap.ConfigureMarketClientService,
			)
			if err != nil {
				return errors.Wrap(err, "could not configure service container")
			}

			market, err := market.From(ctn)
			if err != nil {
				return errors.WithStack(err)
			}

			installer := market.Installer()

			if _, err := installer.InstallAppLatestVersion(ctx.Context, int(appID)); err != nil {
				return errors.WithStack(err)
			}

			return nil
		},
	}
}
