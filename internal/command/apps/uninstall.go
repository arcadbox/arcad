package apps

import (
	"strconv"

	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
	"gitlab.com/arcadbox/arcad/internal/command/bootstrap"
	"gitlab.com/arcadbox/arcad/internal/market"
)

func UninstallCommand() *cli.Command {
	return &cli.Command{
		Name:      "uninstall",
		Usage:     "uninstall the specified app",
		ArgsUsage: "<appID>",
		Action: func(ctx *cli.Context) error {
			rawAppID := ctx.Args().First()

			appID, err := strconv.ParseInt(rawAppID, 10, 32)
			if err != nil {
				return errors.Wrap(err, "invalid appID format")
			}

			conf, ctn, err := bootstrap.StartUp(ctx)
			if err != nil {
				return errors.WithStack(err)
			}

			// Configure service container
			err = bootstrap.ConfigureContainer(
				ctx.Context, ctn, conf,
				bootstrap.ConfigureMarketClientService,
			)
			if err != nil {
				return errors.Wrap(err, "could not configure service container")
			}

			market := market.Must(ctn)

			if err := market.Installer().UninstallApp(ctx.Context, int(appID)); err != nil {
				return errors.Wrap(err, "could not uninstall app")
			}

			return nil
		},
	}
}
