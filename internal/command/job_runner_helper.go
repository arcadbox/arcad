package command

import (
	"context"
	"fmt"

	"gitlab.com/arcadbox/arcad/internal/config"
	"gitlab.com/arcadbox/arcad/internal/job"

	"github.com/pkg/errors"
	cron "github.com/robfig/cron/v3"
	"gitlab.com/wpetit/goweb/logger"
	"gitlab.com/wpetit/goweb/service"
)

func startJobRunner(ctx context.Context, conf *config.Config, ctn *service.Container) (*cron.Cron, error) {
	runner := cron.New(
		cron.WithLogger(&cronLogger{ctx}),
	)

	cleanupJob := job.NewCleanupJob(ctn)

	logger.Info(ctx, "register users cleanup job", logger.F("spec", conf.Job.CleanupSpec))

	if _, err := runner.AddJob(conf.Job.CleanupSpec, cleanupJob); err != nil {
		return nil, errors.Wrap(err, "could not schedule cleanup job")
	}

	runner.Start()

	return runner, nil
}

type cronLogger struct {
	ctx context.Context
}

func (l *cronLogger) Info(msg string, keysAndValues ...interface{}) {
	fields := l.createFields(keysAndValues)
	logger.Debug(l.ctx, msg, fields...)
}

func (l *cronLogger) Error(err error, msg string, keysAndValues ...interface{}) {
	fields := l.createFields(keysAndValues)
	fields = append(fields, logger.E(err))
	logger.Error(l.ctx, msg, fields...)
}

func (l *cronLogger) createFields(keysAndValues ...interface{}) []logger.Field {
	fields := make([]logger.Field, 0)

	var key string

	for _, v := range keysAndValues {
		children, ok := v.([]interface{})
		if !ok {
			continue
		}

		for i, vv := range children {
			if i%2 == 0 {
				key = fmt.Sprintf("%v", vv)

				continue
			}

			fields = append(fields, logger.F(key, vv))
		}
	}

	return fields
}
