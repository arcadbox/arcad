package command

import (
	"context"
	"crypto/subtle"
	"fmt"
	"io"
	"net"
	"os"
	"path/filepath"

	"forge.cadoles.com/wpetit/go-tunnel"

	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
	"gitlab.com/arcadbox/arcad/internal/command/bootstrap"
	"gitlab.com/arcadbox/arcad/internal/config"
	gateway "gitlab.com/arcadbox/arcad/internal/tunnel"
	gatewayServer "gitlab.com/arcadbox/arcad/internal/tunnel/server"
	"gitlab.com/wpetit/goweb/logger"
	"gitlab.com/wpetit/goweb/service"
)

func TunnelServerCommand() *cli.Command {
	return &cli.Command{
		Name:  "tunnel-server",
		Usage: "start the tunnel server",
		Action: func(ctx *cli.Context) error {
			conf, ctn, err := bootstrap.StartUp(ctx)
			if err != nil {
				return errors.WithStack(err)
			}

			// Configure service container
			err = bootstrap.ConfigureContainer(ctx.Context, ctn, conf,
				bootstrap.ConfigureConfigService,
				bootstrap.ConfigureSessionService,
				bootstrap.ConfigureTemplateService,
				configureTunnelServerService,
			)
			if err != nil {
				return errors.Wrap(err, "could not configure service container")
			}

			gateway, err := gatewayServer.From(ctn)
			if err != nil {
				return errors.WithStack(err)
			}

			// Start listening for tunnel clients
			logger.Info(ctx.Context, "listening for tunnel clients", logger.F("address", conf.GatewayServer.TunnelAddress))

			if err := gateway.Server().Listen(ctx.Context); err != nil {
				return errors.WithStack(err)
			}

			return nil
		},
	}
}

func configureTunnelServerService(ctx context.Context, conf *config.Config, ctn *service.Container) error {
	ctn.Provide(gatewayServer.ServiceName, gatewayServer.ServiceProvider(
		tunnel.WithServerAddress(conf.TunnelServer.TunnelAddress),
		tunnel.WithServerBlockCrypt(
			"salsa20",
			conf.TunnelServer.SharedSecret,
			conf.TunnelServer.SharedSalt,
			2048,
			32,
		),
		tunnel.WithServerOnClientAuth(createTunnelOnClientAuthHook(ctn, conf.TunnelServer)),
		tunnel.WithServerOnClientDisconnect(createTunnelOnClientDisconnectHook(ctn, conf.TunnelServer)),
	))

	return nil
}

func createTunnelOnClientAuthHook(ctn *service.Container, conf config.TunnelServerConfig) tunnel.OnClientAuthFunc {
	clientsByID := make(map[string]config.TunnelRemoteClientConfig)

	for _, c := range conf.Clients {
		clientsByID[c.ID] = c
	}

	return func(ctx context.Context, remoteClient *tunnel.RemoteClient, rawCredentials interface{}) (bool, error) {
		remoteAddr := remoteClient.RemoteAddr().String()

		logger.Info(ctx, "new tunnel client auth request", logger.F("remoteAddress", remoteAddr))

		credentials := &gateway.Credentials{}
		if err := mapstructure.Decode(rawCredentials, credentials); err != nil {
			if err != nil {
				return false, errors.Wrap(err, "could not decode credentials")
			}
		}

		if credentials.ID == "" {
			return false, nil
		}

		clientConf, exists := clientsByID[credentials.ID]
		if !exists {
			return false, nil
		}

		secretMatches := subtle.ConstantTimeCompare([]byte(clientConf.Secret), []byte(credentials.Secret)) != 1

		if !secretMatches {
			return false, nil
		}

		if err := startClientTunnel(ctx, credentials.ID, remoteClient, conf); err != nil {
			return false, errors.Wrap(err, "could not start client tunnel")
		}

		gateway := gatewayServer.Must(ctn)

		gateway.Clients().Add(credentials.ID, remoteAddr, remoteClient)

		return true, nil
	}
}

func createTunnelOnClientDisconnectHook(ctn *service.Container, conf config.TunnelServerConfig) tunnel.OnClientDisconnectFunc {
	return func(ctx context.Context, remoteClient *tunnel.RemoteClient) error {
		remoteAddr := remoteClient.RemoteAddr().String()

		logger.Info(ctx, "tunnel client disconnected", logger.F("remoteAddress", remoteAddr))

		gateway := gatewayServer.Must(ctn)

		gateway.Clients().RemoveByRemoteAddr(remoteAddr)

		return nil
	}
}

func startClientTunnel(ctx context.Context, clientID string, remoteClient *tunnel.RemoteClient, conf config.TunnelServerConfig) error {
	if err := os.MkdirAll(conf.SocketDir, os.ModePerm); err != nil {
		return errors.Wrapf(err, "could not create '%s' directory", conf.SocketDir)
	}

	connName := fmt.Sprintf(conf.SocketFilenamePattern, clientID)
	connPath := filepath.Join(conf.SocketDir, connName)

	if err := os.RemoveAll(connPath); err != nil {
		return errors.Wrapf(err, "could not delete unix socket '%s'", connPath)
	}

	l, err := net.Listen("unix", connPath)
	if err != nil {
		return errors.Wrapf(err, "could not listen on unix socket '%s'", connPath)
	}

	go func() {
		for {
			conn, err := l.Accept()
			if err != nil {
				logger.Fatal(ctx, "error while accepting socket conn", logger.E(errors.WithStack(err)))
			}

			if err := handleSocketConn(ctx, conn, remoteClient, conf); err != nil {
				logger.Fatal(ctx, "error while handling socket conn", logger.E(errors.WithStack(err)))
			}
		}
	}()

	return nil
}

func handleSocketConn(ctx context.Context, localConn net.Conn, remoteClient *tunnel.RemoteClient, conf config.TunnelServerConfig) error {
	remoteConn, err := remoteClient.Proxy(ctx, "tcp", conf.ProxyTargetAddress)
	if err != nil {
		return errors.Wrap(err, "could not proxy connection")
	}

	defer func() {
		if err := remoteConn.Close(); err != nil {
			logger.Error(ctx, "could not close remote connection", logger.E(errors.WithStack(err)))
		}

		if err := localConn.Close(); err != nil {
			logger.Error(ctx, "could not close local connection", logger.E(errors.WithStack(err)))
		}
	}()

	go func() {
		if _, err = tunnel.Copy(localConn, remoteConn); err != nil {
			if errors.Is(err, io.EOF) {
				return
			}

			logger.Error(ctx, "could not copy local connection to remote", logger.E(errors.WithStack(err)))
		}
	}()

	if _, err = tunnel.Copy(remoteConn, localConn); err != nil {
		if errors.Is(err, io.EOF) {
			return nil
		}

		return errors.WithStack(err)
	}

	return nil
}
