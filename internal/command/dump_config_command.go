package command

import (
	"os"

	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
	"gitlab.com/arcadbox/arcad/internal/command/bootstrap"
	"gitlab.com/arcadbox/arcad/internal/config"
	"gitlab.com/arcadbox/arcad/internal/hook"
	"gitlab.com/wpetit/goweb/middleware/container"
)

func DumpConfigCommand() *cli.Command {
	return &cli.Command{
		Name:  "dump-config",
		Usage: "Dump configuration to standard output",
		Action: func(ctx *cli.Context) error {
			conf, err := config.FromContext(ctx.Context)
			if err != nil {
				return errors.WithStack(err)
			}

			configFile := ctx.String("config")

			if configFile == "" {
				conf = config.NewDefault()
			}

			ctn, err := bootstrap.CreateBaseServiceContainer(ctx.Context, conf)
			if err != nil {
				return errors.Wrap(err, "could not create base service container")
			}

			containerCtx := container.WithContainer(ctx.Context, ctn)

			if err := hook.InvokeDumpConfigHook(containerCtx, conf); err != nil {
				return errors.Wrap(err, "could not dump plugins config")
			}

			if err := config.Dump(conf, os.Stdout); err != nil {
				return errors.Wrap(err, "could not dump config")
			}

			return nil
		},
	}
}
