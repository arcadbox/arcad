package cookie

import (
	"crypto/rand"

	"github.com/gorilla/sessions"
)

func NewStore(authenticationKey []byte, encryptionKey []byte, defaultOptions *sessions.Options) sessions.Store {
	store := sessions.NewCookieStore(authenticationKey, encryptionKey)
	store.Options = defaultOptions
	store.MaxAge(defaultOptions.MaxAge)

	return store
}

func GenerateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)

	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}

	return b, nil
}
