package jwt

import (
	"image/png"
	"io/ioutil"
	"os"
	"testing"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/arcadbox/arcad/internal/repository"
)

func TestSigningKeyGenerationAndLoad(t *testing.T) {
	t.Parallel()

	tmpFile, err := ioutil.TempFile("", "arcad-test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(tmpFile.Name())

	privateKeyPath := tmpFile.Name()

	srv := createService()

	if err := srv.GenerateSigningKey(privateKeyPath); err != nil {
		t.Fatal(err)
	}

	privateKeyData, err := ioutil.ReadFile(privateKeyPath)
	if err != nil {
		t.Error(err)
	}

	privateKey, err := jwt.ParseRSAPrivateKeyFromPEM(privateKeyData)
	if err != nil {
		t.Error(err)
	}

	if privateKey == nil {
		t.Error("privateKey should not be nil")
	}

	if err := srv.LoadSigningKey(privateKeyPath); err != nil {
		t.Error(err)
	}

	if srv.signingKey == nil {
		t.Error("srv.signingKey should not be nil")
	}
}

func TestMemberCardGenerationAndValidation(t *testing.T) {
	srv := createService()

	if err := srv.LoadSigningKey("./testdata/signing.pem"); err != nil {
		t.Fatal(err)
	}

	u := &repository.User{
		ID:       repository.UserID("test"),
		Nickname: "Test",
	}

	img, err := srv.GenerateCard(u)
	if err != nil {
		t.Error(err)
	}

	if img == nil {
		t.Error("img should not be nil")
	}

	imgBuff, err := srv.EncodeUserID(img, u.ID)
	if err != nil {
		t.Error(err)
	}

	if imgBuff == nil {
		t.Error("imgData should not be nil")
	}

	newImg, err := png.Decode(imgBuff)
	if err != nil {
		t.Error(err)
	}

	userID, err := srv.ValidateCard(newImg)
	if err != nil {
		t.Error(err)
	}

	if e, g := u.ID, userID; e != g {
		t.Errorf("userID: expected '%v', got '%v'", e, g)
	}
}

func createService() *Service {
	return NewService(
		"../../../cmd/arcad/asset/member-card.png",
		"../../../cmd/arcad/asset/member-card.ttf",
		TextLayout{
			FontSize:    48,
			AnchorX:     240,
			AnchorY:     240,
			LineSpacing: 10,
		},
	)
}
