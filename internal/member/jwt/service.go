package jwt

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"image"
	"io/ioutil"
	"os"
	"time"

	"github.com/fogleman/gg"
	"gopkg.in/auyer/steganography.v2"

	"github.com/pkg/errors"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/arcadbox/arcad/internal/repository"
)

const DefaultSigningKeySize = 2048

var (
	ErrEmptyMessage  = errors.New("empty message")
	ErrInvalidClaims = errors.New("invalid claims")
	ErrInvalidUserID = errors.New("invalid user id")
)

type Service struct {
	cardLayoutPath     string
	cardFontPath       string
	cardNicknameLayout TextLayout
	signingKey         *rsa.PrivateKey
}

type MemberClaims struct {
	UserID repository.UserID `json:"uid"`
	jwt.StandardClaims
}

func (s *Service) GenerateSigningKey(privateKeyPath string) error {
	privateKey, err := rsa.GenerateKey(rand.Reader, DefaultSigningKeySize)
	if err != nil {
		return errors.Wrap(err, "could not generate rsa key")
	}

	if err := privateKey.Validate(); err != nil {
		return errors.Wrap(err, "could not validate rsa key")
	}

	privDER := x509.MarshalPKCS1PrivateKey(privateKey)

	privBlock := &pem.Block{
		Type:    "RSA PRIVATE KEY",
		Headers: nil,
		Bytes:   privDER,
	}

	privateKeyFile, err := os.Create(privateKeyPath)
	if err != nil {
		return errors.Wrap(err, "could not create file")
	}
	defer privateKeyFile.Close()

	if err := pem.Encode(privateKeyFile, privBlock); err != nil {
		return errors.Wrap(err, "could not encode pem block")
	}

	return nil
}

func (s *Service) LoadSigningKey(privateKeyPath string) error {
	signingKeyBytes, err := ioutil.ReadFile(privateKeyPath)
	if err != nil {
		return errors.Wrap(err, "could not load signing key")
	}

	signingKey, err := jwt.ParseRSAPrivateKeyFromPEM(signingKeyBytes)
	if err != nil {
		return errors.Wrap(err, "could not parse ras pem")
	}

	s.signingKey = signingKey

	return nil
}

func (s *Service) GenerateCard(user *repository.User) (image.Image, error) {
	img, err := gg.LoadImage(s.cardLayoutPath)
	if err != nil {
		return nil, errors.Wrap(err, "could load member card layout")
	}

	ctx := gg.NewContextForImage(img)
	ctx.SetRGB(0, 0, 0)

	if err := ctx.LoadFontFace(s.cardFontPath, s.cardNicknameLayout.FontSize); err != nil {
		return nil, errors.Wrap(err, "could load member card font face")
	}

	rect := img.Bounds()

	ctx.DrawStringWrapped(
		user.Nickname,
		s.cardNicknameLayout.AnchorX, s.cardNicknameLayout.AnchorY,
		0.5, 0.5,
		float64(rect.Max.X),
		s.cardNicknameLayout.LineSpacing,
		gg.AlignCenter,
	)

	return ctx.Image(), nil
}

func (s *Service) EncodeUserID(img image.Image, id repository.UserID) (*bytes.Buffer, error) {
	data := new(bytes.Buffer)

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, MemberClaims{
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(876600 * time.Hour).Unix(), // now + 100 years
			IssuedAt:  time.Now().UTC().Unix(),
			NotBefore: time.Now().UTC().Unix(),
		},
		UserID: id,
	})

	tokenString, err := token.SignedString(s.signingKey)
	if err != nil {
		return nil, errors.Wrap(err, "could not sign jwt")
	}

	if err := steganography.Encode(data, img, []byte(tokenString)); err != nil {
		return nil, errors.Wrap(err, "could not hide jwt")
	}

	return data, nil
}

func (s *Service) ValidateCard(img image.Image) (repository.UserID, error) {
	messageSize := steganography.GetMessageSizeFromImage(img)

	message := steganography.Decode(messageSize, img)
	if message == nil {
		return "", ErrEmptyMessage
	}

	tokenString := string(message)

	token, err := jwt.ParseWithClaims(tokenString, &MemberClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, errors.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return s.signingKey.Public(), nil
	})
	if err != nil {
		return "", errors.Wrap(err, "could not parse jwt")
	}

	claims, ok := token.Claims.(*MemberClaims)
	if !ok || !token.Valid {
		return "", ErrInvalidClaims
	}

	if claims.UserID == "" {
		return "", ErrInvalidUserID
	}

	return claims.UserID, nil
}

func NewService(cardLayoutPath, cardFontPath string, cardNicknameLayout TextLayout) *Service {
	return &Service{
		cardLayoutPath:     cardLayoutPath,
		cardFontPath:       cardFontPath,
		cardNicknameLayout: cardNicknameLayout,
	}
}
