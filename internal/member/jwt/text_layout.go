package jwt

type TextLayout struct {
	FontSize    float64
	AnchorX     float64
	AnchorY     float64
	LineSpacing float64
}
