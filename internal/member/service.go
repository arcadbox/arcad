package member

import (
	"bytes"
	"image"

	"gitlab.com/arcadbox/arcad/internal/repository"

	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/service"
)

const (
	ServiceName service.Name = "member"
)

type Service interface {
	GenerateCard(user *repository.User) (image.Image, error)
	EncodeUserID(img image.Image, id repository.UserID) (*bytes.Buffer, error)
	ValidateCard(img image.Image) (repository.UserID, error)
}

func ServiceProvider(m Service) service.Provider {
	return func(ctn *service.Container) (interface{}, error) {
		return m, nil
	}
}

// From retrieves the member service in the given service container
func From(container *service.Container) (Service, error) {
	service, err := container.Service(ServiceName)
	if err != nil {
		return nil, errors.Wrapf(err, "error while retrieving '%s' service", ServiceName)
	}

	srv, ok := service.(Service)
	if !ok {
		return nil, errors.Errorf("retrieved service is not a valid '%s' service", ServiceName)
	}

	return srv, nil
}

// Must retrieves the member service in the given service container or panic otherwise
func Must(container *service.Container) Service {
	srv, err := From(container)
	if err != nil {
		panic(err)
	}

	return srv
}
