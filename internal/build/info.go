package build

type Info struct {
	ProjectVersion string
	GitRef         string
	BuildDate      string
}
