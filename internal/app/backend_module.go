package app

import (
	"github.com/dop251/goja"
	"gitlab.com/arcadbox/arcad/internal/repository"
)

type BackendModuleFactory func(repository.AppID, *Backend) BackendModule

type BackendModule interface {
	Name() string
	Export(*goja.Object)
}
