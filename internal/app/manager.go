package app

import (
	"io/ioutil"
	"os"
	"sync"

	"github.com/pkg/errors"

	"gitlab.com/arcadbox/arcad/internal/bundle"
	"gitlab.com/arcadbox/arcad/internal/repository"
)

var (
	ErrBundleNotFound = errors.New("bundle not found")
	ErrNoBackend      = errors.New("no backend")
)

type Manager struct {
	bundles  sync.Map
	backends sync.Map
}

func (m *Manager) GetBundle(id repository.AppID) (bundle.Bundle, error) {
	val, exists := m.bundles.Load(id)
	if !exists {
		return nil, ErrBundleNotFound
	}

	b, ok := val.(bundle.Bundle)
	if !ok {
		return nil, errors.New("unexpected bundle value")
	}

	return b, nil
}

func (m *Manager) SetBundle(id repository.AppID, b bundle.Bundle) {
	m.bundles.Store(id, b)
}

func (m *Manager) GetBackend(id repository.AppID, modules ...BackendModuleFactory) (*Backend, error) {
	rawBackend, exists := m.backends.Load(id)
	if exists {
		backend, ok := rawBackend.(*Backend)
		if !ok {
			return nil, errors.New("unexpected backend value")
		}

		return backend, nil
	}

	backend, err := m.loadBackend(id, modules...)
	if err != nil {
		return nil, err
	}

	m.backends.Store(id, backend)

	return backend, nil
}

func (m *Manager) loadBackend(id repository.AppID, modules ...BackendModuleFactory) (*Backend, error) {
	b, err := m.GetBundle(id)
	if err != nil {
		return nil, errors.Wrapf(err, "could not retrieve app '%s' bundle", id)
	}

	reader, _, err := b.File("backend/main.js")
	if err != nil {
		if os.IsNotExist(errors.Cause(err)) {
			return nil, ErrNoBackend
		}

		return nil, errors.Wrap(err, "could not open backend/main.js")
	}

	srcBytes, err := ioutil.ReadAll(reader)
	if err != nil {
		return nil, errors.Wrap(err, "could not read backend/main.js")
	}

	backend := NewBackend(id, modules...)

	if err := backend.Load(string(srcBytes)); err != nil {
		return nil, errors.Wrap(err, "could not load backend/main.js")
	}

	return backend, nil
}

func NewManager() *Manager {
	return &Manager{}
}
