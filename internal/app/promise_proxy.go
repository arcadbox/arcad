package app

import (
	"sync"

	"github.com/dop251/goja"
)

type PromiseProxy struct {
	*goja.Promise
	wg      sync.WaitGroup
	resolve func(result interface{})
	reject  func(reason interface{})
}

func (p *PromiseProxy) Resolve(result interface{}) {
	defer p.wg.Done()
	p.resolve(result)
}

func (p *PromiseProxy) Reject(reason interface{}) {
	defer p.wg.Done()
	p.resolve(reason)
}

func (p *PromiseProxy) Wait() {
	p.wg.Wait()
}

func NewPromiseProxy(promise *goja.Promise, resolve func(result interface{}), reject func(reason interface{})) *PromiseProxy {
	proxy := &PromiseProxy{
		Promise: promise,
		wg:      sync.WaitGroup{},
		resolve: resolve,
		reject:  reject,
	}

	proxy.wg.Add(1)

	return proxy
}
