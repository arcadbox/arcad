package app

import "github.com/pkg/errors"

var ErrUnknownBundleArchiveFormat = errors.New("unknown bundle archive format")
