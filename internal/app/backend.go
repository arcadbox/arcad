package app

import (
	"context"
	"math/rand"
	"sync"

	"gitlab.com/wpetit/goweb/logger"

	"gitlab.com/arcadbox/arcad/internal/repository"

	"github.com/dop251/goja"
	"github.com/dop251/goja_nodejs/eventloop"
	"github.com/pkg/errors"
)

var ErrFuncDoesNotExist = errors.New("function does not exist")

type Backend struct {
	appID   repository.AppID
	runtime *goja.Runtime
	loop    *eventloop.EventLoop
}

func (b *Backend) Load(src string) error {
	_, err := b.runtime.RunString(src)
	if err != nil {
		return errors.Wrap(err, "could not run js script")
	}

	return nil
}

func (b *Backend) OnUserConnect(id repository.UserID) error {
	if _, err := b.ExecFuncByName("onUserConnect", id); err != nil {
		if errors.Is(err, ErrFuncDoesNotExist) {
			logger.Warn(
				context.Background(),
				"onUserConnect() not found in backend script",
				logger.E(err),
			)

			return nil
		}

		return errors.Wrap(err, "could not execute onUserConnect() method")
	}

	return nil
}

func (b *Backend) OnUserDisconnect(id repository.UserID) error {
	if _, err := b.ExecFuncByName("onUserDisconnect", id); err != nil {
		if errors.Is(err, ErrFuncDoesNotExist) {
			logger.Warn(
				context.Background(),
				"onUserDisconnect() not found in backend script",
				logger.E(err),
			)

			return nil
		}

		return errors.Wrap(err, "could not execute onUserDisconnect() method")
	}

	return nil
}

func (b *Backend) OnUserMessage(id repository.UserID, data interface{}) error {
	if _, err := b.ExecFuncByName("onUserMessage", id, data); err != nil {
		if errors.Is(err, ErrFuncDoesNotExist) {
			logger.Warn(
				context.Background(),
				"onUserMessage() not found in backend script",
				logger.E(err),
			)

			return nil
		}

		return errors.Wrap(err, "could not execute onUserMessage() method")
	}

	return nil
}

func (b *Backend) OnInit() error {
	if _, err := b.ExecFuncByName("onInit"); err != nil {
		if errors.Is(err, ErrFuncDoesNotExist) {
			return nil
		}

		return errors.Wrap(err, "could not execute onInit() method")
	}

	return nil
}

func (b *Backend) ExecFuncByName(funcName string, args ...interface{}) (goja.Value, error) {
	callable, ok := goja.AssertFunction(b.runtime.Get(funcName))
	if !ok {
		return nil, errors.WithStack(ErrFuncDoesNotExist)
	}

	return b.Exec(callable, args...)
}

func (b *Backend) Exec(callable goja.Callable, args ...interface{}) (goja.Value, error) {
	var (
		wg    sync.WaitGroup
		value goja.Value
		err   error
	)

	wg.Add(1)

	b.loop.RunOnLoop(func(vm *goja.Runtime) {
		jsArgs := make([]goja.Value, 0, len(args))
		for _, a := range args {
			jsArgs = append(jsArgs, vm.ToValue(a))
		}

		value, err = callable(nil, jsArgs...)
		if err != nil {
			err = errors.WithStack(err)
		}

		wg.Done()
	})

	wg.Wait()

	return value, err
}

func (b *Backend) IsPromise(v goja.Value) (*goja.Promise, bool) {
	promise, ok := v.Export().(*goja.Promise)
	return promise, ok
}

func (b *Backend) WaitForPromise(promise *goja.Promise) goja.Value {
	var (
		wg    sync.WaitGroup
		value goja.Value
	)

	wg.Add(1)

	// Wait for promise completion
	go func() {
		for {
			var loopWait sync.WaitGroup
			loopWait.Add(1)

			breakLoop := false

			b.loop.RunOnLoop(func(vm *goja.Runtime) {
				defer loopWait.Done()

				if promise.State() == goja.PromiseStatePending {
					return
				}

				value = promise.Result()

				breakLoop = true
			})

			loopWait.Wait()

			if breakLoop {
				wg.Done()

				return
			}
		}
	}()

	wg.Wait()

	return value
}

func (b *Backend) NewPromise() *PromiseProxy {
	promise, resolve, reject := b.runtime.NewPromise()

	return NewPromiseProxy(promise, resolve, reject)
}

func (b *Backend) ToValue(v interface{}) goja.Value {
	return b.runtime.ToValue(v)
}

func (b *Backend) Start() {
	b.loop.Start()
}

func (b *Backend) Stop() {
	b.loop.Stop()
}

func (b *Backend) initRuntime(modules ...BackendModuleFactory) {
	runtime := goja.New()

	runtime.SetRandSource(createRandomSource())

	for _, moduleFactory := range modules {
		mod := moduleFactory(b.appID, b)
		export := runtime.NewObject()
		mod.Export(export)
		runtime.Set(mod.Name(), export)
	}

	b.runtime = runtime
}

func NewBackend(appID repository.AppID, modules ...BackendModuleFactory) *Backend {
	backend := &Backend{
		appID: appID,
		loop: eventloop.NewEventLoop(
			eventloop.EnableConsole(false),
		),
	}

	backend.initRuntime(modules...)

	return backend
}

func createRandomSource() goja.RandSource {
	rnd := rand.New(&cryptoSource{})
	return rnd.Float64
}
