package app

import (
	"github.com/pkg/errors"
	"gitlab.com/wpetit/goweb/service"
)

const (
	ServiceName service.Name = "app"
)

func ServiceProvider() service.Provider {
	manager := NewManager()
	return func(ctn *service.Container) (interface{}, error) {
		return manager, nil
	}
}

// From retrieves the app manager in the given service container
func From(container *service.Container) (*Manager, error) {
	service, err := container.Service(ServiceName)
	if err != nil {
		return nil, errors.Wrapf(err, "error while retrieving '%s' service", ServiceName)
	}

	srv, ok := service.(*Manager)
	if !ok {
		return nil, errors.Errorf("retrieved service is not a valid '%s' service", ServiceName)
	}

	return srv, nil
}

// Must retrieves the app manager in the given service container or panic otherwise
func Must(container *service.Container) *Manager {
	srv, err := From(container)
	if err != nil {
		panic(err)
	}

	return srv
}
