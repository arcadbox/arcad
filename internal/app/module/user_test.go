package module

import (
	"errors"
	"io/ioutil"
	"testing"

	"gitlab.com/arcadbox/arcad/internal/app"
	"gitlab.com/arcadbox/arcad/internal/repository"
)

func TestUserModuleGetUserByID(t *testing.T) {
	repo := &fakeUserRepository{}

	appID := repository.AppID("test")
	backend := app.NewBackend(appID,
		ConsoleModuleFactory(),
		UserModuleFactory(repo),
	)

	data, err := ioutil.ReadFile("testdata/user_getbyid.js")
	if err != nil {
		t.Fatal(err)
	}

	if err := backend.Load(string(data)); err != nil {
		t.Fatal(err)
	}

	backend.Start()
	defer backend.Stop()

	if err := backend.OnInit(); err != nil {
		t.Error(err)
	}
}

type fakeUserRepository struct{}

func (r *fakeUserRepository) Create() (*repository.User, error) {
	return nil, errors.New("not implemented")
}

func (r *fakeUserRepository) Save(user *repository.User) error {
	return errors.New("not implemented")
}

func (r *fakeUserRepository) Get(userID repository.UserID) (*repository.User, error) {
	if userID == "0" {
		return &repository.User{}, nil
	}

	return nil, errors.New("not implemented")
}

func (r *fakeUserRepository) Delete(userID repository.UserID) error {
	return errors.New("not implemented")
}

func (r *fakeUserRepository) Touch(userID repository.UserID, rawUserAgent string) error {
	return errors.New("not implemented")
}

func (r *fakeUserRepository) List() ([]*repository.User, error) {
	return nil, errors.New("not implemented")
}

func (r *fakeUserRepository) ListByID(userIDs ...repository.UserID) ([]*repository.User, error) {
	return nil, errors.New("not implemented")
}
