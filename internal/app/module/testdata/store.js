function onInit() {
  var obj = store.save("test", {"foo": "bar"});
  var obj1 = store.get("test", obj.id);

  console.log(obj, obj1);

  for (var key in obj) {
    if (!obj.hasOwnProperty(key)) {
      continue;
    }

    if (obj[key] !== obj1[key]) {
      throw new Error("obj['"+key+"'] !== obj1['"+key+"']");
    }
  }

  var results = store.query("test", { "eq": {"foo": "bar"} }, {"orderBy": "foo", "limit": 10, "skip": 0});

  if (!results || results.length !== 1) {
    throw new Error("results should contains 1 item");
  }

  store.delete("test", obj.id);

  var obj2 = store.get("test", obj.id);

  if (obj2 != null) {
    throw new Error("obj2 should be null");
  }
}