var called = false;
var before, after;

function onInit() {
  before = timer.now();
  var delay = 15;
  console.log("delaying onTimeout execution by", delay, "ms");
  timer.delay('onTimeout', delay, 'foo')
}

function onTimeout(firstArg) {
  after = timer.now();
  if (firstArg !== "foo") throw new Error("firstArg should be equal to 'foo'");
  console.log("onTimeout called");
  console.log("before", before);
  console.log("after", after);
  console.log("delta", after-before);
  called = true;
}

function onTimeoutCalled() {
  return called;
}