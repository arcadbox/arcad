package module

import (
	"time"

	"github.com/dop251/goja"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/app"
	"gitlab.com/arcadbox/arcad/internal/repository"
)

type TimerModule struct {
	backend *app.Backend
}

func (m *TimerModule) Name() string {
	return "timer"
}

func (m *TimerModule) now(call goja.FunctionCall) goja.Value {
	return m.backend.ToValue(time.Now().UnixNano())
}

func (m *TimerModule) delay(call goja.FunctionCall) goja.Value {
	funcName := call.Argument(0).String()
	if funcName == "" {
		panic(errors.New("First argument must be a function name"))
	}

	rawDelay := call.Argument(1).ToInteger()
	delay := time.Duration(rawDelay) * time.Millisecond

	args := make([]interface{}, 0)

	if len(call.Arguments) > 2 {
		for _, a := range call.Arguments[2:] {
			args = append(args, a.Export())
		}
	}

	time.AfterFunc(delay, func() {
		_, err := m.backend.ExecFuncByName(funcName, args...)
		if err != nil {
			panic(errors.Wrap(err, "error while executing timer.after() callback"))
		}
	})

	return nil
}

func (m *TimerModule) Export(export *goja.Object) {
	if err := export.Set("delay", m.delay); err != nil {
		panic(errors.Wrap(err, "could not set 'delay' function"))
	}

	if err := export.Set("now", m.now); err != nil {
		panic(errors.Wrap(err, "could not set 'now' function"))
	}
}

func TimerModuleFactory() app.BackendModuleFactory {
	return func(appID repository.AppID, backend *app.Backend) app.BackendModule {
		return &TimerModule{backend}
	}
}
