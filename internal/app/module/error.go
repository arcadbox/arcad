package module

import "github.com/pkg/errors"

var ErrUnexpectedArgumentsNumber = errors.New("unexpected number of arguments")
