package module

import (
	"context"
	"sync"

	"github.com/dop251/goja"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/app"
	"gitlab.com/arcadbox/arcad/internal/bus"
	"gitlab.com/arcadbox/arcad/internal/repository"
	"gitlab.com/wpetit/goweb/logger"
)

type AuthorizationModule struct {
	appID   repository.AppID
	bus     bus.Bus
	backend *app.Backend
	admins  sync.Map
}

func (m *AuthorizationModule) Name() string {
	return "authorization"
}

func (m *AuthorizationModule) Export(export *goja.Object) {
	if err := export.Set("isAdmin", m.isAdmin); err != nil {
		panic(errors.Wrap(err, "could not set 'register' function"))
	}
}

func (m *AuthorizationModule) isAdmin(call goja.FunctionCall) goja.Value {
	userID := call.Argument(0).String()
	if userID == "" {
		panic(errors.New("first argument must be a user id"))
	}

	rawValue, exists := m.admins.Load(repository.UserID(userID))
	if !exists {
		return m.backend.ToValue(false)
	}

	isAdmin, ok := rawValue.(bool)
	if !ok {
		return m.backend.ToValue(false)
	}

	return m.backend.ToValue(isAdmin)
}

func (m *AuthorizationModule) handleEvents() {
	ctx := logger.With(context.Background(), logger.F("moduleAppID", m.appID))

	ns := AppMessageNamespace(m.appID)

	userConnectedMessages, err := m.bus.Subscribe(ctx, ns, MessageTypeUserConnected)
	if err != nil {
		panic(errors.WithStack(err))
	}

	userDisconnectedMessages, err := m.bus.Subscribe(ctx, ns, MessageTypeUserDisconnected)
	if err != nil {
		panic(errors.WithStack(err))
	}

	defer func() {
		m.bus.Unsubscribe(ctx, ns, MessageTypeUserConnected, userConnectedMessages)
		m.bus.Unsubscribe(ctx, ns, MessageTypeUserDisconnected, userDisconnectedMessages)
	}()

	for {
		select {
		case msg := <-userConnectedMessages:
			userConnectedMsg, ok := msg.(*MessageUserConnected)
			if !ok {
				continue
			}

			logger.Debug(ctx, "user connected", logger.F("msg", userConnectedMsg))

			m.admins.Store(userConnectedMsg.UserID, userConnectedMsg.IsAdmin)

		case msg := <-userDisconnectedMessages:
			userDisconnectedMsg, ok := msg.(*MessageUserDisconnected)
			if !ok {
				continue
			}

			logger.Debug(ctx, "user disconnected", logger.F("msg", userDisconnectedMsg))

			m.admins.Delete(userDisconnectedMsg.UserID)
		}
	}
}

func AuthorizationModuleFactory(b bus.Bus) app.BackendModuleFactory {
	return func(appID repository.AppID, backend *app.Backend) app.BackendModule {
		mod := &AuthorizationModule{
			appID:   appID,
			bus:     b,
			backend: backend,
			admins:  sync.Map{},
		}

		go mod.handleEvents()

		return mod
	}
}
