package module

import (
	"gitlab.com/arcadbox/arcad/internal/bus"
	"gitlab.com/arcadbox/arcad/internal/repository"
)

const (
	MessageTypeFrontendMessage  bus.MessageType = "frontendMessage"
	MessageTypeBackendMessage   bus.MessageType = "backendMessage"
	MessageTypeUserConnected    bus.MessageType = "userConnected"
	MessageTypeUserDisconnected bus.MessageType = "userDisconnected"
)

type MessageUserConnected struct {
	UserID  repository.UserID
	IsAdmin bool

	ns bus.MessageNamespace
}

func (m *MessageUserConnected) MessageNamespace() bus.MessageNamespace {
	return m.ns
}

func (m *MessageUserConnected) MessageType() bus.MessageType {
	return MessageTypeUserConnected
}

func NewMessageUserConnected(appID repository.AppID, userID repository.UserID, isAdmin bool) *MessageUserConnected {
	ns := createAppMessageNamespace(appID)

	return &MessageUserConnected{userID, isAdmin, ns}
}

type MessageUserDisconnected struct {
	UserID repository.UserID
	ns     bus.MessageNamespace
}

func (m *MessageUserDisconnected) MessageNamespace() bus.MessageNamespace {
	return m.ns
}

func (m *MessageUserDisconnected) MessageType() bus.MessageType {
	return MessageTypeUserDisconnected
}

func NewMessageUserDisconnected(appID repository.AppID, userID repository.UserID) *MessageUserDisconnected {
	ns := createAppMessageNamespace(appID)

	return &MessageUserDisconnected{userID, ns}
}

type MessageBackendMessage struct {
	UserID repository.UserID
	Data   interface{}

	ns bus.MessageNamespace
}

func (m *MessageBackendMessage) MessageNamespace() bus.MessageNamespace {
	return m.ns
}

func (m *MessageBackendMessage) MessageType() bus.MessageType {
	return MessageTypeBackendMessage
}

func NewMessageBackendMessage(appID repository.AppID, userID repository.UserID, data interface{}) *MessageBackendMessage {
	ns := createAppMessageNamespace(appID)

	return &MessageBackendMessage{userID, data, ns}
}

type MessageFrontendMessage struct {
	UserID repository.UserID
	Data   map[string]interface{}

	ns bus.MessageNamespace
}

func (m *MessageFrontendMessage) MessageNamespace() bus.MessageNamespace {
	return m.ns
}

func (m *MessageFrontendMessage) MessageType() bus.MessageType {
	return MessageTypeFrontendMessage
}

func NewMessageFrontedMessage(appID repository.AppID, userID repository.UserID, data map[string]interface{}) *MessageFrontendMessage {
	ns := createAppMessageNamespace(appID)

	return &MessageFrontendMessage{userID, data, ns}
}

func createAppMessageNamespace(appID repository.AppID) bus.MessageNamespace {
	return bus.NewMessageNamespace(MessageNamespace, bus.MessageNamespace(appID))
}
