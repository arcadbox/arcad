package filter

type GtOperator struct {
	fields map[string]interface{}
}

func (o *GtOperator) Token() string {
	return TokenGt
}

func (o *GtOperator) ToSQL() (string, []interface{}, error) {
	return fieldsToSQL(">", false, o.fields)
}

func NewGtOperator(fields OperatorFields) *GtOperator {
	return &GtOperator{fields}
}
