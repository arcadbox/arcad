package filter

import (
	"encoding/json"
	"fmt"
	"testing"
)

type (
	op   map[string]interface{}
	aggr []interface{}
)

type testCase struct {
	RawFilter    string
	ExpectedSQL  string
	ExpectedArgs []interface{}
}

var testCases = []testCase{
	{
		RawFilter: `
			{
				"or": [
					{"eq": {"foo": "bar"}},
					{"neq": {"hello": "world"}}
				]
			}
		`,
		ExpectedSQL:  "(((foo = ?) OR (hello != ?)))",
		ExpectedArgs: []interface{}{"bar", "world"},
	},
}

func TestFilter(t *testing.T) {
	t.Parallel()

	for i, tc := range testCases {
		func(tc testCase) {
			t.Run(fmt.Sprintf("Test case #%d", i), func(t *testing.T) {
				t.Parallel()

				raw := make(map[string]interface{})
				if err := json.Unmarshal([]byte(tc.RawFilter), &raw); err != nil {
					t.Fatal(err)
				}

				filter, err := NewFilterFrom(raw)
				if err != nil {
					t.Fatal(err)
				}

				sql, args, err := filter.ToSQL()
				if err != nil {
					t.Error(err)
				}

				if e, g := tc.ExpectedSQL, sql; e != g {
					t.Errorf("sql: expected '%s', got '%s'", e, g)
				}

				if args == nil {
					t.Fatal("args should not be nil")
				}

				for i, a := range args {
					if i >= len(tc.ExpectedArgs) {
						t.Errorf("args[%d]: expected nil, got '%v'", i, a)
						continue
					}

					if e, g := tc.ExpectedArgs[i], a; e != g {
						t.Errorf("args[%d]: expected '%v', got '%v'", i, e, g)
					}
				}

				for i, a := range tc.ExpectedArgs {
					if i >= len(args) {
						t.Errorf("args[%d]: expected '%v', got nil", i, a)
					}
				}
			})
		}(tc)
	}
}
