package filter

import (
	"github.com/pkg/errors"
)

type NotOperator struct {
	children []Operator
}

func (o *NotOperator) Token() string {
	return TokenOr
}

func (o *NotOperator) ToSQL() (string, []interface{}, error) {
	sql, args, err := aggregatorToSQL("AND", o.children...)
	if err != nil {
		return "", nil, errors.WithStack(err)
	}

	return "NOT " + sql, args, nil
}

func NewNotOperator(ops ...Operator) *NotOperator {
	return &NotOperator{ops}
}
