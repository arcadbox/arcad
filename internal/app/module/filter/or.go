package filter

type OrOperator struct {
	children []Operator
}

func (o *OrOperator) Token() string {
	return TokenOr
}

func (o *OrOperator) ToSQL() (string, []interface{}, error) {
	return aggregatorToSQL("OR", o.children...)
}

func NewOrOperator(ops ...Operator) *OrOperator {
	return &OrOperator{ops}
}
