package filter

import (
	"strings"

	"github.com/pkg/errors"
)

func aggregatorToSQL(operator string, children ...Operator) (string, []interface{}, error) {
	var sb strings.Builder

	if _, err := sb.WriteString("("); err != nil {
		return "", nil, errors.WithStack(err)
	}

	args := make([]interface{}, 0)

	for i, c := range children {
		if i != 0 {
			if _, err := sb.WriteString(" " + operator + " "); err != nil {
				return "", nil, errors.WithStack(err)
			}
		}

		cSQL, cArgs, err := c.ToSQL()
		if err != nil {
			return "", nil, errors.WithStack(err)
		}

		args = append(args, cArgs...)

		if _, err := sb.WriteString(cSQL); err != nil {
			return "", nil, errors.WithStack(err)
		}
	}

	if _, err := sb.WriteString(")"); err != nil {
		return "", nil, errors.WithStack(err)
	}

	return sb.String(), args, nil
}

func fieldsToSQL(operator string, invert bool, fields map[string]interface{}) (string, []interface{}, error) {
	var sb strings.Builder

	args := make([]interface{}, 0)

	i := 0
	for k, v := range fields {
		if i != 0 {
			if _, err := sb.WriteString(" AND "); err != nil {
				return "", nil, errors.WithStack(err)
			}
		}

		if invert {
			if _, err := sb.WriteString("?"); err != nil {
				return "", nil, errors.WithStack(err)
			}
		} else {
			if _, err := sb.WriteString(k); err != nil {
				return "", nil, errors.WithStack(err)
			}
		}

		if _, err := sb.WriteString(" "); err != nil {
			return "", nil, errors.WithStack(err)
		}

		if _, err := sb.WriteString(operator); err != nil {
			return "", nil, errors.WithStack(err)
		}

		if invert {
			if _, err := sb.WriteString(" "); err != nil {
				return "", nil, errors.WithStack(err)
			}

			if _, err := sb.WriteString(k); err != nil {
				return "", nil, errors.WithStack(err)
			}
		} else {
			if _, err := sb.WriteString(" ?"); err != nil {
				return "", nil, errors.WithStack(err)
			}
		}

		args = append(args, v)

		i++
	}

	return sb.String(), args, nil
}
