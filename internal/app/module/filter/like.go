package filter

type LikeOperator struct {
	fields map[string]interface{}
}

func (o *LikeOperator) Token() string {
	return TokenLike
}

func (o *LikeOperator) ToSQL() (string, []interface{}, error) {
	return fieldsToSQL("LIKE", false, o.fields)
}

func NewLikeOperator(fields OperatorFields) *LikeOperator {
	return &LikeOperator{fields}
}
