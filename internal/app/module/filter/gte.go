package filter

type GteOperator struct {
	fields OperatorFields
}

func (o *GteOperator) Token() string {
	return TokenGt
}

func (o *GteOperator) ToSQL() (string, []interface{}, error) {
	return fieldsToSQL(">=", false, o.fields)
}

func NewGteOperator(fields OperatorFields) *GteOperator {
	return &GteOperator{fields}
}
