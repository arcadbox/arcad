package filter

type LteOperator struct {
	fields map[string]interface{}
}

func (o *LteOperator) Token() string {
	return TokenLt
}

func (o *LteOperator) ToSQL() (string, []interface{}, error) {
	return fieldsToSQL("<=", false, o.fields)
}

func NewLteOperator(fields OperatorFields) *LteOperator {
	return &LteOperator{fields}
}
