package filter

const (
	TokenAnd  = "and"
	TokenOr   = "or"
	TokenNot  = "not"
	TokenEq   = "eq"
	TokenNeq  = "neq"
	TokenGt   = "gt"
	TokenGte  = "gte"
	TokenLt   = "lt"
	TokenLte  = "lte"
	TokenIn   = "in"
	TokenLike = "like"
)

type OperatorFields map[string]interface{}

type Operator interface {
	ToSQL() (string, []interface{}, error)
	Token() string
}
