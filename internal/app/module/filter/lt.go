package filter

type LtOperator struct {
	fields map[string]interface{}
}

func (o *LtOperator) Token() string {
	return TokenLt
}

func (o *LtOperator) ToSQL() (string, []interface{}, error) {
	return fieldsToSQL("<", false, o.fields)
}

func NewLtOperator(fields OperatorFields) *LtOperator {
	return &LtOperator{fields}
}
