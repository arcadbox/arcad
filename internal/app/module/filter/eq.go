package filter

type EqOperator struct {
	fields map[string]interface{}
}

func (o *EqOperator) Token() string {
	return TokenEq
}

func (o *EqOperator) ToSQL() (string, []interface{}, error) {
	return fieldsToSQL("=", false, o.fields)
}

func NewEqOperator(fields map[string]interface{}) *EqOperator {
	return &EqOperator{fields}
}
