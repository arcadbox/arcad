package filter

type NeqOperator struct {
	fields map[string]interface{}
}

func (o *NeqOperator) Token() string {
	return TokenNeq
}

func (o *NeqOperator) ToSQL() (string, []interface{}, error) {
	return fieldsToSQL("!=", false, o.fields)
}

func NewNeqOperator(fields map[string]interface{}) *NeqOperator {
	return &NeqOperator{fields}
}
