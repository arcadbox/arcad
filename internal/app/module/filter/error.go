package filter

import "errors"

var (
	ErrInvalidFieldOperator       = errors.New("invalid field operator")
	ErrInvalidAggregationOperator = errors.New("invalid aggregation operator")
	ErrInvalidFieldMap            = errors.New("invalid field map")
	ErrUnknownOperator            = errors.New("uknown operator")
	ErrInvalidRoot                = errors.New("invalid root")
)
