package filter

type InOperator struct {
	fields map[string]interface{}
}

func (o *InOperator) Token() string {
	return TokenIn
}

func (o *InOperator) ToSQL() (string, []interface{}, error) {
	return fieldsToSQL("IN", true, o.fields)
}

func NewInOperator(fields OperatorFields) *InOperator {
	return &InOperator{fields}
}
