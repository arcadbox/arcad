package filter

type AndOperator struct {
	children []Operator
}

func (o *AndOperator) Token() string {
	return TokenAnd
}

func (o *AndOperator) ToSQL() (string, []interface{}, error) {
	return aggregatorToSQL("AND", o.children...)
}

func NewAndOperator(ops ...Operator) *AndOperator {
	return &AndOperator{ops}
}
