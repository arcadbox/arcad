package module

import (
	"io"
	"mime/multipart"

	"github.com/google/uuid"
	"gitlab.com/arcadbox/arcad/internal/bus"
	"gitlab.com/arcadbox/arcad/internal/repository"
)

const (
	MessageTypeUploadRequest    bus.MessageType = "uploadRequest"
	MessageTypeUploadResponse   bus.MessageType = "uploadResponse"
	MessageTypeDownloadRequest  bus.MessageType = "downloadRequest"
	MessageTypeDownloadResponse bus.MessageType = "downloadResponse"
)

type MessageUploadRequest struct {
	AppID     repository.AppID
	UserID    repository.UserID
	RequestID string
	Header    *multipart.FileHeader
	File      multipart.File
	Metadata  map[string]interface{}

	ns bus.MessageNamespace
}

func (m *MessageUploadRequest) MessageNamespace() bus.MessageNamespace {
	return m.ns
}

func (m *MessageUploadRequest) MessageType() bus.MessageType {
	return MessageTypeUploadRequest
}

func NewMessageUploadRequest(appID repository.AppID, userID repository.UserID, header *multipart.FileHeader, file multipart.File, metadata map[string]interface{}) *MessageUploadRequest {
	return &MessageUploadRequest{
		AppID:     appID,
		UserID:    userID,
		RequestID: uuid.New().String(),
		Header:    header,
		File:      file,
		Metadata:  metadata,

		ns: AppMessageNamespace(appID),
	}
}

type MessageUploadResponse struct {
	AppID     repository.AppID
	UserID    repository.UserID
	RequestID string
	FileID    string
	Allow     bool

	ns bus.MessageNamespace
}

func (m *MessageUploadResponse) MessageNamespace() bus.MessageNamespace {
	return m.ns
}

func (m *MessageUploadResponse) MessageType() bus.MessageType {
	return MessageTypeUploadResponse
}

func NewMessageUploadResponse(appID repository.AppID, userID repository.UserID, requestID string) *MessageUploadResponse {
	return &MessageUploadResponse{
		AppID:     appID,
		UserID:    userID,
		RequestID: requestID,

		ns: AppMessageNamespace(appID),
	}
}

type MessageDownloadRequest struct {
	AppID     repository.AppID
	UserID    repository.UserID
	RequestID string
	FileID    string

	ns bus.MessageNamespace
}

func (m *MessageDownloadRequest) MessageNamespace() bus.MessageNamespace {
	return m.ns
}

func (m *MessageDownloadRequest) MessageType() bus.MessageType {
	return MessageTypeDownloadRequest
}

func NewMessageDownloadRequest(appID repository.AppID, userID repository.UserID, fileID string) *MessageDownloadRequest {
	return &MessageDownloadRequest{
		AppID:     appID,
		UserID:    userID,
		RequestID: uuid.New().String(),
		FileID:    fileID,
		ns:        AppMessageNamespace(appID),
	}
}

type MessageDownloadResponse struct {
	AppID       repository.AppID
	UserID      repository.UserID
	RequestID   string
	Allow       bool
	File        io.ReadCloser
	ContentType string
	Filename    string
	Size        int64

	ns bus.MessageNamespace
}

func (m *MessageDownloadResponse) MessageNamespace() bus.MessageNamespace {
	return m.ns
}

func (e *MessageDownloadResponse) MessageType() bus.MessageType {
	return MessageTypeDownloadResponse
}

func NewMessageDownloadResponse(appID repository.AppID, userID repository.UserID, requestID string) *MessageDownloadResponse {
	return &MessageDownloadResponse{
		AppID:     appID,
		UserID:    userID,
		RequestID: requestID,

		ns: AppMessageNamespace(appID),
	}
}
