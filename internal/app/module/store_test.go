package module

import (
	"io/ioutil"
	"testing"

	"cdr.dev/slog"
	"gitlab.com/arcadbox/arcad/internal/app"
	"gitlab.com/arcadbox/arcad/internal/repository"
	"gitlab.com/wpetit/goweb/logger"
)

func TestStoreModule(t *testing.T) {
	logger.SetLevel(slog.LevelDebug)

	appID := repository.AppID("test")

	backend := app.NewBackend(appID,
		ConsoleModuleFactory(),
		StoreModuleFactory(":memory:"),
	)

	data, err := ioutil.ReadFile("testdata/store.js")
	if err != nil {
		t.Fatal(err)
	}

	if err := backend.Load(string(data)); err != nil {
		t.Fatal(err)
	}

	backend.Start()
	defer backend.Stop()

	if err := backend.OnInit(); err != nil {
		t.Error(err)
	}
}
