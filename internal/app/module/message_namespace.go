package module

import (
	"gitlab.com/arcadbox/arcad/internal/bus"
	"gitlab.com/arcadbox/arcad/internal/repository"
)

const (
	MessageNamespace bus.MessageNamespace = "module"
)

func AppMessageNamespace(appID repository.AppID) bus.MessageNamespace {
	return bus.NewMessageNamespace(MessageNamespace, bus.MessageNamespace(appID))
}
