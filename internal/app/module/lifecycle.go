package module

import (
	"context"

	"github.com/dop251/goja"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/app"
	"gitlab.com/arcadbox/arcad/internal/bus"
	"gitlab.com/arcadbox/arcad/internal/repository"
	"gitlab.com/wpetit/goweb/logger"
)

type LifecycleModule struct {
	ctx     context.Context
	bus     bus.Bus
	appID   repository.AppID
	backend *app.Backend
}

func (m *LifecycleModule) Name() string {
	return "lifecycle"
}

func (m *LifecycleModule) Export(export *goja.Object) {
}

func (m *LifecycleModule) handleMessages() {
	logger.Debug(
		m.ctx,
		"subscribing to bus messages",
	)

	ns := AppMessageNamespace(m.appID)

	userConnectedMessages, err := m.bus.Subscribe(m.ctx, ns, MessageTypeUserConnected)
	if err != nil {
		panic(errors.WithStack(err))
	}

	userDisconnectedMessages, err := m.bus.Subscribe(m.ctx, ns, MessageTypeUserDisconnected)
	if err != nil {
		panic(errors.WithStack(err))
	}

	frontendMessageMessages, err := m.bus.Subscribe(m.ctx, ns, MessageTypeFrontendMessage)
	if err != nil {
		panic(errors.WithStack(err))
	}

	defer func() {
		logger.Debug(
			m.ctx,
			"unsubscribing from bus messages",
		)

		m.bus.Unsubscribe(m.ctx, ns, MessageTypeFrontendMessage, frontendMessageMessages)
		m.bus.Unsubscribe(m.ctx, ns, MessageTypeUserDisconnected, userDisconnectedMessages)
		m.bus.Unsubscribe(m.ctx, ns, MessageTypeUserConnected, userConnectedMessages)
	}()

	for {
		logger.Debug(
			m.ctx,
			"waiting for next message",
		)
		select {
		case <-m.ctx.Done():
			logger.Debug(
				m.ctx,
				"context done",
			)
			return

		case msg := <-userConnectedMessages:
			userConnected, ok := msg.(*MessageUserConnected)
			if !ok {
				logger.Error(
					m.ctx,
					"unexpected message type",
					logger.F("message", msg),
				)

				continue
			}

			logger.Debug(
				m.ctx,
				"received user connected message",
				logger.F("message", userConnected),
			)

			// Invoke OnUserMessage hook
			if err := m.backend.OnUserConnect(userConnected.UserID); err != nil {
				logger.Error(
					m.ctx,
					"on user connected error",
					logger.E(err),
				)

				continue
			}

		case msg := <-userDisconnectedMessages:
			userDisconnected, ok := msg.(*MessageUserDisconnected)
			if !ok {
				logger.Error(
					m.ctx,
					"unexpected message type",
					logger.F("message", msg),
				)

				continue
			}

			logger.Debug(
				m.ctx,
				"received user disconnected message",
				logger.F("message", userDisconnected),
			)

			// Invoke OnUserDisconnect hook
			if err := m.backend.OnUserDisconnect(userDisconnected.UserID); err != nil {
				logger.Error(
					m.ctx,
					"on user disconnected error",
					logger.E(err),
				)

				continue
			}

		case msg := <-frontendMessageMessages:
			frontendMessage, ok := msg.(*MessageFrontendMessage)
			if !ok {
				logger.Error(
					m.ctx,
					"unexpected message type",
					logger.F("message", msg),
				)

				continue
			}

			logger.Debug(
				m.ctx,
				"received frontend message",
				logger.F("message", frontendMessage),
			)

			// Invoke OnUserMessage hook
			if err := m.backend.OnUserMessage(frontendMessage.UserID, frontendMessage.Data); err != nil {
				logger.Error(
					m.ctx,
					"on user message error",
					logger.E(err),
				)

				continue
			}
		}
	}
}

func LifecycleModuleFactory(bus bus.Bus) app.BackendModuleFactory {
	return func(appID repository.AppID, backend *app.Backend) app.BackendModule {
		module := &LifecycleModule{
			appID:   appID,
			backend: backend,
			bus:     bus,
			ctx: logger.With(
				context.Background(),
				logger.F("appID", appID),
			),
		}

		go module.handleMessages()

		return module
	}
}
