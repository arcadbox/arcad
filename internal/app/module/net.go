package module

import (
	"context"

	"github.com/dop251/goja"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/app"
	"gitlab.com/arcadbox/arcad/internal/bus"
	"gitlab.com/arcadbox/arcad/internal/repository"
)

type NetModule struct {
	appID   repository.AppID
	bus     bus.Bus
	backend *app.Backend
}

func (m *NetModule) Name() string {
	return "net"
}

func (m *NetModule) Export(export *goja.Object) {
	if err := export.Set("broadcast", m.broadcast); err != nil {
		panic(errors.Wrap(err, "could not set 'broadcast' function"))
	}

	if err := export.Set("send", m.send); err != nil {
		panic(errors.Wrap(err, "could not set 'send' function"))
	}
}

func (m *NetModule) broadcast(call goja.FunctionCall) goja.Value {
	if len(call.Arguments) < 1 {
		panic(m.backend.ToValue("invalid number of argument"))
	}

	data := call.Arguments[0].Export()

	msg := NewMessageBackendMessage(m.appID, "", data)
	if err := m.bus.Publish(context.Background(), msg); err != nil {
		panic(errors.WithStack(err))
	}

	return nil
}

func (m *NetModule) send(call goja.FunctionCall) goja.Value {
	if len(call.Arguments) < 2 {
		panic(m.backend.ToValue("invalid number of argument"))
	}

	userID := repository.UserID(call.Arguments[0].String())
	data := call.Arguments[1].Export()

	msg := NewMessageBackendMessage(m.appID, userID, data)
	if err := m.bus.Publish(context.Background(), msg); err != nil {
		panic(errors.WithStack(err))
	}

	return nil
}

func NetModuleFactory(b bus.Bus) app.BackendModuleFactory {
	return func(appID repository.AppID, backend *app.Backend) app.BackendModule {
		return &NetModule{appID, b, backend}
	}
}
