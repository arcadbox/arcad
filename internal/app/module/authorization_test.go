package module

import (
	"context"
	"io/ioutil"
	"testing"
	"time"

	"gitlab.com/arcadbox/arcad/internal/app"
	"gitlab.com/arcadbox/arcad/internal/bus/memory"

	"gitlab.com/arcadbox/arcad/internal/repository"
)

func TestAuthorizationModule(t *testing.T) {
	t.Parallel()

	testAppID := repository.AppID("test-app")
	testUserID := repository.UserID("test-user-id")

	b := memory.NewBus()

	backend := app.NewBackend(testAppID,
		ConsoleModuleFactory(),
		AuthorizationModuleFactory(b),
	)

	data, err := ioutil.ReadFile("testdata/authorization.js")
	if err != nil {
		t.Fatal(err)
	}

	if err := backend.Load(string(data)); err != nil {
		t.Fatal(err)
	}

	backend.Start()
	defer backend.Stop()

	if err := backend.OnInit(); err != nil {
		t.Error(err)
	}

	// Test non connected user

	retValue, err := backend.ExecFuncByName("isAdmin", testUserID)
	if err != nil {
		t.Error(err)
	}

	isAdmin := retValue.ToBoolean()

	if e, g := false, isAdmin; e != g {
		t.Errorf("isAdmin: expected '%v', got '%v'", e, g)
	}

	// Test user connection as normal user

	ctx := context.Background()

	b.Publish(ctx, NewMessageUserConnected(testAppID, testUserID, false))
	time.Sleep(2 * time.Second)

	retValue, err = backend.ExecFuncByName("isAdmin", testUserID)
	if err != nil {
		t.Error(err)
	}

	isAdmin = retValue.ToBoolean()

	if e, g := false, isAdmin; e != g {
		t.Errorf("isAdmin: expected '%v', got '%v'", e, g)
	}

	// Test user connection as admin

	b.Publish(ctx, NewMessageUserConnected(testAppID, testUserID, true))
	time.Sleep(2 * time.Second)

	retValue, err = backend.ExecFuncByName("isAdmin", testUserID)
	if err != nil {
		t.Error(err)
	}

	isAdmin = retValue.ToBoolean()

	if e, g := true, isAdmin; e != g {
		t.Errorf("isAdmin: expected '%v', got '%v'", e, g)
	}

	// Test user disconnection

	b.Publish(ctx, NewMessageUserDisconnected(testAppID, testUserID))
	time.Sleep(2 * time.Second)

	retValue, err = backend.ExecFuncByName("isAdmin", testUserID)
	if err != nil {
		t.Error(err)
	}

	isAdmin = retValue.ToBoolean()

	if e, g := false, isAdmin; e != g {
		t.Errorf("isAdmin: expected '%v', got '%v'", e, g)
	}
}
