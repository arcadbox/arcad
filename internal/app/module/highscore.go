package module

import (
	"context"

	"github.com/dop251/goja"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/app"
	"gitlab.com/arcadbox/arcad/internal/repository"
	"gitlab.com/wpetit/goweb/logger"
)

type HighscoreModule struct {
	ctx     context.Context
	appID   repository.AppID
	repo    *repository.Repository
	backend *app.Backend
}

func (m *HighscoreModule) Name() string {
	return "highscore"
}

func (m *HighscoreModule) Export(export *goja.Object) {
	if err := export.Set("add", m.add); err != nil {
		panic(errors.Wrap(err, "could not set 'add' function"))
	}

	if err := export.Set("list", m.list); err != nil {
		panic(errors.Wrap(err, "could not set 'list' function"))
	}

	if err := export.Set("userList", m.userList); err != nil {
		panic(errors.Wrap(err, "could not set 'userList' function"))
	}
}

func (m *HighscoreModule) list(call goja.FunctionCall) goja.Value {
	if len(call.Arguments) != 0 {
		panic(errors.New("invalid number of arguments"))
	}

	highscores, err := m.repo.Highscore().FindByAppID(m.appID)
	if err != nil {
		if err == repository.ErrNotFound {
			highscores = make([]*repository.Highscore, 0)
		} else {
			err = errors.Wrap(err, "could not list highscores")
			logger.Error(m.ctx, "could not list highscores", logger.E(err))
			panic(errors.WithStack(err))
		}
	}

	values := make([]goja.Value, 0, len(highscores))
	for _, h := range highscores {
		values = append(values, m.backend.ToValue(h))
	}

	return m.backend.ToValue(values)
}

func (m *HighscoreModule) add(call goja.FunctionCall) goja.Value {
	if len(call.Arguments) < 2 {
		panic(m.backend.ToValue("invalid number of arguments"))
	}

	userID := repository.UserID(call.Arguments[0].String())
	score := call.Arguments[1].ToInteger()

	highscore, err := m.repo.Highscore().Create(m.appID, userID, score)
	if err != nil {
		err = errors.Wrap(err, "could not save highscore")
		logger.Error(m.ctx, "could not save highscore", logger.E(err))
		panic(m.backend.ToValue(err.Error()))
	}

	return m.backend.ToValue(highscore)
}

func (m *HighscoreModule) userList(call goja.FunctionCall) goja.Value {
	if len(call.Arguments) < 1 {
		panic(m.backend.ToValue("invalid number of arguments"))
	}

	userID := repository.UserID(call.Arguments[0].String())

	highscores, err := m.repo.Highscore().FindByAppID(m.appID, userID)
	if err != nil {
		if err == repository.ErrNotFound {
			highscores = make([]*repository.Highscore, 0)
		} else {
			err = errors.Wrap(err, "could not list user highscores")
			logger.Error(m.ctx, "could not list user highscores", logger.E(err))

			panic(m.backend.ToValue(err.Error()))
		}
	}

	values := make([]goja.Value, 0, len(highscores))
	for _, h := range highscores {
		values = append(values, m.backend.ToValue(h))
	}

	return m.backend.ToValue(values)
}

func HighscoreModuleFactory(repo *repository.Repository) app.BackendModuleFactory {
	return func(appID repository.AppID, backend *app.Backend) app.BackendModule {
		return &HighscoreModule{
			appID:   appID,
			repo:    repo,
			backend: backend,
			ctx: logger.With(
				context.Background(),
				logger.F("appID", appID),
			),
		}
	}
}
