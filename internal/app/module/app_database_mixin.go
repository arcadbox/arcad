package module

import (
	"fmt"
	"os"
	"path/filepath"
	"sync"

	"github.com/genjidb/genji"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/repository"
)

const (
	inMemory = ":memory:"
)

type AppDatabaseMixin struct {
	dbName  string
	db      *genji.DB
	mutex   sync.RWMutex
	dataDir string
	appID   repository.AppID
}

func (m *AppDatabaseMixin) openDatabase() (*genji.DB, error) {
	m.mutex.RLock()
	if m.db != nil {
		defer m.mutex.RUnlock()

		return m.db, nil
	}
	m.mutex.RUnlock()

	m.mutex.Lock()
	defer m.mutex.Unlock()

	var dbPath string

	if m.dataDir != inMemory {
		dbDirPath := filepath.Join(m.dataDir, string(m.appID))

		if err := os.MkdirAll(dbDirPath, 0755); err != nil && !os.IsExist(err) {
			return nil, errors.WithStack(err)
		}

		dbPath = filepath.Join(dbDirPath, m.dbName)
	} else {
		dbPath = inMemory
	}

	db, err := genji.Open(
		dbPath,
	)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	m.db = db

	return db, nil
}

func (m *AppDatabaseMixin) WithCollectionTx(collection string, h func(tx *genji.Tx) error) error {
	db, err := m.openDatabase()
	if err != nil {
		return errors.WithStack(err)
	}

	tx, err := db.Begin(true)
	if err != nil {
		return errors.WithStack(err)
	}

	createTableQuery := fmt.Sprintf("CREATE TABLE IF NOT EXISTS `%s` (id TEXT PRIMARY KEY NOT NULL);", collection)
	if err := tx.Exec(createTableQuery); err != nil {
		return errors.WithStack(err)
	}

	defer func() {
		if err := tx.Rollback(); err != nil {
			panic(errors.Wrap(err, "could not rollback transaction"))
		}
	}()

	if err := h(tx); err != nil {
		return errors.WithStack(err)
	}

	if err := tx.Commit(); err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func NewAppDatabaseMixin(dataDir string, dbName string, appID repository.AppID) *AppDatabaseMixin {
	return &AppDatabaseMixin{
		dbName:  dbName,
		dataDir: dataDir,
		appID:   appID,
	}
}
