package module

import (
	"io/ioutil"
	"testing"
	"time"

	"gitlab.com/arcadbox/arcad/internal/app"

	"gitlab.com/arcadbox/arcad/internal/repository"
)

func TestTimerModuleAfter(t *testing.T) {
	appID := repository.AppID("test")
	backend := app.NewBackend(appID,
		ConsoleModuleFactory(),
		TimerModuleFactory(),
	)

	data, err := ioutil.ReadFile("testdata/timer_delay.js")
	if err != nil {
		t.Fatal(err)
	}

	if err := backend.Load(string(data)); err != nil {
		t.Fatal(err)
	}

	backend.Start()
	defer backend.Stop()

	if err := backend.OnInit(); err != nil {
		t.Error(err)
	}

	// Wait for 30ms
	timer := time.NewTimer(30 * time.Millisecond)
	<-timer.C

	result, err := backend.ExecFuncByName("onTimeoutCalled")
	if err != nil {
		t.Error(err)
	}

	onTimeoutCalled := result.ToBoolean()

	if e, g := true, onTimeoutCalled; e != g {
		t.Errorf("timeoutCalled: expected '%v', got '%v'", e, g)
	}
}
