package module

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/wpetit/goweb/logger"

	"github.com/dop251/goja"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/app"
	"gitlab.com/arcadbox/arcad/internal/repository"
)

type ConsoleModule struct {
	ctx context.Context
}

func (m *ConsoleModule) Name() string {
	return "console"
}

func (m *ConsoleModule) log(call goja.FunctionCall) goja.Value {
	var sb strings.Builder

	for _, arg := range call.Arguments {
		sb.WriteString(fmt.Sprintf("%+v", arg.Export()))
		sb.WriteString(" ")
	}

	logger.Debug(m.ctx, sb.String())

	return nil
}

func (m *ConsoleModule) Export(export *goja.Object) {
	if err := export.Set("log", m.log); err != nil {
		panic(errors.Wrap(err, "could not set 'log' function"))
	}
}

func ConsoleModuleFactory() app.BackendModuleFactory {
	return func(appID repository.AppID, backend *app.Backend) app.BackendModule {
		return &ConsoleModule{
			ctx: logger.With(
				context.Background(),
				logger.F("appID", appID),
			),
		}
	}
}
