package module

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"

	"github.com/dop251/goja"
	"github.com/genjidb/genji"
	"github.com/genjidb/genji/database"
	"github.com/genjidb/genji/document"
	"github.com/mitchellh/mapstructure"
	"github.com/pborman/uuid"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/app"
	"gitlab.com/arcadbox/arcad/internal/app/module/filter"
	"gitlab.com/arcadbox/arcad/internal/repository"
	"gitlab.com/wpetit/goweb/logger"
)

const storeKeyID = "id"

type StoreModule struct {
	appID   repository.AppID
	backend *app.Backend
	db      *genji.DB
	mutex   sync.RWMutex
	dataDir string
}

func (m *StoreModule) Name() string {
	return "store"
}

func (m *StoreModule) Export(export *goja.Object) {
	if err := export.Set("save", m.save); err != nil {
		panic(errors.Wrap(err, "could not set 'save' function"))
	}

	if err := export.Set("get", m.get); err != nil {
		panic(errors.Wrap(err, "could not set 'get' function"))
	}

	if err := export.Set("query", m.query); err != nil {
		panic(errors.Wrap(err, "could not set 'query' function"))
	}

	if err := export.Set("delete", m.delete); err != nil {
		panic(errors.Wrap(err, "could not set 'delete' function"))
	}
}

func (m *StoreModule) save(call goja.FunctionCall) goja.Value {
	if len(call.Arguments) < 2 {
		panic(m.backend.ToValue("invalid number of arguments"))
	}

	collection := call.Arguments[0].ToString().String()

	doc, ok := call.Arguments[1].Export().(map[string]interface{})
	if !ok {
		m.logAndPanic("unexpected data format", nil)

		return nil
	}

	if doc == nil {
		doc = make(map[string]interface{})
	}

	err := m.WithCollectionTx(collection, func(tx *genji.Tx) error {
		docID, hasID := doc[storeKeyID].(string)

		insert := func(newID string) error {
			doc[storeKeyID] = newID

			logger.Debug(
				context.Background(),
				"inserting document",
				logger.F("collection", collection),
				logger.F("doc", doc),
			)

			insertQuery := fmt.Sprintf("INSERT INTO `%s` VALUES ?;", collection)
			if err := tx.Exec(insertQuery, &doc); err != nil {
				return errors.WithStack(err)
			}

			return nil
		}

		if !hasID || docID == "" {
			if err := insert(uuid.New()); err != nil {
				return errors.WithStack(err)
			}
		} else {
			existsQuery := fmt.Sprintf("SELECT %s FROM `%s` WHERE %s = ?", storeKeyID, collection, storeKeyID)
			existsResult, err := tx.Query(existsQuery, doc[storeKeyID])
			if err != nil {
				return errors.WithStack(err)
			}

			defer existsResult.Close()

			found, err := existsResult.Count()
			if err != nil {
				return errors.WithStack(err)
			}

			logger.Debug(
				context.Background(),
				"searched document for update",
				logger.F("found", found),
				logger.F("collection", collection),
				logger.F("id", docID),
			)

			if found == 0 {
				if err := insert(docID); err != nil {
					return errors.WithStack(err)
				}
			} else {
				logger.Debug(
					context.Background(),
					"updating document",
					logger.F("collection", collection),
					logger.F("doc", doc),
				)

				updateQuery, args, err := m.createUpdateQuery(collection, doc)
				if err != nil {
					return errors.WithStack(err)
				}

				if err := tx.Exec(updateQuery, args...); err != nil {
					return errors.WithStack(err)
				}
			}
		}

		return nil
	})
	if err != nil {
		m.logAndPanic("could not execute transaction", err)

		return nil
	}

	return m.backend.ToValue(doc)
}

func (m *StoreModule) createUpdateQuery(collection string, doc map[string]interface{}) (string, []interface{}, error) {
	var sb strings.Builder

	if _, err := sb.WriteString("UPDATE "); err != nil {
		return "", nil, errors.WithStack(err)
	}

	if _, err := sb.WriteString("`" + collection + "`"); err != nil {
		return "", nil, errors.WithStack(err)
	}

	if _, err := sb.WriteString(" SET "); err != nil {
		return "", nil, errors.WithStack(err)
	}

	docID := doc[storeKeyID]
	args := make([]interface{}, 0, len(doc))
	first := true

	for key, value := range doc {
		if !first {
			if _, err := sb.WriteString(", "); err != nil {
				return "", nil, errors.WithStack(err)
			}
		}

		if _, err := sb.WriteString("`" + key + "`"); err != nil {
			return "", nil, errors.WithStack(err)
		}

		if _, err := sb.WriteString(" = ?"); err != nil {
			return "", nil, errors.WithStack(err)
		}

		args = append(args, value)

		first = false
	}

	if _, err := sb.WriteString(" WHERE id = ?"); err != nil {
		return "", nil, errors.WithStack(err)
	}

	args = append(args, docID)

	return sb.String(), args, nil
}

func (m *StoreModule) get(call goja.FunctionCall) goja.Value {
	if len(call.Arguments) < 2 {
		m.logAndPanic("invalid number of arguments", nil)

		return nil
	}

	collection := call.Arguments[0].ToString().String()
	recordID := call.Arguments[1].ToString().String()

	var doc map[string]interface{}

	err := m.WithCollectionTx(collection, func(tx *genji.Tx) error {
		insertQuery := fmt.Sprintf("SELECT * FROM `%s` WHERE id = ?;", collection)

		result, err := tx.QueryDocument(insertQuery, recordID)
		if err != nil {
			if errors.Is(err, database.ErrDocumentNotFound) {
				return nil
			}

			return errors.WithStack(err)
		}

		doc = make(map[string]interface{})

		err = result.Iterate(func(field string, value document.Value) error {
			doc[field] = value.V

			return nil
		})
		if err != nil {
			return errors.WithStack(err)
		}

		return nil
	})
	if err != nil {
		m.logAndPanic("could not execute transaction", err)

		return nil
	}

	if doc == nil {
		return nil
	}

	return m.backend.ToValue(doc)
}

type queryOptions struct {
	Limit   *int64  `mapstructure:"limit"`
	Skip    *int64  `mapstructure:"skip"`
	OrderBy *string `mapstructure:"orderBy"`
}

func (m *StoreModule) query(call goja.FunctionCall) goja.Value {
	if len(call.Arguments) < 1 {
		m.logAndPanic("invalid number of arguments", nil)

		return nil
	}

	collection := call.Arguments[0].ToString().String()

	var (
		rawQuery map[string]interface{}
		ok       bool
	)

	if len(call.Arguments) > 1 && call.Arguments[1].Export() != nil {
		rawQuery, ok = call.Arguments[1].Export().(map[string]interface{})
		if !ok {
			m.logAndPanic("invalid query format", nil)

			return nil
		}
	}

	options := &queryOptions{}

	if len(call.Arguments) > 2 && call.Arguments[2].Export() != nil {
		rawOptions, ok := call.Arguments[2].Export().(map[string]interface{})
		if !ok {
			m.logAndPanic("invalid options format", nil)

			return nil
		}

		if err := mapstructure.Decode(rawOptions, options); err != nil {
			m.logAndPanic("invalid options format", err)

			return nil
		}
	}

	var (
		sql  string
		args []interface{}
	)

	if rawQuery != nil {
		filter, err := filter.NewFilterFrom(rawQuery)
		if err != nil {
			m.logAndPanic("invalid filter", err)

			return nil
		}

		sql, args, err = filter.ToSQL()
		if err != nil {
			m.logAndPanic("invalid filter", err)

			return nil
		}

		sql = "WHERE " + sql
	}

	var extra strings.Builder

	if options.OrderBy != nil {
		if _, err := extra.WriteString(" ORDER BY "); err != nil {
			m.logAndPanic("could not build sql", err)

			return nil
		}

		if _, err := extra.WriteString(*options.OrderBy); err != nil {
			m.logAndPanic("could not build sql", err)

			return nil
		}
	}

	if options.Limit != nil {
		if _, err := extra.WriteString(" LIMIT "); err != nil {
			m.logAndPanic("could not build sql", err)

			return nil
		}

		if _, err := extra.WriteString(strconv.FormatInt(*options.Limit, 10)); err != nil {
			m.logAndPanic("could not build sql", err)

			return nil
		}
	}

	if options.Skip != nil {
		if _, err := extra.WriteString(" OFFSET "); err != nil {
			m.logAndPanic("could not build sql", err)

			return nil
		}

		if _, err := extra.WriteString(strconv.FormatInt(*options.Skip, 10)); err != nil {
			m.logAndPanic("could not build sql", err)

			return nil
		}
	}

	var items []map[string]interface{}

	err := m.WithCollectionTx(collection, func(tx *genji.Tx) error {
		selectQuery := fmt.Sprintf("SELECT * FROM `%s` %s %s", collection, sql, extra.String())

		logger.Debug(
			context.Background(), "exec query",
			logger.F("appID", m.appID),
			logger.F("query", selectQuery),
			logger.F("args", args),
		)

		result, err := tx.Query(selectQuery, args...)
		if err != nil {
			return errors.WithStack(err)
		}

		defer result.Close()

		items = make([]map[string]interface{}, 0, result.RowsAffected)

		err = result.Iterate(func(d document.Document) error {
			var m map[string]interface{}

			if err := document.MapScan(d, &m); err != nil {
				return errors.WithStack(err)
			}

			items = append(items, m)

			return nil
		})
		if err != nil {
			m.logAndPanic("could not iterate results", err)

			return nil
		}

		return nil
	})
	if err != nil {
		m.logAndPanic("could not execute transaction", err)

		return nil
	}

	return m.backend.ToValue(items)
}

func (m *StoreModule) delete(call goja.FunctionCall) goja.Value {
	if len(call.Arguments) < 2 {
		m.logAndPanic("invalid number of arguments", nil)

		return nil
	}

	collection := call.Arguments[0].ToString().String()
	recordID := call.Arguments[1].ToString().String()

	err := m.WithCollectionTx(collection, func(tx *genji.Tx) error {
		deleteQuery := fmt.Sprintf("DELETE FROM `%s` WHERE id = ?;", collection)
		if err := tx.Exec(deleteQuery, recordID); err != nil {
			return errors.WithStack(err)
		}

		return nil
	})
	if err != nil {
		m.logAndPanic("could not execute transaction", err)

		return nil
	}

	return nil
}

func (m *StoreModule) openDatabase() (*genji.DB, error) {
	m.mutex.RLock()

	if m.db != nil {
		defer m.mutex.RUnlock()

		return m.db, nil
	}

	m.mutex.RUnlock()

	m.mutex.Lock()
	defer m.mutex.Unlock()

	var dbPath string

	if m.dataDir != ":memory:" {
		dbDirPath := filepath.Join(m.dataDir, string(m.appID))

		if err := os.MkdirAll(dbDirPath, 0755); err != nil && !os.IsExist(err) {
			return nil, errors.WithStack(err)
		}

		dbPath = filepath.Join(dbDirPath, "store.db")
	} else {
		dbPath = ":memory:"
	}

	db, err := genji.Open(
		dbPath,
	)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	m.db = db

	return db, nil
}

func (m *StoreModule) WithCollectionTx(collection string, h func(tx *genji.Tx) error) error {
	db, err := m.openDatabase()
	if err != nil {
		return errors.WithStack(err)
	}

	tx, err := db.Begin(true)
	if err != nil {
		return errors.WithStack(err)
	}

	createTableQuery := fmt.Sprintf("CREATE TABLE IF NOT EXISTS `%s` (id TEXT PRIMARY KEY NOT NULL);", collection)
	if err := tx.Exec(createTableQuery); err != nil {
		return errors.WithStack(err)
	}

	defer func() {
		if err := tx.Rollback(); err != nil {
			m.logAndPanic("could not rollback transaction", err)
		}
	}()

	if err := h(tx); err != nil {
		return errors.WithStack(err)
	}

	if err := tx.Commit(); err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func (m *StoreModule) logAndPanic(msg string, err error) {
	err = errors.Wrap(err, msg)
	logger.Error(context.Background(), msg, logger.E(err))
	panic(m.backend.ToValue(err.Error()))
}

func StoreModuleFactory(dataDir string) app.BackendModuleFactory {
	return func(appID repository.AppID, backend *app.Backend) app.BackendModule {
		return &StoreModule{
			appID:   appID,
			backend: backend,
			dataDir: dataDir,
		}
	}
}
