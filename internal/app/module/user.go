package module

import (
	"context"

	"github.com/dop251/goja"
	"github.com/pkg/errors"
	"gitlab.com/arcadbox/arcad/internal/app"
	"gitlab.com/arcadbox/arcad/internal/repository"
	"gitlab.com/wpetit/goweb/logger"
)

type UserModule struct {
	appID   repository.AppID
	repo    repository.UserRepository
	backend *app.Backend
	ctx     context.Context
}

func (m *UserModule) Name() string {
	return "user"
}

func (m *UserModule) Export(export *goja.Object) {
	if err := export.Set("getUserById", m.getUserByID); err != nil {
		panic(errors.Wrap(err, "could not set 'getUserById' function"))
	}
}

func (m *UserModule) getUserByID(call goja.FunctionCall) goja.Value {
	if len(call.Arguments) != 1 {
		panic(m.backend.ToValue("invalid number of arguments"))
	}

	userID := repository.UserID(call.Arguments[0].String())

	user, err := m.repo.Get(userID)
	if err != nil {
		err = errors.Wrapf(err, "could not find user '%s'", userID)
		logger.Error(m.ctx, "could not find user", logger.E(err), logger.F("userID", userID))
		panic(m.backend.ToValue(err))
	}

	return m.backend.ToValue(user)
}

func UserModuleFactory(repo repository.UserRepository) app.BackendModuleFactory {
	return func(appID repository.AppID, backend *app.Backend) app.BackendModule {
		return &UserModule{
			appID:   appID,
			repo:    repo,
			backend: backend,
			ctx: logger.With(
				context.Background(),
				logger.F("appID", appID),
			),
		}
	}
}
