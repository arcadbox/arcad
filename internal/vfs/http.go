package vfs

import (
	"net/http"
	"os"

	"github.com/spf13/afero"
)

// HTTPDir serves the files in the given afero.Fs or
// uses the given Handler to handles missing files.
func HTTPDir(fs afero.Fs, stripPrefix string, notFound http.Handler) http.HandlerFunc {
	httpFs := afero.NewHttpFs(fs)
	root := httpFs.Dir("/")
	fileServer := http.FileServer(root)

	fn := func(w http.ResponseWriter, r *http.Request) {
		if _, err := fs.Stat(r.RequestURI); os.IsNotExist(err) {
			notFound.ServeHTTP(w, r)
			return
		}

		fileServer.ServeHTTP(w, r)
	}

	handler := http.StripPrefix(stripPrefix, http.HandlerFunc(fn))

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		handler.ServeHTTP(w, r)
	})
}
