package vfs

import (
	"path/filepath"

	"github.com/spf13/afero"
	"gitlab.com/wpetit/goweb/template/html"
)

type TemplateLoader struct {
	fs afero.Fs
}

func (l *TemplateLoader) Load(srv *html.TemplateService) error {
	blockFiles, err := afero.Glob(l.fs, filepath.Join("blocks", "*.tmpl"))
	if err != nil {
		return err
	}

	for _, f := range blockFiles {
		blockContent, err := afero.ReadFile(l.fs, f)
		if err != nil {
			return err
		}

		blockName := filepath.Base(f)
		srv.AddBlock(blockName, string(blockContent))
	}

	layoutFiles, err := afero.Glob(l.fs, filepath.Join("layouts", "*.tmpl"))
	if err != nil {
		return err
	}

	// Generate our templates map from our layouts/ and blocks/ directories
	for _, f := range layoutFiles {
		templateData, err := afero.ReadFile(l.fs, f)
		if err != nil {
			return err
		}

		templateName := filepath.Base(f)

		if err := srv.LoadLayout(templateName, string(templateData)); err != nil {
			return err
		}
	}

	return nil
}

func NewTemplateLoader(fs afero.Fs) *TemplateLoader {
	return &TemplateLoader{fs}
}
