package vfs

import (
	"github.com/spf13/afero"
)

// NewLayeredFilesystem returns a "chrooted" and layered filesystem abstraction.
func NewLayeredFilesystem(dirs ...string) afero.Fs {
	var (
		overlay afero.Fs
		base    afero.Fs
	)

	for _, d := range dirs {
		overlay = afero.NewBasePathFs(afero.NewOsFs(), d)

		if base != nil {
			base = afero.NewCopyOnWriteFs(base, overlay)
		} else {
			base = overlay
		}
	}

	return base
}
