package config

import (
	"bytes"
	"io"
	"io/ioutil"
	"path/filepath"
	"text/template"
	"time"

	"github.com/Masterminds/sprig"
	"github.com/pkg/errors"

	"gitlab.com/arcadbox/arcad/internal/market/client"
	"gitlab.com/wpetit/goweb/logger"

	"github.com/caarlos0/env/v6"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Debug         bool                              `yaml:"debug" env:"ARCAD_DEBUG"`
	HTTP          HTTPConfig                        `yaml:"http"`
	Contact       ContactConfig                     `yaml:"contact"`
	App           AppConfig                         `yaml:"app"`
	Data          DataConfig                        `yaml:"data"`
	MemberCard    MemberCardConfig                  `yaml:"memberCard"`
	Job           JobConfig                         `yaml:"job"`
	Log           LogConfig                         `yaml:"log"`
	Sentry        SentryConfig                      `yaml:"sentry"`
	Theme         ThemeConfig                       `yaml:"theme"`
	ClockSync     ClockSyncConfig                   `yaml:"clockSync"`
	CaptivePortal CaptivePortalConfig               `yaml:"captivePortal"`
	GatewayServer GatewayServerConfig               `yaml:"gatewayServer"`
	GatewayClient GatewayClientConfig               `yaml:"gatewayClient"`
	TunnelServer  TunnelServerConfig                `yaml:"tunnelServer"`
	TunnelClient  TunnelClientConfig                `yaml:"tunnelClient"`
	MarketClient  MarketClientConfig                `yaml:"marketClient"`
	Plugins       map[string]map[string]interface{} `yaml:"plugins"`
}

type HTTPConfig struct {
	Address                 string `yaml:"address" env:"ARCAD_HTTP_ADDRESS"`
	TemplateDir             string `yaml:"templateDir" env:"ARCAD_HTTP_TEMPLATE_DIR"`
	PublicDir               string `yaml:"publicDir" env:"ARCAD_HTTP_PUBLIC_DIR"`
	CookieAuthenticationKey string `yaml:"cookieAuthenticationKey" env:"ARCAD_HTTP_COOKIE_AUTHENTICATION_KEY"`
	CookieEncryptionKey     string `yaml:"cookieEncryptionKey" env:"ARCAD_HTTP_COOKIE_ENCRYPTION_KEY"`
	CookieMaxAge            int    `yaml:"cookieMaxAge" env:"ARCAD_HTTP_COOKIE_MAX_AGE"`
}

type ContactConfig struct {
	Email   string `yaml:"email" env:"ARCAD_CONTACT_EMAIL"`
	Subject string `yaml:"subject" env:"ARCAD_CONTACT_SUBJECT"`
}

type DataConfig struct {
	DBPath string `yaml:"dbPath" env:"ARCAD_DATA_DB_PATH"`
}

type AppConfig struct {
	SearchPatterns    []string `yaml:"searchPatterns" env:"ARCAD_APP_SEARCH_PATTERNS"`
	DataDir           string   `yaml:"dataDir" env:"ARCAD_APP_DATA_DIR"`
	MaxFileUploadSize int64    `yaml:"maxFileUploadSize" env:"ARCAD_APP_MAX_FILE_UPLOAD_SIZE"`
}

type MemberCardConfig struct {
	SigningKeyPath      string  `yaml:"signingKeyPath" env:"ARCAD_MEMBER_CARD_SIGNING_KEY_PATH"`
	LayoutPath          string  `yaml:"layoutPath" env:"ARCAD_MEMBER_CARD_LAYOUT_PATH"`
	FontPath            string  `yaml:"fontPath" env:"ARCAD_MEMBER_CARD_FONT_PATH"`
	NicknameFontSize    float64 `yaml:"nicknameFontSize" env:"ARCAD_MEMBER_CARD_NICKNAME_FONT_SIZE"`
	NicknameAnchorX     float64 `yaml:"nicknameAnchorX" env:"ARCAD_MEMBER_CARD_NICKNAME_ANCHOR_X"`
	NicknameAnchorY     float64 `yaml:"nicknameAnchorY" env:"ARCAD_MEMBER_CARD_NICKNAME_ANCHOR_Y"`
	NicknameLineSpacing float64 `yaml:"nicknameLineSpacing" env:"ARCAD_MEMBER_CARD_NICKNAME_LINE_SPACING"`
}

type JobConfig struct {
	CleanupSpec string `yaml:"cleanupSpec" env:"ARCAD_JOB_CLEANUP_SPEC"`
}

type LogConfig struct {
	Format logger.Format `yaml:"format" env:"ARCAD_LOG_FORMAT"`
	Level  logger.Level  `yaml:"level" env:"ARCAD_LOG_LEVEL"`
}

type SentryConfig struct {
	DSN             string  `yaml:"dsn" env:"ARCAD_SENTRY_DSN"`
	Environment     string  `yaml:"environment" env:"ARCAD_SENTRY_ENVIRONMENT"`
	TraceSampleRate float64 `yaml:"traceSampleRate" env:"ARCAD_SENTRY_TRACE_SAMPLE_RATE"`
}

type ThemeConfig struct {
	Path string `yaml:"path" env:"ARCAD_THEME_PATH"`
}

type ClockSyncConfig struct {
	Enabled bool     `yaml:"enabled" env:"ARCAD_CLOCK_SYNC_ENABLED"`
	Format  string   `yaml:"format" env:"ARCAD_CLOCK_SYNC_FORMAT"`
	Command []string `yaml:"command" env:"ARCAD_CLOCK_SYNC_COMMAND"`
}

type CaptivePortalConfig struct {
	Enabled     bool               `yaml:"enabled" env:"ARCAD_CAPTIVE_PORTAL_ENABLED"`
	RedirectURL string             `yaml:"redirectUrl" env:"ARCAD_CAPTIVE_PORTAL_REDIRECT_URL"`
	ARPWatcher  []ARPWatcherConfig `yaml:"arpWatchers"`
}

type ARPWatcherConfig struct {
	RouterIP        string        `yaml:"routerIp"`
	RouterNetwork   string        `yaml:"routerNetwork"`
	HostIP          string        `yaml:"hostIp"`
	NIC             string        `yaml:"nic"`
	OfflineDeadline time.Duration `yaml:"offlineDeadline"`
	ProbeInterval   time.Duration `yaml:"probeInterval"`
	PurgeDeadline   time.Duration `yaml:"purgeDeadline"`
}

type GatewayServerConfig struct {
	TunnelAddress  string                      `yaml:"tunnelAddress" env:"ARCAD_GATEWAY_SERVER_TUNNEL_ADDRESS"`
	HTTPAddress    string                      `yaml:"httpAddress" env:"ARCAD_GATEWAY_SERVER_HTTP_ADDRESS"`
	SharedSecret   string                      `yaml:"sharedSecret" env:"ARCAD_GATEWAY_SERVER_SHARED_SECRET"`
	SharedSalt     string                      `yaml:"sharedSalt" env:"ARCAD_GATEWAY_SERVER_SHARED_SALT"`
	ProxyTargetURL string                      `yaml:"proxyTargetUrl" env:"ARCAD_GATEWAY_SERVER_PROXY_TARGET_URL"`
	MatchRegExp    string                      `yaml:"matchRegExp" env:"ARCAD_GATEWAY_SERVER_MATCH_REGEXP"`
	Cache          GatewayCacheConfig          `yaml:"cache"`
	Clients        []GatewayRemoteClientConfig `yaml:"clients"`
}

type GatewayRemoteClientConfig struct {
	ID     string `yaml:"id"`
	Secret string `yaml:"secret"`
	Match  string `yaml:"match"`
}

type GatewayCacheConfig struct {
	Enabled        bool          `yaml:"enabled" env:"ARCAD_GATEWAY_SERVER_CACHE_ENABLED"`
	MaxSize        int64         `yaml:"maxSize" env:"ARCAD_GATEWAY_SERVER_CACHE_MAX_SIZE"`
	TTL            time.Duration `yaml:"ttl" env:"ARCAD_GATEWAY_SERVER_CACHE_TTL"`
	RefreshTimeout time.Duration `yaml:"refreshTimeout" env:"ARCAD_GATEWAY_SERVER_CACHE_REFRESH_TIMEOUT"`
}

type GatewayClientConfig struct {
	ServerAddress    string        `yaml:"serverAddress" env:"ARCAD_GATEWAY_CLIENT_SERVER_ADDRESS"`
	ID               string        `yaml:"id" env:"ARCAD_GATEWAY_CLIENT_ID"`
	Secret           string        `yaml:"secret" env:"ARCAD_GATEWAY_CLIENT_SECRET"`
	SharedSecret     string        `yaml:"sharedSecret" env:"ARCAD_GATEWAY_CLIENT_SHARED_SECRET"`
	SharedSalt       string        `yaml:"sharedSalt" env:"ARCAD_GATEWAY_CLIENT_SHARED_SALT"`
	BackoffBaseDelay time.Duration `yaml:"backoffBaseDelay" env:"ARCAD_GATEWAY_CLIENT_BACKOFF_BASE_DELAY"`
	BackoffFactor    float32       `yaml:"backoffFactor" env:"ARCAD_GATEWAY_CLIENT_BACKOFF_FACTOR"`
}

type TunnelServerConfig struct {
	TunnelAddress         string                     `yaml:"tunnelAddress" env:"ARCAD_TUNNEL_SERVER_TUNNEL_ADDRESS"`
	SharedSecret          string                     `yaml:"sharedSecret" env:"ARCAD_TUNNEL_SERVER_SHARED_SECRET"`
	SharedSalt            string                     `yaml:"sharedSalt" env:"ARCAD_TUNNEL_SERVER_SHARED_SALT"`
	ProxyTargetAddress    string                     `yaml:"proxyTargetAddress" env:"ARCAD_TUNNEL_SERVER_PROXY_TARGET_ADDRESS"`
	Clients               []TunnelRemoteClientConfig `yaml:"clients"`
	SocketDir             string                     `yaml:"socketDir" env:"ARCAD_TUNNEL_SERVER_SOCKET_DIR"`
	SocketFilenamePattern string                     `yaml:"socketFilenamePattern" env:"ARCAD_TUNNEL_SERVER_SOCKET_FILENAME_PATTERN"`
}

type TunnelRemoteClientConfig struct {
	ID     string `yaml:"id"`
	Secret string `yaml:"secret"`
}

type TunnelClientConfig struct {
	ServerAddress    string        `yaml:"serverAddress" env:"ARCAD_TUNNEL_CLIENT_SERVER_ADDRESS"`
	ID               string        `yaml:"id" env:"ARCAD_TUNNEL_CLIENT_ID"`
	Secret           string        `yaml:"secret" env:"ARCAD_TUNNEL_CLIENT_SECRET"`
	SharedSecret     string        `yaml:"sharedSecret" env:"ARCAD_TUNNEL_CLIENT_SHARED_SECRET"`
	SharedSalt       string        `yaml:"sharedSalt" env:"ARCAD_TUNNEL_CLIENT_SHARED_SALT"`
	BackoffBaseDelay time.Duration `yaml:"backoffBaseDelay" env:"ARCAD_TUNNEL_CLIENT_BACKOFF_BASE_DELAY"`
	BackoffFactor    float32       `yaml:"backoffFactor" env:"ARCAD_TUNNEL_CLIENT_BACKOFF_FACTOR"`
}

type MarketClientConfig struct {
	MarketURL      string        `yaml:"marketUrl" env:"ARCAD_MARKET_CLIENT_MARKET_URL"`
	IPFSGatewayURL string        `yaml:"ipfsGatewayUrl" env:"ARCAD_MARKET_CLIENT_IPFS_GATEWAY_URL"`
	Timeout        time.Duration `yaml:"timeout" env:"ARCAD_MARKET_CLIENT_TIMEOUT"`
	RetryMax       int           `yaml:"retryMax" env:"ARCAD_MARKET_CLIENT_RETRY_MAX"`
	AppsDir        string        `yaml:"appsDir" env:"ARCAD_MARKET_CLIENT_APPS_DIR"`
}

func (c *ThemeConfig) EmptyPath() bool {
	return c.Path == ""
}

// ThemedDirectories returns a slice of directories with the theme subdirectory
// appended if a theme is defined in the configuration.
func (c *ThemeConfig) ThemedDirectories(themeSubDir string, dirs ...string) []string {
	if !c.EmptyPath() {
		dirs = append(dirs, filepath.Join(c.Path, themeSubDir))
	}

	return dirs
}

func NewDefault() *Config {
	return &Config{
		Debug: false,
		HTTP: HTTPConfig{
			Address:                 ":3000",
			TemplateDir:             "./template",
			PublicDir:               "./public/dist",
			CookieAuthenticationKey: "",
			CookieEncryptionKey:     "",
			CookieMaxAge:            int((time.Hour * 24 * 365 * 10).Seconds()), // 10 years
		},
		Contact: ContactConfig{
			Email:   "contact@arcad.games",
			Subject: "[Arcad] Help needed",
		},
		App: AppConfig{
			SearchPatterns: []string{
				"./apps/*",
				"./apps/market/*/*",
			},
			DataDir:           ":memory:",
			MaxFileUploadSize: 1024 * 1024 * 10, // 10 Mb
		},
		Data: DataConfig{
			DBPath: "./data.db",
		},
		MemberCard: MemberCardConfig{
			SigningKeyPath:      "./member-card-pkey.pem",
			LayoutPath:          "./asset/member-card.png",
			FontPath:            "./asset/member-card.ttf",
			NicknameFontSize:    48,
			NicknameAnchorX:     240,
			NicknameAnchorY:     240,
			NicknameLineSpacing: 10,
		},
		Job: JobConfig{
			CleanupSpec: "@every 1h",
		},
		Log: LogConfig{
			Format: logger.FormatHuman,
			Level:  logger.LevelInfo,
		},
		Sentry: SentryConfig{
			DSN:             "",
			Environment:     "development",
			TraceSampleRate: 0.2,
		},
		ClockSync: ClockSyncConfig{
			Enabled: false,
			Format:  "2006-01-02 15:04:05",
			Command: []string{"date", "--utc", "--set", "%s"},
		},
		CaptivePortal: CaptivePortalConfig{
			Enabled:     false,
			RedirectURL: "http://localhost:3000/captive",
			ARPWatcher:  []ARPWatcherConfig{},
		},
		GatewayServer: GatewayServerConfig{
			HTTPAddress:    ":3001",
			TunnelAddress:  ":57326",
			ProxyTargetURL: "http://localhost:3000",
			SharedSecret:   "arcadbox",
			SharedSalt:     "arcadbox",
			MatchRegExp:    "^(?P<Match>[^.]+)\\..+$",
			Cache: GatewayCacheConfig{
				Enabled:        true,
				TTL:            time.Hour * 24,
				RefreshTimeout: time.Minute,
				MaxSize:        128000000,
			},
			Clients: []GatewayRemoteClientConfig{
				{
					ID:     "dummy",
					Secret: "notsosecret",
					Match:  "dummy",
				},
			},
		},
		GatewayClient: GatewayClientConfig{
			ServerAddress:    "127.0.0.1:57326",
			ID:               "dummy",
			Secret:           "notsosecret",
			SharedSecret:     "arcadbox",
			SharedSalt:       "arcadbox",
			BackoffBaseDelay: 5 * time.Second,
			BackoffFactor:    1.5,
		},
		TunnelServer: TunnelServerConfig{
			TunnelAddress:         ":57327",
			SharedSecret:          "arcadbox",
			SharedSalt:            "arcadbox",
			ProxyTargetAddress:    "127.0.0.1:22",
			SocketDir:             "./tunnels",
			SocketFilenamePattern: "%s.sock",
			Clients: []TunnelRemoteClientConfig{
				{
					ID:     "dummy",
					Secret: "notsosecret",
				},
			},
		},
		TunnelClient: TunnelClientConfig{
			ServerAddress:    "127.0.0.1:57327",
			ID:               "dummy",
			Secret:           "notsosecret",
			SharedSecret:     "arcadbox",
			SharedSalt:       "arcadbox",
			BackoffBaseDelay: 5 * time.Second,
			BackoffFactor:    1.5,
		},
		MarketClient: MarketClientConfig{
			MarketURL:      client.DefaultMarketURL,
			IPFSGatewayURL: client.DefaultIPFSGatewayURL,
			RetryMax:       client.DefaultRetryMax,
			Timeout:        client.DefaultTimeout,
			AppsDir:        "./apps/market",
		},
		Plugins: make(map[string]map[string]interface{}),
	}
}

// NewFromFile retrieves the configuration from the given file
func NewFromFile(filepath string) (*Config, error) {
	config := NewDefault()

	data, err := ioutil.ReadFile(filepath)
	if err != nil {
		return nil, errors.Wrapf(err, "could not read file '%s'", filepath)
	}

	tmpl, err := template.New("").Funcs(sprig.TxtFuncMap()).Parse(string(data))
	if err != nil {
		return nil, errors.Wrap(err, "could not compile configuration template")
	}

	var buf bytes.Buffer

	context := struct {
		ConfigPath string
	}{
		ConfigPath: filepath,
	}

	if err := tmpl.Execute(&buf, context); err != nil {
		return nil, errors.Wrap(err, "could not execute configuration template")
	}

	if err := yaml.Unmarshal(buf.Bytes(), config); err != nil {
		return nil, errors.Wrapf(err, "could not unmarshal configuration")
	}

	return config, nil
}

func WithEnvironment(conf *Config) error {
	if err := env.Parse(conf); err != nil {
		return err
	}

	return nil
}

func Dump(config *Config, w io.Writer) error {
	data, err := yaml.Marshal(config)
	if err != nil {
		return errors.Wrap(err, "could not dump config")
	}

	if _, err := w.Write(data); err != nil {
		return err
	}

	return nil
}
