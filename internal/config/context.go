package config

import (
	"context"

	"github.com/pkg/errors"
)

type contextKey string

const configKey contextKey = "config"

func WithConfig(ctx context.Context, conf *Config) context.Context {
	return context.WithValue(ctx, configKey, conf)
}

// FromContext retrieves the config service in the given context.
func FromContext(ctx context.Context) (*Config, error) {
	srv, ok := ctx.Value(configKey).(*Config)
	if !ok {
		return nil, errors.Errorf("could not find config in given context")
	}

	return srv, nil
}

// MustContext retrieves the config service in the given context or panic otherwise.
func MustContext(ctx context.Context) *Config {
	srv, err := FromContext(ctx)
	if err != nil {
		panic(err)
	}

	return srv
}
