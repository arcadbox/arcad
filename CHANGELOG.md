
<a name="UNRELEASED"></a>
## [UNRELEASED](https://gitlab.com/arcadbox/arcad/compare/pkg/dev/ubuntu-bionic/0.0.1-0...UNRELEASED) (2021-12-10)

### Bug Fixes

* use tunnel client configuration
* fix default clock sync command
* sync securecookie max age
* quote column identifier in update query
* dokku dockerfile
* release script
* automatically restart arp watcher goroutine on error
* remove obsolete pipeline steps
* do not loose overrides on app loading
* limit rpc calls to the module associated app
* **arp-watcher:** do not hang on startup
* **dokku:** fix deployment
* **dokku:** deployment tasks
* **dokku:** fix market apps directory + variant definition

### Features

* basic tcp tunneling command to allow maintenance remote access
* configurable socket filename
* allow more specific GOOS/GOARCH combinations in build variants
* use variant files to configure releases
* app market with ipfs storage backend
* remove custom bulma theme
* add yjs signaling server plugin
* app email module
* do not lie (except to iOS)
* automatically resize frame to content (take two)
* integrate sentry http handler
* automatically resize frame to content
* dump goroutine stack on sigquit signal
* refactor message bus
* add static plugins system
* integrate rfc8908 api endpoint
* integrate rfc8908 api endpoint
* allow admin to publish/unpublish apps
* allow apps attributes overriding in admin section
* synchronize page title with app frame's one
* enhance sentry browser integration
* add simple remote backup scripts and systemd unit files
* handle bus publish timeouts
* enable sentry configurable performance monitoring
* **bus:** use one goroutine with timeout
* **cast-plugin:** basic plugin to communicate with chromecast-compatible devices
* **wip:** add cast module


<a name="pkg/dev/ubuntu-bionic/0.0.1-0"></a>
## [pkg/dev/ubuntu-bionic/0.0.1-0](https://gitlab.com/arcadbox/arcad/compare/0.0.0...pkg/dev/ubuntu-bionic/0.0.1-0) (2021-02-15)

### Bug Fixes

* set logo fill color to white
* correctly upsert record with proper id
* upsert record in store
* prevent member card caching
* correctly upsert non existent records with an id
* correctly handle Cache-Control: no-cache in gateway
* correctly handle sockjs urls
* generate default entries for CaptivePortal.ARPWatcher and GatewayServer.Client
* add missing cert script
* dokku deployment recipe
* rename app tile js controller
* update dokku app name and tags fetching strategy
* automatically create collection table in store module before querying
* better server disconnection handling in client
* remove deadlock in backend message processing + add debug messages
* update .env.dist environment variables
* filter sql generation
* update apps administration page labels
* better cache-control headers for gateway
* **store-api:** initialize doc id if empty
* **store-api:** allow empty query
* **store-api:** allow update of existing document with save()
* **store-api:** remove apps subfolder
* **store-api:** fix 'in' operator
* **ui:** typo

### Features

* add tunneling gateway commands
* add debian basic packaging recipe
* implements gateway custom http cache
* add logging output to debug system timezone
* activate etags for static assets
* add bus events buffering + reqres pattern
* add etag headers to get requests
* add prometheus metrics exporter
* basic app file upload/download api
* use UTC all time related operations
* allow list of patterns for apps loading
* authorization module for apps
* add debug mode to arcad client
* add "in" and "like" filter operators
* add limit/skip/orderby options to storage query api
* basic querying api
* extract features from user agent and associate them to users accounts
* configuration file templating
* update logo and title font
* active debug by default in development
* synchronize page and app hash changes
* swith to yaml configuration file
* expose header to detect gateway proxied accesses
* add server-side lru caching for gateway
* performance tuning for gateway tunneling
* better proxy handling
* add optional captive portal environment based arp table client identification
* refactoru sockjs endpoint
* **cli:** use subcommands for server
* **dokku:** expose gateway ports
* **dokku:** configure arcad server and gateway procs
* **file-api:** add progress observability on file upload
* **logging:** add debugging info about loaded theme
* **ui:** display time in local timezone
* **ui:** force game tile links to open in blank window
* **wip:** storage api
* **wip:** file api


<a name="0.0.0"></a>
## 0.0.0 (2020-07-10)

