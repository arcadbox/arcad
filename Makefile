REGISTRY ?= registry.gitlab.com
REGISTRY_NS ?= arcadbox/arcad
IMAGE_LABEL ?= latest
LOCAL_LABEL := $(shell hostname)-latest
CHANGELOG_NEXT_TAG ?= UNRELEASED

ARCH_TARGETS ?=
GENERATE_CHANGELOG ?=

GOBUILD_ARGS ?=
GOTEST_ARGS ?=

ARCAD_PLUGINS ?= backend_module_email route_yjs market backend_module_cast
GOBUILD_TAGS ?= $(foreach p,$(ARCAD_PLUGINS),plugin_$(p))

ARCAD_COMMAND ?=

RELEASE_VARIANTS ?= full openwrt

DOKKU_DEPLOY_URL ?= dokku@dev.lookingfora.name
DOKKU_APPS ?= arcad-server arcad-gateway arcad-tunnel arcad-market

build:
	CGO_ENABLED=0 go build $(GOBUILD_ARGS) -tags '$(GOBUILD_TAGS)' -v -o bin/arcad ./cmd/arcad
	@if [ "$(RUN_SETCAP)" == "yes" ]; then sudo setcap cap_net_raw=pe ./bin/arcad; fi

webpack:
	npm run build

test:
	go test -v -count=1 -tags '$(GOBUILD_TAGS)' $(GOTEST_ARGS) -test.v ./...

release: $(foreach p,$(RELEASE_VARIANTS),release-$(p))

release-%:
	./misc/script/release misc/variants/$*.json

tidy:
	go mod tidy

$(GOPATH)/bin/modd:
	go install github.com/cortesi/modd/cmd/modd@latest

watch: $(GOPATH)/bin/modd build
	$(GOPATH)/bin/modd

lint:
	golangci-lint run --enable-all

clean:
	rm -rf release
	rm -rf data
	rm -rf vendor
	rm -rf bin

ci-image:
	docker build -t $(REGISTRY)/$(REGISTRY_NS)/gitlab-ci:$(IMAGE_LABEL)  --file ./misc/ci/Dockerfile ./

publish-ci-image: ci-image
	docker login $(REGISTRY)
	docker tag $(REGISTRY)/$(REGISTRY_NS)/gitlab-ci:$(IMAGE_LABEL) $(REGISTRY)/$(REGISTRY_NS)/gitlab-ci:$(LOCAL_LABEL)
	docker push $(REGISTRY)/$(REGISTRY_NS)/gitlab-ci:$(IMAGE_LABEL)
	docker logout

dokku-build:
	docker build -t arcad-dokku ./

dokku-deploy: $(foreach app,$(DOKKU_APPS),dokku-deploy-$(app))

dokku-deploy-%:
	git push -f $(DOKKU_DEPLOY_URL):$* $(shell git rev-parse HEAD):refs/heads/master

.PHONY: env
env:
	[ -e data/arcad.yml ] || ( mkdir -p data && bin/arcad dump-config > data/arcad.yml )
	[ -e .env ] || ( cp .env.dist .env )
	[ -e data/gateway.arcad.local.crt ] || ( ./misc/script/cert gateway.arcad.local ./data )

.PHONY: run
run:
	$(MAKE) ARCAD_COMMAND="serve" run-arcad

.PHONY: run-gateway-server
run-gateway-server:
	$(MAKE) ARCAD_COMMAND="gateway-server" run-arcad

.PHONY: run-gateway-client
run-gateway-client:
	$(MAKE) ARCAD_COMMAND="gateway-client" run-arcad

.PHONY: run-tunnel-server
run-tunnel-server:
	$(MAKE) ARCAD_COMMAND="tunnel-server" run-arcad

.PHONY: run-tunnel-client
run-tunnel-client:
	$(MAKE) ARCAD_COMMAND="tunnel-client" run-arcad

.PHONY: migrate-market-server
migrate-market-server:
	$(MAKE) ARCAD_COMMAND="market server migrate" run-arcad

.PHONY: index-market-server
index-market-server:
	rm -rf data/market.index
	$(MAKE) ARCAD_COMMAND="market server index" run-arcad

.PHONY: run-market-server
run-market-server:
	$(MAKE) ARCAD_COMMAND="market server run --auto-migrate --auto-reindex" run-arcad

.PHONY: run-arcad
run-arcad: env
	source ./.env && bin/arcad --workdir "./cmd/arcad" --config ../../data/arcad.yml $(ARCAD_COMMAND)

.PHONY: reset-admin
reset-admin:
	$(MAKE) ARCAD_COMMAND="reset-admin" run-arcad

install-git-hooks:
	git config core.hooksPath .githooks

$(GOPATH)/bin/git-chglog:
	go install github.com/git-chglog/git-chglog/cmd/git-chglog@v0.14.2

changelog: $(GOPATH)/bin/git-chglog
	$(GOPATH)/bin/git-chglog $(if $(CHANGELOG_NEXT_TAG),--next-tag $(CHANGELOG_NEXT_TAG),) -o CHANGELOG.md

.PHONY: run-fakesmtp
run-fakesmtp: stop-fakesmtp
	docker run --rm -p 8080:8080 -p 2525:2525 --name arcad-fakesmtp bornholm/fake-smtp

.PHONY: stop-fakesmtp
stop-fakesmtp:
	@if [ ! -z "$(shell docker ps -qf Name=arcad-fakesmtp)" ]; then docker kill arcad-fakesmtp; fi

.PHONY: gateway-slow-network
gateway-slow-network:
	# Simulate slow DSL connection, see https://github.com/tylertreat/comcast#network-condition-profiles
	comcast \
		--device=lo \
		--latency=70 \
		--target-bw=1500 \
		--packet-loss=10% \
		--target-addr=127.0.0.1 \
		--target-proto=udp \
		--target-port=57326

.PHONY: gateway-stop-slow-network
gateway-stop-slow-network:
	comcast --stop --device=lo

.PHONY: webpack ci-image publish-ci-image lint watch build vendor tidy release