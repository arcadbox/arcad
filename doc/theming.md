# Gestion des thèmes

## Fonctionnement général

Il est possible d'appliquer un "thème" (une personnalisation visuelle) à Arcad.

Pour ce faire, il est nécessaire de renseigner l'attribut `path` de la section `theme` dans le fichier de configuration. La valeur de l'attribut `path` est un chemin vers un répertoire.

**Exemple**
```yaml
#...

theme:
    path: path/to/mytheme

# ...
```

Un thème est un dossier contenant l'arborescence suivante:

```
mytheme/
    |-> template/
    |       |-> blocks/
    |       |-> layouts/
    |
    |-> public/
            |-> css/
            |-> img/
```

Le sous répertoire `template` reproduit l'arborescence du répertoire contenant les templates par défaut d'Arcad (voir dossier `cmd/server/template`).

Le répertoire `public` reproduit l'arborescence du répertoire contenant les fichiers statiques servis par le serveur Arcad (scripts JS, feuilles de style CSS, polices, images...).

Lorsqu'un thème est configuré, Arcad chargera en priorité un fichier présent dans le thème, si il existe. Dans le cas contraire, il servira le fichier par défaut.