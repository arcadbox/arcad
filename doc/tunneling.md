# Tunnels

Les commandes `tunnel-server`, `tunnel-client`, `gateway-server` et `gateway-client` permettent de créer des tunnels entre la machine hôte hébergeant Arcad et le serveur "passerelle" accessible publiquement.

```
                                                       Firewall

                                                           |
                                                           |
                                                           |
                                                           |
+-----------+           +-------------------+              |          +------------------+
|           |    TCP    |                   |         UDP Tunnel      |                  |
|   User    +---------->|   Arcad Gateway   |<-------------+----------+   Arcad Hotspot  |
|           |           |                   |              |          |                  |
+-----------+           +-------------------+              |          +------------------+
                                                           |
                                                           |
                                                           |
                                                           |
                                                           |
```

Ces tunnels sont typiquement utilisés afin d'administrer les bornes Arcad à distance via SSH (commandes `tunnel-*`) ou pour exposer l'application Arcad sur Internet (commands `gateway-*`).

## Les commandes `tunnel-*`

Les commandes `tunnel-server` et `tunnel-client` permettent de créer un tunnel entre la borne Arcad et le serveur passerelle associé à celle ci (via une connexion [kcp](https://github.com/xtaci/kcptun/))

Les paramètres de ces connexions sont modifiables dans les sections `tunnelClient` et `tunnelServer` du fichier de configuration.

Les connexions entrantes sur le serveur passerelle sont transformées en socket Unix dans le répertoire défini par le paramètre de configuration `tunnelServer.socketDir`. Le nommage des fichiers résultants suit la nomenclature `<socketDir>/<clientID>.sock`.

La configuration par défaut permet d'utiliser ces tunnels pour se connecter en SSH sur les bornes appairées sans exposer celles ci sur Internet. Cette opération peut être réalisée sur le serveur passerelle via la commande suivante:

```
ssh -o "ProxyCommand socat - UNIX-CLIENT:<socketDir>/<clientID>.sock" root
```