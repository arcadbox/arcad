# Créer un nouveau plugin

Un système de plugins "statiques" (au sens que l'inclusion des plugins se fait à la compilation) est disponible afin d'étendre les fonctionnalités d'Arcad.

> **Pourquoi des plugins statiques ?**
> 
> L'utilisation du package natif `plugin` nécessite l'utilisation de `cgo` ce qui complexifierait grandement le déploiement d'Arcad sur des plateformes de type OpenWRT ou Raspberry Pi...
>
> L'inclusion conditionnelle via les tags de compilation permet d'avoir les avantages organisationnels des plugins sans avoir la complexité liée à la compilation croisée.

## Procédure

1. Créer un nouveau package dans le répertoire `plugin`. Par exemple `plugin/myplugin`.
2. Implémenter l'interface [`plugin.Plugin`](https://pkg.go.dev/gitlab.com/wpetit/goweb/plugin#Plugin). Par exemple, dans le fichier `plugin/myplugin/plugin.go`:

    ```go
    package myplugin

    import (
        "context"
    )

    type Plugin struct{}

    func (p *Plugin) PluginName() string {
        return "myplugin"
    }

    func (p *Plugin) PluginVersion() string {
        return "0.0.0"
    }

    func NewPlugin() *Plugin {
        return &Plugin{}
    }
    ```

3. Créer un fichier `plugin/register_myplugin.go` qui permettra de déclarer le nouveau plugin:

    ```go
    // +build plugin_myplugin

    package plugin

    import "gitlab.com/arcadbox/arcad/plugin/myplugin"

    func init() {
        registry.Add(myplugin.NewPlugin())
    }
    ```

    Le commentaire ligne 1 est l'élément le plus important. Il permet de rendre l'inclusion du plugin conditionnelle via les tags de compilation Go. Le tag devrait forcément avoir la forme `plugin_<pluginId>`.

4. Lancer la compilation du projet en incluant votre plugin:

    ```shell
    make ADDITIONAL_PLUGINS=myplugin build run
    ```

Si le plugin doit être ajouté aux plugins par défaut, ajouter son nom dans à liste assignée à la variable `DEFAULT_PLUGINS` du fichier `Makefile`.

Afin d'étendre les fonctionnalités d'Arcad, votre plugin devra implémenter les différentes interfaces définies dans le package `internal/hook`.

### Exemple

Le plugin `plugin/example` sert d'exemple d'implémentation des différentes interfaces disponibles.

Pour générer/exécuter une version l'incluant, faire:

```shell
make ADDITIONAL_PLUGINS=example build run
```