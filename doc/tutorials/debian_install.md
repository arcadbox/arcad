# Installation sur Debian

Des paquets sont automatiquement compilés pour Debian. Les architectures suivantes sont disponibles:

- `amd64`
- `armhf` (Raspberry Pi)

Les paquets sont compilés automatiquement à partir des sources via le pipeline d'intégration continue et diffusés sur la plateforme [packagecloud.io](https://packagecloud.io/).

Deux dépôts Debian sont disponibles:

- le dépôt ["edge"](https://packagecloud.io/arcadbox/edge), contenant les paquets générés avec les dernières versions de développement (branche `develop`);
- **PROCHAINEMENT DISPONIBLE** le dépôt ["stable"](https://packagecloud.io/arcadbox/stable), contenant les paquets générés avec les dernières versions stables (branche `master`).

## Procédure d'installation

1. S'assurer de la présence des dépendances

    ```bash
    sudo apt update
    sudo apt install ca-certificates debian-archive-keyring curl gnupg apt-transport-https
    ```

2. Ajouter la clé GPG de packagecloud.io

    ```bash
    curl -L https://packagecloud.io/arcadbox/edge/gpgkey | sudo apt-key add -
    ```

3. Ajouter le dépôt Debian de votre choix sur votre serveur

    ```bash
    REPO=edge # ou REPO=stable pour la version stable
    echo "deb https://packagecloud.io/arcadbox/$REPO/any/ any main" > /etc/apt/sources.list.d/arcadbox.list
    echo "deb-src https://packagecloud.io/arcadbox/$REPO/any/ any main" >> /etc/apt/sources.list.d/arcadbox.list
    ```

4. Mettre à jour la liste des paquets

    ```bash
    sudo apt update
    ```

5. Installer le paquet `arcad-server`

    ```shell
    sudo apt install arcad-server
    ```

6. Vérifier que le service est démarré

    ```shell
    sudo systemctl status arcad-server
    ```

Le répertoire contenant les fichiers de configuration est le suivant `/etc/arcad`.

Par défaut, le serveur Arcad devrait être disponible à l'adresse http://ip_machine/.

## Opérations courantes

### Effectuer un diagnostic de l'état de fonctionnement du service

```
sudo systemctl status arcad-server
```

### Redémarrer le service

```
sudo systemctl restart arcad-server
```

### Visualiser les journaux

```
sudo journalctl -xfu arcad-server
```
