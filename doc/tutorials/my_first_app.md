# Développer sa première "app"

Une application Arcad (ou "app") est une page Web utilisant les APIs proposées par le serveur Arcad et respectant certaines conventions d'empaquetage.

Nous allons dans ce tutoriel comment créer une simple application de discussion avec Arcad.

## Premiers pas

> Nous allons télécharger Arcad et lancer celui ci sur votre machine.

1. [Télécharger sur votre machine la dernière version disponible d'Arcad](../../README.md)
2. Décompresser l'archive téléchargée. L'arborescence du répertoire résultant devrait ressembler à ceci:

    ```bash
    arcad-<os>-<arch>
        ├── arcad.yml        # Fichier de configuration
        ├── asset            # Fichiers statiques utilisés par Arcad (ex: image pour la carte de membre)
        ├── bin
        │   └── arcad        # Exécutable Arcad
        ├── public           # Répertoire contenant les fichiers publiquement accessibles en HTTP
        └── template         # Répertoire contenant les patrons des pages HTML d'Arcad
    ```

3. Se positionner dans le répertoire et lancer le serveur:

    ```bash
    cd arcad-<os>-<arch>
    ./bin/arcad serve
    ```

    Arcad devrait être disponible à l'adresse http://localhost:3000

## Préparation de l'environnement

> Nous allons préparer le répertoire de travail pour votre "app".

1. Créez les répertoires pour votre "app":

    ```bash
    cd arcad-<os>-<arch>
    mkdir -p apps/easy-chat/{public,backend}   # On créait l'arborescence de base de notre application
    ```

    L'arborescence devrait ressembler à ceci:

    ```bash
    apps
     ├── easy-chat
            ├── public       # Répertoire contenant les fichiers accessibles publiquement dans votre application.
            ├── backend      # Répertoire contenant le code Javascript qui sera exécuté sur le serveur Arcad.
    ```

2. Créez le fichier `apps/easy-chat/arcad.yml`:

    ```yaml
    ---
    # L'identifiant de votre application. Il doit être globalement unique. 
    # Un identifiant du type nom de domaine inversé est en général conseillé (ex: com.mycompany.easychat)
    id: com.mycompany.easychat
    
    # Le titre de votre application.
    # Il sera utilisé dans la page d'accueil.
    title: Easy Chat
    
    # Les mots-clés associés à votre applications.
    # Dans le thème par défaut, le premier mot-clé est affiché en sous titre dans la tuile représentant votre application.
    tags: ["chat"]
    
    # La description de votre application.
    # Dans le thème par défaut, elle est visible lorsque l'utilisateur clique sur l'icone "?".
    # Vous pouvez utiliser la syntaxe Markdown pour mettre en forme votre description.
    description: |>
        A simple chat application
    
    # La section "options" du manifeste permet d'activer/désactiver certaines fonctionnalités pour votre application.
    options:
        highscoresEnabled: false
    ```

    > Ce fichier est le manifeste de votre application. Il permet au serveur Arcad d'identifier celle ci et d'afficher ses informations dans la page d'accueil.

3. Redémarrez le serveur Arcad. Vous devriez voir apparaitre votre application sur la page d'accueil.

    ![](resources/my_first_app_screenshot_1.png)

## Création de la page d'accueil

1. Créez le fichier `apps/easy-chat/public/index.html`:

    ```html
    <html>
        <head>
            <title>Easy Chat</title>
            <style>
                body {
                    font-family: Sans-Serif;
                }

                #chat {
                    border: 1px solid #ccc;
                    height: 50%;
                    overflow-y: auto;
                    border: 1px solid #ccc;
                    height: 50%;
                    overflow-y: auto;
                    background-color: white;
                    padding: 10px;
                }

                #chat-input {
                    margin-top: 5px;
                    width: 100%;
                    height: 40px;
                    font-size: 25px;
                }

                .chat-entry {

                }

                .chat-nickname {
                    font-weight: bold;
                    margin-right: 15px;
                }

                .chat-message {
                    display: inline-block;
                }
            </style>
        </head>
        <body>
            <h1>Easy Chat</h1>
            <div id="chat"></div>
            <input type="text" id="chat-input" placeholder="Tapez votre message..." />
            <!-- On inclue le client Arcad fourni par le serveur -->
            <script type="text/javascript" src="/client.js"></script>
            <template id="chat-entry-template">
                <div class="chat-entry">
                    <span class="chat-nickname"></span>
                    <div class="chat-message"></div>
                </div>
            </template>
            <script type="text/javascript">
                const chatInput = document.getElementById("chat-input");
                const chatContainer = document.getElementById("chat");
                const chatEntryTemplate = document.getElementById("chat-entry-template");

                // On connecte le client Arcad
                Arcad.connect("app.arcad.easychat")
                    .then(() => {
                        // Le client est connecté. On peut commencer à interagir avec le serveur

                        chatInput.addEventListener("keydown", evt => {
                            if (evt.keyCode !== 13 || chatInput.value === "") return;
                            // Si l'utilisateur appuie sur la touche "Entrée", on envoie son message
                            const message = chatInput.value;

                            // On envoie notre message au "backend" de notre application via un appel RPC.
                            Arcad.invoke("sendChatMessage", { message });

                            // On réinitialise la valeur du champ
                            chatInput.value = "";
                        });

                        // On écoute les "évènements" provenant du serveur.
                        // Dans le cas de notre application, nous recevons uniquement des messages
                        // des autres utilisateurs.
                        Arcad.addEventListener("event", evt => {
                            // On clone le template d'entrée pour le chat
                            const newChatEntry = document.importNode(chatEntryTemplate.content, true);

                            newChatEntry.querySelector(".chat-nickname").innerText = evt.detail.nickname;
                            newChatEntry.querySelector(".chat-message").innerText = evt.detail.message;

                            chatContainer.appendChild(newChatEntry);
                        });
                    
                    })
                    .catch(err => console.error(err))
                ;

                
            </script>
        </body>
    </html> 
    ```

## Création du "backend"

1. Créez le fichier `apps/easy-chat/backend/main.js`

    ```javascript
    function onInit() {
        rpc.register("sendChatMessage");
    }

    function sendChatMessage(userId, params) {
        // On récupère les données de l'utilisateur à l'origine du message
        var from = user.getUserById(userId);

        // Si le message est vide, on ignore celui ci
        if (!params.message) return;

        var payload = { 
            nickname: from.Nickname,
            message: params.message
        };
        // On envoie le message de l'utilisateur à l'ensemble des
        // utilisateurs connectés
        net.broadcast(payload);
    };
    ```

2. Redémarrez le serveur Arcad.