# Tutoriels

- [Installation sur Debian et dérivées](./debian_install.md)
- [Développer sa première "app"](./my_first_app.md)
- [Créer un plugin](./create_plugin.md)