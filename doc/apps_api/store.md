## Module `store`

Ce module permet de stocker et récupérer des données structurées sur le serveur Arcad.

### `store.save(string collectionName, object obj)`

Enregistre l'objet dans la collection.

Si l'objet a une propriété `id`, celle ci sera utilisée comme identifiant pour l'objet. Dans le cas contraire

#### Arguments

- `collectionName` **string** Nom de la collection dans laquelle retrouver l'objet
- `obj` **object** L'objet à enregistrer

#### Valeur de retour

L'objet stocké, avec une nouvelle propriété `id` si celle ci est autogénérée.

#### Usage

```js
function onInit() {
    var obj = store.save("myCollection", {"foo": "bar"});
}
```

### `store.get(string collectionName, string objectId)`

Retourne l'utilisateur associé à l'identifiant `userId` ou `null` si celui ci n'est pas trouvé.

#### Arguments

- `collectionName` **string** Nom de la collection dans laquelle retrouver l'objet
- `objectId` **string** Identifiant de l'objet à récupérer

#### Valeur de retour

L'objet stocké si il existe, `null` sinon.

#### Usage

```js
function onInit() {
    var obj = store.get("myCollection", "my-object-id");
}
```

### `store.delete(string collectionName, string objectId)`

Supprime l'objet associé à l'identifiant dans la collection.

#### Arguments

- `collectionName` **string** Nom de la collection dans laquelle retrouver l'objet
- `objectId` **string** Identifiant de l'objet à supprime

#### Valeur de retour

Aucune

#### Usage

```js
function onInit() {
    store.delete("myCollection", "my-item-id");
}
```

### `store.query(string collectionName, object filter, object options)`

Filtre la collection et récupère les objets associés à la requête.

#### Arguments

- `collectionName` **string** Nom de la collection dans laquelle retrouver l'objet
- `filter` **object** Filtre à appliquer à la collection, voir "Filtres".
- `options` **object** Options de filtrage, voir "Options".

##### Filtres

Un filtre prend la forme d'une hiérarchie d'opérateurs sous la forme d'un objet.

On distingue deux types d'opérateurs:

- Les opérateurs de combinaison logique
- Les opérateurs de filtrage

Les opérateurs d'aggrégation prennent la forme suivante:

```
{
    "<comb_op>": [
        <filter_op>,
        <filter_op>,
        etc
    ]
}
```

**Exemple**

```json
{
    "and": [
        { "eq": { "foo": "bar" } },
        { "eq": { "foo": "bar" } }
    ]
}
```

Voici la liste des opérateurs de combinaison logique:

- `and` - "ET" logique
- `or` - "OU" logique
- `not` - Négation logique

Les opérateurs de filtrage prennent la forme suivantes:

```
{
    <filter_op>: {
        "key1": "val1",
        "key2": "val2",
        "key3": "val3",
        etc
    }
}
```

**Exemple**

```json
{ "gt": { "foo": "bar" } },
```

Voici la liste des opérateurs de filtrage:

- `eq` - Comparaison `==`
- `neq` - Comparaison `!=`
- `gt` - Comparaison `>`
- `gte` - Comparaison `>=`
- `lt` - Comparaison `<`
- `lte` - Comparaison `<=`
- `like` - Comparaison type `LIKE` MySQL/SQLite
- `in` - Comparaison type `IN` MySQL/SQLite

##### Options

#### Valeur de retour

Tableau d'objets correspondant au filtre.

#### Usage

```js
function onInit() {
    var results = store.query("myCollection", {
        eq: {
            "foo": "bar",
        }
    }, {
        limit: 10,
        skip: 5,
        orderBy: "foo",
    });
}
```
