## Module `authorization`

Ce module permet de vérifier le niveau d'autorisation d'un utilisateur connecté.

### `authorization.isAdmin(string userId)`

Retourne vrai si l'utilisateur est identifié comme administrateur.

#### Arguments

- `userId` **string** Identifiant de l'utilisateur

#### Valeur de retour

Booléen, `true` si l'utilisateur est connecté et identifié comme administrateur, `false` sinon.

#### Usage

```js
function onInit() {
    var isAdmin = authorization.isAdmin("user-id");
}
```
