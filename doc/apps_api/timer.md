## Module `timer`

Ce module permet de récupérer d'effectuer des opérations en rapport avec le temps. Par exemple, exécuter une fonction après un délai spécifique.

### `timer.delay(string funcName, number delay, args...)`

Délaie l'exécution d'une fonction.

#### Arguments

- `funcName` **string** Nom de la fonction à appeler
- `delay` **number** Délai avant exécution de la fonction, en millisecondes
- `args...` **\*** Liste des arguments à passer à la fonction appelée

#### Valeur de retour

Aucune

#### Usage

```js
function onInit() {
    // On demande à exécuter la fonction 
    // "onTimeout" dans 1 seconde 
    // avec un argument "john doe"
    timer.delay("onTimeout", 1000, "john doe");
}

function onTimeout(nickname) {
    // Affichera "hello john doe"
    console.log("hello", nickname); 
}
```

### `timer.now() -> number`

Retourne l'horodatage Unix en nanosecondes. 

#### Arguments

Aucun

#### Valeur de retour

Horodatage Unix en nanosecondes.

#### Usage

```js
function onInit() {
    var timestamp = timer.now();
}
```
