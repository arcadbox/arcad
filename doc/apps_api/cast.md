## Module `cast`

Ce module permet de communiquer avec des appareils de présentation de type [Chromecast](https://store.google.com/fr/product/chromecast_setup?hl=fr).

> ⚠️ L'utilisation de ce module requiert la compilation et la configuration du plugin `backend_module_cast` !

## API

### `cast.refreshDevices(timeout? = '30s') -> Promise<Device[]>`

Rafraichit la liste locale des appareils de présentation disponibles sur les réseaux locaux de la borne.

L'appel à cette méthode rafraîchit également la liste mise en cache et renvoyée par `cast.getDevices()`.

### `cast.getDevices() -> []Device`

Retourne la liste mise en cache des appareils de présentation disponibles sur les réseaux locaux de la borne.

La liste est initialement vide. Un appel initial à `cast.refreshDevices()` est nécessaire afin de mettre à jour celle ci.

### `cast.loadUrl(deviceUuid: string, url: string, timeout? = '30s') -> Promise<void>`

Charge l'URL donnée sur l'appareil de présentation identifié par l'UUID `deviceUuid`.

### `cast.quitApp(deviceUuid: string, timeout? = '30s') -> Promise<void>`

Stoppe l'application courante sur l'appareil de présentation identifié par l'UUID `deviceUuid`. 

## Structures

### `Device`

```typescript
interface Device {
    UUID: string    // UUID de l'appareil
    Name: string    // Nom de l'appareil
    Host: string    // Adresse IPv4 de l'appareil
    Port: number    // Port distant du service
}
```