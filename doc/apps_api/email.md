# Module `email`

Ce module permet d'envoyer des courriels au format texte et/ou HTML via le protocole SMTP.

> ⚠️ L'utilisation de ce module requiert la compilation et la configuration du plugin `backend_module_email` !

## API

### `email.send(mail)`

Envoie un courriel aux adresses spécifiées dans l'option `recipients` de l'argument `mail`.

#### Arguments

- `mail` **object** Objet décrivant le courriel à envoyer.

    **Attributs**

    - `senderAddress` **string** Adresse courriel de l'émetteur
    - `senderName` **string** Nom de l'émetteur du courriel
    - `recipients` **string[]** Liste des adresses courriels de destination
    - `textBody` **string** Corps texte du courriel
    - `htmlBody` **string** Corps HTML du courriel
    - `subject` **string** Sujet du courriel

#### Valeur de retour

Aucune

#### Usage

```js
email.send({
  senderAddress: "test@arcad.app",
  recipients: ["user1@foobar.com"],
  textBody: "Hello world !",
  htmlBody: "<html><body><h1>Hello world !</h1></body></html>",
  subject: "Test Email",
})
```