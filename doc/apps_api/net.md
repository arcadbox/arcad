## Module `net`

Ce module permet d'envoyer des messages au(x) client(s) Arcad connecté(s) au backend.

### `net.send(string userId, object data)`

Envoie un message au client Arcad connecté au serveur. Si aucun client correspondant à l'identifiant fourni n'est connecté, le message est ignoré.

#### Arguments

- `userId` **string** Identifiant de l'utilisateur connecté à qui envoyer le message
- `data` **object** Données à envoyer au client Arcad

#### Valeur de retour

Aucune

#### Usage

**Côté navigateur**

```js
// Les données envoyées par le serveur sont accessibles
// via la propriété evt.detail.
Arcad.on('event', evt => console.log(evt.detail));

Arcad.connect();
```

**Côté serveur**

```js
function onInit() {
    var userId = "a-user-id...";
    net.send(userId, {"foo", "bar"});
}
```

### `net.broadcast(object data)`

Envoie un message à l'ensemble des clients Arcad connectés au serveur.

#### Arguments

- `data` **object** Données à envoyer aux clients Arcad

#### Valeur de retour

Aucune

#### Usage

Voir usage `net.send()`.