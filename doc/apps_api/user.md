## Module `user`

Ce module permet de récupérer des informations sur les joueurs enregistrés dans Arcad.

### `user.getUserById(string userId)`

Retourne l'utilisateur associé à l'identifiant `userId` ou `null` si celui ci n'est pas trouvé

#### Arguments

- `userId` **string** Identifiant de l'utilisateur

#### Valeur de retour

```js
// User
{
    ID: string,             // Identifiant de l'utilisateur
    Nickname: string        // Pseudonyme de l'utilisateur
    CreationTime: string    // Date de création de l'utilisateur
    LastSeenTime: string    // Date de création de l'utilisateur
}
```

#### Usage

```js
function onInit() {
    var u = user.getUserById("user-id");
}
```
