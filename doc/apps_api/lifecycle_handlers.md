# Cycle de vie d'une application

Un backend peut déclarer des fonctions qui seront automatiquement appelées lors de certains évènements du cycle de vie de l'application. Voici la liste des fonctions:

- `onInit()`
- `onUserConnected(string userId)`
- `onUserDisconnected(string userId)`
- `onUserMessage(string userId, object data)`

Il n'est pas obligatoire de déclarer ces fonctions dans le backend de l'application.

## `onInit()`

Cette fonction est automatiquement appelée par Arcad lorsque le serveur démarre.

## `onUserConnected(string userId)`

Cette fonction est automatiquement appelée lorsqu'un utilisateur entre sur l'application.

## `onUserDisconnected(string userId)`

Cette fonction est automatiquement appelée lorsqu'un utilisateur quitte l'application.

## `onUserMessage(string userId, object data)`

Cette fonction est appelée pour chaque message envoyé par un utilisateur via la méthode `Arcad.send()` côté client.