## Apps API

### Cycle de vie

### Modules

Listes des modules disponibles aux "backends" des applications exécutées dans l'environnement Arcad.

- [`authorization`](./authorization.md)
- [`cast`](./cast.md)
- [`console`](./console.md)
- [`email`](./email.md)
- [`highscore`](./highscore.md)
- [`net`](./net.md)
- [`rpc`](./rpc.md)
- [`store`](./store.md)
- [`timer`](./timer.md)
- [`user`](./user.md)
