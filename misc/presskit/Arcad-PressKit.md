<div align="center">
    <img src="https://gitlab.com/arcadbox/arcad/-/raw/develop/misc/presskit/resources/logo.svg?inline=false" width="25%" />
    <div>
        <h3>Services numériques de proximité</h3>
    </div>
</div>

## Description

Arcad est une suite de [logiciels libres](https://fr.wikipedia.org/wiki/Logiciel_libre) permettant de proposer des services numériques (_applications web_) de proximité i.e. hébergés sur des équipements réseaux du type borne WiFi ([Linksys WRT1900AC](https://www.linksys.com/fr/p/P-WRT1900AC/) par exemple) ou micro-serveur domestique (serveur NAS, [Raspberry Pi](https://www.raspberrypi.org/)).

Un des objectifs du projet est d'offrir un système capable de fonctionner en autonomie: les terminaux numériques (ordinateur, smartphones, tablettes...) des utilisateurs ne doivent pas nécessiter de connexion Internet pour pouvoir accéder aux services.

De manière optionnelle, les services intégrés sur une instance Arcad peuvent être exposés sur Internet via un serveur relais disposant d'une adresse publique.

## Cas d'usage

Voici quelques cas d'usages pour Arcad:

- Borne WiFi proposant des jeux multijoueurs pour les lieux de vie (bars, restaurants, associations...);
- Borne WiFi proposant des outils de travail collaboratif (prise de note, partage de document, tableau blanc...) dans les salles de réunion, salles de classe, espaces de coworking;
- Borne WiFi de services (formulaire de sondage, diffusion d'informations, partage de documents...) pour les travailleurs nomades (collecte diverses, stands sur festivals...);
- Micro-serveur de services internes pour les TPE, associations...

## Statut

Le projet est en phase active de développement et est actuellement au statut "alpha". Le code source du projet est disponible sur [Gitlab](https://gitlab.com/arcadbox) et publié sous licence [AGPL-3.0](https://www.gnu.org/licenses/agpl-3.0.html).

## Contact

Pour tout renseignement complémentaire n'hésitez pas à nous contacter à l'adresse [arcad-project@protonmail.com](mailto:arcad-project@protonmail.com).