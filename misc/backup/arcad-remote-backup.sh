#!/bin/bash

set -eo pipefail

REMOTE_HOST=${REMOTE_HOST}
RESTIC_REPO=${RESTIC_REPO:-${REMOTE_HOST}}
BACKEDUP_DIRS=${BACKEDUP_DIR:-/mnt/sda1/arcadbox /opt/arcadbox/data /etc/arcadbox}
MANAGED_REMOTE_SERVICES="arcadbox-gateway-client arcadbox-server"
TEMP_DIR=${TEMP_DIR:-/tmp}
RESTIC_KEEP_DAILY=${RESTIC_KEEP_DAILY:-7}

# Mount remote directory with SSHFS
function mount_remote_dir {
    local remote_dir="$1"
    local mountpoint="${TEMP_DIR}/arcad-backup/${REMOTE_HOST}"
    mkdir -p "${mountpoint}"
    sshfs "root@${REMOTE_HOST}:${remote_dir}" "${mountpoint}"
    echo ${mountpoint}
}

function stop_remote_service {
    local service=$1
    manage_remote_service "${service}" stop
}

function restart_remote_service {
    local service=$1
    manage_remote_service "${service}" restart
}

function manage_remote_service {
    local service=$1
    local command=$2
    ssh "root@${REMOTE_HOST}" "SHELL=/bin/ash /etc/init.d/${service} ${command}"
}

function backup_dir {
    local backedup_dir=$1
    restic -r "${RESTIC_REPO}" --cleanup-cache --verbose backup "${backedup_dir}"
}

function unmount_dir {
    local mountpoint=$1
    umount -l "${mountpoint}"
}

# Cleanup SSHFS mountpoint and ensure remote services are restarted
function cleanup {
    local mountpoint=$1
    ( unmount_dir "${mountpoint}" 2>/dev/null || exit 0 )
    for srv in ${MANAGED_REMOTE_SERVICES}; do
        echo "Restarting remote service '${srv}'..."
        restart_remote_service "${srv}"
    done
}

function main {
    # Stop remote services to prevent writes while backing up
    for srv in ${MANAGED_REMOTE_SERVICES}; do
        echo "Stopping remote service '${srv}'..."
        stop_remote_service "${srv}"
    done
    
    # For each backed up dirs...
    for dir in ${BACKEDUP_DIRS}; do
        echo "Backing up remote directory '${dir}' from '${REMOTE_HOST}' using repo '${RESTIC_REPO}'..."
        
        local mountpoint=$(mount_remote_dir $dir)
        echo "Mounted remote directory on '${mountpoint}'..."
        
        # Ensure cleanup on script exit
        trap "cleanup '${mountpoint}'" EXIT
        
        backup_dir "${mountpoint}"

        echo "Unmounting '${mountpoint}'..."
        unmount_dir "${mountpoint}"
        
        echo "Done"
    done

    # Restart previously stopped remote services
    for srv in ${MANAGED_REMOTE_SERVICES}; do
        echo "Restarting remote service '${srv}'..."
        restart_remote_service "${srv}"
    done

    # Prune backups
    echo "Pruning backups..."
    restic -r "${RESTIC_REPO}" forget --keep-daily "${RESTIC_KEEP_DAILY}" --prune
}

# Run main function
main