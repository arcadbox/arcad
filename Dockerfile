FROM golang:1.17 as build

ARG TZ=Europe/Paris

RUN apt-get update -y \
    && apt-get install -y git-core build-essential curl tzdata bash jq gcc g++ make

# Install NodeJS
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash - \
    && apt-get install -y nodejs

WORKDIR /app

RUN npm install npm@latest -g

COPY . ./

RUN npm ci \
    && GENERATE_CHANGELOG=no make release-dokku \
    && mv release/arcad-dokku-linux-amd64 /dist

FROM busybox

ENV ARCAD_COMMAND=serve

COPY --from=build /dist /app
COPY misc/dokku/CHECKS /app/CHECKS
COPY misc/dokku/Procfile /app/Procfile
COPY misc/dokku/bin/run.sh /app/bin/run
COPY misc/dokku/config.yml /app/config.yml

RUN chmod +x /app/bin/run

WORKDIR /app

EXPOSE 3000/tcp
EXPOSE 3001/tcp
EXPOSE 3002/tcp
EXPOSE 57326/udp
EXPOSE 57327/udp

RUN mkdir -p /app/data
VOLUME /app/data

RUN mkdir -p /app/apps
VOLUME /app/apps

CMD ["bin/run"]